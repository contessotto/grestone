<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<h3><span id="data"></span><br/>
<script language="JavaScript" type="text/javascript">
function orologio()
{
    var time = new Date();
    var hours = time.getHours();
	if (hours <= 9) hours = "0" + hours;
    var minutes = time.getMinutes();
	if (minutes <= 9) minutes = "0" + minutes;
	var day=time.getDate();
	if (day<10) day = "0" + day;
	var month=time.getMonth();
	month = month + 1;
	if (month <= 9) month = "0" + month;
	var year=time.getYear();
	if (year < 1000) year += 1900;
	var seconds = time.getSeconds()
	if (seconds <= 9) seconds = "0" + seconds;
    //ampm = (hours >= 12) ? "PM" : "AM";
    // hours=(hours > 12) ? hours-12 : hours;
    // hours=(hours == 0) ? 12 : hours;
    var clock = day + "/" +  month + "/" + year + " - " + hours + ":" + minutes;// + ":" + seconds;
    if(clock != document.getElementById('data').innerHTML) document.getElementById('data').innerHTML = clock;
    timer = setTimeout("orologio()",1000);
}
orologio();
//-->
</script>
<span style="font-size: 7pt">
<?php include ("santo.php"); ?>
<br/></span></h3>
<hr width="100%" size="1" color="#0033CC" />
