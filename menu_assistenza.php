<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<div id="menu">
 
<?php include ("dataora.php");?>
	<a href="assistenza.php">Home Assistenza</a><br/><br/>
    <hr width="100%" size="1" color="#0033CC" />
    <a href="assistenza_faq.php"><img src="immagini/assistenza.png" border="0"/><br/>F.A.Q. Domande Frequenti</a><br/>
    <hr width="100%" size="1" color="#0033CC" />
	<a href="assistenza_contatta_parrocchia.php"><img src="immagini/mail.png" border="0"/><br/>Contatta l'amministratore della tua parrocchia</a>
    <hr width="100%" size="1" color="#0033CC" />
	<a href="assistenza_contatta_gestore.php"><img src="immagini/mail.png" border="0"/><br/>Contatta il gestore del programma</a>
    <hr width="100%" size="1" color="#0033CC" /><br/>
	<a href="http://www.grestone.it/forum/phpBB3" target ="_blank">Poni le tue domande nel nostro forum</a><br/><br/>
    <hr width="100%" size="1" color="#0033CC" />
	<a href="assistenza_contatta_sviluppatori.php"><img src="immagini/mail.png" border="0"/><br/>Contatta gli Sviluppatori di GrestOne</a><br/>
    <hr width="100%" size="1" color="#0033CC" />
	<a href="credits.php"><br/>Credits</a><br/><br/>
</div>
