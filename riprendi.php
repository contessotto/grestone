<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>Conferma Eliminazione Campo</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
<body>
<div id="eliminato"><br/><br/><strong>
<?php
	include ("funzioni.php"); 
	verifica_amministratore($_SESSION[Grestone]);
	$dati_grest = verifica_grest();
	
	switch ($_GET[oggetto]) {
	case 'gruppi':
		mysql_query("UPDATE  `grests` SET  `gruppi` =  '1' WHERE  `id_grest` = $_SESSION[id_grest];");
		mysql_query("UPDATE  `grests` SET  `s_gruppi` =  '0' WHERE  `id_grest` = $_SESSION[id_grest];");
		print 'Gestione gruppi ripresa';
	break;
	case 'eta':
		mysql_query("UPDATE  `grests` SET  `eta` =  '1' WHERE  `id_grest` = $_SESSION[id_grest];");
		mysql_query("UPDATE  `grests` SET  `s_eta` =  '0' WHERE  `id_grest` = $_SESSION[id_grest];");
		print 'Gestione fasce d\'età ripresa';
	break;
	case 'laboratori':
		mysql_query("UPDATE  `grests` SET  `laboratori` =  '1' WHERE  `id_grest` = $_SESSION[id_grest];");
		mysql_query("UPDATE  `grests` SET  `s_laboratori` =  '0' WHERE  `id_grest` = $_SESSION[id_grest];");
		print 'Gestione laboratori ripresa';
	break;
	
	case 'squadre':
		mysql_query("UPDATE  `grests` SET  `squadre` =  '1' WHERE  `id_grest` = $_SESSION[id_grest];");
		mysql_query("UPDATE  `grests` SET  `s_squadre` =  '0' WHERE  `id_grest` = $_SESSION[id_grest];");
		print 'Gestione squadre ripresa';
	break;
	
	case 'periodo':
		mysql_query("UPDATE  `grests` SET  `periodo` =  '1' WHERE  `id_grest` = $_SESSION[id_grest];");
		mysql_query("UPDATE  `grests` SET  `s_periodo` =  '0' WHERE  `id_grest` = $_SESSION[id_grest];");
		print 'Gestione periodo ripresa';
	break;	

	case 'eventi':
		mysql_query("UPDATE  `grests` SET  `gite` =  '1' WHERE  `id_grest` = $_SESSION[id_grest];");
		mysql_query("UPDATE  `grests` SET  `s_gite` =  '0' WHERE  `id_grest` = $_SESSION[id_grest];");
		print 'Gestione eventi ripresa';
	break;
}
	
	
?>
		<meta http-equiv="refresh" content="2;
		URL=<?=$_SERVER['HTTP_REFERER']?>">
</strong><br/></div>
</body> 

</html>
