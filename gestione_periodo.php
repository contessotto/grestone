<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
<script src="script.js" type="text/javascript"></script>
<body>

<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>

<?php
?>
	
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_personalizzazione.php"); ?>
		
		<div id="contenuto">
<?php
if ($dati_grest[s_periodo] == 1)
{
	print '<h2>Gestione Periodo</h2>';
	print 'La gestione del Periodo è attualmente sospesa.<br/>
	<a href="riprendi.php?oggetto=periodo">Riprendi</a>';
}
else
{
if ($dati_grest[periodo] == 0)
{


/*per differenziare il comportamento della pagina a seconda del punto della configurazione a cui si è arrivati
 * si usa il campo nascosto "passaggi": se non è settato mostra il form iniziale.
*/
if ($_POST[passaggi] == null)
	{if ($_GET[errore] == 'ordine')
		{$errore = '<err>Data finale posteriore a quella iniziale!</err>';}
		print '
			<h2>Gestione Periodo</h2>
		<form action="gestione_periodo.php" method="post">
		<input type="hidden" name="passaggi" value="primo">
		<h2 id="errore">'.$errore.'</h2>
		<br/>
		Anno Grest:
		<input type="text" name="anno"size="4" placeholder="AAAA" required><br/><br/>
		Data inizio grest:<br/>
		<input type="text" name="giorno_inizio"maxlength="2"size="2" placeholder="GG" required>:
		<input type="text" name="mese_inizio" maxlength="2"size="2" placeholder="MM" required>
		<br/>Data fine grest:<br/>
		<input type="text" name="giorno_fine"maxlength="2"size="2" placeholder="GG" required>:
		<input type="text" name="mese_fine" maxlength="2"size="2" placeholder="MM" required>
		<br/><br/>
		Giornate di Grest:<br/>
		<input type="checkbox" name="lun" value="1" checked="checked"/> Lunedì 
		<input type="checkbox" name="mar" value="1" checked="checked"/> Martedì 
		<input type="checkbox" name="mer" value="1" checked="checked"/> Mercoledì 
		<input type="checkbox" name="gio" value="1" checked="checked"/> Giovedì 
		<input type="checkbox" name="ven" value="1" checked="checked"/> Venerdì 
		<input type="checkbox" name="sab" value="1"/> Sabato 
		<input type="checkbox" name="dom" value="1"/> Domenica 
		<br/><br/>
		<input type="submit">
		</form>';
	}
if ($_POST[passaggi] == "primo") // se "passaggi" è primo esegue l'elaborazione del calendario.
	{
	$mktime_inizio = mktime(0, 0, 0, $_POST[mese_inizio], $_POST[giorno_inizio], $_POST[anno]); //mktime giorno inizio
	$mktime_fine = mktime(0, 0, 0, $_POST[mese_fine], $_POST[giorno_fine], $_POST[anno]);		//mktime giorno fine

	if ($mktime_inizio>=$mktime_fine)
	{
		print '<strong>Errore: data iniziale posteriore a quella finale!!</strong><br/>
		<input type="button" value="Torna al primo passaggio" onclick="javascript:history.back();">';
	}
	else
	{
	print '
	<h3>In questa pagina puoi escludere o includere <br/>alcuni giorni in cui si svolgono o meno attività del Grest</h3>
	<form action="gestione_periodo.php" method="post">
	<table align="center" id="calendario">
	<tr>
	<td colspan="8">Calendario Grest</td>

	</tr>
	<tr>
		<td style="background-color:#A4A2A7">Settimana</td>
		<td class="intestazione">Lun</td>
		<td class="intestazione">Mar</td>
		<td class="intestazione">Mer</td>
		<td class="intestazione">Gio</td>
		<td class="intestazione">Ven</td>
		<td class="intestazione">Sab</td>
		<td class="intestazione">Dom</td>
	</tr>
	<tr>';
	$contatore_settimane = 1;
	$s = date("N", $mktime_inizio); // Queste tre righe si occupano di creare dei <td> vuoti all'inizio della tabella
	if ($s <> 1)
	{
		print '<td style="background-color:white;
		border:solid 3px black;;">Set. '.$contatore_settimane.'</td>';
		$contatore_settimane++;		
	}
	for ($r = 1; $r < $s; $r++)		// in modo che i giorni numerici inizino nel corrispondente giorno della settimana.
	{print '<td style="background-color:white;border:0px;"></td>';}
	
	$giorni_effettivi = 0; // contatore dei giorni effettivi
	
	for ($i = $mktime_inizio; $i <= $mktime_fine; $i = $i + 24*3600) //ciclo che scrive i vari <td> con il numero del giorno.
	{// il ciclo sfrutta il mktime e aumenta di un giorno (24*/3600) ad ogni iterazione, fino al mktime dell'ultimo giorno.
			if (date("N", $i) == 1)
		{
			print '<td style="background-color:white;
		border:solid 3px black;">Set. '.$contatore_settimane.'</td>';
		if ($contatore_settimane<>1)
		{print '<input type="hidden" name="fine_settimana_'.$contatore_settimane.'" value="'.($i-24*3600).'">';
		print '<input type="hidden" name="inizio_settimana_'.$contatore_settimane.'" value="'.$i.'">';}
		$contatore_settimane++;
		}
	
	print '<td nowrap';
	
	$giorni_effettivi++;
	
	if (date("z", $i) == date("z", time())) // queste due righe danno lo sfondo bianco alla casella del giorno corrnete.
	{print ' style="background-color:white; border-color:red;"';}
	
	/* if (date("N", $i) == 7)
	{print ' style="background-color:red;"';}
	
	if (date("N", $i) == 6)
	{print ' style="background-color:blue;"';} */
	
	/* le seguenti righe si occupano di verificare se il giorno della settimana che si stà considerando è un giorno di grest
	 * oppure no (confrontando con quelli indicati nel form). Se no i giorni vengono mostrati in trasparenza.
	*/
	$giorno_incluso = 1;
	$nomi_giorni = array ('ciao','lun', 'mar', 'mer', 'gio', 'ven', 'sab', 'dom'); //per avere i nomi in italiano dei giorni. 
	$num = date("N", $i);	// indica il numero del giorno della settimana (1=>lun...7=>dom).
	$z = $nomi_giorni[$num]; // $z è il nome del giorno della settimana.
	
	if ($_POST[$z] <> 1)	// $_POST[$z] è il nome del campo del form in cui è richiesto quel giorno della settmana.
							// se è 1 (true) il giorno fa parte del grest, altrimenti no.
		{
		$giorni_effettivi--; //se quel giorno non c'è il grest allora non viene contato tra i giorni effettivi.
		$giorno_incluso = 0;
		}	

	print '>';
	
	print date("j", $i); // scrive il giorno in numero
	if ($giorno_incluso == 1)
		{print '<input type="checkbox" name="'.$i.'" value="1" checked="checked"/>';}
	else 
		{print '<input type="checkbox" name="'.$i.'" value="2" />';}
	
	print '</td>';
	if (date("N", $i) == 7) // se il giorno è una domenica chiude anche la riga e inizia quella dopo.
		{print '</tr><tr>';}
	}
	
	print '</tr>';
	print '</table>';
	

	print '
	<input type="hidden" name="passaggi" value="conferma">
	<input type="hidden" name="mktime_inizio" value="'.$mktime_inizio.'">
	<input type="hidden" name="mktime_fine" value="'.$mktime_fine.'">
	<input type="hidden" name="lun" value="'.$_POST[lun].'">
	<input type="hidden" name="mar" value="'.$_POST[mar].'">
	<input type="hidden" name="mer" value="'.$_POST[mer].'">
	<input type="hidden" name="gio" value="'.$_POST[gio].'">
	<input type="hidden" name="ven" value="'.$_POST[ven].'">
	<input type="hidden" name="sab" value="'.$_POST[sab].'">
	<input type="hidden" name="dom" value="'.$_POST[dom].'">
	<input type="submit" value="conferma">
	</form>';
	}
	}
	
	if ($_POST[passaggi] == "conferma") // se "passaggi" è "conferma" esegue invia i dati al database.
	{
	connetti();
	for ($i = $_POST[mktime_inizio]; $i <= $_POST[mktime_fine]; $i = $i + 24*3600)
	{// il ciclo sfrutta il mktime e aumenta di un giorno (24*/3600) ad ogni iterazione, fino al mktime dell'ultimo giorno.
		$nomi_giorni = array ('ciao','lun', 'mar', 'mer', 'gio', 'ven', 'sab', 'dom'); //per avere i nomi in italiano dei giorni. 
		$num = date("N", $i);	// indica il numero del giorno della settimana (1=>lun...7=>dom).
		$z = $nomi_giorni[$num]; // $z è il nome del giorno della settimana.
	
		if ($_POST[$z] == 0)	// $_POST[$z] è il nome del campo del form in cui è richiesto quel giorno della settmana.
							// se è 1 (true) il giorno della settimana fa parte del grest, altrimenti no.
		{// se il giorno della settimana non è incluso nel grest.
			if ($_POST[$i] <> 0) //se il check del giorno è attivato
			{
				$giorni_inclusi .= "$i-"; //aggiunge il giorno a quelli inclusi
			}			
		}
		else 
		{// se il giorno della settimana è incluso nel grest.
			if ($_POST[$i] == 0) //se il check del giorno non è attivato
			{
				$giorni_esclusi .= "$i-"; //aggiunge il giorno a quelli esclusi
			}
		}
	}
	mysql_query("UPDATE `periodo` SET mktime_inizio = '$_POST[mktime_inizio]', mktime_fine = '$_POST[mktime_fine]', giorni_esclusi = '$giorni_esclusi', giorni_inclusi = '$giorni_inclusi', lun = '$_POST[lun]', mar = '$_POST[mar]',
	mer = '$_POST[mer]', gio = '$_POST[gio]', ven = '$_POST[ven]', sab = '$_POST[sab]', dom = '$_POST[dom]' WHERE `id_grest` = '$_SESSION[id_grest]';");
	registro("$dati_utente[nome_utente]" , "$_SESSION[id_grest]" , 
	"Modifica periodo Grest $impostazioni_grest[titolo_grest]");
	
	mysql_query("UPDATE `grests` SET periodo = '1' WHERE `id_grest` = '$_SESSION[id_grest]';");
	
	print'<h3>Il periodo di Grest è stato inserito con successo.</h3>
	È ora possibile avere il calendario nella home e inserire gli eventi come gite, serate ecc.';
	include ("calendario.php");
	
	$mktime_differenza = $mktime_fine - $mktime_inizio;
	
	$giorni_totali = $mktime_differenza/24/3600+1; //giorni totali di grest
	$numero_settimane = intval ($giorni_totali/7); //settimane totali di grest
	if ($giorni_totali % 7 <> 0) //aggiustamento delle settimane totali
	{$numero_settimane++;}
	$giorni_effettivi = $giorni_effettivi + $numero_giorni_inclusi - $numero_giorni_esclusi;
	print "<h3>Periodo totale Grest: $giorni_totali giorni</h3>";
	print "<h3>Durata effettiva Grest: $giorni_effettivi giorni</h3>";
	print "<h3>Numero di Settimane del Grest: $numero_settimane</h3>";
	mysql_query("UPDATE `periodo` SET numero_settimane = '$numero_settimane' WHERE `id_grest` = '$_SESSION[id_grest]';");
	for ($k = 1; $k <= $numero_settimane; $k++)
	{
		$inzio_per_query = 'inizio_settimana_'.$k;
		$fine_per_query = 'fine_settimana_'.$k;
		$inizi_settimane .= "-$_POST[$inzio_per_query]";
		$fini_settimane .= "$_POST[$fine_per_query]-";

	}
mysql_query("UPDATE `periodo` SET inizi_settimane = '$inizi_settimane' WHERE `id_grest` = '$_SESSION[id_grest]';");
mysql_query("UPDATE `periodo` SET fini_settimane = '$fini_settimane' WHERE `id_grest` = '$_SESSION[id_grest]';");
	
	for ($a = 1; $a <= $numero_settimane; $a++)
	{
		mysql_query("ALTER TABLE  `iscritti_$_SESSION[id_grest]` ADD  `settimana_$a` INT NOT NULL");
		mysql_query("ALTER TABLE  `animatori_$_SESSION[id_grest]` ADD  `settimana_$a` INT NOT NULL");
		mysql_query("ALTER TABLE  `collaboratori_$_SESSION[id_grest]` ADD  `settimana_$a` INT NOT NULL");
	}
	disconnetti();
	}
}
else
{
	print'<h2>La gestione periodo del Grest è attiva</h2>
	<a href="disattiva.php?oggetto=periodo" onclick="return confermadisattiva ();">Disattiva definitivamente gestione Periodo</a><br/><br/>
	<a href="sospendi.php?oggetto=periodo">Sospendi temporaneamente gestione Periodo</a><br/><br/>';

	include("calendario.php");
	
	$mktime_differenza = $mktime_fine - $mktime_inizio;
	
	$giorni_totali = $mktime_differenza/24/3600+1; //giorni totali di grest
	$numero_settimane = intval ($giorni_totali/7); //settimane totali di grest
	if ($giorni_totali % 7 <> 0) //aggiustamento delle settimane totali
	{$numero_settimane++;}
	$giorni_effettivi = $giorni_effettivi + $numero_giorni_inclusi - $numero_giorni_esclusi;
	print '<br>';
	print "<h3>Periodo totale Grest: $giorni_totali giorni</h3>";
	print "<h3>Durata effettiva Grest: $giorni_effettivi giorni</h3>";
	print "<h3>Numero di Settimane del Grest: $numero_settimane</h3>";
	
	
}
}
?>
<?php 

?>

        </div>
        
        <?php include ("pedice.php"); ?>   
</body> 

</html>
