<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>
	<script src="script.js" type="text/javascript"></script>
</head>
<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_statistiche.php"); ?>

<div id="contenuto">
<?php
connetti();
if ($dati_grest[periodo] == 1)
{
	$periodo = mysql_query("SELECT * FROM periodo WHERE id_grest=$_SESSION[id_grest]");
	$dati_periodo = mysql_fetch_array($periodo, MYSQL_ASSOC);
}
if ($dati_grest[eta] == 1)
{
	$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest]");
	while ($dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC))
	{

		$eta_selezionate[id][] = $dati_eta[id_eta];
		$eta_selezionate[nome][] = $dati_eta[nome];
		$numero_eta_selezionate++;
	}
}


$iscanicol = array('iscritti','animatori','collaboratori');
$iscanicol[bello] = array('Animati','Animatori','Collaboratori');

print '<h2>Statistiche Generali</h2><br/>';
$c_iscanicol=0;
while ($c_iscanicol <3)
{
	$query = 'SELECT * FROM '.$iscanicol[$c_iscanicol].'_'.$_SESSION[id_grest];
	$numero = mysql_query($query);
	$numero_iscritti += mysql_num_rows($numero);
	print 'Totale '.$iscanicol[bello][$c_iscanicol].': <h2>'.mysql_num_rows($numero).'</h2>';
	$c_iscanicol++;
}

print '<span style="color: red;"><strong>Totale Generale:</strong><h2>'.$numero_iscritti.'</h2></span>';


print '<h3><br/><a id="img_ElGenIs" href="javascript:mostra(\'mostra_ElGenIs\',\'img_ElGenIs\');">
<img src="immagini/piu.png" title="Espandi" border="0" /></a> Mostra in Dettaglio</h3>';
print '<div id="mostra_ElGenIs" style="display: none;">';
if (!isset($dati_periodo[numero_settimane]))
{$dati_periodo[numero_settimane] = 1;}
for ($a=1; $a<=$dati_periodo[numero_settimane]; $a++)
{
	if ($dati_grest[periodo] == 1)
	{
	print '<br/><b>';
	print 'Settimana '.$a;
	print '</b><br/>';}
	
	
	print '<table id="lista" align="center">';
	
	print '<tr>';
	if ($numero_eta_selezionate <> 0)
		{print '<th></th>';}
	$c_iscanicol=0;
	while ($c_iscanicol <3)
	{
		print '<th>'.$iscanicol[bello][$c_iscanicol].'</th>';
		$c_iscanicol++;
	}
	print '</tr>';
	$b = 0;
	do
	{
		print '<tr>';
		if ($numero_eta_selezionate <> 0)
			{print '<td>'.$eta_selezionate[nome][$b].'</td>';}
		$c_iscanicol =0;
		while ($c_iscanicol <3)
		{
			$query = "SELECT * FROM $iscanicol[$c_iscanicol]_$_SESSION[id_grest] WHERE 1";
			//print '<td><a href="elenchi_generale.php?';
			if ($dati_grest[periodo] == 1)
				{	//print '
					$query .= ' AND settimana_'.$a.'= 1 ';
				}
			if ($dati_grest[eta] == 1)
				{$query .= ' AND eta = '.$eta_selezionate[id][$b];}
			//print $query;
			$numero = mysql_query("$query");
			$numero_iscritti = mysql_num_rows($numero);
			print '<td><h2>'.$numero_iscritti.'</h2></td>';
			$c_iscanicol++;
		}
		print '</tr>';
	
		$b++;
	}
	while ($b<$numero_eta_selezionate);
	if ($numero_eta_selezionate <> 0)
	{
		print '<tr>';
		print '<td><h3><span style="color: red;">Totale</span></h3></td>';
		$c_iscanicol=0;
		while ($c_iscanicol <3)
		{
			$query = 'SELECT * FROM '.$iscanicol[$c_iscanicol].'_'.$_SESSION[id_grest].' WHERE 1';
			if ($dati_grest[periodo] == 1)
			{
				$query .= ' AND settimana_'.$a.'= 1';
			}
			$numero = mysql_num_rows(mysql_query("$query"));		
			print '<td><h2><span style="color: red;">'.$numero.'</span></h2></td>';
			$c_iscanicol++;
		}
		print '</tr>';
	}
	print '</table>';
}
print '</div>';
?>
</div>
        <?php include ("pedice.php"); ?>
</div>
</body> 

</html>
