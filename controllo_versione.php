<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<?php 

    print '<br/><br/><br/><br/><br/><br/>';
    print '<div id="pannelloadmin">';
	print '<h2>Controllo Aggiornamenti</h2>';

//controllo versione locale
$locale=fopen("info_versione.txt","r");
$versione_in_uso = fgets($locale);
fclose($locale);
//controllo versione su server grestone.it
$corrente_server=fopen("http://www.grestone.it/versioni/corrente.txt","r") or print("<br/><br/><h2><err>Non è stato possibile verificare la presenza di aggiornamenti.<br>Controllare che il server abbia connessione ad internet.</err></h2>");
$versione_aggiornata = fgets ($corrente_server);
fclose($corrente_server);


print'<br/>versione in uso:<br/><b>GrestOne ver. '.$versione_in_uso.'</b><br/>';

if ($versione_in_uso == $versione_aggiornata)
{
	print'<br/><img src="immagini/ico_ok.png"> La tua versione di GrestOne è aggiornata <img src="immagini/ico_ok.png"><br/><br/><br/><br/>Aiutaci a far crescere GrestOne!<br/> Visita <a href="http://www.grestone.it/" target="blank">grestone.it</a><br/>';	
}
else if($versione_aggiornata != '')
print'<img src="immagini/ico_no.png"><span style="color: red;">La tua versione di GrestOne NON è aggiornata</span><img src="immagini/ico_no.png"><br/><br/>La versione attuale è:<br/><b>GrestOne ver. '.$versione_aggiornata.'</b><br/>Consigliamo di scaricare e installare la versione più recente, con errori risolti e funzionalità in più!<br/> Visita <a href="http://www.grestone.it/" target="blank">grestone.it</a><br/>';	

print '</div>';

?>