<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>
	<script src="script.js" type="text/javascript"></script>
</head>
<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_report.php"); ?>

<div id="contenuto">
	<?php
	connetti();
	print '<h2>ELENCO RAPIDO GENERALE</h2><br/>';
	
	
// INIZIO BLOCCO PER RIMANDO A STAMPE


		
		print '<form action="stampe_generale.php" method="get" id="generale">';
		if ($dati_grest[periodo]==1) //periodo
		{
			$periodo = mysql_query("SELECT * FROM periodo WHERE id_grest = $_SESSION[id_grest]");
			$dati_periodo = mysql_fetch_array($periodo, MYSQL_ASSOC);
			for ($a=1; $a<=$dati_periodo[numero_settimane]; $a++)
			{
				$settimana = 'settimana_'.$a;
				if ($_GET[$settimana]==1)
					{print '<input type="hidden" name="'.$settimana.'" value="1">';}
			}
		}
		if ($dati_grest[eta]==1) //eta
		{
			$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest]");
			while ($dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC))
			{
				if ($_GET['eta_'.$dati_eta[id_eta]]==1)
				print '<input type="hidden" name="eta_'.$dati_eta[id_eta].'" value="1">';	
			}
		}			
		print '<input type="hidden" name="iscritti" value="'.$_GET[iscritti].'">';
		print '<input type="hidden" name="animatori" value="'.$_GET[animatori].'">';
		print '<input type="hidden" name="collaboratori" value="'.$_GET[collaboratori].'">
		<input type="submit" value="visualizza stampe">
		</form><br/>';


// FINE BLOCCO PER RIMANDO A STAMPE

$iscanicol = array('iscritti','animatori','collaboratori');
$iscanicol[bello] = array('Animati','Animatori','Collaboratori');

if ($dati_grest[periodo] == 1)
{
	$periodo = mysql_query("SELECT * FROM periodo WHERE id_grest = $_SESSION[id_grest]");
	$dati_periodo = mysql_fetch_array($periodo, MYSQL_ASSOC);
	$a = 1;
	while ($a <= $dati_periodo[numero_settimane])
	{
		if ($_GET['settimana_'.$a])
			{
				$settimane_selezionate[] = 'settimana_'.$a;
				$settimane_selezionate[id][] = $a;
				$numero_settimane_selezionate++;
			}
		$a++;
	}
}
if ($dati_grest[eta] == 1)
{
	$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest]");
	while ($dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC))
	{
		if ($_GET['eta_'.$dati_eta[id_eta]]== 1)
			{
				$eta_selezionate[id][] = $dati_eta[id_eta];
				$eta_selezionate[nome][] = $dati_eta[nome];
				$numero_eta_selezionate++;
				
			}
	}
}

$a = -1;
do
{
$a++;
if ($dati_grest[periodo] == 1)
	{print '<h2>Settimana '.$settimane_selezionate[id][$a].'</h2><br/>';}
$b = -1;
do
{
$b++;
if ($numero_eta_selezionate != 0)
	{print '<h2>Età: '.$eta_selezionate[nome][$b].'</h2><br/>';}
$i = 0;
while ($i < 3)
{
if ($_GET[$iscanicol[$i]])
{
	print '<br/><h3>'.$iscanicol[bello][$i].'</h3>';
	$query = "SELECT * FROM	$iscanicol[$i]_$_SESSION[id_grest] WHERE 1 ";
	if ($dati_grest[periodo] == 1)
		{
			if ($numero_settimane_selezionate != 0)
				{$query .= " AND $settimane_selezionate[$a] = 1";}
		}
	if ($dati_grest[eta] == 1)
		{
			if ($numero_eta_selezionate != 0)
				{
					$eta_per_query = $eta_selezionate[id][$b];
					$query .= " AND eta = '$eta_per_query'";
				}
		}
	if ($dati_grest[squadre] == 1)
		{
			if ($numero_squadre_selezionate != 0)
				{
					$squadra_per_query = $squadre_selezionate[id_squadra][$c];
					$query .= " AND squadra = '$squadra_per_query'";
				}
		}
	$query .= " ORDER BY  `cognome`,`nome` ASC ";
	//print 'query:'.$query;
	$iscritti = mysql_query("$query");
	if (mysql_num_rows($iscritti) == null)
	{
		print '<h3><span style="color: red;">NESSUN ISCRITTO AL GREST</span></h3>';
	}
	else
	{
		print '
			<table id="lista" width="100%"><thead>
			<tr>
			<th scope="col">NOME</th>
			<th scope="col">COGNOME</th>';
			if ($iscanicol[$i] == 'iscritti' or $iscanicol[$i] == 'animatori')
			{
				print '<th scope="col">NASCITA</th>
				<th scope="col">CLASSE</th>';
			}
			print '<th scope="col">RECAPITI TELEFONICI</th>';
			if ($iscanicol[$i]=='iscritti') 
			{	
				if ($numero_settimane_selezionate == 0) //se non è selezionata nessuna settimana mostra le 3 settimane si/no
				{
					for ($set=1;$set<=$dati_periodo[numero_settimane];$set++)
					{print '<th scope="col">SET '.$set.'</th>';}
				}
			//	else //se è selezionata una settimana mostra in dettaglio i giorni
			//	{
			//		print '<th scope="col">SET '.$settimane_selezionate[id][$a].'</th>';
			//	}
			}
		print '<th scope="col">NOTE</th>';
		print '</tr></thead><tbody>';
		if ($iscanicol[$i] == 'iscritti')
		{$visualizza='iscritto';}
		if ($iscanicol[$i] == 'animatori')
		{$visualizza='animatore';}
		if ($iscanicol[$i] == 'collaboratori')
		{$visualizza='collaboratore';}	
		while ($dati_iscritti =  mysql_fetch_array($iscritti, MYSQL_ASSOC))
		{
			print'
			<tr>
			<td><a href="visualizza_'.$visualizza.'.php?id='.$dati_iscritti[id].'">'.$dati_iscritti[nome].'</a></td>
			<td><a href="visualizza_'.$visualizza.'.php?id='.$dati_iscritti[id].'">'.$dati_iscritti[cognome].'</a></td>';
			if ($iscanicol[$i]!='collaboratori') 
			{
				print '<td>';
				if ($dati_iscritti[nascita] != null)
					{print $nascita = date("d-m-Y",$dati_iscritti[nascita]);}
				else
					{print 'DATA NON INSERITA';}
				print '</td>';
				print '<td>'.classe($dati_iscritti[classe]).'</td>';
			}
			print"<td>$dati_iscritti[telefono]<br/>$dati_iscritti[cellulare]</td>";
			
			if ($iscanicol[$i]=='iscritti') 
			{
				if ($numero_settimane_selezionate == 0) //se non è selezionata nessuna settimana mostra le 3 settimane si/no
				{
					for ($set=1;$set<=$dati_periodo[numero_settimane];$set++)
					{
					if ($dati_iscritti['settimana_'.$set] == '1')
		                {print '<td><img src="immagini/ico_ok.png" title="Presente" alt="SI"/></td>';}
		            if ($dati_iscritti['settimana_'.$set] == '0')
						{print '<td><img src="immagini/ico_no.png" title="Assente" alt="NO"/></td>';}
					}
				}
			//	else //se è selezionata una settimana mostra in dettaglio i giorni
			//	{
			//		print '<td>';
			//		presenze($a+1);
			//		print '</td>';
			//	}
			}
			print '<td>'.$dati_iscritti[note].'</td>';
			print '</tr>';
		}
		print'	</tbody></table>';
	}	
}
$i++;
}
print '<br/>';

}while ($b < $numero_eta_selezionate-1);

}while ($a < ($numero_settimane_selezionate-1));

	?>
</div>
        <?php include ("pedice.php"); ?>
</div>
</body> 

</html>
