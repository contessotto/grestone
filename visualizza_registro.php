<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>Configurazione di GrestOne</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
<script src="script.js" type="text/javascript"></script>
<body>

		<?php 
		include ("funzioni.php"); 
		verifica_admin($_SESSION[Grestone]);
		?>
		
	<div id="principale">
		<div id="intestazione">
		<br/><h2>Visualizza Registro di GrestOne</h2>
		</div>
        
        <?php include ("menu_configurazione.php"); ?>

        
        
        <div id="contenuto">
		<h3>Visualizza Registro di GrestOne</h3>
	<?php		
		$reg = mysql_query("SELECT * FROM registro ORDER BY  `registro`.`id_registro` DESC ");
		print'
		<table id="lista"><thead>
		<tr>
		<th scope="col">DATA</th>
		<th scope="col">NOME UTENTE</th>
		<th scope="col">GREST</th>
		<th scope="col">OPERAZIONE</th>
		</tr></thead><tbody>';
		//ciclo per scrivere le parrocchie
		while ($visualizza_registro = mysql_fetch_array($reg, MYSQL_ASSOC)) 
		{	
			print"
			<tr>
			<td>$visualizza_registro[data]</td>
			<td>$visualizza_registro[nome_utente]</td>
			<td>$visualizza_registro[id_grest]</td>
			<td>$visualizza_registro[operazione]</td>";
			print '</tr>';

		}
		print'<tbody></table>';	
	?>
        
        </div>
        
        <?php include ("pedice.php"); ?>     
        
    </div>
</body> 

</html>
