<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>
	<script src="script.js" type="text/javascript"></script>
</head>

<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_report.php"); ?>

<div id="contenuto">
	<?php
	connetti();
	print '<h2>ELENCO RAPIDO ISCRITTI EVENTI</h2><br/>';
	
	
// INIZIO BLOCCO PER RIMANDO A STAMPE

		print '<form action="stampe_gite.php" method="get" id="gite">';
if ($dati_grest[periodo] == 1)
{
	$periodo = mysql_query("SELECT * FROM periodo WHERE id_grest = $_SESSION[id_grest]");
	$dati_periodo = mysql_fetch_array($periodo, MYSQL_ASSOC);
	$a = 1;
	while ($a <= $dati_periodo[numero_settimane])
	{
		if ($_GET['settimana_'.$a])
			{
				$settimana = 'settimana_'.$a;
				if ($_GET[$settimana]==1)
					{print '<input type="hidden" name="'.$settimana.'" value="1">';}
			}
		$a++;
	}
}
if ($dati_grest[eta] == 1)
{
	$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest]");
	while ($dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC))
	{
		if ($_GET['eta_'.$dati_eta[id_eta]]== 1)
			{
				print '<input type="hidden" name="eta_'.$dati_eta[id_eta].'" value="1">';
			}
	}

}
if ($dati_grest[squadre] == 1)
{
	$squadre = mysql_query("SELECT * FROM squadre_$_SESSION[id_grest]");
	$conto = mysql_num_rows($squadre);
	$a = 0;
	while ($a <= $conto)
	{
		if ($_GET['squadra_'.$a])
			{
				$squadra = 'squadra_'.$a;
				if ($_GET[$squadra]==1)
					{print '<input type="hidden" name="'.$squadra.'" value="1">';}
			}
		$a++;
	}
}	

if ($dati_grest[gite] == 1)
{
	$eventi = mysql_query("SELECT * FROM gite_$_SESSION[id_grest]");
	$conto = mysql_num_rows($eventi);
	$a = 0;
	while ($a <= $conto)
	{
		if ($_GET['evento_'.$a])
			{
				$evento = 'evento_'.$a;
				if ($_GET[$evento]==1)
					{print '<input type="hidden" name="'.$evento.'" value="1">';}
			}
		$a++;
	}
}

		print '<input type="hidden" name="iscritti" value="'.$_GET[iscritti].'">';
		print '<input type="hidden" name="animatori" value="'.$_GET[animatori].'">';
		print '<input type="hidden" name="collaboratori" value="'.$_GET[collaboratori].'">
		<input type="submit" value="visualizza stampe">
		</form><br/>';

// FINE BLOCCO PER RIMANDO A STAMPE
	
	
	
	
	
	

$iscanicol = array('iscritti','animatori','collaboratori');
$iscanicol[bello] = array('Animati','Animatori','Collaboratori');


	$eventi = mysql_query("SELECT * FROM gite_$_SESSION[id_grest]");
	while ($dati_eventi = mysql_fetch_array($eventi, MYSQL_ASSOC))
	{
		if ($_GET['evento_'.$dati_eventi[id_gita]] == 1)
			{
				$eventi_selezionati[id][] = $dati_eventi[id_gita];
				$eventi_selezionati[nome][] = $dati_eventi[nome];
				$numero_eventi_selezionati++;
				
			}
	}

if ($dati_grest[eta] == 1)
{
	$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest]");
	while ($dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC))
	{
		if ($_GET['eta_'.$dati_eta[id_eta]]== 1)
			{
				$eta_selezionate[id][] = $dati_eta[id_eta];
				$eta_selezionate[nome][] = $dati_eta[nome];
				$numero_eta_selezionate++;
				
			}
	}
}
if ($dati_grest[squadre] == 1)
{
	$squadre = mysql_query("SELECT * FROM squadre_$_SESSION[id_grest]");
	$impostazioni_squadre = mysql_fetch_array($squadre, MYSQL_ASSOC);
	while ($dati_squadre = mysql_fetch_array($squadre, MYSQL_ASSOC))
	{
		if ($_GET['squadra_'.$dati_squadre[id_squadra]]== 1)
			{
				$squadre_selezionate[id_squadra][] = $dati_squadre[id_squadra];
				if ($impostazioni_squadre[nome])
				{$squadre_selezionate[nome][] = $dati_squadre[nome];}
				if ($impostazioni_squadre[colore])
				{$squadre_selezionate[colore][] = $dati_squadre[colore];}
				$numero_squadre_selezionate++;
			}
	}
}

$a = -1;
do
{
$a++;
print '<h2>'.$eventi_selezionati[nome][$a].'</h2><br/>';
if ($numero_eventi_selezionati == 0)
{print 'è necessario selezionare almeno un evento';
break;}
$c = -1;
do
{
$c++;
if ($numero_squadre_selezionate != 0)
	{print '<h2 style="color:'.$squadre_selezionate[colore][$c].'">Squadra: '.$squadre_selezionate[nome][$c].'</h2><br/>';}
$b = -1;
do
{
$b++;
if ($numero_eta_selezionate != 0)
	{print '<h2>Età: '.$eta_selezionate[nome][$b].'</h2><br/>';}
$i = 0;
while ($i < 3)
{
if ($_GET[$iscanicol[$i]])
{
	print '<h3>'.$iscanicol[bello][$i].'</h3>';
	$query = "SELECT * FROM	$iscanicol[$i]_$_SESSION[id_grest] WHERE 1 ";
	if ($dati_grest[gite] == 1)
		{
			if ($numero_eventi_selezionati != 0)
			{
				$eventi_per_query = $eventi_selezionati[id][$a];
				$query .= " AND presenza_evento_$eventi_per_query = '1'";
			}			
		}
	if ($dati_grest[eta] == 1)
		{
			if ($numero_eta_selezionate != 0)
				{
					$eta_per_query = $eta_selezionate[id][$b];
					$query .= " AND eta = '$eta_per_query'";
				}
		}
	if ($dati_grest[squadre] == 1)
		{
			if ($numero_squadre_selezionate != 0)
				{
					$squadra_per_query = $squadre_selezionate[id_squadra][$c];
					$query .= " AND squadra = '$squadra_per_query'";
				}
		}
	$query .= " ORDER BY  `cognome`,`nome` ASC ";	
	//print 'query:'.$query;
	$iscritti = mysql_query("$query");
	if (mysql_num_rows($iscritti) == null)
	{
		print '<h3><span style="color: red;">NESSUN ISCRITTO AL GREST</span></h3>';
	}
	else
	{
		print '
			<table id="lista" width="100%"><thead>
			<tr>
			<th scope="col">NOME</th>
			<th scope="col">COGNOME</th>
			<th scope="col">PAGAMENTO<br/>E NOTE</th>
			<th scope="col">NOTE</th>';
		print '</tr></thead><tbody>';
		
		if ($iscanicol[$i] == 'iscritti')
		{$visualizza='iscritto';}
		if ($iscanicol[$i] == 'animatori')
		{$visualizza='animatore';}
		if ($iscanicol[$i] == 'collaboratori')
		{$visualizza='collaboratore';}	
		
		while ($dati_iscritti =  mysql_fetch_array($iscritti, MYSQL_ASSOC))
		{
			print'
			<tr>
			<td><a href="visualizza_'.$visualizza.'.php?id='.$dati_iscritti[id].'">'.$dati_iscritti[nome].'</a></td>
			<td><a href="visualizza_'.$visualizza.'.php?id='.$dati_iscritti[id].'">'.$dati_iscritti[cognome].'</a></td>';
			
		//Blocco pagamenti e note della gita	
		$dati_gite = $eventi_selezionati[id][$a]; 
		{
			$presenza_gita = 'presenza_evento_'.$dati_gite[id_gita];
			$pagamento_gita = 'pagamento_evento_'.$dati_gite[id_gita];
			$note_gita = 'note_evento_'.$dati_gite[id_gita];
			print '<td>';
			if ($dati_iscritti[$pagamento_gita] == 1)
				{print '<img src="immagini/pagato.png" alt="Pagato" title="Pagato"/><br/>';}
			else
			    {print '<img src="immagini/ico_no.png" alt="NON Pagato" title="NON Pagato"/><br/>';}
			print $dati_iscritti[$note_gita];
			print '</td>';
		}






			print '<td>'.$dati_iscritti[note].'</td>';
			print '</tr>';
		}
		print'	</tbody></table>';
	}	
}
$i++;
}
print '<br/>';

}while ($b < $numero_eta_selezionate-1);

}while ($c < $numero_squadre_selezionate-1);

}while ($a < ($numero_eventi_selezionati-1));

	?>
</div>
        <?php include ("pedice.php"); ?>
</div>
</body> 

</html>
