<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>
	<script src="script.js" type="text/javascript"></script>
</head>
<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_modifiche.php"); ?>

<div id="contenuto">
<?php
print '<br/><h2>Modifiche Rapide</h2>';
	
if ($_POST[passaggi] == '')
{
	print '<form action="modifiche_rapide_iscritti.php" method="post" name="modifiche_rapide">';
	
	print '<br/>Seleziona i parametri per effettuare le modifiche:</br><table id="lista" align="center">';
	if ($dati_grest[squadre]==1)
	{print '<tr onClick="document.modifiche_rapide.squadre.checked=(! document.modifiche_rapide.squadre.checked);"><td><input onClick="document.modifiche_rapide.squadre.checked=(! document.modifiche_rapide.squadre.checked);"type="checkbox" name="squadre" value="1"></td><td>Squadre</td></tr>';}
	if ($dati_grest[laboratori]==1)
	{print '<tr onClick="document.modifiche_rapide.laboratori.checked=(! document.modifiche_rapide.laboratori.checked);"><td><input onClick="document.modifiche_rapide.laboratori.checked=(! document.modifiche_rapide.laboratori.checked);" type="checkbox" name="laboratori" value="1"></td><td>Laboratori</td></tr>';}
	//if ($dati_grest[gruppi]==1)
	//{print '<tr onClick="document.modifiche_rapide.gruppi.checked=(! document.modifiche_rapide.gruppi.checked);"><td><input type="checkbox" name="gruppi" value="1"></td><td>Gruppi</td></tr>';}
	if ($dati_grest[eta]==1)
	{print '<tr onClick="document.modifiche_rapide.eta.checked=(! document.modifiche_rapide.eta.checked);"><td><input onClick="document.modifiche_rapide.eta.checked=(! document.modifiche_rapide.eta.checked);" type="checkbox" name="eta" value="1"></td><td>Età</td></tr>';}
	print '</table>';
	print '<br/>Seleziona il numero di iscritti per pagina:</br>';
	print '<select name="numero_per_pagina">';
	print '
	<option value="5">5</option>
	<option value="10">10</option>
	<option value="20">20</option>
	<option value="30">30</option>
	<option value="50">50</option>';
	print '</select>';
	
	print '<br/><br/>Ordina per :
	<select name="ordine">
	<option value="nome">nome</option>
	<option value="cognome" selected>cognome</option>
	<option value="sesso">sesso</option>
	<option value="nascita">data di nascita</option>';
	if ($dati_grest[squadre])
	{print '<option value="squadra">squadra</option>';}
	if ($dati_grest[eta])
	{print '<option value="eta">fascia d\' età</option>';}
	//if ($dati_grest[gruppi])
	//{print '<option value="gruppo">gruppo</option>';}
	if ($dati_grest[laboratori]==1)
	{
		for ($a=1; $a<=$dati_grest[laboratori_periodo]; $a++)
		{
			print '<option value="laboratorio_'.$a.'">Laboratorio '.$a.'</option>';
		}
	}
	if ($dati_grest[periodo]==1)
	{
		$periodo = mysql_query("SELECT * FROM periodo WHERE id_grest = $_SESSION[id_grest]");
		$dati_periodo = mysql_fetch_array($periodo, MYSQL_ASSOC);
		for ($a=1; $a<=$dati_periodo[numero_settimane]; $a++)
		{
			$settimana = 'settimana_'.$a;
			print '<option value="'.$settimana.'">Settimana '.$a.'</option>';
		}
	}
	if ($dati_grest[gite]==1)
	{
		$gite = mysql_query("SELECT * FROM gite_$_SESSION[id_grest] WHERE liste = 1 ");
			while($dati_gite = mysql_fetch_array($gite, MYSQL_ASSOC)) //utilizzo il while solamente per contare le gite
		//in questo modo non ci sono neanche problemi in caso di eliminazione di una gita 
			{
				print '<option value="presenza_evento_'.$dati_gite[id_gita].'">'.$dati_gite[nome].'</option>';
			}
	}
	print '
	<option value="id">inserimento</option>
	</select>
	crescente<input type="radio" name="direzione" checked="checked" value="ASC">
	decrescente <input type="radio" name="direzione" value="DESC">';
	print '<input type="hidden" name="passaggi" value="primo">';
	print '</br></br><input type="submit" value="passa alle modifiche">';
	print '</form>';
}



do 
{
if ($_POST[passaggi] == 'primo')
{	
	$numero_per_pagina = $_POST[numero_per_pagina];
	
	$id_iniziale = $_POST[id_iniziale];
	if ($_POST[id_iniziale]==0)
	{$id_iniziale = 0;}
	$conto_iscritti = mysql_query("SELECT * FROM iscritti_$_SESSION[id_grest]");
	$iscritti_totali = mysql_num_rows($conto_iscritti);
	if ($_POST[id_iniziale] != null)
	{
		//INSERIMENTO NEL DB DEI DATI	
		for ($b = 1; $b <=$numero_per_pagina; $b++)
		{
			//print 'autoeta_'.$b.'<br>';
			
			if ($_POST["autoeta_$b"] == 1)
			{$_POST["$b-eta"] = eta($_POST["$b-classe"]);}
			
			$eta = $_POST["$b-eta"];
			
			$squadra = $_POST["$b-squadra"];
		
			$query_inserimento = "UPDATE  iscritti_$_SESSION[id_grest] SET ";
			if ($_POST[eta] == 1)
			{
				$query_inserimento .= "eta =  $eta ";
				if ($_POST[squadre] == 1 OR $_POST[laboratori] == 1) $query_inserimento .= ',';
			}
			if ($_POST[squadre] == 1)
			{
				$query_inserimento .= "squadra =  $squadra ";
				if ($_POST[laboratori] == 1) $query_inserimento .= ',';
			}	
			if ($_POST[laboratori] == 1)			
			{
				for ($c =1;$c<=$dati_grest[laboratori_periodo];$c++)
				{
					$laboratorio = $_POST["$b-laboratorio_$c"];
					$query_inserimento .= "laboratorio_$c =  $laboratorio ";
					if ($c <> $dati_grest[laboratori_periodo]) $query_inserimento .=',';
				}
			}
			
			$query_inserimento .= ' WHERE id = '.$_POST[$b];
			//print $query_inserimento;
			//print '<br>';
			mysql_query("$query_inserimento");
			unset($query_inserimento);
			unset($array_query_inserimento);
		}
	}
	if ($id_iniziale >= $iscritti_totali)
	{
		print '<h2>FINE DELL\'ELENCO</h2>';
		break;
	}
	print '
	<form action="modifiche_rapide_iscritti.php" method="post" id="mod" name="mod">
	<table id="lista" width="100%"><thead>
	<tr>
	<th scope="col">NOME</th>
	<th scope="col">COGNOME</th>
	<th scope="col">CLASSE</th>';
	if ($_POST[squadre]==1)
	{
		print '<th scope="col">SQUADRA</th>';		
	}
	if ($_POST[laboratori]==1)
	{
		print '<th scope="col">LABs</th>';		
	}
	//if ($_POST[gruppi]==1)
	//{
	//	print '<th scope="col">GRUPPI</th>';		
	//}
	if ($_POST[eta]==1)
	{
		print '<th scope="col">ETA</th></tr></thead><tbody>';		
	}
	$ordine = $_POST[ordine];
	$direzione = $_POST[direzione];
	if ($direzione == null)
		{$direzione = 'ASC';}	
	if ($ordine == null)
		{$ordine = 'cognome';}
	$ord =" ORDER BY  $ordine $direzione";
	$query = 'SELECT * FROM iscritti_'.$_SESSION[id_grest];
	$query .= $ord;
	$query .= ' LIMIT '.$id_iniziale.','.$numero_per_pagina;
	// print $query;
	$iscritti = mysql_query("$query");
	
	$numero_query==0;
	//INIZIO DEL CICLO DI VISUALIZZAZIONE
	while ($dati_iscritti = mysql_fetch_array($iscritti, MYSQL_ASSOC))
	{
		$numero_query++;
	print '<tr onmouseover="javascript:mostra(\'mostra_'.$dati_iscritti[id].'\',\'img_'.$dati_iscritti[id].'\');" onmouseout="javascript:mostra(\'mostra_'.$dati_iscritti[id].'\',\'img_'.$dati_iscritti[id].'\');" >
		<td>'.$dati_iscritti[nome].'</td>
		<td>'.$dati_iscritti[cognome].'</td>
		<td>'.classe($dati_iscritti[classe]).
		'</td><input type="hidden" name="'.$numero_query.'-classe" value="'.$dati_iscritti[classe].'">
		<input type="hidden" name="'.$numero_query.'" value="'.$dati_iscritti[id].'">';
	
	
		if ($_POST[squadre]==1)
		{
			$contaradio=0;
			$contaradio++;
			$squadra = mysql_query("SELECT * FROM squadre_$_SESSION[id_grest]");
			$impostazioni_squadre = mysql_fetch_array($squadra, MYSQL_ASSOC);
			print '<td>';
			print '<h3><a id="img_'.$dati_iscritti[id].'">
			<img src="immagini/piu.png" title="Espandi" border="0" /></a></h3>';
			print '<div id="mostra_'.$dati_iscritti[id].'" style="display: none;">';
			print '<table id="lista">';
			
			print '<tr><td><input type="radio" id="'.$numero_query.'-squadra_'.$contaradio.'" name="'.$numero_query.'-squadra" value="'.$dati_squadra[id_squadra].'"></td>';
			if ($impostazioni_squadre[nome]==1)
			{print "<td><label for=\"$numero_query-squadra_$contaradio\">Nessuna</label></td>"; }
			if ($impostazioni_squadre[colore] == 1)
			{
				print '<td><label for="'.$numero_query.'-squadra_'.$contaradio.'">---</label></td>';
			}
            print '</tr>';
			
			while ($dati_squadra = mysql_fetch_array($squadra, MYSQL_ASSOC))
			{
				$contaradio++;
				print '<tr><td><input type="radio" id="'.$numero_query.'-squadra_'.$contaradio.'" name="'.$numero_query.'-squadra" value="'.$dati_squadra[id_squadra].'"';
				
				if ($dati_iscritti[squadra] == $dati_squadra[id_squadra])
					{print ' checked ';}
				print '></td>';
				
				if ($impostazioni_squadre[nome]==1)
				{print "<td><label for=\"$numero_query-squadra_$contaradio\">$dati_squadra[nome]</label></td>"; }
				if ($impostazioni_squadre[colore] == 1)
				{
					print '<td><label for="'.$numero_query.'-squadra_'.$contaradio.'"><img src="immagini/squadre/'.$dati_squadra[colore].'.png"
					alt="'.$dati_squadra[colore].'" border="0" title="'.$dati_squadra[colore].'"/></label></td>';
				}			
				print'</tr>';
			}
			print '</table></div>';
		}
		if ($_POST[laboratori]==1)
		{
			print '<td>';
			for ($a=1; $a<=$dati_grest[laboratori_periodo]; $a++)
			{
				print 'Lab. '.$a.':<select name="'.$numero_query.'-laboratorio_'.$a.'">';
				$laboratori = mysql_query("SELECT * FROM laboratori_$_SESSION[id_grest]");
				print '<option value="">NESSUN LABORATORIO</option>';
				while ($dati_laboratori = mysql_fetch_array($laboratori, MYSQL_ASSOC))
				{
					print '<option value="'.$dati_laboratori[id_laboratorio].'"';
					$laboratorio = 'laboratorio_'.$a;
					if ($dati_iscritti[$laboratorio] == $dati_laboratori[id_laboratorio])
						{print ' selected';}
					print '>'.
					$dati_laboratori[nome]
					.'</option>';
				}
				print'</select></br>';
			}	
			print '</td>';
		}
		//if ($_POST[gruppi]==1)
		//{
		//	print '<td>'.$dati_iscritti[gruppo].'</td>';
		//}
		if ($_POST[eta]==1)
		{
			print '<td>';
			print 'Età: <select name="'.$numero_query.'-eta">';
			$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest]");
			print '<option value="">NESSUNA FASCIA D\'ETA</option>';
			while ($dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC))
			{
				print '<option value="'.$dati_eta[id_eta].'"';
				if ($dati_iscritti[eta] == $dati_eta[id_eta])
					{print ' selected';}
				print '>'.
				$dati_eta[nome]
				.'</option>';
			}
			
			print'</select>';
			print '<table id="lista" align="center">
			<tr onClick="document.mod.autoeta_'.$numero_query.'.checked=(! document.mod.autoeta_'.$numero_query.'.checked);">
			<td><input type="checkbox" onClick="document.mod.autoeta_'.$numero_query.'.checked=(! document.mod.autoeta_'.$numero_query.'.checked);" name="autoeta_'.$numero_query.'" id="autoeta_'.$numero_query.'" value="1">Assegna automaticamente</td>
			</tr></table>';
			print '</td>';
			
			
			
			/*
			print'</select>';
			print '<table id="lista" align="center">
			<tr onClick="document.mod.'.$numero_query.'_etautomatica.checked=(! document.mod.'.$numero_query.'_etautomatica.checked);">
			<td><input type="checkbox" name="'.$numero_query.'_etautomatica" id="'.$numero_query.'_etautomatica" checked value="1">Assegna automaticamente</td>
			</tr></table>';
			print '</td>';
			
			*/
			
			
			

		}
			print '</tr>';
	}
	print '
	<input type="hidden" name="ordine" value="'.$ordine.'">
	<input type="hidden" name="direzione" value="'.$direzione.'">
	<input type="hidden" name="squadre" value="'.$_POST[squadre].'">
	<input type="hidden" name="eta" value="'.$_POST[eta].'">
	<input type="hidden" name="laboratori" value="'.$_POST[laboratori].'">';
	//print '<input type="hidden" name="gruppi" value="'.$_POST[gruppi].'">';
	print '<input type="hidden" name="categoria" value="'.$_POST[categoria].'">
	<input type="hidden" name="id_iniziale" value="'.($id_iniziale+$numero_per_pagina).'">
	</tbody></table>
	<input type="hidden" name="passaggi" value="primo">
	<input type="hidden" name="numero_per_pagina" value="'.$_POST[numero_per_pagina].'">
	<br/><input type="submit" value="inserisci e passa ai successivi">
	</form>';
}
} while (false)


?>
        </div>
        
        <?php include ("pedice.php"); ?>     
        
    </div>
</body> 

</html>
