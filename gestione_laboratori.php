<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>
	<script src="script.js" type="text/javascript"></script>
</head>
	<style>
	tr.modifica{display:none;}
	</style>
    <script>
       	function modifica(id)
       	{document.getElementById('modifica'+id).style.display = 'table-row';}
       	function esci(id)
       	{document.getElementById('modifica'+id).style.display = 'none';}        
    </script>
<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
<?php
verifica_amministratore($_SESSION[Grestone]);
?>
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_personalizzazione.php"); ?>

        <div id="contenuto">

<?php
if ($dati_grest[s_laboratori] == 1)
{
	print '<h2>Gestione Laboratori</h2>';
	print 'La gestione dei Laboratori è attualmente sospesa.<br/>
	<a href="riprendi.php?oggetto=laboratori">Riprendi</a>';
}
else
{
if ($dati_grest[laboratori] == 0)
{
	if (!isset($_POST[passaggi])) //primo passaggio:indica il numero di laboratori
	{
	print '<h2>Gestione Laboratori</h2>';
	print '
	<form action="gestione_laboratori.php" method="post">
	<table id="lista" align="center">
	<input type="hidden" name="passaggi" value="primo">
	<tr><td><strong>Numero laboratori:</strong></td><td>
	<select name="numero">';
	for ($a=1;$a<=20;$a++)
		{print '<option value="'.$a.'">'.$a.'</option>';}
	print '</select></td><tr>
	<tr><td><strong>Periodo laboratori </strong></br> (<a href="http://www.grestone.it/demo/assistenza_faq.php#faq_15" target="_blank"
	>indica il numero di laboratori che ciascun iscritto deve scegliere</a>):</td><td>
	<select name="laboratori_periodo">';
	for ($a=1;$a<=20;$a++)
		{print '<option value="'.$a.'">'.$a.'</option>';}
	print '</select></td><tr>
	<tr><td><strong>Durata laboratori </strong></br> (indica quanti giorni (o momenti) si svolgono gli "incontri" di ciascun 
	laboratorio in modo di avere le caselle predisposte per gli appelli)</td><td>
	<select name="laboratori_durata">';
	for ($a=1;$a<=20;$a++)
		{print '<option value="'.$a.'">'.$a.'</option>';}
	print '</select></table></td><tr>	
	<input type="submit" value="inserisci">';
	}
	
	
	if ($_POST[passaggi] == 'primo') //secondo passaggio: indica i valori per ogni squadra
	{	
	print '<h3>Gestione Laboratori</h3>';
	print '
	Inserisci i laboratori: per ciascuna devi indicare obbligatoriamente il NOME, mentre a tua scelta
	puoi decidere di inserire una descrizione e delle note.<br/>
	<form action="gestione_laboratori.php" method="post">
	<input type="hidden" name="passaggi" value="secondo">
	<input type="hidden" name="numero" value="'.$_POST[numero].'">';	
	for ($a=1; $a<=$_POST[numero]; $a++)
	{
		print '
		<h2>Laboratorio '.$a.'</h2>';
		print'Nome: <input type="text" required name="nome_'.$a.'"><br/>';
		print'Descrizione:<textarea name="descrizione_'.$a.'" rows="5" cols="40"></textarea><br/>';
		print'Note: <textarea name="note_'.$a.'" rows="3" cols="40"></textarea><br/>';
	}	
	print '<br/><br/><input type="hidden" name="laboratori_periodo" value="'.$_POST[laboratori_periodo].'">
	<input type="hidden" name="laboratori_durata" value="'.$_POST[laboratori_durata].'">
	<input type="submit" value="inserisci">
	</form>';
	}
	
	if ($_POST[passaggi] == 'secondo') //terzo passaggio: mostra i valori per la conferma
	{	
		print '<h2>Conferma dati inseriti</h2>';
		print '<form action="gestione_laboratori.php" method="post">';
		print'<input type="hidden" name="numero" value="'.$_POST[numero].'">';		
		print '<input type="hidden" name="passaggi"value="terzo">';
		for ($a=1; $a<=$_POST[numero]; $a++)
		{
			$nome = 'nome_'.$a;
			$descrizione = 'descrizione_'.$a;
			$note = 'note_'.$a;
			print"<h2>$a - $_POST[$nome]</h2>";
			print"Descrizione: $_POST[$descrizione]<br/>";
			print"Note: $_POST[$note]<br/><br/>";
		}
		for ($a=1; $a<=$_POST[numero]; $a++)
		{
			$nome = 'nome_'.$a;
			$descrizione = 'descrizione_'.$a;
			$note = 'note_'.$a;
			print '<input type="hidden" name="'.$nome.'" value="'.$_POST[$nome].'">';
			print '<input type="hidden" name="'.$descrizione.'" value="'.$_POST[$descrizione].'">';
			print '<input type="hidden" name="'.$note.'" value="'.$_POST[$note].'">';
		}
		
	print '<br/><br/><input type="hidden" name="laboratori_periodo" value="'.$_POST[laboratori_periodo].'">
			<input type="hidden" name="laboratori_durata" value="'.$_POST[laboratori_durata].'">
			<input type="submit" value="Inserisci Laboratori">';
	print '</form>';
	}
	
	
	
	
		if ($_POST[passaggi] == 'terzo') //quarto passaggio: inserisce i valori nel database
	{
		connetti();
		mysql_query("UPDATE `grests` SET laboratori = '1' WHERE `id_grest` = '$_SESSION[id_grest]';");
		mysql_query("UPDATE `grests` SET laboratori_periodo = '$_POST[laboratori_periodo]' WHERE `id_grest` = '$_SESSION[id_grest]';");
		mysql_query("UPDATE `grests` SET laboratori_durata = '$_POST[laboratori_durata]' WHERE `id_grest` = '$_SESSION[id_grest]';");
		mysql_query("CREATE TABLE  `laboratori_$_SESSION[id_grest]` (`id_laboratorio` INT( 3 ) NOT NULL AUTO_INCREMENT ,
					`nome` TEXT NOT NULL , `descrizione` TEXT NOT NULL , `note` TEXT NOT NULL ,
					PRIMARY KEY (  `id_laboratorio` ))");		
		for ($a=1; $a<=$_POST[numero]; $a++)
		{
			$nome = 'nome_'.$a;
			$descrizione = 'descrizione_'.$a;
			$note = 'note_'.$a;
		
			mysql_query("INSERT INTO  `laboratori_$_SESSION[id_grest]` (`id_laboratorio` ,`nome` ,`descrizione` ,`note`)
						VALUES (NULL ,  '$_POST[$nome]',  '$_POST[$descrizione]',  '$_POST[$note]');");
		}
		for ($a = 1; $a <= $_POST[laboratori_periodo]; $a++)
		{
			mysql_query("ALTER TABLE  `iscritti_$_SESSION[id_grest]` ADD  `laboratorio_$a` INT NOT NULL");
			mysql_query("ALTER TABLE  `animatori_$_SESSION[id_grest]` ADD  `laboratorio_$a` INT NOT NULL");
			mysql_query("ALTER TABLE  `collaboratori_$_SESSION[id_grest]` ADD  `laboratorio_$a` INT NOT NULL");
		}
		registro("$dati_utente[nome_utente]" , "$_SESSION[id_grest]" , 
		"Inserisce laboratori grest $dati_grest[titolo_grest]");					
		print '<h2>Dati inseriti correttamente</h2><meta http-equiv="refresh" content="0;
			URL='.$_SERVER['HTTP_REFERER'].'">';
	}
}
else
{
	print'<h2>La gestione dei laboratori è attiva</h2>
	<a href="#inserisci">Inserisci nuovo Laboratorio</a><br/>
	<a href="disattiva.php?oggetto=laboratori" onclick="return confermadisattiva ();">Disattiva definitivamente gestione Laboratori</a><br/>
	<a href="sospendi.php?oggetto=laboratori">Sospendi temporaneamente gestione Laboratori</a>

	';
	connetti();
	$laboratori = mysql_query("SELECT * FROM  `laboratori_$_SESSION[id_grest]`");
	print '<table id="lista" align="center" width="100%"><thead>
	<tr>';
	print'<th scope="col"></th>';
	print'<th scope="col">NOME</th>';
	print'<th scope="col">DESCRIZIONE</th>';
	print'<th scope="col">NOTE</th>';
	print'<th scope="col">MODIFICA</th>';
	print'<th scope="col">ELIMINA</th>';
	print'</thead></tr><tbody>';
	while ($dati_laboratori = mysql_fetch_array($laboratori, MYSQL_ASSOC))
	{	
		print '<tr>';
		print '<td></td>';
		print"<td>$dati_laboratori[nome]</td>";
		print"<td>$dati_laboratori[descrizione]</td>";
		print"<td>$dati_laboratori[note]</td>";
		print '<td><a onclick="modifica('.$dati_laboratori[id_laboratorio].');"class="elimina"href="#"><img src="immagini/modifica.png" alt="modifica" border="0" title="Modifica"/></a></td>';	
		print'<td><a href="elimina.php?oggetto=laboratorio&id='.$dati_laboratori[id_laboratorio].'" onclick="return conferma ();"><img src="immagini/ico_no.png" alt="elimina" border="0" title="Elimina"/></a></td>';

		print'</tr>';
		
			print'<form action="modifica.php?oggetto=laboratorio" method="post">
					
		<tr class="modifica" id="modifica'.$dati_laboratori[id_laboratorio].'">
		<td><input type="hidden" name="id_laboratorio" value="'.$dati_laboratori[id_laboratorio].'">
		<a class="elimina" href="#" onclick="esci('.$dati_laboratori[id_laboratorio].');">abbandona modifiche</a>
		</td>
		<td><input type="text" name="nome" size="3" value="'.$dati_laboratori[nome].'"></td>
		<td><textarea name="descrizione" cols="15">'.$dati_laboratori[descrizione].'</textarea></td>
		<td><textarea name="note" cols="15">'.$dati_laboratori[note].'</textarea></td>
		<td><input type="submit" value="modifica"></td>
		</tr>
		</form>';
	
	}
	print '</tbody></table>';
	
	if (isset($_POST[passaggi]))
	{
		if ($_POST[passaggi] == 'modifica_primo')
		{
			mysql_query("INSERT INTO `laboratori_$_SESSION[id_grest]` 
			(`id_laboratorio`, `nome`, `descrizione`, `note`)
			 VALUES (NULL, '$_POST[nome]', '$_POST[descrizione]', '$_POST[note]');");
			print '<h2>Dati inseriti correttamente</h2><
			meta http-equiv="refresh" content="1;
		URL=lista_iscritti.php">';
		}		
	}
	else
	{
		print '<a name="inserisci"></a>
		<br/><h2>Inserisci nuovo Laboratorio</h2>
		<form action="gestione_laboratori.php" method="post">
		Nome:<input type="text" name="nome"><br/>
		Descrizione: <textarea name="descrizione"></textarea><br/>
		Note: <textarea name="note"></textarea><br/>
		<input type="hidden" name="passaggi" value="modifica_primo"><br/>
		<input type="submit" value="inserisci"><br/>
		</form>';
	}
}
}
?>			
        </div>
        
        <?php include ("pedice.php"); ?>
        
    </div>
</body> 

</html>
