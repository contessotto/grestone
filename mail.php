<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>Conferma Eliminazione Campo</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
<body>
<div id="eliminato"><br/><br/><strong>
<?php include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
mail($_POST[destinatario], $_POST[oggetto],
 "$_POST[contenuto] Numero di telefono: $_POST[cellulare_mittente]",
 "FROM: $_POST[utente_mittente] < $_POST[mail_mittente] >");
print '<h2>MESSAGGIO SPEDITO CORRETTAMENTE</h2>';
?>
<meta http-equiv="refresh" content="2;
URL=<?=$_SERVER['HTTP_REFERER']?>">
</strong><br/></div>
</body> 

</html>
