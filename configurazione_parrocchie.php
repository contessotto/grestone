<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele
    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">
<head>
	<title>Configurazione di GrestOne</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>
</head>
	<style>
	tr.modifica{display:none;}
	</style>
	<script src="script.js" type="text/javascript"></script>
		        <script>
        	function modifica(id)
        	{document.getElementById('modifica'+id).style.display = 'table-row';}
        	function esci(id)
        	{document.getElementById('modifica'+id).style.display = 'none';}        
        </script>
<body>
		<?php include ("funzioni.php"); 
		verifica_admin();
		registro('admin', '', 'entra in configurazione parrocchie');
		?>
		
	<div id="principale">
		<div id="intestazione">
		<br/><h2>Pagina di Configurazione Generale di GrestOne</h2>
		</div>
        
        <?php include ("menu_configurazione.php"); ?>
        
        
        <div id="contenuto">
		<h3>Lista Parrocchie Iscritte</h3>
		<?php
			
			if ($_POST[nome_parrocchia] <> null) //se è stato inserito il valore di nome_parrocchia
			{registro("admin", "","inserisce la parrocchia $_POST[nome_parrocchia]"); //inserisce il valore nel database
			$a = mysql_query("
				INSERT INTO  `parrocchie` 
				(`id_parrocchia` ,`nome_parrocchia` ,`mail_parrocchia` ,`logo_parrocchia`)
				VALUES (NULL ,  '$_POST[nome_parrocchia]',  '$_POST[mail_parrocchia]', 'immagini/loghi_parrocchie/g_grestone.png');
				");
			print'<meta http-equiv="refresh" content="3;
			URL='.$_SERVER['HTTP_REFERER'].'">';
			}
			else //se il valore non è stato inserito
			{
				$conto = mysql_query("SELECT * FROM parrocchie");
				$righe = mysql_num_rows($conto);
				if ($righe < 1) // se non esistono parrocchie
				{
					print'<h4>Nessuna Parrocchia Iscritta</h4>';
				}
				else // se ne esistono
				{//scrive l'intestazione
					print'
					<table id="lista" width="100%"><thead>
					<tr>
					<th scope="col">ID</th>
					<th scope="col">NOME</th>
					<th scope="col">MAIL</th>
					<th scope="col">MODIFICA</th>
					<th scope="col">ELIMINA</th>
					</tr></thead><tbody>';
					//ciclo per scrivere le parrocchie
					while ($impostazioni_parrocchia = mysql_fetch_array($conto, MYSQL_ASSOC)) 
					{	
						print"
						<tr>
						<td>$impostazioni_parrocchia[id_parrocchia]</td>
						<td>$impostazioni_parrocchia[nome_parrocchia]</td>
						<td>$impostazioni_parrocchia[mail_parrocchia]</td>";
						print '<td><a onclick="modifica('.$impostazioni_parrocchia[id_parrocchia].');"class="elimina"href="#"><img src="immagini/modifica.png" alt="modifica" border="0" title="Modifica"/></a></td>';	
						print '<td><a class="elimina" href="elimina.php?'.
						"oggetto=parrocchia&id=$impostazioni_parrocchia[id_parrocchia]".
						'" onclick="return conferma ();"><img src="immagini/ico_no.png" alt="elimina" border="0" title="Elimina"/></a></td></tr>';
					
						print'<form action="modifica.php?oggetto=parrocchia" method="post">
					
						<tr class="modifica" id="modifica'.$impostazioni_parrocchia[id_parrocchia].'">
						<td>MODIFICA</td>
						<td><input type="text" size="20" required maxlength="50" name="nome_parrocchia" value="'.$impostazioni_parrocchia[nome_parrocchia].'"></td>
						<td><input type="text" size="15" required maxlength="50" name="mail_parrocchia" value="'.$impostazioni_parrocchia[mail_parrocchia].'"></td>
						<td><input type="hidden" name="id_parrocchia" value="'.$impostazioni_parrocchia[id_parrocchia].'">
						<a class="elimina"href="#" onclick="esci('.$impostazioni_parrocchia[id_parrocchia].');">abbandona modifiche</a>
						</td>
						<td><input type="submit" value="modifica"></td>
						</tr>
						
						</form>';
					}
						
					print'</tbody></table>';	
				}
				print '<br/><br/><br/><br/>';
				print'
				<h3>Aggiungi parrocchia</h3>
				<table align="center" id="lista">
				<tr><td>
				<form action="configurazione_parrocchie.php" method="post">
				<strong>Nome parrocchia</strong></td><td><input type="text" autocomplete="off" required maxlength="50" name="nome_parrocchia"></td></tr><td>
				<strong>Mail parrocchia</strong><br/>
				Puoi inserire più di un indirizzo,<br/>basta che li separi con una virgola<br/>(prima@esempio.it,seconda@esempio.it)</td><td><input type="email" autocomplete="off" required maxlength="50" name="mail_parrocchia" 
				title="sarai contattato per le questioni riguardanti la gestione del software"></td></tr></table>
				<input type="submit" value="crea">
				</form>';
			}
		?>
        
        </div>
        
        <?php include ("pedice.php"); ?>     
        
    </div>
</body> 
</html>
