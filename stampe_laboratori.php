<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<?php
include ("funzioni.php"); 

connetti();
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
$dati_parrocchia = carica_impostazioni_parrocchia();

define(FPDF_FONTPATH,"./font/"); //percorso della cartella font
$pdf=new PDF('P','mm',A4);
$pdf->Settitle("$dati_grest[titolo_grest] - $dati_grest[sottotitolo_grest] - $dati_grest[anno_grest] - Elenchi Generali");
$pdf->SetSubject('Report stampabile dei dati del grest');
$pdf->SetKeywords('grest elenchi liste grestone generali');
$pdf->SetCreator('GrestOne - Software di gestione GrEst tramite FPDF 1.7');
$pdf->SetAuthor("$dati_parrocchia[nome_parrocchia]");
$pdf->SetFont('Arial','',10);


$iscanicol = array('iscritti','animatori','collaboratori');
$iscanicol[bello] = array('Animati','Animatori','Collaboratori');

$numero_periodi_selezionati = 0;
	$a = 1;
	while ($a <= $dati_grest[laboratori_periodo])
	{
		if ($_GET['periodo_'.$a])
			{
				$periodi_selezionati[] = $a;
				$numero_periodi_selezionati++;
			}
		$a++;
	}

if ($dati_grest[eta] == 1)
{
	$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest]");
	while ($dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC))
	{
		if ($_GET['eta_'.$dati_eta[id_eta]]== 1)
			{
				$eta_selezionate[id][] = $dati_eta[id_eta];
				$eta_selezionate[nome][] = $dati_eta[nome];
				$numero_eta_selezionate++;
				
			}
	}
}
if ($dati_grest[laboratori] == 1)
{
	$laboratori = mysql_query("SELECT * FROM laboratori_$_SESSION[id_grest]");
	while ($dati_laboratori = mysql_fetch_array($laboratori, MYSQL_ASSOC))
	{
		if ($_GET['laboratorio_'.$dati_laboratori[id_laboratorio]]== 1)
			{
				$laboratori_selezionati[id_laboratorio][] = $dati_laboratori[id_laboratorio];
				$laboratori_selezionati[nome][] = $dati_laboratori[nome];
				$numero_laboratori_selezionati++;
			}
	}
}

$a = -1;
do
{
	$a++;
	if ($numero_periodi_selezionati != 0)
	{//print '<h2>Periodo: '.$periodi_selezionati[$a].'</h2>';
	$periodi_per_stampa = $periodi_selezionati[$a];
	}

	$b = -1;
	do
	{
		$b++;
		if ($numero_eta_selezionate != 0)
			{$eta_per_stampa = $eta_selezionate[nome][$b];}	
	
		$c = -1;
		do
		{
			$c++;
			if ($numero_laboratori_selezionati != 0)
			{//print '<h2>Laboratorio: '.$laboratori_selezionati[nome][$c].'</h2><br/>';
				$laboratori_per_stampa = $laboratori_selezionati[nome][$c];
			}	

$pdf->AddPage();
if ($periodi_per_stampa != null)
{
$pdf->SetFont('Arial','B',15);
$pdf->Cell(20,10,"Periodo $periodi_per_stampa",0,0,'L'); //Scritta valore settimana
$pdf->SetFont('Arial','',10);
//$eventi_per_stampa = '';
}

$pdf->SetX(10);
$pdf->Ln(10);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(20,10,"$laboratori_per_stampa",0,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->Ln(10);
//$squadre_per_stampa = '';

$pdf->SetX(-30);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(20,-10,"$eta_per_stampa",0,0,'R'); //Scritta in alto a dx Grandi o Piccoli
$pdf->SetFont('Arial','',10);
$pdf->Ln(10);
//$eta_per_stampa = '';


$i = 0;
while ($i < 3)
{
if ($_GET[$iscanicol[$i]])
{
	$query = "SELECT * FROM	$iscanicol[$i]_$_SESSION[id_grest] WHERE 1 ";
	
	if ($dati_grest[eta] == 1)
		{
			if ($numero_eta_selezionate != 0)
				{
					$eta_per_query = $eta_selezionate[id][$b];
					$query .= " AND eta = '$eta_per_query'";
				}
		}
	if ($dati_grest[laboratori] == 1)
		{
			if ($numero_periodi_selezionati != 0)
			{
				if ($numero_laboratori_selezionati != 0)
				{
					$laboratorio_per_query = $laboratori_selezionati[id_laboratorio][$c];
					$query .= " AND laboratorio_$periodi_selezionati[$a] = '$laboratorio_per_query'";
				}
				else
				{
					$query .= " AND laboratorio_$periodi_selezionati[$a] != ''";
				}
			}
			else
			{
				$query .= " AND (";
				for ($a=1;$a<=$dati_grest[laboratori_periodo];$a++)
				{
					if ($numero_laboratori_selezionati != 0)
					{
						$laboratorio_per_query = $laboratori_selezionati[id_laboratorio][$c];
						$query .= " laboratorio_$a = '$laboratorio_per_query'";
					}
					else
					{
						$query .= " laboratorio_$a != ''";
					}
					if ($a <> $dati_grest[laboratori_periodo])
					{$query .= "OR";}
				}
			$query .= ")";
				
			}
			
		}
	$query .= " ORDER BY  `cognome`,`nome` ASC ";	
	// print 'query:'.$query;
	$iscritti = mysql_query("$query");
	
	$pdf->SetFont('Arial','B',15);
	$iscanicol_per_stampa = $iscanicol[bello][$i];
	$pdf->Cell(100,10,"Elenco $iscanicol_per_stampa",0,1,'C');
	$pdf->SetFont('Arial','',10);
	if (mysql_num_rows($iscritti) == null)
	{
		$pdf->Cell(35);
		$pdf->Cell(100,10,"NESSUN ISCRITTO PER QUESTA CATEGORIA",1,1,'C');
		$pdf->Ln(30);
	}
	else
	{
		$tabella .= '
			<table id="lista" border="1" width="500">
			<tr>
			<td width="130">NOME</td>
			<td width="130">COGNOME</td>
			';
			if ($iscanicol[$i]=='iscritti' AND $numero_periodi_selezionati != 0)
			{
				$tabella .= '<td width="100">CLASSE</td><td width="230">PRESENZE</td>';
			}
			
			$tabella .= '<td width="200">NOTE</td>';
		$tabella .= '</tr>';
		while ($dati_iscritti =  mysql_fetch_array($iscritti, MYSQL_ASSOC))
		{
			$tabella .= '
			<tr>
			<td width="130">'.$dati_iscritti[nome].'</td>
			<td width="130">'.$dati_iscritti[cognome].'</td>';
			if ($iscanicol[$i]=='iscritti' AND $numero_periodi_selezionati != 0) 
			{
				/* $tabella .= '<td>';
				if ($dati_iscritti[nascita] != null)
					{$tabella .=  $nascita = date("d-m-Y",$dati_iscritti[nascita]);}
				else
					{$tabella .= 'DATA NON INSERITA';}
				$tabella .= '</td>';
				*/
				$tabella .= '<td  width="100">'.classe($dati_iscritti[classe]).'</td><td width="230">';
				if ($numero_periodi_selezionati != 0)
				{
					for ($conto=1;$conto<=$dati_grest[laboratori_durata];$conto++)
					{$tabella .= $conto.'|_|  ';}
				}
				else
				{
					$tabella .= ' |_| ';
				}
				$tabella .= '</td>';
			}
			if (empty($dati_iscritti[note]) == '0')
				{$tabella .= '<td width="200">'.$dati_iscritti[note].'</td>';}
			else
			    {$tabella .= '<td width="200">&nbsp;</td>';}
            
			$tabella .= '</tr>';
		}
		$tabella .= '</table>';
		$pdf->WriteHTML("$tabella");
		$tabella = '';
	}	
}
$i++;
}

}while ($c < ($numero_laboratori_selezionati-1));

}while ($b < $numero_eta_selezionate-1);

}while ($a < ($numero_periodi_selezionati-1));

$pdf->Output("$dati_grest[titolo_grest]_Elenchi_Laboratori.pdf",D);
?>
