<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet" media="screen"/>
    <script src="script.js" type="text/javascript"></script>
</head>

<body>

		<?php include ("funzioni.php"); 
		$dati_utente = verifica_utente();
		$dati_grest = verifica_grest();
		?>
		
		<div id="principale">
		<?php include ("pannello.php");
		include ("intestazione.php");
		include ("menu.php"); ?>
  
        
        <div id="contenuto">
        <br/>
        <h2><img src="immagini/utente.png" border="0"/> Ciao <?php print $dati_utente[nome_utente];?>
        !</h2>
		<h3>Questo è GrestOne, speriamo possa esserti utile :)</h3>
        <p>
		<?php 
		if ($dati_utente[ruolo_utente] == 'normale' or $dati_utente[ruolo_utente] == 'amministratore')
		{echo '
        <a href="inserisci.php"><img src="immagini/home/inserisci.png" alt="Inserisci" name="inserisci" border="0" onmouseover="cambia(inserisci,\'immagini/home/inserisci_on.png\')" onmouseout="cambia(inserisci,\'immagini/home/inserisci.png\')"/></a>
		';}
		?>
        <?php //CONTROLLO SE C'è ALMENO UN ISCRITTO, ALTRIMENTI NON FACCIO VEDERE VEDI E STAMPA
			$iscanicol = array('iscritti','animatori','collaboratori');
			$iscanicol[bello] = array('Animati','Animatori','Collaboratori');

			$c_iscanicol=0;
			$numero_iscritti=0;
			while ($c_iscanicol <3)
			{
				$query = 'SELECT * FROM '.$iscanicol[$c_iscanicol].'_'.$_SESSION[id_grest];
				$numero = mysql_query($query);
	
				$numero_iscritti += mysql_num_rows($numero);
				$c_iscanicol++;
			}
			
			if ($numero_iscritti !=0)
			{
			  print'<a href="elenchi_home.php"><img src="immagini/home/vedi.png" alt="Vedi" name="vedi" border="0" onmouseover="cambia(vedi,\'immagini/home/vedi_on.png\')" onmouseout="cambia(vedi,\'immagini/home/vedi.png\')"/></a>
        <a href="stampe_home.php"><img src="immagini/home/stampa.png" alt="Stampa" name="stampa" border="0" onmouseover="cambia(stampa,\'immagini/home/stampa_on.png\')" onmouseout="cambia(stampa,\'immagini/home/stampa.png\')"/></a>';
			}
		?>
        <a href="statistiche_home.php"><img src="immagini/home/statistiche.png" alt="Statistiche" name="statistiche" border="0" onmouseover="cambia(statistiche,'immagini/home/statistiche_on.png')" onmouseout="cambia(statistiche,'immagini/home/statistiche.png')"/></a>

		<?php 
		if ($dati_utente[ruolo_utente] == 'normale' or $dati_utente[ruolo_utente] == 'amministratore')
		{
			if ($dati_grest[squadre] != 0 OR $dati_grest[laboratori] != 0 OR $dati_grest[gruppi] != 0 OR $dati_grest[eta] != 0)
			{
				if ($numero_iscritti !=0)
			    {		
				print '
				<a href="modifiche_home.php"><img src="immagini/home/modifiche_rapide.png" alt="ModificheRapide" name="ModificheRapide" border="0" onmouseover="cambia(ModificheRapide,\'immagini/home/modifiche_rapide_on.png\')" onmouseout="cambia(ModificheRapide,\'immagini/home/modifiche_rapide.png\')"/></a>
				';
				}
			}
		}
		?>
        <?php 
        if ($dati_utente[ruolo_utente] == 'amministratore')
		{print '
        <a href="personalizzazione.php"><img src="immagini/home/impostazioni.png" alt="Modifica Impostazioni" name="modifica" border="0" onmouseover="cambia(modifica,\'immagini/home/impostazioni_on.png\')" onmouseout="cambia(modifica,\'immagini/home/impostazioni.png\')"/></a>
		';}
		?>
		
		
          </p>
        <p><br/>
        </p>
    <?php
		if ($dati_grest[periodo] == 1)
		{
			include ("calendario.php");
		}
		print '<br/><br/>';
		include ("pannello_amministrativo.php");
			
	?>
        
        </div>
        
        <?php include ("pedice.php"); ?>     
        
    </div>
</body> 

</html>
