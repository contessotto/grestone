<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Assistenza</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet" media="screen"/>
    <script src="script.js" type="text/javascript"></script>
</head>

<body>

<?php include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
		
<div id="principale">
	<?php 
	include ("intestazione.php");
	include ("menu_assistenza.php");
	?>

	<div id="contenuto">   
		<br/>
		<h2><img src="immagini/utente.png" border="0"/> Hei <?php print $dati_utente[nome_utente];?>! Bisogno di aiuto?</h2>
		<br/>
		<h3>Speriamo davvero di poterti essere utili :)</h3>
		<!--<p style="font-size:18px;text-align:left;color:blue;">
			Cosa è successo? Forse il programma non funziona?<br/>
			O magari hai sbagliato una procedura e credi di aver perso dei dati?<br/>
			Probabilmente hai solo bisogno di una dritta su come adoperare il programma!<br/><br/><br/>
			<strong> - Innanzitutto NON scoraggarti <br/>
			 - Cerca di capire a quale ambito appartiene il tuo problema <br/>
			 - Consulta le nostre FAQ (domande frequenti) <br/>
			 - Se non hai trovato risposta richiedi aiuto ai gestori del programma della tua parrocchia <br/>
			 - E se nemmeno loro sanno risponderti chiedi assistenza direttamente agli sviluppatori del GrestOne,
			ti risponderanno volentieri! </strong><br/>

		</p>-->
        <br/>
        <img src="immagini/assistenza_home.png" alt="
			Cosa è successo? Forse il programma non funziona?<br/>
			O magari hai sbagliato una procedura e credi di aver perso dei dati?<br/>
			Probabilmente hai solo bisogno di una dritta su come adoperare il programma!<br/><br/><br/>
			<strong> - Innanzitutto NON scoraggarti <br/>
			 - Cerca di capire a quale ambito appartiene il tuo problema <br/>
			 - Consulta le nostre FAQ (domande frequenti) <br/>
			 - Se non hai trovato risposta richiedi aiuto ai gestori del programma della tua parrocchia <br/>
			 - E se nemmeno loro sanno risponderti chiedi assistenza direttamente agli sviluppatori del GrestOne,
			ti risponderanno volentieri! </strong><br/>"/>
	
	<?php include ("controllo_versione.php"); ?> 
    
    </div>
    
        
	<?php include ("pedice.php"); ?>     
        
</div>
</body> 

</html>
