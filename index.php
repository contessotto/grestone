<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne</title>
    <link rel="shortcut icon" href="favicon.ico">
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stileindex.css" rel="stylesheet">

</head>
<script src="script.js" type="text/javascript"></script>
<body>
	<div id="login">
	<br/><br/><br/>
	<h1>Accesso a GrestOne</h1>
	<h4>Software di gestione per GrEst</h4>	
<?php 
include ("funzioni.php");
controllo_attivazione();

// inizializzazione della sessione
session_start();
// se la sessione di autenticazione
// è già impostata non sarà necessario effettuare il login
// e il browser verrà reindirizzato alla pagina di scrittura dei post
if (isset($_SESSION['grestone']))
{
 // reindirizzamento alla homepage in caso di login mancato
 header("Location: login_grest.php");
}
// controllo sul parametro d'invio
if(isset($_POST['submit']) && (trim($_POST['submit']) == "Login"))
{
  // controllo sui parametri di autenticazione inviati
  if( !isset($_POST['username']) || $_POST['username']=="" )
  {
    echo "Attenzione, inserire la username.";
  }
  elseif( !isset($_POST['password']) || $_POST['password'] =="")
  {
    echo "Attenzione, inserire la password.";
  }else{
    // validazione dei parametri tramite filtro per le stringhe
    $username = trim(filter_var($_POST['username'], FILTER_SANITIZE_STRING));
    $password = trim(filter_var($_POST['password'], FILTER_SANITIZE_STRING));
    $password = sha1($password);

    // chiamata alla funzione di connessione
    connetti();
    // interrogazione della tabella
    $auth = mysql_query("SELECT * FROM utenti WHERE nome_utente = '$username' AND password = '$password'");
    // controllo sul risultato dell'interrogazione
        if(mysql_num_rows($auth)==0)
    {
        // reindirizzamento alla homepage in caso di insuccesso
          header("Location: index.php?errore=1");
    }else{
          // chiamata alla funzione per l'estrazione dei dati
    $res =  mysql_fetch_array($auth,MYSQL_ASSOC);
          // creazione del valore di sessione
    $_SESSION['grestone'] = $res[id_utente];
	$_SESSION['id_parrocchia'] = $res[id_parrocchia];
          // disconnessione da MySQL
	disconnetti();
        // reindirizzamento alla pagina di amministrazione in caso di successo
    header("Location: login_grest.php");
    }
  }
}else{
  // form per l'autenticazione
  ?>
<form action="index.php" method="POST">
<?php if (isset($_GET[errore])) print'<h2>ERRORE NEI DATI INSERITI</h2>';?>
<img src="immagini/utente.png" alt="" title="Nome Utente" border="0"/> Nome Utente<br />
<input name="username" type="text"><br />
<img src="immagini/password.png" alt="" title="Password" border="0"/> Password<br />
<input name="password" type="password" size="20"><br />
<input name="submit" type="submit" value="Login">
</form>
  <?php
}
?>
<br/><br/><br/><br/><br/><br/>
</div>
</body>

</html>
