<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
<script src="script.js" type="text/javascript"></script>
<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
<?php
verifica_normale($_SESSION[Grestone]);
?>
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_inserisci.php"); ?>

<div id="contenuto">
	<?php
if (isset($_POST[passaggi]))
{
		if ($_POST[passaggi]=='primo')
	{
		$_POST[nome] = trim(ucwords(strtolower($_POST[nome])));
		$_POST[cognome] = trim(ucwords(strtolower($_POST[cognome])));
		$_POST[indirizzo] = trim(ucwords(strtolower($_POST[indirizzo])));
		$_POST[paese] = trim(ucwords(strtolower($_POST[paese])));
		
		// con magic_quotes_gpc disattivo, mi appoggio ad addslashes()
		if ( ! get_magic_quotes_gpc() )
		{
			$_POST[nome] = mysql_escape_string ($_POST[nome]);
			$_POST[cognome] = mysql_escape_string ($_POST[cognome]);
			$_POST[indirizzo] = mysql_escape_string ($_POST[indirizzo]);
			$_POST[paese] = mysql_escape_string ($_POST[paese]);
			$_POST[note] = mysql_escape_string ($_POST[note]);
		}

		$_POST[indirizzo]=$_POST[indirizzo].' '.$_POST[numero];
		print '<form action="inserisci_animatore.php" method="post">';
		print '<h2>Conferma</h2>
		<br/><input type="submit" value="conferma e inserisci">
		<input type="button" value="Torna alle modifiche" onclick="javascript:history.back();"><br/>
	<table width="100%" border="0" id="lista">
	  <tr>
	    <td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NOME:
          '.$_POST[nome].'</td>
	    <td>&nbsp;</td>
      </tr>
	  <tr>
	    <td>COGNOME:
        '.$_POST[cognome].'</td>
	    <td>&nbsp;</td>
      </tr>
	  <tr>
	    <td>
	      <p>DATA NASCITA:
          '.$_POST[giorno_nascita].'-'.$_POST[mese_nascita].'-'.$_POST[anno_nascita].'
        </p></td>
	    <td>
	      <p>CLASSE FREQUENTATA: ';
		print classe($_POST[classe]);
		print '
        </p></td>
      </tr>
	  <tr>
	    <td>SESSO: ';
	     if ($_POST[sesso] == 'm')
		{print '<img src="immagini/m.png" title="Maschio"/>';}
		if ($_POST[sesso] == 'f')
		{print '<img src="immagini/f.png" title="Femmina"/>';}
        print'</td>
	    <td>&nbsp;</td>
      </tr>
	  <tr>
	    <td>
         INDIRIZZO:
         '.$_POST[indirizzo].'</td>
	    <td>PAESE:
         '.$_POST[paese].'</td>
      </tr>
	  <tr>
	    <td>TELEFONO:
        '.$_POST[telefono].'</td>
	    <td>CELL:
        '.$_POST[cellulare].'</td>
      </tr>
	  <tr>
	    <td>NOTE:
        '.$_POST[note].'</td>
	    <td>&nbsp;</td>
      </tr>
</table> <br/>
		';
		
		if ($dati_grest[periodo]==1)
		{
			$periodo = mysql_query("SELECT * FROM periodo WHERE id_grest = $_SESSION[id_grest]");
			$dati_periodo = mysql_fetch_array($periodo, MYSQL_ASSOC);
			print '<br/><br/><h3>Informazioni Periodo</h3>';
			for ($a=1; $a<=$dati_periodo[numero_settimane]; $a++)
			{	
				$settimana = 'settimana_'.$a;
				print 'Settimana '.$a.': ';
				if ($_POST[$settimana] == 1)
				{
					print 'ISCRITTO <img src="immagini/ico_ok.png"/><br/>';
				}
				else
				{
					print 'NON ISCRITTO <img src="immagini/ico_no.png"/><br/>';
				}
				print '<input type="hidden" name="'.$settimana.'" value="'.$_POST[$settimana].'">';
			}
		}
		if ($dati_grest[squadre]==1)
		{
			$squadra = mysql_query("SELECT * FROM squadre_$_SESSION[id_grest]");
			$impostazioni_squadre = mysql_fetch_array($squadra, MYSQL_ASSOC);
			$squadra = mysql_query("SELECT * FROM squadre_$_SESSION[id_grest] WHERE id_squadra='$_POST[id_squadra]'");
			$dati_squadra = mysql_fetch_array($squadra, MYSQL_ASSOC);
			print '<br/><h3>Informazioni Squadra</h3>';
			if ($_POST[id_squadra] == null)
				{print 'NESSUNA SQUADRA SELEZIONATA <img src="immagini/ico_no.png"/>';}
			else
			{
				print 'squadra: ';
				if ($impostazioni_squadre[nome]==1)
					{print "$dati_squadra[nome] "; }
				if ($impostazioni_squadre[colore] == 1)
				{
					print '<img src="immagini/squadre/'.$dati_squadra[colore].'.png"
					alt="'.$dati_squadra[colore].'" border="0" title="'.$dati_squadra[colore].'"/>';
				}	
				print '<input type="hidden" name="id_squadra" value="'.$dati_squadra[id_squadra].'">';
			}
		}
		if ($dati_grest[eta]==1)
		{
			print '<br/><br/><h3>Informazioni Fascia d\'età</h3>';
			print 'Fascia d\'Età: ';
			if ($_POST[eta] == null)
				{print 'NESSUNA FASCIA SCELTA <img src="immagini/ico_no.png"/><br/>';}
			else
			{
				$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest] 
				WHERE id_eta = $_POST[eta]");
				$dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC);
				print $dati_eta[nome].'<br/>';
				print '<input type="hidden" name="eta"
				value="'.$_POST[eta].'">';
			}
		}
		if ($dati_grest[laboratori]==1)
		{
			print '<br/><h3>Informazioni Laboratori</h3>';
			for ($a=1; $a<=$dati_grest[laboratori_periodo]; $a++)
			{	
				$laboratorio = 'laboratorio_'.$a;
				print 'Laboratorio '.$a.': ';
				if ($_POST[$laboratorio] == null)
				{print 'NESSUN LABORATORIO SCELTO <img src="immagini/ico_no.png"/><br/>';}
				else
				{$laboratori = mysql_query("SELECT * FROM laboratori_$_SESSION[id_grest] 
				WHERE id_laboratorio = $_POST[$laboratorio]");
				$dati_laboratorio = mysql_fetch_array($laboratori, MYSQL_ASSOC);
				print $dati_laboratorio[nome].'<br/>';
				print '<input type="hidden" name="'.$laboratorio.'"
						value="'.$_POST[$laboratorio].'">';
				}
			}	
		}
		if ($dati_grest[gruppi]==1)
		{
			print '<br/><h3>Informazioni Gruppi</h3>';
			print 'Gruppo: ';
			if ($_POST[gruppo] == null)
				{print 'NESSUN GRUPPO SCELTO <img src="immagini/ico_no.png"/><br/>';}
			else
			{
				$gruppi = mysql_query("SELECT * FROM gruppi_$_SESSION[id_grest] 
				WHERE id_gruppo = $_POST[gruppo]");
				$dati_gruppo = mysql_fetch_array($gruppi, MYSQL_ASSOC);
				print $dati_gruppo[nome].'<br/>';
				print '<input type="hidden" name="gruppo"
				value="'.$_POST[gruppo].'">';
			}
		}		
		if ($dati_grest[gite]==1)
		{
			print '<br/><h3>Informazioni Eventi</h3>';
			$gite = mysql_query("SELECT * FROM gite_$_SESSION[id_grest] WHERE liste = '1' ");
			while($dati_gite = mysql_fetch_array($gite, MYSQL_ASSOC))
			{
				$presenza_gita = 'presenza_gita_'.$dati_gite[id_gita];
				$pagamento_gita = 'pagamento_gita_'.$dati_gite[id_gita];
				$note_gita = 'note_gita_'.$dati_gite[id_gita];
				print "<strong> $dati_gite[nome]</strong><br/>";
				if ($_POST[$presenza_gita])
				{
					print 'ISCRITTO <img src="immagini/ico_ok.png"/><br/>';
					if ($_POST[$pagamento_gita])
						{print 'PAGATO <img src="immagini/pagato.png"/><br/>';}
					else
						{print 'ANCORA DA PAGARE <img src="immagini/ico_no.png"/><br/>';}
					if ($_POST[$note_gita] != null)
						{print 'Note:'.$_POST[$note_gita];}
				}
				else
					{print 'NON ISCRITTO <img src="immagini/ico_no.png"/><br/>';}
				print '<br/>';
				print '<input type="hidden" name="'.$presenza_gita.'" value="'.$_POST[$presenza_gita].'">';
				print '<input type="hidden" name="'.$pagamento_gita.'" value="'.$_POST[$pagamento_gita].'">';				
				print '<input type="hidden" name="'.$note_gita.'" value="'.$_POST[$note_gita].'">';
			}
		}
		print '
		<input type="hidden" name="passaggi" value="secondo">
		<input type="hidden" name="nome" value="'.$_POST[nome].'">
		<input type="hidden" name="cognome" value="'.$_POST[cognome].'">
		<input type="hidden" name="sesso" value="'.$_POST[sesso].'">';
		$nascita = mktime(0, 0, 0, $_POST[mese_nascita], $_POST[giorno_nascita], $_POST[anno_nascita]);
		print '<input type="hidden" name="nascita" value="'.$nascita.'">
		<input type="hidden" name="classe" value="'.$_POST[classe].'">
		<input type="hidden" name="indirizzo" value="'.$_POST[indirizzo].'">
		<input type="hidden" name="paese" value="'.$_POST[paese].'">
		<input type="hidden" name="telefono" value="'.$_POST[telefono].'">
		<input type="hidden" name="cellulare" value="'.$_POST[cellulare].'">
		<input type="hidden" name="note" value="'.$_POST[note].'">		
		<input type="hidden" name="id" value="'.$_POST[id].'"><br/>
		<input type="submit" value="conferma e inserisci">
		<input type="button" value="Torna alle modifiche" onclick="javascript:history.back();"><br/>
		</form>
		';
	}
	
	if ($_POST[passaggi]=='secondo')
	{
		
		mysql_query("
		INSERT INTO  `animatori_$_SESSION[id_grest]` (
		`id` ,`nome` ,`cognome` ,`sesso` ,`nascita` ,`classe` ,`indirizzo` ,`paese` ,`telefono` ,`cellulare` ,`note`)
		VALUES (NULL ,  '$_POST[nome]',  '$_POST[cognome]',  '$_POST[sesso]',  '$_POST[nascita]',  '$_POST[classe]',
		'$_POST[indirizzo]',  '$_POST[paese]',  '$_POST[telefono]',  '$_POST[cellulare]' , '$_POST[note]');
		");
		$ultimo_id = mysql_insert_id();
		if ($dati_grest[squadre]==1)
		{
			mysql_query("UPDATE  `animatori_$_SESSION[id_grest]` SET  `squadra` =  '$_POST[id_squadra]'
			 WHERE  `id` = $ultimo_id;");
		}

		if ($dati_grest[eta]==1)
		{
				mysql_query("UPDATE  `animatori_$_SESSION[id_grest]` SET  `eta` =  '$_POST[eta]'
				WHERE  `id` = $ultimo_id;");	
		}

		if ($dati_grest[periodo]==1)
		{
			$periodo = mysql_query("SELECT * FROM periodo WHERE id_grest = $_SESSION[id_grest]");
			$dati_periodo = mysql_fetch_array($periodo, MYSQL_ASSOC);
			for ($a=1; $a<=$dati_periodo[numero_settimane]; $a++)
			{	
				$settimana = 'settimana_'.$a;
				if ($_POST[$settimana] == 1)
				{
					mysql_query("UPDATE  `animatori_$_SESSION[id_grest]` SET  `$settimana` =  '$_POST[$settimana]'
					WHERE  `id` = $ultimo_id;");
				}
			}	
		}
		
		if ($dati_grest[laboratori]==1)
		{
			for ($a=1; $a<=$dati_grest[laboratori_periodo]; $a++)
			{	
				$laboratorio = 'laboratorio_'.$a;
				mysql_query("UPDATE  `animatori_$_SESSION[id_grest]` SET  `$laboratorio` =  '$_POST[$laboratorio]'
				WHERE  `id` = $ultimo_id;");
			}	
		}
		if ($dati_grest[gruppi]==1)
		{
				mysql_query("UPDATE  `animatori_$_SESSION[id_grest]` SET  `gruppo` =  '$_POST[gruppo]'
				WHERE  `id` = $ultimo_id;");	
		}		
		if ($dati_grest[gite]==1)
		{
			$gite = mysql_query("SELECT * FROM gite_$_SESSION[id_grest] WHERE liste = '1' ");
			while($dati_gite = mysql_fetch_array($gite, MYSQL_ASSOC)) //utilizzo il while solamente per contare le gite
			//in questo modo non ci sono neanche problemi in caso di eliminazione di una gita 
			{
				$presenza_gita = 'presenza_gita_'.$dati_gite[id_gita];
				$pagamento_gita = 'pagamento_gita_'.$dati_gite[id_gita];
				$note_gita = 'note_gita_'.$dati_gite[id_gita];
				mysql_query("UPDATE  `animatori_$_SESSION[id_grest]` SET
				`presenza_evento_$dati_gite[id_gita]` =  '$_POST[$presenza_gita]',
				`pagamento_evento_$dati_gite[id_gita]` =  '$_POST[$pagamento_gita]',
				`note_evento_$dati_gite[id_gita]` =  '$_POST[$note_gita]'
				WHERE  `id` = $ultimo_id;");
			}
		}

		print'<h2><img src="immagini/ico_ok.png"/> ISRIZIONE EFFETTUATA CORRETTAMENTE <img src="immagini/ico_ok.png"/></h2><a style="color:grey;"
		href="inserisci_animatore.php">Inserisci nuovo utente</a>
			<meta http-equiv="refresh" content="1;
		URL=inserisci_animatore.php">';
		
	}
}
else
{
	print'
	<h2>Inserisci nuovo animatore</h2>
	<form action="inserisci_animatore.php" method="post" id="iscrizione" name="iscrizione">
	<br/><h3>Informazioni Generali</h3>
	<table width="100%" border="0" id="lista">
  <tr>
    <td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NOME:
    <input type="text" required name="nome"></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>COGNOME:
    <input type="text" required name="cognome"></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
      <p>DATA NASCITA:
        <input type="text" required name="giorno_nascita" pattern="[0-9]{2}"size="2" placeholder="GG" maxlength="2">
        -
        <input type="text" required name="mese_nascita" pattern="[0-9]{2}"size="2" placeholder="MM" maxlength="2">
        -
        <input type="text" required name="anno_nascita" pattern="[0-9]{4}"size="2" placeholder="AAAA" maxlength="4">
    </p></td>
    <td>
CLASSE FREQUENTATA:
  <select name="classe" required>
    <option value="9">3 media</option>
    <option value="10">1 superiore</option>
    <option value="11">2 superiore</option>
    <option value="12">3 superiore</option>
    <option value="13">4 superiore</option>
    <option value="14">5 superiore</option>
    <option value="15">altro</option>
  </select></td>
  </tr>
	  <tr>
	    <td>SESSO:
	      <input type="radio" required name="sesso" id="m" value="m">
	      <label for="m">M -</label>
          <input type="radio" required name="sesso" id="f" value="f">
          <label for="f">F</label></td>
	    <td>&nbsp;</td>
      </tr>
  <tr>
    <td>INDIRIZZO:
    <input type="text" name="indirizzo"> N <input type="text" name="numero" size="2"></td>
    <td>PAESE:
    <input type="text" name="paese"></td>
  </tr>
  <tr>
    <td>TELEFONO:
  <input type="text" name="telefono"></td>
    <td>CELL:
  <input type="text" name="cellulare"></td>
  </tr>
  <tr>
    <td>NOTE:
    <textarea name="note"></textarea></td>
    <td>&nbsp;</td>
  </tr>
</table>
	
	
	
	
	<br/>';

	if ($dati_grest[periodo]==1)
	{
		$periodo = mysql_query("SELECT * FROM periodo WHERE id_grest = $_SESSION[id_grest]");
		$dati_periodo = mysql_fetch_array($periodo, MYSQL_ASSOC);
		print '<br/><h3>Informazioni Periodo</h3>';
		print '<table id="lista" align="center">';
		for ($a=1; $a<=$dati_periodo[numero_settimane]; $a++)
		{	
			print '<tr onClick="document.iscrizione.settimana_'.$a.'.checked=(! document.iscrizione.settimana_'.$a.'.checked);"><td> <input type="checkbox" onClick="document.iscrizione.settimana_'.$a.'.checked=(! document.iscrizione.settimana_'.$a.'.checked);" name="settimana_'.$a.'" value="1"></td><td>Settimana '.$a.'</td></tr>';
		}
		 print '</table>';
	}
	if ($dati_grest[squadre]==1)
	{
		$squadra = mysql_query("SELECT * FROM squadre_$_SESSION[id_grest]");
		$impostazioni_squadre = mysql_fetch_array($squadra, MYSQL_ASSOC);
		print '<br/><h3>Informazioni Squadra</h3>';
			print '<table id="lista" align="center">';
			print '<tr><td><input type="radio" id="id_squadra'.$dati_squadra[id_squadra].'" name="id_squadra" value="'.$dati_squadra[id_squadra].'"></td>';
			
			if ($impostazioni_squadre[nome]==1)
			{print "<td><label for=\"id_squadra$dati_squadra[id_squadra]\">Nessuna Squadra</label></td>"; }
			if ($impostazioni_squadre[colore] == 1)
			{
				print '<td><label for="id_squadra'.$dati_squadra[id_squadra].'">---</label></td>';
			}
            print '</tr>';
		while ($dati_squadra = mysql_fetch_array($squadra, MYSQL_ASSOC))
		{
			print '<tr><td>';
			print '<input type="radio" name="id_squadra" id="id_squadra'.$dati_squadra[id_squadra].'" value="'.$dati_squadra[id_squadra].'"></td>';
			if ($impostazioni_squadre[nome]==1)
			{print "<td><label for=\"id_squadra$dati_squadra[id_squadra]\">$dati_squadra[nome]</label></td>"; }
			if ($impostazioni_squadre[colore] == 1)
			{
				print '<td><label for="id_squadra'.$dati_squadra[id_squadra].'"><img src="immagini/squadre/'.$dati_squadra[colore].'.png"
				 alt="'.$dati_squadra[colore].'" border="0" title="'.$dati_squadra[colore].'"/></label></td>';
			}					
			print'</tr>';
		}
			print '</table>';
	}
	
	if ($dati_grest[eta]==1)
	{
		print '<br/><br/><h3>Informazioni Fasce d\'Età</h3>';
		print'<select name="eta">';
		$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest]");
		print '<option value="">NESSUNA ETA</option>';
		while ($dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC))
		{
			print '<option value="'.$dati_eta[id_eta].'">'.
			$dati_eta[nome]
			.'</option>';
		}
		print'</select><br/>';
	}

	if ($dati_grest[laboratori]==1)
	{
		print '<br/><h3>Informazioni Laboratori</h3>';
		print '<table id="lista" align="center">';
		for ($a=1; $a<=$dati_grest[laboratori_periodo]; $a++)
		{
			print '<tr><td>Laboratorio '.$a.':<select name="laboratorio_'.$a.'">';
			$laboratori = mysql_query("SELECT * FROM laboratori_$_SESSION[id_grest]");
			print '<option value="">NESSUN LABORATORIO</option>';
			while ($dati_laboratori = mysql_fetch_array($laboratori, MYSQL_ASSOC))
			{
				print '<option value="'.$dati_laboratori[id_laboratorio].'">'.
				$dati_laboratori[nome]
				.'</option>';
			}
			print'</select></td></tr>';
		}	
		print '</table>';
	}
	if ($dati_grest[gruppi]==1)
	{
		print '<br/><h3>Informazioni Gruppi</h3>';
		print'<select name="gruppo">';
		$gruppi = mysql_query("SELECT * FROM gruppi_$_SESSION[id_grest]");
		print '<option value="">NESSUN GRUPPO</option>';
		while ($dati_gruppi = mysql_fetch_array($gruppi, MYSQL_ASSOC))
		{
			print '<option value="'.$dati_gruppi[id_gruppo].'">'.
			$dati_gruppi[nome]
			.'</option>';
		}
		print'</select><br/>';
	}
	if ($dati_grest[gite]==1)
	{
		print '<br/><h3>Informazioni Eventi</h3>';
		$gite = mysql_query("SELECT * FROM gite_$_SESSION[id_grest] WHERE liste = '1' ");
		while($dati_gite = mysql_fetch_array($gite, MYSQL_ASSOC))
		{
			print "<b>$dati_gite[nome]</b><br/>";
		print '<table id="lista" align="center">';
			print '<tr onClick="document.iscrizione.presenza_gita_'.$dati_gite[id_gita].'.checked=(! document.iscrizione.presenza_gita_'.$dati_gite[id_gita].'.checked);"><td><input type="checkbox" onClick="document.iscrizione.presenza_gita_'.$dati_gite[id_gita].'.checked=(! document.iscrizione.presenza_gita_'.$dati_gite[id_gita].'.checked);"name="presenza_gita_'.$dati_gite[id_gita].'"value="1"></td><td>presenza <img src="immagini/ico_ok.png"/></td></tr>';
			print '<tr onClick="document.iscrizione.pagamento_gita_'.$dati_gite[id_gita].'.checked=(! document.iscrizione.pagamento_gita_'.$dati_gite[id_gita].'.checked);"><td><input type="checkbox" onClick="document.iscrizione.pagamento_gita_'.$dati_gite[id_gita].'.checked=(! document.iscrizione.pagamento_gita_'.$dati_gite[id_gita].'.checked);" name="pagamento_gita_'.$dati_gite[id_gita].'"value="1"></td><td>pagamento  <img src="immagini/pagato.png"/></td></tr>';
			print '<tr><td>note:</td><td><textarea name="note_gita_'.$dati_gite[id_gita].'"></textarea></td></tr>';
			print'</table><br/><br/>';
		}
	}

	
	print'
	<input type="hidden" name="passaggi" value="primo"><br/>
	<input type="submit" value="inserisci">
	</form>
	';
}
	?>
</div>
        <?php include ("pedice.php"); ?>
</div>
</body> 

</html>
