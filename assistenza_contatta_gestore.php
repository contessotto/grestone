<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Assistenza</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet" media="screen"/>
    <script src="script.js" type="text/javascript"></script>
</head>

<body>

<?php include ("funzioni.php");
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
		
<div id="principale">
	<?php 
	include ("intestazione.php");
	include ("menu_assistenza.php");
	?>

	<div id="contenuto">  
	<h2>Contatta il Gestore del programma</h2><br/><br/>
    <p align="left">
	Utilizza questa pagina se i problemi che hai riguardano "guasti" temporanei del programma, come il malfunzionamento
	di alcune procedure, l'impossibilità di accedere ad alcune pagine o altro ancora. <br/><br/>
	Il/i gestore/i del programma ha/hanno inserito il/i seguente/i indirizzo/i:<br/>
	<span style="color: red;"><?php	print $mail_admin = mail_admin();	?>
    </span>.<br/><img src="immagini/mail.png" border="0"/> Puoi mandare una mail tramite il modulo sotto.</p>
	<table align="center" id="lista">
	<tr><td>
	<form action="mail.php" method="post">
	<input type="hidden" name="destinatario" value="<?php print $mail_admin; ?>">	
	<input type="hidden" name="utente_mittente" value="<?php print $dati_utente[nome_utente]; ?>">
	Oggetto</td><td><input type="text" required name="oggetto"></td></tr><td>
	Testo</td><td><textarea name="contenuto" required cols="50" rows="15"></textarea></td></tr><td>
	Il mio indirizzo mail</td><td><input type="email" required name="mail_mittente"></td></tr><td>
	Il mio cellulare (facoltativo)</td><td><input type="text" name="cellulare_mittente"></td></tr></table>
	<input type="submit" value="invia">
	</form>
	</div>
        
	<?php include ("pedice.php"); ?>     
        
</div>
</body> 

</html>
