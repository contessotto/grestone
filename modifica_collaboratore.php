<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
<script src="script.js" type="text/javascript"></script>
<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
<?php
verifica_normale($_SESSION[Grestone]);
?>
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_inserisci.php"); ?>

<div id="contenuto">
	<?php
if (isset($_POST[passaggi]))
{
	if ($_POST[passaggi]=='primo')
	{
		print '<form action="modifica_collaboratore.php" method="post">';
		print '<h2>Conferma</h2>
		<br/><input type="submit" value="conferma e inserisci">
		<input type="button" value="Torna alle modifiche" onclick="javascript:history.back();"><br/>
	<table width="100%" border="0" id="lista">
	  <tr>
	    <td>NOME:
        '.$_POST[nome].'</td>
	    <td>&nbsp;</td>
      </tr>
	  <tr>
	    <td>COGNOME:
        '.$_POST[cognome].'</td>
	    <td>&nbsp;</td>
      </tr>
	  <tr>
	    <td>INDIRIZZO:
        '.$_POST[indirizzo].'</td>
	    <td>PAESE:
        '.$_POST[paese].'</td>
      </tr>
	  <tr>
	    <td>TELEFONO:
        '.$_POST[telefono].'</td>
	    <td>CELLULARE:
        '.$_POST[cellulare].'</td>
      </tr>
	  <tr>
	    <td>NOTE:
        '.$_POST[note].'</td>
	    <td>&nbsp;</td>
      </tr>
</table><br/>
		';
		
		if ($dati_grest[periodo]==1)
		{
			$periodo = mysql_query("SELECT * FROM periodo WHERE id_grest = $_SESSION[id_grest]");
			$dati_periodo = mysql_fetch_array($periodo, MYSQL_ASSOC);
			print '<br/><h3>Informazioni Periodo</h3>';
			for ($a=1; $a<=$dati_periodo[numero_settimane]; $a++)
			{	
				$settimana = 'settimana_'.$a;
				print 'Settimana '.$a.': ';
				if ($_POST[$settimana] == 1)
				{
					print 'ISCRITTO <img src="immagini/ico_ok.png"/><br/>';
				}
				else
				{
					print 'NON ISCRITTO <img src="immagini/ico_no.png"/><br/>';
				}
				print '<input type="hidden" name="'.$settimana.'" value="'.$_POST[$settimana].'">';
			}
		}
		if ($dati_grest[squadre]==1)
		{
			$squadra = mysql_query("SELECT * FROM squadre_$_SESSION[id_grest]");
			$impostazioni_squadre = mysql_fetch_array($squadra, MYSQL_ASSOC);
			$squadra = mysql_query("SELECT * FROM squadre_$_SESSION[id_grest] WHERE id_squadra='$_POST[id_squadra]'");
			$dati_squadra = mysql_fetch_array($squadra, MYSQL_ASSOC);
			print '<br/><h3>Informazioni Squadra</h3>';
			if ($_POST[id_squadra] == null)
				{print 'NESSUNA SQUADRA SELEZIONATA <img src="immagini/ico_no.png"/>';}
			else
			{
				print 'Squadra: ';
				if ($impostazioni_squadre[nome]==1)
					{print "$dati_squadra[nome] "; }
				if ($impostazioni_squadre[colore]==1)
					{print '<img src="immagini/squadre/'.$dati_squadra[colore].'.png" alt="'.$dati_squadra[colore].'" border="0" title="'.$dati_squadra[colore].'"/>';}
				print '<input type="hidden" name="id_squadra" value="'.$dati_squadra[id_squadra].'">';
			}
		}
		
		if ($dati_grest[eta]==1)
		{
			print '<br/><br/><h3>Informazioni Fascia d\'età</h3>';
			print 'Fascia d\'Età: ';
			if ($_POST[eta] == null)
				{print 'NESSUNA FASCIA SCELTA <img src="immagini/ico_no.png"/><br/>';}
			else
			{
				$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest] 
				WHERE id_eta = $_POST[eta]");
				$dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC);
				print $dati_eta[nome].'<br/>';
				print '<input type="hidden" name="eta"
				value="'.$_POST[eta].'">';
			}
		}
		if ($dati_grest[laboratori]==1)
		{
			print '<br/><h3>Informazioni Laboratori</h3>';
			for ($a=1; $a<=$dati_grest[laboratori_periodo]; $a++)
			{	
				$laboratorio = 'laboratorio_'.$a;
				print 'Laboratorio '.$a.': ';
				if ($_POST[$laboratorio] == null)
				{print 'NESSUN LABORATORIO SCELTO <img src="immagini/ico_no.png"/><br/>';}
				else
				{$laboratori = mysql_query("SELECT * FROM laboratori_$_SESSION[id_grest] 
				WHERE id_laboratorio = $_POST[$laboratorio]");
				$dati_laboratorio = mysql_fetch_array($laboratori, MYSQL_ASSOC);
				print $dati_laboratorio[nome].'<br/>';
				print '<input type="hidden" name="'.$laboratorio.'"
						value="'.$_POST[$laboratorio].'">';
				}
			}	
		}	
		if ($dati_grest[gruppi]==1)
		{
			print '<br/><h3>Informazioni Gruppi</h3>';	
			print 'Gruppo: ';
			if ($_POST[gruppo] == null)
				{print 'NESSUN GRUPPO SCELTO <img src="immagini/ico_no.png"/><br/>';}
			else
			{	
				$gruppi = mysql_query("SELECT * FROM gruppi_$_SESSION[id_grest] 
				WHERE id_gruppo = $_POST[gruppo]");
				$dati_gruppo = mysql_fetch_array($gruppi, MYSQL_ASSOC);
				print $dati_gruppo[nome].'<br/>';
				print '<input type="hidden" name="'.gruppo.'"
				value="'.$_POST[gruppo].'">';
			}
		}			
		if ($dati_grest[gite]==1)
		{
			print '<br/><h3>Informazioni Eventi</h3>';
			$gite = mysql_query("SELECT * FROM gite_$_SESSION[id_grest] WHERE liste = '1' ");
			while($dati_gite = mysql_fetch_array($gite, MYSQL_ASSOC))
			{
				$presenza_gita = 'presenza_gita_'.$dati_gite[id_gita];
				$pagamento_gita = 'pagamento_gita_'.$dati_gite[id_gita];
				$note_gita = 'note_gita_'.$dati_gite[id_gita];
				print "<strong>$dati_gite[nome]</strong><br/>";
				if ($_POST[$presenza_gita])
				{
					print 'ISCRITTO <img src="immagini/ico_ok.png"/><br/>';
					if ($_POST[$pagamento_gita])
						{print 'PAGATO <img src="immagini/pagato.png"/><br/>';}
					else
						{print 'ANCORA DA PAGARE <img src="immagini/ico_no.png"/><br/>';}
					if ($_POST[$note_gita] != null)
						{print 'Note:'.$_POST[$note_gita];}
				}
				else
					{print 'NON ISCRITTO <img src="immagini/ico_no.png"/><br/>';}
				print '<br/><br/><br/>';
				print '<input type="hidden" name="'.$presenza_gita.'" value="'.$_POST[$presenza_gita].'">';
				print '<input type="hidden" name="'.$pagamento_gita.'" value="'.$_POST[$pagamento_gita].'">';				
				print '<input type="hidden" name="'.$note_gita.'" value="'.$_POST[$note_gita].'">';
			}
		}
		print '
		<input type="hidden" name="passaggi" value="secondo">
		<input type="hidden" name="nome" value="'.$_POST[nome].'">
		<input type="hidden" name="cognome" value="'.$_POST[cognome].'">
		<input type="hidden" name="indirizzo" value="'.$_POST[indirizzo].'">
		<input type="hidden" name="paese" value="'.$_POST[paese].'">
		<input type="hidden" name="telefono" value="'.$_POST[telefono].'">
		<input type="hidden" name="cellulare" value="'.$_POST[cellulare].'">	
		<input type="hidden" name="note" value="'.$_POST[note].'">		
		<input type="hidden" name="id" value="'.$_POST[id].'"><br/>
		<input type="submit" value="conferma e inserisci">
		<input type="button" value="Torna alle modifiche" onclick="javascript:history.back();"><br/>
		</form>
		';
	}
	if ($_POST[passaggi]=='secondo')
	{
		
		mysql_query("UPDATE  `collaboratori_$_SESSION[id_grest]` SET
		`nome` =  '$_POST[nome]',
		`cognome` =  '$_POST[cognome]',
		`indirizzo` =  '$_POST[indirizzo]',
		`paese` =  '$_POST[paese]',
		`telefono` =  '$_POST[telefono]',
		`cellulare` =  '$_POST[cellulare]',
		`note` =  '$_POST[note]'	
		WHERE  `id` = '$_POST[id]';");

		$ultimo_id = $_POST[id]; // utilizzo questa variabile per mantenere la pagina il più possibile simile a
		//quella da cui è presa, cioè inserisci_iscritto.php
		if ($dati_grest[squadre]==1)
		{
			mysql_query("UPDATE  `collaboratori_$_SESSION[id_grest]` SET  `squadra` =  '$_POST[id_squadra]'
			 WHERE  `id` = $ultimo_id;");
		}

		if ($dati_grest[eta]==1)
		{
				mysql_query("UPDATE  `collaboratori_$_SESSION[id_grest]` SET  `eta` =  '$_POST[eta]'
				WHERE  `id` = $ultimo_id;");	
		}
		
		if ($dati_grest[periodo]==1)
		{
			$periodo = mysql_query("SELECT * FROM periodo WHERE id_grest = $_SESSION[id_grest]");
			$dati_periodo = mysql_fetch_array($periodo, MYSQL_ASSOC);
			for ($a=1; $a<=$dati_periodo[numero_settimane]; $a++)
			{	
				$settimana = 'settimana_'.$a;
				mysql_query("UPDATE  `collaboratori_$_SESSION[id_grest]` SET  `$settimana` =  '$_POST[$settimana]'
				WHERE  `id` = $ultimo_id;");
			}	
		}
		
		if ($dati_grest[laboratori]==1)
		{
			for ($a=1; $a<=$dati_grest[laboratori_periodo]; $a++)
			{	
				$laboratorio = 'laboratorio_'.$a;
				mysql_query("UPDATE  `collaboratori_$_SESSION[id_grest]` SET  `$laboratorio` =  '$_POST[$laboratorio]'
				WHERE  `id` = $ultimo_id;");
			}	
		}
		if ($dati_grest[gruppi]==1)
		{	
			mysql_query("UPDATE  `collaboratori_$_SESSION[id_grest]` SET  `gruppo` =  '$_POST[gruppo]'
			WHERE  `id` = $ultimo_id;");
		}
		
		if ($dati_grest[gite]==1)
		{
			$gite = mysql_query("SELECT * FROM gite_$_SESSION[id_grest] WHERE liste = '1' ");
			while($dati_gite = mysql_fetch_array($gite, MYSQL_ASSOC)) //utilizzo il while solamente per contare le gite
			//in questo modo non ci sono neanche problemi in caso di eliminazione di una gita 
			{
				$presenza_gita = 'presenza_gita_'.$dati_gite[id_gita];
				$pagamento_gita = 'pagamento_gita_'.$dati_gite[id_gita];
				$note_gita = 'note_gita_'.$dati_gite[id_gita];
				mysql_query("UPDATE  `collaboratori_$_SESSION[id_grest]` SET
				`presenza_evento_$dati_gite[id_gita]` =  '$_POST[$presenza_gita]',
				`pagamento_evento_$dati_gite[id_gita]` =  '$_POST[$pagamento_gita]',
				`note_evento_$dati_gite[id_gita]` =  '$_POST[$note_gita]'
				WHERE  `id` = $ultimo_id;");
			}
		}

		print'<h2><img src="immagini/ico_ok.png"/> MODIFICA EFFETTUATA CORRETTAMENTE <img src="immagini/ico_ok.png"/></h2>
		<meta http-equiv="refresh" content="1;
		URL=lista_collaboratori.php">';
		
	}
}
else
{
	$iscritti = mysql_query("SELECT * FROM  collaboratori_$_SESSION[id_grest] WHERE id = '$_GET[id]'");
	$dati_iscritti =  mysql_fetch_array($iscritti, MYSQL_ASSOC);

	print'
	<h2>Modifica dei dati di un Collaboratore</h2>
	<form action="modifica_collaboratore.php" method="post" name="modifica" id="modifica">
	<br/><h3>Informazioni Generali</h3>
	<table width="100%" border="0" id="lista">
	  <tr>
	    <td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NOME:
        <input type="text" name="nome"value="'.$dati_iscritti[nome].'"></td>
	    <td>&nbsp;</td>
      </tr>
	  <tr>
	    <td>COGNOME:
        <input type="text" name="cognome" value="'.$dati_iscritti[cognome].'"></td>
	    <td>&nbsp;</td>
      </tr>
	  <tr>
	    <td>INDIRIZZO:
        <input type="text" name="indirizzo" value="'.$dati_iscritti[indirizzo].'"></td>
	    <td>PAESE:
        <input type="text" name="paese" value="'.$dati_iscritti[paese].'"></td>
      </tr>
	  <tr>
	    <td>TELEFONO:
        <input type="text" name="telefono" value="'.$dati_iscritti[telefono].'"></td>
	    <td>CELLULARE:
      <input type="text" name="cellulare" value="'.$dati_iscritti[cellulare].'"></td>
      </tr>
	  <tr>
	    <td>NOTE:
        <textarea name="note">'.$dati_iscritti[note].'</textarea></td>
	    <td>&nbsp;</td>
      </tr>
</table><br/>
	';
	if ($dati_grest[periodo]==1)
	{
		$periodo = mysql_query("SELECT * FROM periodo WHERE id_grest = $_SESSION[id_grest]");
		$dati_periodo = mysql_fetch_array($periodo, MYSQL_ASSOC);
		print '<br/><h3>Informazioni Periodo</h3>';
		print '<table id="lista" align="center">';
		for ($a=1; $a<=$dati_periodo[numero_settimane]; $a++)
		{	
			print '<tr onClick="document.modifica.settimana_'.$a.'.checked=(! document.modifica.settimana_'.$a.'.checked);"><td> <input onClick="document.modifica.settimana_'.$a.'.checked=(! document.modifica.settimana_'.$a.'.checked);" type="checkbox" name="settimana_'.$a.'" value="1"';
			$settimana = 'settimana_'.$a.'';
			if ($dati_iscritti[$settimana] == 1)
				{print'checked';}
			print'></td><td>Settimana '.$a.'</td></tr>';
		}
		 print '</table>';
	}
	if ($dati_grest[squadre]==1)
	{
		$squadra = mysql_query("SELECT * FROM squadre_$_SESSION[id_grest]");
		$impostazioni_squadre = mysql_fetch_array($squadra, MYSQL_ASSOC);
		print '<br/><h3>Informazioni Squadra</h3>';
			print '<table id="lista" align="center">';
			print '<tr><td><input type="radio" name="id_squadra" id="id_squadra'.$dati_squadra[id_squadra].'" value="'.$dati_squadra[id_squadra].'"></td>';
			
			if ($impostazioni_squadre[nome]==1)
			{print "<td><label for=\"id_squadra$dati_squadra[id_squadra]\">Nessuna Squadra</label></td>"; }
			if ($impostazioni_squadre[colore] == 1)
			{
				print '<td><label for="id_squadra'.$dati_squadra[id_squadra].'">---</label></td>';
			}
            print '</tr>';
		while ($dati_squadra = mysql_fetch_array($squadra, MYSQL_ASSOC))
		{
			print '<tr><td>';
			print '<input type="radio" name="id_squadra" id="id_squadra'.$dati_squadra[id_squadra].'"value="'.$dati_squadra[id_squadra].'"';
			if ($dati_squadra[id_squadra] == $dati_iscritti[squadra])
				{print'checked="checked"';}
			print '></td>';
			if ($impostazioni_squadre[nome]==1)
			{print "<td><label for=\"id_squadra$dati_squadra[id_squadra]\">$dati_squadra[nome]</label></td>"; }
			if ($impostazioni_squadre[colore] == 1)
			{
				print '<td><label for="id_squadra'.$dati_squadra[id_squadra].'"><img src="immagini/squadre/'.$dati_squadra[colore].'.png"
				 alt="'.$dati_squadra[colore].'" border="0" title="'.$dati_squadra[colore].'"/></label></td>';
			}					
			print'</tr>';
		}
			print '</table>';
	}
	if ($dati_grest[eta]==1)
	{
		print '<br/><br/><h3>Informazioni Fasce d\'Età</h3>';
			print 'Età: <select name="eta">';
			$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest]");
				print '<option value="">NESSUNA FASCIA D\'ETA <img src="immagini/ico_no.png"/></option>';
			while ($dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC))
			{
				print '<option value="'.$dati_eta[id_eta].'"';
				if ($dati_iscritti[eta] == $dati_eta[id_eta])
					{print 'selected';}
				print '>'.
				$dati_eta[nome]
				.'</option>';
			}
			print'</select><br/>';	
	}
	
	if ($dati_grest[laboratori]==1)
	{
		print '<br/><h3>Informazioni Laboratori</h3>';
		for ($a=1; $a<=$dati_grest[laboratori_periodo]; $a++)
		{
			print 'Laboratorio '.$a.':<select name="laboratorio_'.$a.'">';
			$laboratori = mysql_query("SELECT * FROM laboratori_$_SESSION[id_grest]");
				print '<option value="">NESSUN LABORATORIO <img src="immagini/ico_no.png"/></option>';
			while ($dati_laboratori = mysql_fetch_array($laboratori, MYSQL_ASSOC))
			{
				print '<option value="'.$dati_laboratori[id_laboratorio].'"';
				$laboratorio = 'laboratorio_'.$a;
				if ($dati_iscritti[$laboratorio] == $dati_laboratori[id_laboratorio])
					{print 'selected';}
				print '>'.
				$dati_laboratori[nome]
				.'</option>';
			}
			print'</select><br/>';
		}	
	}
	if ($dati_grest[gruppi]==1)
	{
		print '<br/><h3>Informazioni Gruppi</h3>';
			print 'Gruppo: <select name="gruppo">';
			$gruppi = mysql_query("SELECT * FROM gruppi_$_SESSION[id_grest]");
				print '<option value="">NESSUN GRUPPO <img src="immagini/ico_no.png"/></option>';
			while ($dati_gruppi = mysql_fetch_array($gruppi, MYSQL_ASSOC))
			{
				print '<option value="'.$dati_gruppi[id_gruppo].'"';
				if ($dati_iscritti[gruppo] == $dati_gruppi[id_gruppo])
					{print 'selected';}
				print '>'.
				$dati_gruppi[nome]
				.'</option>';
			}
			print'</select><br/>';	
	}	
	if ($dati_grest[gite]==1)
	{
		print '<br/><h3>Informazioni Eventi</h3>';
		$gite = mysql_query("SELECT * FROM gite_$_SESSION[id_grest] WHERE liste = '1' ");
		while($dati_gite = mysql_fetch_array($gite, MYSQL_ASSOC))
		{
			print "<b>$dati_gite[nome]</b><br/>";
		print '<table id="lista" align="center">';
			print '<tr onClick="document.modifica.presenza_gita_'.$dati_gite[id_gita].'.checked=(! document.modifica.presenza_gita_'.$dati_gite[id_gita].'.checked);"><td><input onClick="document.modifica.presenza_gita_'.$dati_gite[id_gita].'.checked=(! document.modifica.presenza_gita_'.$dati_gite[id_gita].'.checked);" type="checkbox" name="presenza_gita_'.$dati_gite[id_gita].'"value="1"';
			$presenza_per_array = "presenza_evento_$dati_gite[id_gita]";
			if ($dati_iscritti[$presenza_per_array] == '1')
				{print'checked="checked"';}
			print'></td><td>presenza <img src="immagini/ico_ok.png"/></td></tr>';
			print '<tr onClick="document.modifica.pagamento_gita_'.$dati_gite[id_gita].'.checked=(! document.modifica.pagamento_gita_'.$dati_gite[id_gita].'.checked);"><td><input onClick="document.modifica.pagamento_gita_'.$dati_gite[id_gita].'.checked=(! document.modifica.pagamento_gita_'.$dati_gite[id_gita].'.checked);" type="checkbox" name="pagamento_gita_'.$dati_gite[id_gita].'"value="1"';
			$pagamento_per_array = "pagamento_evento_$dati_gite[id_gita]";
			if ($dati_iscritti[$pagamento_per_array] == '1')
				{print'checked="checked"';}
			print'></td><td>pagamento  <img src="immagini/pagato.png"/></td></tr>';
			print '<tr><td>note:</td><td><textarea name="note_gita_'.$dati_gite[id_gita].'"></textarea></td></tr>';
			print'</table><br/><br/>';
		}
	}

	
	print'
	<input type="hidden" name="id" value="'.$dati_iscritti[id].'">	
	<input type="hidden" name="passaggi" value="primo"><br/>
	<input type="submit" value="modifica">
	</form>
	';
}
	?>
</div>
        <?php include ("pedice.php"); ?>
</div>
</body> 

</html>
