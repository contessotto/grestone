<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
verifica_admin();
?>
	
    <div id="principale">

        <?php $impostazioni = carica_impostazioni_parrocchia();?>

        <div id="intestazione">
		<br/><h2>Pagina di Configurazione Generale di GrestOne</h2>
		</div>

        <?php include ("menu_configurazione.php"); ?>

        <div id="contenuto">
			
<?php 
if ($_GET[grest]==null)
{print"<br/><br/><br/><br/><br/><br/><h2>ATTENEZIONE! Selezionare un grest!!!</h2><br/><br/><br/><br/><br/><br/><br/><br/>";}
else
{
		connetti();
		print'<h2 name="utenti">Modifica Impostazioni Grest</h2>';
		$grest = mysql_query("SELECT * FROM grests WHERE id_grest = $_GET[grest]");
		$dati_grest = mysql_fetch_array($grest, MYSQL_ASSOC);
		$parrocchia = mysql_query("SELECT * FROM parrocchie WHERE id_parrocchia = $dati_grest[id_parrocchia]");
		$impostazioni_parrocchia = mysql_fetch_array($parrocchia, MYSQL_ASSOC);
		print '<form action="modifica.php?oggetto=grest&deviazione=configurazione_grest" method="post">
		<input type="hidden" name="id_grest" value="'.$_GET[grest].'">
		<table id="lista" align="center">
		<tr><td>'.$impostazioni_parrocchia[nome_parrocchia].'</td></tr>
		<tr><td>Titolo <input type="text" required maxlength="20" size="10" name="titolo_grest" value="'.$dati_grest[titolo_grest].'"></td></tr>
		<tr><td>Sottotitolo <input type="text" maxlength="50" size="10" name="sottotitolo_grest" value="'.$dati_grest[sottotitolo_grest].'"></td></tr>
		<tr><td>Anno <input type="text"  maxlength="15" size="2"name="anno_grest" value="'.$dati_grest[anno_grest].'"></td></tr>
		</table>
		<input type="submit" value="modifica">
		</form>';
}
?>

        </div>
        
        <?php include ("pedice.php"); ?>
        
    </div>
</body> 

</html>
