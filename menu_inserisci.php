<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?> 
 <div id="menu">
		
<?php include ("dataora.php"); ?>
<?php include ("ricerca_rapida.php"); ?>

        <br/>
        <a href="home.php"><img src="immagini/menu/menu_principale.png" alt="MENU PRINCIPALE" name="menu" border="0" onmouseover="cambia(menu,'immagini/menu/menu_principale_on.png')" onmouseout="cambia(menu,'immagini/menu/menu_principale.png')"/></a><br/>
		<br/>
		<?php 
		if ($dati_utente[ruolo_utente] == 'normale' or $dati_utente[ruolo_utente] == 'amministratore')
		{echo '
		

		<a href="inserisci_iscritto.php"><img src="immagini/menu/inserisci_iscritto.png" alt="Inserisci Iscritto" name="InserisciIscritto" border="0" onmouseover="cambia(InserisciIscritto,\'immagini/menu/inserisci_iscritto_on.png\')" onmouseout="cambia(InserisciIscritto,\'immagini/menu/inserisci_iscritto.png\')"/></a>
		<br/>
		
		<a href="inserisci_animatore.php"><img src="immagini/menu/inserisci_animatore.png" alt="Inserisci Animatore" name="InserisciAnimatore" border="0" onmouseover="cambia(InserisciAnimatore,\'immagini/menu/inserisci_animatore_on.png\')" onmouseout="cambia(InserisciAnimatore,\'immagini/menu/inserisci_animatore.png\')"/></a>
		<br/>
		
		<a href="inserisci_collaboratore.php"><img src="immagini/menu/inserisci_collaboratore.png" alt="Inserisci Collaboratore" name="InserisciCollaboratore" border="0" onmouseover="cambia(InserisciCollaboratore,\'immagini/menu/inserisci_collaboratore_on.png\')" onmouseout="cambia(InserisciCollaboratore,\'immagini/menu/inserisci_collaboratore.png\')"/></a><br/>
		<br/>
		
		';}
		?>  
        
    </div>
