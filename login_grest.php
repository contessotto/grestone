<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<?php
// inizializzazione della sessione
session_start();
// controllo sul valore di sessione
if (!isset($_SESSION['grestone']))
{
 // reindirizzamento alla homepage in caso di login mancato
 header("Location: index.php");
 exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne</title>
    <link rel="shortcut icon" href="favicon.ico">
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stileindex.css" rel="stylesheet">

</head>
<script src="script.js" type="text/javascript"></script>
<body>
	<div id="login">
	<br/><br/><br/>
	<h1>Accesso a GrestOne</h1>
	<h4>Software di gestione per GrEst</h4>

<?php
if($_SESSION['grestone'] == '1')
{
	header("location: configurazione.php");
	break;
	//accede all'area amministrativa
}
else
{
	if (isset ($_SESSION['id_grest']))
	{
		header("location: home.php");
	}
	else
	{
		if ($_POST[passaggi] == 'primo')
		{
			$_SESSION['id_grest'] = $_POST[id_grest];
			header("location: home.php");
		}
		else
		{
			include("funzioni.php");
			$dati_utente = verifica_utente();
			$id_grest_utente = explode("-",$dati_utente[id_grest]);
			$numero_grest_utente = count($id_grest_utente);
			print '<h3><img src="immagini/utente.png" alt="" title="Nome Utente" border="0"/> Ciao '.$dati_utente[nome_utente].' !</h3>';
			if ($dati_utente[id_grest] == null)
			{
				print 'Non puoi amministrare alcun Grest.<br/><br/>';
			}
			else
			{
				if ($numero_grest_utente == 1)
				{
					$_SESSION['id_grest'] = $id_grest_utente[0];
					header("location: home.php");
					exit;
				}
				print "A quale Grest vuoi accedere?<br/>";
				print '
				<form action="login_grest.php"method="post">
				<select name="id_grest" >';
				$c = "SELECT * FROM grests WHERE ";
				$i  = 0;
				
				$c .= " id_grest = $id_grest_utente[$i] ";
				$i  = 1;
				while ($i < $numero_grest_utente)
				{
					$c .= " OR id_grest = $id_grest_utente[$i] ";
					$i++;
				}
				$c .= " ORDER BY id_grest DESC";
				//print $c;
				$query = mysql_query("$c");
				while ($impostazioni_grest = mysql_fetch_array($query, MYSQL_ASSOC))
				{
					print '<option value="';
					print $impostazioni_grest[id_grest];
					print '">';
					print "$impostazioni_grest[titolo_grest]";
					print '</option>';
				}

				print '</select><br/><br/>
				<input type="hidden" name="passaggi" value="primo">
				<input type="submit" value="Accedi al Grest">
				</form><br/>';
			}
		}	
		if ($dati_utente[ruolo_utente] == 'amministratore')
		{
			print'oppure<br/>
			<form action=" personalizzazione_parrocchia.php" method="post">
			<input type="submit" value="Accedi al pannello di controllo Parrocchia">
			</form>';
		}
	}
}
?>
<br/><br/><br/><br/><br/><br/>
</div>
</body>

</html>
