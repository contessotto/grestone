<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Assistenza</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet" media="screen"/>
    <script src="script.js" type="text/javascript"></script>
</head>

<body>

<?php include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
		
<div id="principale">
	<?php 
	include ("intestazione.php");
	include ("menu_assistenza.php");
	?>

	<div id="contenuto">   
		<br/>
		<a name="inizio"><h2>F.A.Q. Domande Frequenti</h2></a><br/>
			<?php
		$faq[domanda][] = 'Perché sto usando il GrestOne e non un normale foglio eletronico?';
		$faq[risposta][] = 'Perchè con il GrestOne potrai avere pronti gli elenchi divisi per categorie, squadre, eventi.. 
		pronti da stampare; potrai sapere in maniera semplice qunati sono gli iscritti e spotrattutto come sono divisi
		nelle varie attività del grest. Esplora le altre potenzialità nel sito www.GrestOne.it';
		
		
		$faq[domanda][] = 'Come mai non posso modificare le impostazioni del grest?';
		$faq[risposta][] = '
		Per poter accedere alla modifica delle impostazioni devi essere un utente amministratore.
		Per ottenere i permessi, rivolgiti al gestore della tua parrocchia e richiedigli di diventare utente amministratore.';
		
		$faq[domanda][] = 'A cosa serve il logo della parrocchia? Perchè dovrei caricarlo?';
		$faq[risposta][] = 'Il logo della parrocchia una volta caricato comparirà in alto a sinistra in tutte le pagine di GrestOne (al posto della G).<br/> Inoltre sarà visualizzato in alto nell\'intestazione della parrocchia all\' interno di tutte le stampe che andrai a generare.<br/> In assenza di aggiunta del logo, il sistema includerà automaticamente la G del logo GrestOne.';

		$faq[domanda][] = 'Non riesco a caricare nessun logo per la parrocchia. Perchè?';
		$faq[risposta][] = 'GrestOne accetta queste estensioni di file: PNG, GIF, JPG. Controlla che il tuo file non sia di un altra estensione (tipo BMP) ed eventualmente converti l\'immagine. Assicurati inoltre di aver dato i diritti di Lettura e Scrittura (777) alla cartella immagini/loghi_parrocchie di GrestOne. In mancanza di questi permessi non sarà possibile salvare l\'immagine su server!! Modifica la configurazione o contatta il tuo gestore del server per l\' applicazione dei parametri.';

		$faq[domanda][] = 'Cosa vuol dire "assegna automaticamente" riferito alle fasce d\'età?';		
		$faq[risposta][] = '
		La funzione assegna automaticamente consente di velocizzare la divisione in fasce d\'età lasciando che sia il programma a
		dividere gli iscritti sulla base della classe frequentata. Se è attiva la funzione (tic inserito) il programma non terrà conto
		della fascia specificata dall\'utente';
			
		$faq[domanda][] = 'Come posso modificare i dati di un iscritto?';		
		$faq[risposta][] = '
		Per modificare i dati di un singolo iscritto puoi individuarlo tramine la barra laterale di ricarca o sfogliando le
		liste complete. Una volta individuato utilizza il pulsante modifica <img src="immagini/modifica.png" border="0"/> per cambiare i suoi dati personali e le opzioni relative al
		grest.';
		
		$faq[domanda][] = 'Una volta che ho eliminato un iscritto, posso ripristinarlo?';		
		$faq[risposta][] = 'No, una volta eliminato un iscritto non è possibile ripristinarlo. Dovrai re-inserirlo nuovamente come un nuovo iscritto.<br/>E\' comunque raro che un iscritto venga eliminato accidentalmente in quando il software chiede conferma ben 2 volte prima di procedere con l\' eliminazione dal database di un iscritto.';
		
		$faq[domanda][] = 'Come faccio a stampare?';
		$faq[risposta][] = '
		Per stampare devi recarti nella sezione Stampe e selezionare gli elenchi che vuoi stampare. Hai a disposizione delle stampe
		predisposte, i cui parametri sono già preimpostati, oppure le stampe personalizzate, che ti consentono di stmpare solamente
		ciò di cui avrai bisogno. Una volta scelto cosa stampare ti verrà chiesto dal browser di salvare o aprire un file.<br/>
		Se desideri tenere la stampa anche in formato digitale salvalo sul computer, ma ricordati che quel file non si aggiorna automaticamente
		e se inserirai nuovi dati nel GrestOne questi non comparanno nel file che hai salvato. <br/>Per stampare il file aprilo con 
		un porgramma di visualizzazione di pdf (puoi usare <a href="http://get.adobe.com/it/reader/" target="_blank">Adobe Reader</a>) e clicca su Stampa.';
		
		$faq[domanda][] = 'E\' possibile modificare la G di GrestOne che viene fuori nelle stampe?';
		$faq[risposta][] = 'Certo, basta andare in Impostazioni -> Gestione Parrocchia e caricare un logo della parrocchia.';

		$faq[domanda][] = 'Perché i totali delle statistiche non sono corretti?';
		$faq[risposta][] = 'In effetti i totali delle statistiche possono sembrare sbagliati. Devi però considerare il fatto che 
		potrebbero esserci degli iscritti che non appartengono a nessuna delle categorie di cui si sta facendo il totale
		e quindi non compaiono nel totale della singola categoria, ma sono comunque conteggiati nel totale finale. <br/>Esempio: statistiche squadre divise per fascia d\'età: se ad un iscritto imposto la squadra ma non la fascia d\'età, sarà sì conteggiato nella squadra, ma nel dettaglio di visualizzazione squadre per fasce non sarà conteggiato proprio perchè non gli è stata assegnata nessuna fascia.';
		
		$faq[domanda][] = 'Gli elenchi predisposti non contengono le liste che mi interessano, posso cambiarli?';
		$faq[risposta][] = 'Purtroppo non è possibile cambiare gli elenchi predisposti. Puoi comunque utilizzare degli elenchi
		personalizzati e se ritieni utile avere un particolare elenco tra quelli personalizzati segnalacelo e vederemo di
		includerlo nelle prossime versioni';

		$faq[domanda][] = 'Cosa succede se non seleziono certi parametri degli elenchi o nelle stampe personalizzati?';
		$faq[risposta][] = 'Dipende dal parametro, comunque ci sono due possibilità: se il parametro è necessario l\'elenco
		o la stampa risulterà vuota; se il parametro non è necessario semplicemente l\'elenco o la stampa che andrai a generare
		ti mostrerà gli iscritti secondo una divisione che non tiene conto del parametro che hai ignorato (ad esempio la
		divisione in fasce d\'età o le squadre)';

		$faq[domanda][] = 'Quando devo stampare clicco sul bottone stampa ma non arriva nessun file. Cosa devo fare?';
		$faq[risposta][] = 'Se dopo aver cliccato su link per stampare non succede niente, prova a ricaricare la pagina (F5)
		e riporvare. Se invece si apre la finestra di salvataggio files del browser ma dopo aver premuto apri o salva
		non succede niente, verfica a che punto è lo scaricamento del file tramite il pannello download del 
		browser (in firefox è su strumenti, download). In Google Chrome ad esempio il download parte tacitamente sulla barra in basso del browser. Nelle versioni più nuove di Internet Explorer (che noi consigliamo per scaricare il browser Mozilla Firefox) la richiesta di salvataggio file è sempre sulla barra sotto.</br>Se il file fa fatica a scaricarsi significa che internet è abbastanza
		lento. Attendi pazientmente, è normale che ci metta una decina di secondi.';
		
		$faq[domanda][] = 'A cosa mi servono le modifiche rapide?';
		$faq[risposta][] = 'Alcuni dei parametri, come ad esempio le squadre o i gruppi, è probabile che andrai a determinerarli non
		durante le iscrizioni ma in un secondo momento. Le modifiche rapide servono per poterlo fare in modo più agevole,
		modificando più utenti nello stesso momento. Utilizza attentamente la scelta di quanti utenti modificare nello stesso
		momento: se per caso hai selezionato di vederne 50 e a metà dell\'operazione il pc ha dei problemi o il browser si
		chiude perderai tutte le modifiche!';

		$faq[domanda][] = 'Per cosa è fatta la categoria gruppi?';
		$faq[risposta][] = 'I gruppi sono pensati per racchiudere Animatori e Collaboratori in diverse categorie (es.
		gruppo balli, gruppo giochi, gruppo spazzini, mamme laboratori, mamme ristoro ecc..). A te la massima fantasia su come adoperare questa funzione del
		GrestOne!';
		
		$faq[domanda][] = 'Come funziona la gestione dei laboratori?';
		$faq[risposta][] = 'La gestione dei laboratorio richiede di sapere alcuni parametri iniziali:<br/>
		1) quali laboratori saranno disponibili (traforo, modollino, decoupage, ecc). Puoi indicare anche una descrizione
		e delle note.<br/>
		2) quanti sono i laboratori che un iscritto potra scegliere durante il grest (uno per settimana?, uno in tutto?,
			vedete voi...)<br/>
		3) di quanti incontri è composto ciascun laboratorio (uno al giorno? 3 alla settimana?). Questa indicazione serve
		per stampare le liste con i posti già pronti per fare gli appelli.';
		
		$faq[domanda][] = 'Cosa sono i periodi dei laboratori?';
		$faq[risposta][] = 'Nella gestione laboratori del GrestOne abbiamo inserito il parametro periodi per indicare il numero di
		laboratori differenti che ciascun iscritto può frequentare durante il grest. Per intenderci se il grest dura 3 settimane
		e ogni settimana gli iscritti frequenteranno un laboratorio differente ci saranno tre periodi. Se invece in un grest di
		4 settimane si dovessero scegliere solamente 3 laboratori allora i periodi sarebbero 3.';
		
		$faq[domanda][] = 'Mi vengono fuori un sacco di "Notice" all\' interno delle pagine... è normale?';
		$faq[risposta][] = 'No, non è normale, o meglio lo è se l\'interprete PHP del tuo server è impostato per visualizzare le avvertenze del codice. Ti consigliamo di rivedere il file di configurazione php.ini e disattivare la notifica errori. Se non hai accesso al file contatta il gestore del tuo server web. In caso di GrestOne Portable contatta gli sviluppatori o il forum di assistenza al sito <a href="http://www.grestone.it">grestone.it</a>.';

		$faq[domanda][] = 'Se inserisco un nome con un apostrofo compare prima un segno \. Come lo tolgo?';
		$faq[risposta][] = 'Non ti preoccupare non devi toglierlo. Comparirà solamente in rare occasioni, per il resto sarà nascosto. Si tratta di un provvedimento per la sicurezza.';
			
		$faq[domanda][] = 'Ho voluto fare delle modifiche ai file di GrestOne, ma ora ho problemi di visualizzazione o altro. Posso riparare?';
		$faq[risposta][] = 'Sì, se le tue modifiche non hanno comportato modifiche alle tabelle del database, puoi ripristinare GrestOne scaricando nuovamente le sorgenti dal sito <a href="http://www.grestone.it">grestone.it</a>. Una volta decompresso il pacchetto sostiutisci i file nel tuo server e tutto tornerà magicamente a funzionare correttamente.<br/> ATTENZIONE: sostituisci i file, ma NON creare una nuova installazione! Ricordati di impostare i parametri della connessione al database all\'interno del file connessione.php e di rinominare il file creazione.php.';
		
		$faq[domanda][] = 'A cosa mi serve sapere qual\'è il santo del giono?';
		$faq[risposta][] = 'Per poterlo pregare nel momento del bosogno e perchè protegga i dati contenuti nel GrestOne.<br/>
		Inoltre da un tocco di classe al programma :)';
		
		$faq[domanda][] = 'Come di pronuncia "GrestOne"?';
		$faq[risposta][] = 'GrestOne si pronuncia in italiano perchè è la fusione tra Grest e GestiOne (gestione grest). Perciò "One" non c\' entra niente con l\'inglese "one". :)';
		
		$faq[domanda][] = 'Ma... vi pagano?';
		$faq[risposta][] = 'Assolutamente NO!<br/>GrestOne è un progetto libero e gratuito (e così deve rimanere). Noi lo abbiamo sviluppato e lo manterremo aggiornato con nuove funzionalità. <br/>Lo lasciamo disponibile e te e a chi ne abbia bisogno per i propri grest. <br/>Tutto senza chiederti nulla. Wow! Spargete la voce! :)';
		
		$faq[domanda][] = 'Posso offrirvi da bere?';
		$faq[risposta][] = 'Se ti sei trovato bene con GrestOne puoi fare una donazione dal sito <a href="http://www.grestone.it">grestone.it</a>. I soldi saranno utilizzati per mantere vivo il progetto e i costi di gestione del sito internet. Te ne saremo veramente grati!';
		
		$numero_faq = count ($faq[domanda]);
		
		print '<table id="faq" width="100%">';
		for ($i=0;$i<$numero_faq;$i++)
		{
			print '<tr><td><img src="immagini/assistenza.png" border="0"/></td>';
			print '<td><a href="#faq_'.$i.'">'.$faq[domanda][$i].'</a></td></tr>';
		}
		print '</table><br/>';
		print '<br/><br/><br/>';
		for ($i=0;$i<$numero_faq;$i++)
		{
			print '<a name="faq_'.$i.'"><h4>'.$faq[domanda][$i].'</h4></a>';
			print '<table id="faq" width="100%">';
			print '<tr><td><img src="immagini/assistenza.png" border="0"/></td>';
			print '<td>'.$faq[risposta][$i].'</td></tr>';
			print '</table><br/>';
			print '<h3><img src="immagini/freccia_su.png" border="0"/> <a href="#inizio">Torna all\'inizio</a></h3><br/><br/>';
			
		}
			?>
	</div>
        
	<?php include ("pedice.php"); ?>     
        
</div>
</body> 

</html>
