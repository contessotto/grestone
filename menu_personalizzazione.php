<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
        <div id="menu">
<?php include ("dataora.php"); ?>
        
        <a href="home.php"><img src="immagini/menu/menu_principale.png" alt="MENU PRINCIPALE" name="menu" border="0" onmouseover="cambia(menu,'immagini/menu/menu_principale_on.png')" onmouseout="cambia(menu,'immagini/menu/menu_principale.png')"/></a><br/>
		<br/>
        
        <a href="gestione_periodo.php"><img src="immagini/menu/gestione_periodo.png" alt="Gestione Periodo" name="GestionePeriodo" border="0" onmouseover="cambia(GestionePeriodo,'immagini/menu/gestione_periodo_on.png')" onmouseout="cambia(GestionePeriodo,'immagini/menu/gestione_periodo.png')"/></a>  <br/>
        
        <a href="gestione_squadre.php"><img src="immagini/menu/gestione_squadre.png" alt="Gestione squadre" name="GestioneSquadre" border="0" onmouseover="cambia(GestioneSquadre,'immagini/menu/gestione_squadre_on.png')" onmouseout="cambia(GestioneSquadre,'immagini/menu/gestione_squadre.png')"/></a><br/>
        
        <a href="gestione_laboratori.php"><img src="immagini/menu/gestione_laboratori.png" alt="Gestione laboratori" name="GestioneLaboratori" border="0" onmouseover="cambia(GestioneLaboratori,'immagini/menu/gestione_laboratori_on.png')" onmouseout="cambia(GestioneLaboratori,'immagini/menu/gestione_laboratori.png')"/></a><br/>
        
        <a href="gestione_gite.php"><img src="immagini/menu/gestione_gite.png" alt="Gestione Gite" name="GestioneGite" border="0" onmouseover="cambia(GestioneGite,'immagini/menu/gestione_gite_on.png')" onmouseout="cambia(GestioneGite,'immagini/menu/gestione_gite.png')"/></a><br/> 

        <a href="gestione_gruppi.php"><img src="immagini/menu/gestione_gruppi.png" alt="Gestione Gruppi" name="GestioneGruppi" border="0" onmouseover="cambia(GestioneGruppi,'immagini/menu/gestione_gruppi_on.png')" onmouseout="cambia(GestioneGruppi,'immagini/menu/gestione_gruppi.png')"/></a><br/> 

        <a href="gestione_eta.php"><img src="immagini/menu/gestione_eta.png" alt="Gestione fasce d\'Età" name="GestioneEta" border="0" onmouseover="cambia(GestioneEta,'immagini/menu/gestione_eta_on.png')" onmouseout="cambia(GestioneEta,'immagini/menu/gestione_eta.png')"/></a><br/> 

        <br/><br/>
        
		<?php
		if ($dati_utente[ruolo_utente] == 'amministratore')
		{
			print '
        <a href="personalizzazione_parrocchia.php"><img src="immagini/menu/gestione_parrocchia.png" alt="Gestione Parrocchia" name="GestioneParrocchia" border="0" onmouseover="cambia(GestioneParrocchia,\'immagini/menu/gestione_parrocchia_on.png\')" onmouseout="cambia(GestioneParrocchia,\'immagini/menu/gestione_parrocchia.png\')"/></a><br/> 
		';}
		?>
        
		<?php
		if ($dati_utente[ruolo_utente] == 'amministratore')
		{
			print '
			<!--<form action="login_grest.php" method="post">
			<input type="hidden" name="id_utente" value="'.$dati_utente[id_utente].'">
			<input type="hidden" name="pannello" value="1">
			<input type="submit" value="Gestione Parrocchia">
			</form>-->';
		}
		?>
        </div>
