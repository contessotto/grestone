<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?> 
 <div id="menu">
 
<?php include ("dataora.php"); ?>
<?php include ("ricerca_rapida.php"); ?>
<?php 
$dati_utente = verifica_utente;
$dati_grest = verifica_grest();
?>

		<br/>
        <a href="home.php"><img src="immagini/menu/menu_principale.png" alt="MENU PRINCIPALE" name="menu" border="0" onmouseover="cambia(menu,'immagini/menu/menu_principale_on.png')" onmouseout="cambia(menu,'immagini/menu/menu_principale.png')"/></a><br/>
		<br/>
        <a href="statistiche_generali.php"><img src="immagini/menu/statistiche_generali.png" alt="Statistiche Generali" name="StatisticheGenerali" border="0" onmouseover="cambia(StatisticheGenerali,'immagini/menu/statistiche_generali_on.png')" onmouseout="cambia(StatisticheGenerali,'immagini/menu/statistiche_generali.png')"/></a><br/>
        
        
<?php        
     if ($dati_grest[squadre]==1)
	{
		print '<a href="statistiche_squadre.php"><img src="immagini/menu/statistiche_squadre.png" alt="Statistiche Squadre" name="StatisticheSquadre" border="0" onmouseover="cambia(StatisticheSquadre,\'immagini/menu/statistiche_squadre_on.png\')" onmouseout="cambia(StatisticheSquadre,\'immagini/menu/statistiche_squadre.png\')"/></a><br/>';		
	}
     if ($dati_grest[laboratori]==1)
	{
        print '<a href="statistiche_laboratori.php"><img src="immagini/menu/statistiche_laboratori.png" alt="Statistiche Laboratori" name="StatisticheLaboratori" border="0" onmouseover="cambia(StatisticheLaboratori,\'immagini/menu/statistiche_laboratori_on.png\')" onmouseout="cambia(StatisticheLaboratori,\'immagini/menu/statistiche_laboratori.png\')"/></a><br/>';
	}
     if ($dati_grest[gite]==1)
	{
         print '<a href="statistiche_eventi.php"><img src="immagini/menu/statistiche_eventi.png" alt="Statistiche Eventi" name="StatisticheEventi" border="0" onmouseover="cambia(StatisticheEventi,\'immagini/menu/statistiche_eventi_on.png\')" onmouseout="cambia(StatisticheEventi,\'immagini/menu/statistiche_eventi.png\')"/></a><br/>';
	}
     if ($dati_grest[gruppi]==1)
	{
         print '<a href="statistiche_gruppi.php"><img src="immagini/menu/statistiche_gruppi.png" alt="Statistiche Gruppi" name="StatisticheGruppi" border="0" onmouseover="cambia(StatisticheGruppi,\'immagini/menu/statistiche_gruppi_on.png\')" onmouseout="cambia(StatisticheGruppi,\'immagini/menu/statistiche_gruppi.png\')"/></a><br/>';
	}
?>        
    </div>
