<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>
	<script src="script.js" type="text/javascript"></script>
</head>

<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_report.php"); ?>

<div id="contenuto">
	<?php
	connetti();
	print '<h2>ELENCO RAPIDO LABORATORI</h2><br/>';
	
// INIZIO BLOCCO PER RIMANDO A STAMPE

print '<form action="stampe_laboratori.php" method="get" id="laboratori">';

if ($dati_grest[eta] == 1)
{
	$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest]");
	while ($dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC))
	{
		if ($_GET['eta_'.$dati_eta[id_eta]]== 1)
			{
				print '<input type="hidden" name="eta_'.$dati_eta[id_eta].'" value="1">';
			}
	}

}
if ($dati_grest[squadre] == 1)
{
	$squadre = mysql_query("SELECT * FROM squadre_$_SESSION[id_grest]");
	$conto = mysql_num_rows($squadre);
	$a = 0;
	while ($a <= $conto)
	{
		if ($_GET['squadra_'.$a])
			{
				$squadra = 'squadra_'.$a;
				if ($_GET[$squadra]==1)
					{print '<input type="hidden" name="'.$squadra.'" value="1">';}
			}
		$a++;
	}
}	

if ($dati_grest[laboratori] == 1)
{
	$laboratori = mysql_query("SELECT * FROM laboratori_$_SESSION[id_grest]");
	$conto = mysql_num_rows($laboratori);
	$a = 0;
	while ($a <= $conto)
	{
		if ($_GET['laboratorio_'.$a])
			{
				$laboratori = 'laboratorio_'.$a;
				if ($_GET[$laboratori]==1)
					{print '<input type="hidden" name="'.$laboratori.'" value="1">';}
			}
		$a++;
	}
}
if ($dati_grest[periodo] == 1)
{
	$a = 0;
	while ($a <= $numero_periodi_selezionati)
	{
		if ($_GET['periodo_'.$a])
			{
				$periodourl = 'periodo_'.$a;
				if ($_GET[$periodourl]==1)
					{print '<input type="hidden" name="'.$periodourl.'" value="1">';}
			}
		$a++;
	}
}

		print '<input type="hidden" name="iscritti" value="'.$_GET[iscritti].'">';
		print '<input type="hidden" name="animatori" value="'.$_GET[animatori].'">';
		print '<input type="hidden" name="collaboratori" value="'.$_GET[collaboratori].'">
		<input type="submit" value="visualizza stampe">
		</form><br/>';

// FINE BLOCCO PER RIMANDO A STAMPE

$iscanicol = array('iscritti','animatori','collaboratori');
$iscanicol[bello] = array('Animati','Animatori','Collaboratori');

	$a = 1;
	while ($a <= $dati_grest[laboratori_periodo])
	{
		if ($_GET['periodo_'.$a])
			{
				$periodi_selezionati[] = $a;
				$numero_periodi_selezionati++;
			}
		$a++;
	}

if ($dati_grest[eta] == 1)
{
	$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest]");
	while ($dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC))
	{
		if ($_GET['eta_'.$dati_eta[id_eta]]== 1)
			{
				$eta_selezionate[id][] = $dati_eta[id_eta];
				$eta_selezionate[nome][] = $dati_eta[nome];
				$numero_eta_selezionate++;
				
			}
	}
}
if ($dati_grest[laboratori] == 1)
{
	$laboratori = mysql_query("SELECT * FROM laboratori_$_SESSION[id_grest]");
	while ($dati_laboratori = mysql_fetch_array($laboratori, MYSQL_ASSOC))
	{
		if ($_GET['laboratorio_'.$dati_laboratori[id_laboratorio]]== 1)
			{
				$laboratori_selezionati[id_laboratorio][] = $dati_laboratori[id_laboratorio];
				$laboratori_selezionati[nome][] = $dati_laboratori[nome];
				$numero_laboratori_selezionati++;
			}
	}
}

$a = -1;
do
{
	$a++;
	if ($numero_periodi_selezionati != 0)
	{print '<h2>Periodo: '.$periodi_selezionati[$a].'</h2>';}

	$b = -1;
	do
	{
		$b++;
		if ($numero_eta_selezionate != 0)
			{print '<h2>Età: '.$eta_selezionate[nome][$b].'</h2><br/>';}	
	
		$c = -1;
		do
		{
			$c++;
			if ($numero_laboratori_selezionati != 0)
				{print '<br/><h2>Laboratorio: '.$laboratori_selezionati[nome][$c].'</h2>';}	

$i = 0;
while ($i < 3)
{
if ($_GET[$iscanicol[$i]])
{
	print '<br/><h3>'.$iscanicol[bello][$i].'</h3>';
	$query = "SELECT * FROM	$iscanicol[$i]_$_SESSION[id_grest] WHERE 1 ";
	
	if ($dati_grest[eta] == 1)
		{
			if ($numero_eta_selezionate != 0)
				{
					$eta_per_query = $eta_selezionate[id][$b];
					$query .= " AND eta = '$eta_per_query'";
				}
		}
	if ($dati_grest[laboratori] == 1)
		{
			if ($numero_periodi_selezionati != 0)
			{
				if ($numero_laboratori_selezionati != 0)
				{
					$laboratorio_per_query = $laboratori_selezionati[id_laboratorio][$c];
					$query .= " AND laboratorio_$periodi_selezionati[$a] = '$laboratorio_per_query'";
				}
				else
				{
					$query .= " AND laboratorio_$periodi_selezionati[$a] != ''";
				}
			}
			else
			{
				$query .= " AND (";
				for ($a=1;$a<=$dati_grest[laboratori_periodo];$a++)
				{
					if ($numero_laboratori_selezionati != 0)
					{
						$laboratorio_per_query = $laboratori_selezionati[id_laboratorio][$c];
						$query .= " laboratorio_$a = '$laboratorio_per_query'";
					}
					else
					{
						$query .= " laboratorio_$a != ''";
					}
					if ($a <> $dati_grest[laboratori_periodo])
					{$query .= "OR";}
				}
			$query .= ")";
				
			}
			
		}
	$query .= " ORDER BY  `cognome`,`nome` ASC ";	
	// print 'query:'.$query;
	$iscritti = mysql_query("$query");
	if (mysql_num_rows($iscritti) == null)
	{
		print '<h3><span style="color: red;">NESSUN ISCRITTO AL GREST</span></h3>';
	}
	else
	{
		print '
			<table id="lista" width="100%"><thead>
			<tr>
			<th scope="col">NOME</th>
			<th scope="col">COGNOME</th>';
			if ($iscanicol[$i]=='iscritti')
			{
				print '<th scope="col">NASCITA</th>
				<th scope="col">CLASSE</th>';
			//	print '<th scope="col">PRESENZE</th>';
			}
			
			print '<th scope="col">CELLULARE</th>
			<th scope="col">NOTE</th>';
			
		if ($iscanicol[$i] == 'iscritti')
		{$visualizza='iscritto';}
		if ($iscanicol[$i] == 'animatori')
		{$visualizza='animatore';}
		if ($iscanicol[$i] == 'collaboratori')
		{$visualizza='collaboratore';}	
			
		print '</tr></thead><tbody>';
		while ($dati_iscritti =  mysql_fetch_array($iscritti, MYSQL_ASSOC))
		{
			print'
			<tr>
			<td><a href="visualizza_'.$visualizza.'.php?id='.$dati_iscritti[id].'">'.$dati_iscritti[nome].'</a></td>
			<td><a href="visualizza_'.$visualizza.'.php?id='.$dati_iscritti[id].'">'.$dati_iscritti[cognome].'</a></td>';
			if ($iscanicol[$i]=='iscritti') 
			{
				print '<td>';
				if ($dati_iscritti[nascita] != null)
					{print $nascita = date("d-m-Y",$dati_iscritti[nascita]);}
				else
					{print 'DATA NON INSERITA';}
				print '</td>';
				print '<td>'.classe($dati_iscritti[classe]).'</td>';
				//print '<td>';
				//if ($numero_periodi_selezionati == 0)
				//{
				//	for ($conto=1;$conto<=$dati_grest[laboratori_durata];$conto++)
				//	{print $conto.'|_|';}
				//}
				//else
				//{
				//	print' |_| ';
				//}
				//print '</td>';
			}
			print"<td>$dati_iscritti[telefono]<br/>$dati_iscritti[cellulare]</td>";
			print '<td>'.$dati_iscritti[note].'</td>';
			print '</tr>';
		}
		print'	</tbody></table>';
	}	
}
$i++;
}
print '<br/>';

}while ($c < ($numero_laboratori_selezionati-1));

}while ($b < $numero_eta_selezionate-1);

}while ($a < ($numero_periodi_selezionati-1));

	?>
</div>
        <?php include ("pedice.php"); ?>
</div>
</body> 

</html>
