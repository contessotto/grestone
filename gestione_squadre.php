<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>
	<script src="script.js" type="text/javascript"></script>
</head>
	<style>
	tr.modifica{display:none;}
	</style>
    <script>
       	function modifica(id)
       	{document.getElementById('modifica'+id).style.display = 'table-row';}
       	function esci(id)
       	{document.getElementById('modifica'+id).style.display = 'none';}        
    </script><body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
<?php
verifica_amministratore($_SESSION[Grestone]);
?>
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_personalizzazione.php"); ?>

        <div id="contenuto">

<?php
if ($dati_grest[s_squadre] == 1)
{
	print '<h2>Gestione Squadre</h2>';
	print 'La gestione delle squadre è attualmente sospesa.<br/>
	<a href="riprendi.php?oggetto=squadre">Riprendi</a>';
}
else
{
if ($dati_grest[squadre] == 0)
{
	if (!isset($_POST[passaggi])) //primo passaggio:indica il numero di squadre
	{
	print '<h2>Gestione Squadre</h2>';
	print 'Numero di squadre:<br/>
	<form action="gestione_squadre.php" method="post" id="gestsq" name="gestsq">
	<input type="hidden" name="passaggi" value="primo">
	Indica di quali informazioni hai bisogno nella squadra:<br/>
	<table id="lista" align="center">
	<tr onClick="document.gestsq.colore.checked=(! document.gestsq.colore.checked);"><td><input onClick="document.gestsq.colore.checked=(! document.gestsq.colore.checked);" type="checkbox" name="colore" value="1"></td><td>Colore (rossi, gialli, verdi, blu, etc.)</td><tr>
	<tr onClick="document.gestsq.nome.checked=(! document.gestsq.nome.checked);"><td><input onClick="document.gestsq.nome.checked=(! document.gestsq.nome.checked);" type="checkbox" name="nome" value="1"></td><td>Nome</td><tr>
	<tr><td><select name="numero">';
	for ($a=1;$a<=20;$a++)
		{print '<option value="'.$a.'">'.$a.'</option>';}
	print '</select></td><td>Numero di Squadre</td><tr></table><br/>
	<input type="submit" value="inserisci">';
	}
	
	
	if ($_POST[passaggi] == 'primo') //secondo passaggio: indica i valori per ogni squadra
	{	
	print '<h3>Gestione Squadre</h3>';
	if ($_POST[colore]==null AND $_POST[nome]==null)
	{
		print '<script language="Javascript">
		alert("ATTENZIONE! Non hai selezionato nè il nome nè il colore della squadra. Verrà automaticamente selezionato il nome");
		</script>';
		$_POST[nome] = '1';
	}
	print '
	Inserisci le squadre:<br/>
	<form action="gestione_squadre.php" method="post">
	<input type="hidden" name="passaggi" value="secondo">
	<input type="hidden" name="colore" value="'.$_POST[colore].'">
	<input type="hidden" name="nome" value="'.$_POST[nome].'">
	<input type="hidden" name="numero" value="'.$_POST[numero].'">';	
	for ($a=1; $a<=$_POST[numero]; $a++)
	{
		print '
		<h2>Squadra '.$a.'</h2>';
		if ($_POST[colore] == 1)
			{print'Colore: 		<select name="colore_'.$a.'">
		<option value="">NESSUN COLORE</option>
		<option style="background:red;" value="Red">Rossi</option>
		<option style="background:yellow;" value="Yellow">Gialli</option>
		<option style="background:green;" value="Green">Verdi</option>
		<option style="background:blue;"value="Blue">Blu</option>
		<option style="background:maroon;"value="Maroon">Marroni</option>
		<option style="background:grey;"value="Gray">Grigi</option>
		<option style="background:white;"value="White">Bianchi</option>
		<option style="background:cyan;"value="Cyan">Azzurri</option>
		<option style="background:orange;"value="Orange">Arancioni</option>
		<option style="background:black;"value="Black">Neri</option>
		<option style="background:magenta;"value="Magenta">Magenta</option>
		</select><br/>';}
		if ($_POST[nome] == 1)
			{print'Nome: <input type="text" name="nome_'.$a.'"><br/>';}
	}	
	print '<br/><br/>
	<input type="submit" value="inserisci">
	</form>';
	}
	
	if ($_POST[passaggi] == 'secondo') //terzo passaggio: mostra i valori per la conferma
	{	
		print '<form action="gestione_squadre.php" method="post">';
		print'<input type="hidden" name="numero" value="'.$_POST[numero].'">';	
		print'<input type="hidden" name="colore" value="'.$_POST[colore].'">';	
		print'<input type="hidden" name="nome" value="'.$_POST[nome].'">';	
		print '<input type="hidden" name="passaggi"value="terzo">';
		for ($a=1; $a<=$_POST[numero]; $a++)
		{
			print '
			<h2>Squadra '.$a.'</h2>';
			$colore = 'colore_'.$a;
			$nome = 'nome_'.$a;
			$eta = 'eta_'.$a;
			if ($_POST[colore] == 1)
				{print'<b>Colore:</b> <img src="immagini/squadre/'.$_POST[$colore].'.png" alt="'.$_POST[$colore].'" border="0" title="'.$_POST[$colore].'"/>   ';}
			if ($_POST[nome] == 1)
				{print"<b>Nome:</b> $_POST[$nome]<br/>";}
		}
		for ($a=1; $a<=$_POST[numero]; $a++)
		{
			$colore = 'colore_'.$a;
			$nome = 'nome_'.$a;
			if ($_POST[colore] == 1)
				{print '<input type="hidden" name="'.$colore.'" value="'.$_POST[$colore].'">';}
			if ($_POST[nome] == 1)
				{print '<input type="hidden" name="'.$nome.'" value="'.$_POST[$nome].'">';}
		}
	print '<br/><br/><input type="submit" value="Inserisci Squadre">';
	print '</form>';
	}
	
	
	
	
		if ($_POST[passaggi] == 'terzo') //quarto passaggio: inserisce i valori nel database
	{
		connetti();
		mysql_query("UPDATE `grests` SET squadre = '1' WHERE `id_grest` = '$_SESSION[id_grest]';");
		mysql_query("CREATE TABLE  `squadre_$_SESSION[id_grest]` (`id_squadra` INT( 3 ) NOT NULL AUTO_INCREMENT ,
					`colore` VARCHAR( 20 ) NOT NULL , `nome` VARCHAR( 50 ) NOT NULL ,
					PRIMARY KEY (  `id_squadra` ))");
		mysql_query ("INSERT INTO  `squadre_$_SESSION[id_grest]` (`id_squadra` ,`colore` ,`nome`)
				VALUES ('1',  '$_POST[colore]',  '$_POST[nome]');");		
		mysql_query ("ALTER TABLE  `iscritti_$_SESSION[id_grest]` ADD  `squadra` INT NOT NULL");
		mysql_query ("ALTER TABLE  `animatori_$_SESSION[id_grest]` ADD  `squadra` INT NOT NULL");
		mysql_query ("ALTER TABLE  `collaboratori_$_SESSION[id_grest]` ADD  `squadra` INT NOT NULL");
				
		for ($a=1; $a<=$_POST[numero]; $a++)
		{
			$colore = 'colore_'.$a;
			$nome = 'nome_'.$a;
		
			mysql_query("INSERT INTO  `squadre_$_SESSION[id_grest]` (`id_squadra` ,`colore` ,`nome`)
						VALUES (NULL ,  '$_POST[$colore]',  '$_POST[$nome]');");
		}				
		print '<h2>Dati inseriti correttamente</h2><meta http-equiv="refresh" content="0;
			URL='.$_SERVER['HTTP_REFERER'].'">';
	}
}
else
{
	print'<h2>La gestione delle squadre è attiva</h2>
	<a href="#inserisci">Inserisci nuova Squadra</a><br/>
	<a href="disattiva.php?oggetto=squadre" onclick="return confermadisattiva ();">Disattiva definitivametne gestione Squadre</a><br/>
	<a href="sospendi.php?oggetto=squadre">Sospendi temporaenamente gestione Squadre</a><br/>
	';	connetti();
	$squadre = mysql_query("SELECT * FROM  `squadre_$_SESSION[id_grest]`");
	$dati_squadre = mysql_fetch_array($squadre, MYSQL_ASSOC);
	if ($dati_squadre['colore']!=1)
		{
			print '<a href="impostazioni_squadre.php?oggetto=colori&funzione=abilita">Abilita colori squadre</a><br/>';
		}
	else
		{
			if ($dati_squadre['nome']==1)
			{print '<a href="impostazioni_squadre.php?oggetto=colori&funzione=disabilita">Disabilita colori squadre</a><br/>';}
		}


	if ($dati_squadre['nome']!=1)
		{
			print '<a href="impostazioni_squadre.php?oggetto=nomi&funzione=abilita">Abilita nomi squadre</a><br/>';
		}
	else
		{
			if ($dati_squadre['colore']==1)
			{
				print '<a href="impostazioni_squadre.php?oggetto=nomi&funzione=disabilita">Disabilita nomi squadre</a><br/>';
			}
		}
	print '<table id="lista" align="center"><thead>
	<tr>';
	print'<th scope="col"></th>';

	if($dati_squadre['colore']==1)
	{
		$colore=1;
		print'<th scope="col">COLORE</th>';
	}
	if($dati_squadre['nome']==1)
	{
		$nome=1;
		print'<th scope="col">NOME</th>';
	}
	print'<th scope="col">MODIFICA</th>';
	print'<th scope="col">ELIMINA</th>';
	print'</tr>';
	while ($dati_squadre = mysql_fetch_array($squadre, MYSQL_ASSOC))
	{
		print '<td></td>';
		if($colore==1)
		{
			print'<td><img src="immagini/squadre/'.$dati_squadre[colore].'.png" alt="'.$dati_squadre[colore].'" border="0" title="'.$dati_squadre[colore].'"/></td>';
		}
		if($nome==1)
		{
			print"<td>$dati_squadre[nome]</td>";
		}
		print '<td><a onclick="modifica('.$dati_squadre[id_squadra].');"class="elimina"href="#"><img src="immagini/modifica.png" alt="modifica" border="0" title="Modifica"/></a></td>';	
		print'<td><a href="elimina.php?oggetto=squadra&id='.$dati_squadre[id_squadra].'" onclick="return conferma ();"><img src="immagini/ico_no.png" alt="elimina" border="0" title="Elimina"/></a></td>';

		print'</tr>';
		
		print'<form action="modifica.php?oggetto=squadra" method="post">
					
		<tr class="modifica" id="modifica'.$dati_squadre[id_squadra].'">
		<td>
		<a class="elimina" href="#" onclick="esci('.$dati_squadre[id_squadra].');">abbandona modifiche</a>
		</td>';
		if ($colore == 1)
			{
				print'<td>
				<select name="colore">
				<option value=""';
				if ($dati_squadre[colore] =="")
				{print 'selected';}
				print '>NESSUN COLORE</option><option style="background:red;" value="Red"';
				if ($dati_squadre[colore] =="Red")
				{print 'selected';}
				print '>Rossi</option><option style="background:yellow;" value="Yellow"';
				if ($dati_squadre[colore] =="Yellow")
				{print 'selected';}
				print '>Gialli</option><option style="background:green;" value="Green"';
				if ($dati_squadre[colore] =="Green")
				{print 'selected';}
				print '>Verdi</option><option style="background:blue;"value="Blue"';
				if ($dati_squadre[colore] =="Blue")
				{print 'selected';}
				print '>Blu</option><option style="background:maroon;"value="Maroon"';
				if ($dati_squadre[colore] =="Maroon")
				{print 'selected';}
				print '>Marroni</option><option style="background:grey;"value="Gray"';
				if ($dati_squadre[colore] =="Gray")
				{print 'selected';}
				print '>Grigi</option><option style="background:white;"value="White"';
				if ($dati_squadre[colore] =="White")
				{print 'selected';}
				print '>Bianchi</option><option style="background:cyan;"value="Cyan"';
				if ($dati_squadre[colore] =="Cyan")
				{print 'selected';}
				print '>Azzurri</option><option style="background:orange;"value="Orange"';
				if ($dati_squadre[colore] =="Orange")
				{print 'selected';}
				print '>Arancioni</option><option style="background:black;"value="Black"';
				if ($dati_squadre[colore] =="Black")
				{print 'selected';}
				print '>Neri</option><option style="background:magenta;"value="Magenta"';
				if ($dati_squadre[colore] =="Magenta")
				{print 'selected';}
				print '>Magenta</option>
				</select>';
				}
				
		if ($nome == 1)
			{print'<td><input type="text" size="8" name="nome" value="'.$dati_squadre[nome].'"></td>';}
		
		print'
		<td><input type="hidden" name="id_squadra" value="'.$dati_squadre[id_squadra].'">
		<input type="submit" value="modifica"></td>
		</tr>
		</form>';
	}
	print '</table>';

	if (isset($_POST[passaggi]))
	{
		if ($_POST[passaggi] == 'modifica_primo')
		{
			mysql_query("INSERT INTO `squadre_$_SESSION[id_grest]` 
			(`id_squadra`, `colore`, `nome`)
			 VALUES (NULL, '$_POST[colore]', '$_POST[nome]');");
			print '<h2>Dati inseriti correttamente</h2>';
			print '<meta http-equiv="refresh" content="1;
		URL=gestione_squadre.php">';
		}		
	}
	else
	{
		print '<a name="inserisci"></a>
		<br/><h2>Inserisci nuova Squadra</h2>
		<form action="gestione_squadre.php" method="post">';
		if($colore==1)
		{print 'Colore:
		<select name="colore">
		<option value="">NESSUN COLORE</option>
		<option style="background:red;" value="Red">Rossi</option>
		<option style="background:yellow;" value="Yellow">Gialli</option>
		<option style="background:green;" value="Green">Verdi</option>
		<option style="background:blue;"value="Blue">Blu</option>
		<option style="background:maroon;"value="Maroon">Marroni</option>
		<option style="background:grey;"value="Gray">Grigi</option>
		<option style="background:white;"value="White">Bianchi</option>
		<option style="background:cyan;"value="Cyan">Azzurri</option>
		<option style="background:orange;"value="Orange">Arancioni</option>
		<option style="background:black;"value="Black">Neri</option>
		<option style="background:magenta;"value="Magenta">Magenta</option>
		</select>
		<br/>';}
		if($nome==1)	
		{print 'Nome:<input type="text" name="nome"><br/>';}
		print'
		<input type="hidden" name="passaggi" value="modifica_primo"><br/>
		<input type="submit" value="inserisci"><br/>
		</form>';
	}
}
}
?>			
        </div>
        
        <?php include ("pedice.php"); ?>
        
    </div>
</body> 

</html>
