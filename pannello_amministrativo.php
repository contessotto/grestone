<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
    <?php
    if ($dati_utente[ruolo_utente] == 'amministratore')
    {
	print '<div id="pannelloadmin">';
	print '<h2>Pannello amministrativo</h2>';
	print '<table width="100%" border="0" cellspacing="-10" ><tr>';
	print '<td width="50%" align="right">Gestione periodo:</td><td width="50%" align="left">';
	if ($dati_grest[periodo] == 1)
		{print '<img src="immagini/ico_ok.png" /> <strong>attivata</strong><br/>';}
	else
	{
		if ($dati_grest[s_periodo] == 1)
		{
			print '<img src="immagini/ico_sospeso.png" /> <strong>sospesa</strong> <a href="gestione_periodo.php">riprendi</a><br/>';
		}
		else
		{
			print '<img src="immagini/ico_no.png" /> <strong>non attivata</strong> <a href="gestione_periodo.php">attiva</a><br/>';
		}
	}
	print '</td></tr><tr><td align="right">Gestione squadre:</td><td align="left">';
	
	if ($dati_grest[squadre] == 1)
		{print '<img src="immagini/ico_ok.png" /> <strong>attivata</strong><br/>';}
	else
	{
		if ($dati_grest[s_squadre] == 1)
		{	
			print '<img src="immagini/ico_sospeso.png" /> <strong>sospesa</strong> <a href="gestione_squadre.php">riprendi</a><br/>';
		}
		else
		{
			print '<img src="immagini/ico_no.png" /> <strong>non attivata</strong> <a href="gestione_squadre.php">attiva</a><br/>';
		}
	}
	print '</td></tr><tr><td align="right">Gestione gite:</td><td align="left">';
	
	if ($dati_grest[gite] == 1)
		{print '<img src="immagini/ico_ok.png" /> <strong>attivata</strong><br/>';}
	else
	{
		if ($dati_grest[s_gite] == 1)
		{
		print '<img src="immagini/ico_sospeso.png" /> <strong>sospesa</strong> <a href="gestione_gite.php">riprendi</a><br/>';
		}
		else
		{
		print '<img src="immagini/ico_no.png" /> <strong>non attivata</strong> <a href="gestione_gite.php">attiva</a><br/>';	
		}
	}
	print '</td></tr><tr><td align="right">Gestione laboratori:</td><td align="left">';
	
	if ($dati_grest[laboratori] == 1)
		{print '<img src="immagini/ico_ok.png" /> <strong>attivata</strong><br/>';}
	else
	{
		if ($dati_grest[s_laboratori] == 1)
		{
		print '<img src="immagini/ico_sospeso.png" /> <strong>sospesa</strong> <a href="gestione_laboratori.php">riprendi</a><br/>';
		}
		else
		{
		print '<img src="immagini/ico_no.png" /> <strong>non attivata</strong> <a href="gestione_laboratori.php">attiva</a><br/>';
		}
	}
	print '</td></tr><tr><td align="right">Gestione gruppi:</td><td align="left">';
	
	if ($dati_grest[gruppi] == 1)
		{print '<img src="immagini/ico_ok.png" /> <strong>attivata</strong><br/>';}
	else
	{
		if ($dati_grest[s_gruppi] == 1)
		{
			print '<img src="immagini/ico_sospeso.png" /> <strong>sospesa</strong> <a href="gestione_gruppi.php">riprendi</a><br/>';
		}
		else
		{
			print '<img src="immagini/ico_no.png" /> <strong>non attivata</strong> <a href="gestione_gruppi.php">attiva</a><br/>';
		}
	}
	print '</td></tr><tr><td align="right">Gestione fasce d\'età:</td><td align="left">';
	if ($dati_grest[eta] == 1)
		{print '<img src="immagini/ico_ok.png" /> <strong>attivata</strong><br/>';}
	else
	{
		if ($dati_grest[s_eta] == 1)
		{
		print '<img src="immagini/ico_sospeso.png" /> <strong>sospesa</strong> <a href="gestione_eta.php">riprendi</a><br/>';
		}
		else
		{
		print '<img src="immagini/ico_no.png" /> <strong>non attivata</strong> <a href="gestione_eta.php">attiva</a><br/>';
		}
	}
	
	print'</td></tr></table></div>';
	}
	?>
