<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?> 
 <div id="menu">
 
 
<?php include ("dataora.php"); ?>
<?php include ("ricerca_rapida.php"); ?>
<?php
$dati_utente = verifica_utente;
$dati_grest = verifica_grest();
?>

        <br/>
        <a href="home.php"><img src="immagini/menu/menu_principale.png" alt="MENU PRINCIPALE" name="menu" border="0" onmouseover="cambia(menu,'immagini/menu/menu_principale_on.png')" onmouseout="cambia(menu,'immagini/menu/menu_principale.png')"/></a><br/>
		<br/>

<!--<h3>STAMPE</h3>-->
		<a href="stampe_predisposte.php"><img src="immagini/menu/stampe_predisposte.png" alt="stampe predisposte" name="stampepredisposte" border="0" onmouseover="cambia(stampepredisposte,'immagini/menu/stampe_predisposte_on.png')" onmouseout="cambia(stampepredisposte,'immagini/menu/stampe_predisposte.png')"/></a>
<br/><br/>

<h3>STAMPE<br/>PERSONALIZZABILI</h3>
		<a href="stampe.php?oggetto="><img src="immagini/menu/stampe_generali.png" alt="stampe Generali" name="stampeGenerali" border="0" onmouseover="cambia(stampeGenerali,'immagini/menu/stampe_generali_on.png')" onmouseout="cambia(stampeGenerali,'immagini/menu/stampe_generali.png')"/></a>
        
<?php        
     if ($dati_grest[squadre]==1)
	{
        print '<a href="stampe.php?oggetto=squadre"><img src="immagini/menu/stampe_squadre.png" alt="stampe Squadre" name="stampeSquadre" border="0" onmouseover="cambia(stampeSquadre,\'immagini/menu/stampe_squadre_on.png\')" onmouseout="cambia(stampeSquadre,\'immagini/menu/stampe_squadre.png\')"/></a>';		
	}      
     if ($dati_grest[laboratori]==1)
	{
		print '<a href="stampe.php?oggetto=laboratori"><img src="immagini/menu/stampe_laboratori.png" alt="stampe Laboratori" name="stampeLaboratori" border="0" onmouseover="cambia(stampeLaboratori,\'immagini/menu/stampe_laboratori_on.png\')" onmouseout="cambia(stampeLaboratori,\'immagini/menu/stampe_laboratori.png\')"/></a>';		
	}
     if ($dati_grest[gite]==1)
	{
		print '<a href="stampe.php?oggetto=eventi"><img src="immagini/menu/stampe_eventi.png" alt="stampe Eventu" name="stampeEventi" border="0" onmouseover="cambia(stampeEventi,\'immagini/menu/stampe_eventi_on.png\')" onmouseout="cambia(stampeEventi,\'immagini/menu/stampe_eventi.png\')"/></a>';		
	}
     if ($dati_grest[gruppi]==1)
	{
		print '<a href="stampe.php?oggetto=gruppi"><img src="immagini/menu/stampe_gruppi.png" alt="stampe Gruppi" name="stampeGruppi" border="0" onmouseover="cambia(stampeGruppi,\'immagini/menu/stampe_gruppi_on.png\')" onmouseout="cambia(stampeGruppi,\'immagini/menu/stampe_gruppi.png\')"/></a>';		
	}
?>
    
    </div>
