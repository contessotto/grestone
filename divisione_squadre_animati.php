<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>
	<script src="script.js" type="text/javascript"></script>
</head>

<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_modifiche.php"); ?>

<div id="contenutoaltezzafissa">
<?php
$a = 'iscritti_'.$_SESSION[id_grest];
$b = 'squadre_'.$_SESSION[id_grest];

	if ($dati_grest[squadre]==1)
	{
		if ($_POST[procedura]== null)
		{
			print '<br/><h2>Assegnazione automatica squadre Animati</h2><br/>
			La procedura consente di dividere automaticamente gli animati in squadre in base ad alcuni parametri fondamentali:<br/>
			età, sesso, provenienza. La divisione consente di ottenere squadre abbastanza omoegenee pur mantenendo la possibilità
			di modificare manualmente i risultati.<br/><br/>
			<form action="divisione_squadre_animati.php" method="post">
			Sovrascrivi squadre già assegnate: <input type="checkbox" name="sovrascrivi" value="1"/><br/><br/>
			<input type="submit" name="procedura" value="inizia"/>
			</form>';	
		}
		if ($_POST[procedura]== 'inizia')
		{
			$squadre = mysql_query("SELECT id_squadra FROM $b");
			
			$numero_squadre = (mysql_num_rows($squadre)-1);

			$blablabla = mysql_fetch_assoc($squadre); //correzione per eliminare la prima riga della tabella
			for ($i=0;$i<$numero_squadre;$i++)
			{
				$temp = mysql_fetch_assoc($squadre);
				$id_squadre[$i] = $temp[id_squadra];
			}
			print '<br/>';
			print '<br/>';
			
			if ($_POST[sovrascrivi] == 0)
			{$sovrascrivi = " WHERE squadra = 0 ";}
			$query = mysql_query("SELECT id FROM $a $sovrascrivi ORDER BY sesso, classe, paese DESC");
			$i = 0;
			while ($elenco = mysql_fetch_assoc($query))
			{
				$conto++;
				if ($i>($numero_squadre-1))
				{$i=0;}
				//print "$id_squadre[$i] - $elenco[id] <br/>";
				$modifica = mysql_query("UPDATE $a SET squadra = $id_squadre[$i] WHERE id = $elenco[id]");
				$i++;

			} print '<meta http-equiv="refresh" content="5; URL=modifiche_home.php">';
			if ($conto != 0)
			{
				print '<h2><img src="immagini/ico_ok.png"/> SQUADRE ASSEGNATE CORRETTAMENTE! <img src="immagini/ico_ok.png"/></h2><br/>';
				print "Sono stati assegnati alle squadre <b>$conto</b> animati";
			}
			else
			{
				print '<h2><img src="immagini/ico_sospeso.png"/> Tutti gli animatori hanno già una squadra! <img src="immagini/ico_sospeso.png"/></h2><br/>';
			}
		}
	}
?>
</div>
        <?php include ("pedice.php"); ?>
</div>
</body> 

</html>
