<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<script type="text/javascript">
// JavaScript per GrestOne

/* Funzione cambiamento immagine */
function cambia(ImageName,ImageFile){
    ImageName.src = ImageFile;}
    

	
/* Funzione preload immagini */
// Array
var imgadm = new Array(1); 
    imgadm[0] = "immagini/menu/gestione_parrocchie_on.png"; 
    imgadm[1] = "immagini/menu/gestione_grest_on.png";
    imgadm[2] = "immagini/menu/gestione_utenti_on.png";
    imgadm[3] = "immagini/menu/impostazioni_generali_on.png";
    imgadm[4] = "immagini/menu/visualizza_registro_on.png";
    imgadm[5] = "immagini/menu/torna_on.png"; 
	
	
// preload
function preload() { 
   var tmp = null; 
   for (var j = 0; j < imgadm.length; j++) { 
     tmp = imgadm[j]; 
     imgadm[j] = new Image(); 
     imgadm[j].src = tmp; 
   } 
  } 
  void(preload());
</script>

        <div id="menu">
        <?php include ("dataora.php"); ?>
		<br/>
		<?php
			$conto_parrocchie = mysql_query("SELECT * FROM parrocchie"); //conta tutte le parrocchie
			$righe_parrocchie = mysql_num_rows($conto_parrocchie);
			$conto_grests = mysql_query("SELECT * FROM grests"); //conta tutti i grest
			$righe_grests = mysql_num_rows($conto_grests);
			print '
		    <a href="configurazione_parrocchie.php"><img src="immagini/menu/gestione_parrocchie.png" alt="Gestione Parrocchie" name="GestioneParrocchie" border="0" onmouseover="cambia(GestioneParrocchie,\'immagini/menu/gestione_parrocchie_on.png\')" onmouseout="cambia(GestioneParrocchie,\'immagini/menu/gestione_parrocchie.png\')"/></a>
<br/><br/>';

			if($righe_parrocchie !=  0)
			{
				print'
		    <a href="configurazione_grest.php"><img src="immagini/menu/gestione_grest.png" alt="Gestione grest" name="Gestionegrest" border="0" onmouseover="cambia(Gestionegrest,\'immagini/menu/gestione_grest_on.png\')" onmouseout="cambia(Gestionegrest,\'immagini/menu/gestione_grest.png\')"/></a>
<br/><br/>';
				print '
		    <a href="configurazione_utenti.php"><img src="immagini/menu/gestione_utenti.png" alt="Gestione utenti" name="Gestioneutenti" border="0" onmouseover="cambia(Gestioneutenti,\'immagini/menu/gestione_utenti_on.png\')" onmouseout="cambia(Gestioneutenti,\'immagini/menu/gestione_utenti.png\')"/></a>
<br/><br/>';
			}
			print '
		    <a href="reset.php"><img src="immagini/menu/impostazioni_generali.png" alt="Impostazioni Generali" name="ImpostazioniGenerali" border="0" onmouseover="cambia(ImpostazioniGenerali,\'immagini/menu/impostazioni_generali_on.png\')" onmouseout="cambia(ImpostazioniGenerali,\'immagini/menu/impostazioni_generali.png\')"/></a>
<br/><br/>
		    <a href="visualizza_registro.php"><img src="immagini/menu/visualizza_registro.png" alt="Visualizza Registro" name="VisualizzaRegistro" border="0" onmouseover="cambia(VisualizzaRegistro,\'immagini/menu/visualizza_registro_on.png\')" onmouseout="cambia(VisualizzaRegistro,\'immagini/menu/visualizza_registro.png\')"/></a>
<br/><br/>
		    <a href="logout.php?nome=admin"><img src="immagini/menu/torna.png" alt="Torna a GrestOne" name="Torna" border="0" onmouseover="cambia(Torna,\'immagini/menu/torna_on.png\')" onmouseout="cambia(Torna,\'immagini/menu/torna.png\')"/></a>
<br/><br/>
			</div>';
