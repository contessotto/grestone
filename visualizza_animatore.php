<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
<script src="script.js" type="text/javascript"></script>
<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
<?php
verifica_normale($_SESSION[Grestone]);
?>
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_report.php"); ?>

<div id="contenuto">
	<?php
if ($_GET[id]!=null)
{
	$iscritti = mysql_query("SELECT * FROM  animatori_$_SESSION[id_grest] WHERE id = '$_GET[id]'");
	$dati_iscritti =  mysql_fetch_array($iscritti, MYSQL_ASSOC);
	print '<h2>Dettagli Animatore</h2>
		<a href="modifica_animatore.php?id='.$_GET[id].'">modifica</a>
		<br/>
	<table width="100%" border="0" id="lista">
	  <tr>
	    <td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NOME:
          '.$dati_iscritti[nome].'</td>
	    <td>&nbsp;</td>
      </tr>
	  <tr>
	    <td>COGNOME:
        '.$dati_iscritti[cognome].'</td>
	    <td>&nbsp;</td>
      </tr>
	  <tr>
	    <td>
	      <p>DATA NASCITA:
          '.date("d-m-Y", $dati_iscritti[nascita]).'
        </p></td>
	    <td>
	      <p>CLASSE FREQUENTATA: ';
		print classe($dati_iscritti[classe]);
		print '
        </p></td>
      </tr>
	  <tr>
	    <td>SESSO: ';
	     if ($dati_iscritti[sesso] == 'm')
		{print '<img src="immagini/m.png" title="Maschio"/>';}
		if ($dati_iscritti[sesso] == 'f')
		{print '<img src="immagini/f.png" title="Femmina"/>';}
        print'</td>
	    <td>&nbsp;</td>
      </tr>
	  <tr>
	    <td>
         INDIRIZZO:
         '.$dati_iscritti[indirizzo].'</td>
	    <td>PAESE:
         '.$dati_iscritti[paese].'</td>
      </tr>
	  <tr>
	    <td>TELEFONO:
        '.$dati_iscritti[telefono].'</td>
	    <td>CELL:
        '.$dati_iscritti[cellulare].'</td>
      </tr>
	  <tr>
	    <td>NOTE:
        '.$dati_iscritti[note].'</td>
	    <td>&nbsp;</td>
      </tr>
</table> <br/>
		';
		
		if ($dati_grest[periodo]==1)
		{
			$periodo = mysql_query("SELECT * FROM periodo WHERE id_grest = $_SESSION[id_grest]");
			$dati_periodo = mysql_fetch_array($periodo, MYSQL_ASSOC);
			print '<br/><br/><h3>Informazioni Periodo</h3>';
			for ($a=1; $a<=$dati_periodo[numero_settimane]; $a++)
			{	
				$settimana = 'settimana_'.$a;
				print 'Settimana '.$a.': ';
				if ($dati_iscritti[$settimana] == 1)
				{
					print 'ISCRITTO <img src="immagini/ico_ok.png"/><br/>';
				}
				else
				{
					print 'NON ISCRITTO <img src="immagini/ico_no.png"/><br/>';
				}
			}
		}
		if ($dati_grest[squadre]==1)
		{
			$squadra = mysql_query("SELECT * FROM squadre_$_SESSION[id_grest]");
			$impostazioni_squadre = mysql_fetch_array($squadra, MYSQL_ASSOC);
			$squadra = mysql_query("SELECT * FROM squadre_$_SESSION[id_grest] WHERE id_squadra='$dati_iscritti[squadra]'");
			$dati_squadra = mysql_fetch_array($squadra, MYSQL_ASSOC);
			print '<br/><h3>Informazioni Squadra</h3>';
			if ($dati_iscritti[squadra] == null)
				{print 'NESSUNA SQUADRA SELEZIONATA <img src="immagini/ico_no.png"/>';}
			else
			{
				print 'squadra: ';
				if ($impostazioni_squadre[nome]==1)
					{print "$dati_squadra[nome] "; }
				if ($impostazioni_squadre[colore] == 1)
				{
					print '<img src="immagini/squadre/'.$dati_squadra[colore].'.png"
					alt="'.$dati_squadra[colore].'" border="0" title="'.$dati_squadra[colore].'"/>';
				}	
			}
		}
		if ($dati_grest[eta]==1)
		{
			print '<br/><br/><h3>Informazioni Fascia d\'età</h3>';
			print 'Fascia d\'Età: ';
			if ($dati_iscritti[eta] == null)
				{print 'NESSUNA FASCIA SCELTA <img src="immagini/ico_no.png"/><br/>';}
			else
			{
				$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest] 
				WHERE id_eta = $dati_iscritti[eta]");
				$dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC);
				print $dati_eta[nome].'<br/>';
			}
		}
		if ($dati_grest[laboratori]==1)
		{
			print '<br/><h3>Informazioni Laboratori</h3>';
			for ($a=1; $a<=$dati_grest[laboratori_periodo]; $a++)
			{	
				$laboratorio = 'laboratorio_'.$a;
				print 'Laboratorio '.$a.': ';
				if ($dati_iscritti[$laboratorio] == null)
				{print 'NESSUN LABORATORIO SCELTO <img src="immagini/ico_no.png"/><br/>';}
				else
				{$laboratori = mysql_query("SELECT * FROM laboratori_$_SESSION[id_grest] 
				WHERE id_laboratorio = $dati_iscritti[$laboratorio]");
				$dati_laboratorio = mysql_fetch_array($laboratori, MYSQL_ASSOC);
				print $dati_laboratorio[nome].'<br/>';
				}
			}	
		}
		if ($dati_grest[gruppi]==1)
		{
			print '<br/><h3>Informazioni Gruppi</h3>';
			print 'Gruppo: ';
			if ($dati_iscritti[gruppo] == null)
				{print 'NESSUN GRUPPO SCELTO <img src="immagini/ico_no.png"/><br/>';}
			else
			{
				$gruppi = mysql_query("SELECT * FROM gruppi_$_SESSION[id_grest] 
				WHERE id_gruppo = $dati_iscritti[gruppo]");
				$dati_gruppo = mysql_fetch_array($gruppi, MYSQL_ASSOC);
				print $dati_gruppo[nome].'<br/>';
			}
		}		
		if ($dati_grest[gite]==1)
		{
			print '<br/><h3>Informazioni Eventi</h3>';
			$gite = mysql_query("SELECT * FROM gite_$_SESSION[id_grest] WHERE liste = '1' ");
			while($dati_gite = mysql_fetch_array($gite, MYSQL_ASSOC))
			{
				$presenza_gita = 'presenza_evento_'.$dati_gite[id_gita];
				$pagamento_gita = 'pagamento_evento_'.$dati_gite[id_gita];
				$note_gita = 'note_evento_'.$dati_gite[id_gita];
				print "<strong> $dati_gite[nome]</strong><br/>";
				if ($dati_iscritti[$presenza_gita])
				{
					print 'ISCRITTO <img src="immagini/ico_ok.png"/><br/>';
					if ($dati_iscritti[$pagamento_gita])
						{print 'PAGATO <img src="immagini/pagato.png"/><br/>';}
					else
						{print 'ANCORA DA PAGARE <img src="immagini/ico_no.png"/><br/>';}
					if ($dati_iscritti[$note_gita] != null)
						{print 'Note:'.$dati_iscritti[$note_gita];}
				}
				else
					{print 'NON ISCRITTO <img src="immagini/ico_no.png"/><br/>';}
				print '<br/>';
			}
		}
	}
else
{print'NESSUN ANIMATORE SELEZIONATO';}
	?>
</div>
        <?php include ("pedice.php"); ?>
</div>
</body> 

</html>
