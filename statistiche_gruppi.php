<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>
	<script src="script.js" type="text/javascript"></script>
</head>
<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_statistiche.php"); ?>

<div id="contenuto">
<?php
connetti();

/*SEZIONE PRIMA: SI OCCUPA DI LEGGERE IL DB E CARICARE SU ARRAY TUTTI I DATI NECESSARI ALLA VISUALIZZAZIONE DELLA PAGINA*/
if ($dati_grest[eta] == 1)
{
	$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest]");
	while ($dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC))
	{

		$eta_selezionate[id][] = $dati_eta[id_eta];
		$eta_selezionate[nome][] = $dati_eta[nome];
		$numero_eta_selezionate++;
	}
}

if ($dati_grest[gruppi] == 1)
{
	$gruppi = mysql_query("SELECT * FROM gruppi_$_SESSION[id_grest]");
	while ($dati_gruppi = mysql_fetch_array($gruppi, MYSQL_ASSOC))
	{
		$gruppi_selezionati[id][] = $dati_gruppi[id_gruppo];
		$gruppi_selezionati[nome][] = $dati_gruppi[nome];
		$numero_gruppi_selezionati++;
	}
}




/*SEZIONE SECONDA: SI OCCUPA DELLA VISUALIZZAZIONE*/

$iscanicol = array('iscritti','animatori','collaboratori');
$iscanicol[bello] = array('Animati','Animatori','Collaboratori');

print '<h2>Statistiche Gruppi</h2><br/>';
	print '<table id="lista" width="100%">';
	print '</tr><th></th>';
	$c = 0;
	do
	{
		print '<th>';
		print $gruppi_selezionati[nome][$c];
		print '</th>';
		$c++;
	}
	while ($c < $numero_gruppi_selezionati);
	print '</tr>';
$c_iscanicol=1;
while ($c_iscanicol <3)
{
print '<tr><td><strong>'.$iscanicol[bello][$c_iscanicol].'</strong></td>';
	$c = 0;
	do
	{	
		print '<td><h2>';
		$query = "SELECT * FROM $iscanicol[$c_iscanicol]_$_SESSION[id_grest] WHERE 1 ";
		$query .= ' AND gruppo = '.$gruppi_selezionati[id][$c];
		$numero = mysql_query("$query");
		print $numero_iscritti = mysql_num_rows($numero);
		$numero_totale_iscritti[$c] += $numero_iscritti;		
		print '</h2></td>';
		$c++;
	}
	while ($c < $numero_gruppi_selezionati);
	print '</tr>';
	$c_iscanicol++;
}
print '<tr><td><strong><span style="color: red;">TOTALE</span></strong></td>';
	$c = 0;
	do
	{
		print '<td><h2><span style="color: red;">'.$numero_totale_iscritti[$c].'</h2></td>';
		$c++;
	} while($c<$numero_gruppi_selezionati);



	print '</table>';





$a = 0;
do
{
print '<br/>';
print '<h3><a id="img_'.$gruppi_selezionati[nome][$a].'" href="javascript:mostra(\'mostra_'.$gruppi_selezionati[nome][$a].'\',\'img_'.$gruppi_selezionati[nome][$a].'\');">
<img src="immagini/piu.png" title="Espandi" border="0" /></a> Visualizza il dettaglio per Gruppo '.$gruppi_selezionati[nome][$a].'</h3>';
print '<div id="mostra_'.$gruppi_selezionati[nome][$a].'" style="display: none;">';
	print '<br/>';
	print '<table id="lista" align="center">';
	print '<tr><th></th>';
	$c_iscanicol=1;
    while ($c_iscanicol <3)
	{
		print '<th>'.$iscanicol[bello][$c_iscanicol].'</th>';
		$c_iscanicol++;
	}
	print '</tr>';
	$b = 0;
	do
	{
		if ($numero_eta_selezionate <> 0)
			{print '<td><b>'.$eta_selezionate[nome][$b].'</b></td>';}
		else
			{print '<td></td>';}
		$c_iscanicol=1;
		while ($c_iscanicol <3)
		{
			$query = "SELECT * FROM $iscanicol[$c_iscanicol]_$_SESSION[id_grest] WHERE 1 ";
			$query .= ' AND gruppo = '.$gruppi_selezionati[id][$a];
			if ($dati_grest[eta] == 1)
				{$query .= ' AND eta = '.$eta_selezionate[id][$b];}
			//print $query;
			$numero = mysql_query("$query");
			$numero_iscritti = mysql_num_rows($numero);
			print '<td><h2>'.$numero_iscritti.'</h2></td>';	
			$c_iscanicol++;
		}
		print '</tr>';
		$b++;
	}
	while ($b<$numero_eta_selezionate);
	if ($numero_eta_selezionate <> 0)
	{
		print '<tr><td><strong><span style="color: red;">TOTALE</span></strong></td>';
		$c_iscanicol=1;
		while ($c_iscanicol <3)
		{
			$query = "SELECT * FROM $iscanicol[$c_iscanicol]_$_SESSION[id_grest] WHERE 1 ";
			$query .= ' AND gruppo = '.$gruppi_selezionati[id][$a];
			//print $query;
			$numero = mysql_query("$query");
			$numero_iscritti = mysql_num_rows($numero);
			print '<td><h2><span style="color: red;">';
			print $numero_iscritti;
			print '</span></h2></td>';
			$c_iscanicol++;
		}
		print '</tr>';
	}
	print '</table>';
	print '</div>';
	$a++;
} while($a<$numero_gruppi_selezionati);
?>
</div>
        <?php include ("pedice.php"); ?>
</div>
</body> 

</html>
