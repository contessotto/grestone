<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<?php
include ("funzioni.php"); 

connetti();
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
$dati_parrocchia = carica_impostazioni_parrocchia();

define(FPDF_FONTPATH,"./font/"); //percorso della cartella font
$pdf=new PDF('L','mm',A4);
$pdf->Settitle("$dati_grest[titolo_grest] - $dati_grest[sottotitolo_grest] - $dati_grest[anno_grest] - Elenchi Generali");
$pdf->SetSubject('Report stampabile dei dati del grest');
$pdf->SetKeywords('grest elenchi liste grestone generali');
$pdf->SetCreator('GrestOne - Software di gestione GrEst tramite FPDF 1.7');
$pdf->SetAuthor("$dati_parrocchia[nome_parrocchia]");
$pdf->SetFont('Arial','',10);


$iscanicol = array('iscritti','animatori','collaboratori');
$iscanicol[bello] = array('Animati','Animatori','Collaboratori');

if ($dati_grest[periodo] == 1)
{
	$periodo = mysql_query("SELECT * FROM periodo WHERE id_grest = $_SESSION[id_grest]");
	$dati_periodo = mysql_fetch_array($periodo, MYSQL_ASSOC);
	$a = 1;
	while ($a <= $dati_periodo[numero_settimane])
	{
		if ($_GET['settimana_'.$a])
			{
				$settimane_selezionate[] = 'settimana_'.$a;
				$settimane_selezionate[id][] = $a;
				$numero_settimane_selezionate++;
			}
		$a++;
	}
}
if ($dati_grest[eta] == 1)
{
	$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest]");
	while ($dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC))
	{
		if ($_GET['eta_'.$dati_eta[id_eta]]== 1)
			{
				$eta_selezionate[id][] = $dati_eta[id_eta];
				$eta_selezionate[nome][] = $dati_eta[nome];
				$numero_eta_selezionate++;
				
			}
	}
}
if ($dati_grest[squadre] == 1)
{
	$squadre = mysql_query("SELECT * FROM squadre_$_SESSION[id_grest]");
	$impostazioni_squadre = mysql_fetch_array($squadre, MYSQL_ASSOC);
	while ($dati_squadre = mysql_fetch_array($squadre, MYSQL_ASSOC))
	{
		if ($_GET['squadra_'.$dati_squadre[id_squadra]]== 1)
			{
				$squadre_selezionate[id_squadra][] = $dati_squadre[id_squadra];
				if ($impostazioni_squadre[nome])
				{$squadre_selezionate[nome][] = $dati_squadre[nome];}
				if ($impostazioni_squadre[colore])
				{$squadre_selezionate[colore][] = $dati_squadre[colore];}
				$numero_squadre_selezionate++;
			}
	}
}
$a = -1;
do
{
$a++;
if ($numero_settimane_selezionate != 0)
{
$settimane_per_stampa = $settimane_selezionate[id][$a];
}
$c = -1;
do
{
$c++;
if ($numero_squadre_selezionate != 0)
	{
	  $colore_per_stampa = traduci_colori($squadre_selezionate[colore][$c]);
	  $squadre_per_stampa = $squadre_selezionate[nome][$c].$colore_per_stampa;
	}
$b = -1;
do
{
$b++;
if ($numero_eta_selezionate != 0)
{
	$eta_per_stampa = $eta_selezionate[nome][$b];
}

$pdf->AddPage();
if ($numero_settimane_selezionate != 0)
{
	$pdf->SetFont('Arial','B',15);
	$pdf->Cell(20,3,"Settimana: $settimane_per_stampa",0,0,'L'); //Scritta valore settimana
	$pdf->SetFont('Arial','',10);
	//$eventi_per_stampa = '';
}

$pdf->SetX(10);
$pdf->Ln(10);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(20,3,"$squadre_per_stampa",0,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->Ln(10);
//$squadre_per_stampa = '';

$pdf->SetX(-30);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(20,-15,"$eta_per_stampa",0,0,'R'); //Scritta in alto a dx Grandi o Piccoli
$pdf->SetFont('Arial','',10);
$pdf->Ln(10);
//$eta_per_stampa = '';


$i = 0;
while ($i < 3)
{
if ($_GET[$iscanicol[$i]])
{
	$query = "SELECT * FROM	$iscanicol[$i]_$_SESSION[id_grest] WHERE 1 ";
	if ($dati_grest[periodo] == 1)
		{
			if ($numero_settimane_selezionate != 0)
				{$query .= " AND $settimane_selezionate[$a] = 1";}
		}
	if ($dati_grest[eta] == 1)
		{
			if ($numero_eta_selezionate != 0)
				{
					$eta_per_query = $eta_selezionate[id][$b];
					$query .= " AND eta = '$eta_per_query'";
				}
		}
	if ($dati_grest[squadre] == 1)
		{
			if ($numero_squadre_selezionate != 0)
				{
					$squadra_per_query = $squadre_selezionate[id_squadra][$c];
					$query .= " AND squadra = '$squadra_per_query'";
				}
		}		
	// print 'query:'.$query;
	$query .= " ORDER BY  `cognome`,`nome` ASC ";
	$iscritti = mysql_query("$query");
    // poichè orizzontale rifaccio linee intestazione e pedice
	$pdf->Line(10, 197, 287, 197);
	$pdf->Line(10, 25, 287, 25);
	$pdf->SetFont('Arial','B',15);
	$iscanicol_per_stampa = $iscanicol[bello][$i];
	$pdf->Cell(100,7,"Elenco $iscanicol_per_stampa",0,1,'C');
	$pdf->SetFont('Arial','',10);
	
	if (mysql_num_rows($iscritti) == null)
	{
		$pdf->Cell(35);
		$pdf->Cell(100,7,"NESSUN ISCRITTO PER QUESTA CATEGORIA",1,1,'C');
		$pdf->Ln(30);
	}
	else
	{
		$tabella .= '
			<table border="1" width="500">
			<tr>
			<td width="120">NOME</td>
			<td width="120">COGNOME</td>
			';
			if ($iscanicol[$i] == 'iscritti' or $iscanicol[$i] == 'animatori')
			{
			$tabella .= '<td width="120">CLASSE</td>';
			} 
			$tabella .= '<td width="120">TELEFONO</td><td width="120">CELLULARE</td>';
			if ($iscanicol[$i]=='iscritti') 
			{
				if ($numero_settimane_selezionate == 0) //se non è selezionata nessuna settimana mostra le 3 settimane si/no
				{
					for ($set=1;$set<=$dati_periodo[numero_settimane];$set++)
					{$tabella .= '<td width="30">S '.$set.'</td>';}
				}
				else //se è selezionata una settimana mostra in dettaglio i giorni
				{
					$tabella .= '<td width="250">APPELLI SETT. '.$settimane_selezionate[id][$a].'</td>';
				}
			}
			$tabella .= '<td width="180">NOTE</td>';
		$tabella .= '</tr>';
		while ($dati_iscritti =  mysql_fetch_array($iscritti, MYSQL_ASSOC))
		{
			$tabella .= '
			<tr>
			<td width="120">'.$dati_iscritti[nome].'</td>
			<td width="120">'.$dati_iscritti[cognome].'</td>';
			/*
			if ($iscanicol[$i]!='collaboratori') 
			{
				$tabella .= '<td width="90">';
				if ($dati_iscritti[nascita] != null)
					{$tabella .=  $nascita = date("d-m-Y",$dati_iscritti[nascita]);}
				else
					{$tabella .= '&nbsp;';}
				$tabella .= '</td>';
				$tabella .= '<td width="90">'.classe($dati_iscritti[classe]).'</td>';
			}
			*/
			if ($iscanicol[$i]!='collaboratori') 
			{
			$tabella .= '<td width="120">'.classe($dati_iscritti[classe]).'</td>';
			}
			
			// INIZIO Blocco Telefoni con controllo
			$tabella .='<td width="120">';
			if (empty($dati_iscritti[telefono]) == '0')
				{$tabella .=''.$dati_iscritti[telefono].'';}
			else
			    {$tabella .='&nbsp;';}
			$tabella .='</td>';
			
			$tabella .='<td width="120">';
			if (empty($dati_iscritti[cellulare]) == '0')
				{$tabella .=''.$dati_iscritti[cellulare].'';}
			else
			    {$tabella .='&nbsp;';}
			$tabella .='</td>';
			// FINE Blocco Telefoni con controllo
			
			if ($iscanicol[$i]=='iscritti') 
			{
				if ($numero_settimane_selezionate == 0) //se non è selezionata nessuna settimana mostra le 3 settimane si/no
				{
					for ($set=1;$set<=$dati_periodo[numero_settimane];$set++)
					{
						if ($dati_iscritti['settimana_'.$set] == 1)
						{ $tabella .=  '<td width="30">SI</td>'; }
						if ($dati_iscritti['settimana_'.$set] == 0)
						{ $tabella .=  '<td width="30">NO</td>'; }
					}
				}
				else //se è selezionata una settimana mostra in dettaglio i giorni
				{
					$tabella .= '<td width="250">';
					$tabella .= $pdf ->presenze_stampa($a+1);
					$tabella .= '</td>';
				}
			}
			if (empty($dati_iscritti[note]) == '0')
				{$tabella .= '<td width="180">'.$dati_iscritti[note].'</td>';}
			else
			    {$tabella .= '<td width="180">&nbsp;</td>';}
             
			$tabella .= '</tr>';
		}
		$tabella .= '</table>';
		$pdf->WriteHTML("$tabella");
		$tabella = '';
	}	
}
$i++;
}

}while ($b < $numero_eta_selezionate-1);

}while ($c < $numero_squadre_selezionate-1);

}while ($a < ($numero_settimane_selezionate-1));

$pdf->Output("$dati_grest[titolo_grest]_Elenchi_Squadre.pdf",D);

?>
