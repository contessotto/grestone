<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>
	<script src="script.js" type="text/javascript"></script>
</head>
	<style>
	tr.modifica{display:none;}
	</style>
    <script>
       	function modifica(id)
       	{document.getElementById('modifica'+id).style.display = 'table-row';}
       	function esci(id)
       	{document.getElementById('modifica'+id).style.display = 'none';}        
    </script>
<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
<?php
verifica_amministratore($_SESSION[Grestone]);
?>
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_personalizzazione.php"); ?>

        <div id="contenuto">
<?php
if ($dati_grest[s_gite] == 1)
{
	print '<h2>Gestione Eventi</h2>';
	print 'La gestione degli Eventi è attualmente sospesa.<br/>
	<a href="riprendi.php?oggetto=eventi">Riprendi</a>';
}
else
{
if ($dati_grest[gite] == 0)
{
	if (!isset($_POST[passaggi])) //primo passaggio
	{
		print '<h2>Gestione Eventi</h2>';
	
		if ($dati_grest[periodo] == 0)
		{
			print '<strong>ATTENZIONE! Il periodo del grest non è ancora stato inserito.
			Non è necessario ma ti consigliamo di attivare la gestione del periodo prima di procedere
			con l\'inserimento degli eventi, per migliorare la gestione del grestone</strong>
			<form action="gestione_gite.php" method="post">
			<input type="hidden" name="passaggi" value="primosenzaperiodo">
			Numero eventi/gite:
			<select name="numero">';
			for ($a=1;$a<=20;$a++)
				{print '<option value="'.$a.'">'.$a.'</option>';}
			print '</select><br/>
			<input type="submit" value="inserisci">
			</form>';
		}
		else
		{
	/*INIZIO DELLA VISUALIZZAZIONE DEL CALENDARIO*/
	print'<h2>Inserimento eventi nel calendario</h2>
	<h3>Scegli i giorni degli eventi</h3>
	<form action="gestione_gite.php" method="post">
	<input type="hidden" name="passaggi" value="primo">';

    $calendario = mysql_query("SELECT * FROM periodo WHERE id_grest = $_SESSION[id_grest]");
    $dati_calendario = mysql_fetch_array($calendario, MYSQL_ASSOC);
    $mktime_inizio = $dati_calendario[mktime_inizio];
    $mktime_fine = $dati_calendario[mktime_fine];
    print '
	<table align="center" style="width:100px;height:50px;font-size:100%;">
	<tr>
	<td colspan="8" style="background-color:#B5BEC1;
	border:solid 3px black;">Calendario Grest</td>

	</tr>
	<tr>
		<td style="background-color:#A4A2A7;border:solid 3px black;">Settimana</td>
		<td style="background-color:#A4A2A7;border:solid 3px black;">Lun</td>
		<td style="background-color:#A4A2A7;border:solid 3px black;">Mar</td>
		<td style="background-color:#A4A2A7;border:solid 3px black;">Mer</td>
		<td style="background-color:#A4A2A7;border:solid 3px black;">Gio</td>
		<td style="background-color:#A4A2A7;border:solid 3px black;">Ven</td>
		<td style="background-color:#A4A2A7;border:solid 3px black;">Sab</td>
		<td style="background-color:#A4A2A7;border:solid 3px black;">Dom</td>
	</tr>
	<tr>';
	/* PROCEDURE PER LA PRIMA SETTIMANA */
	$contatore_settimane = 1;
	$s = date("N", $mktime_inizio);
	if ($s <> 1) //controllo per verificare se la settimana comincia di lunedì ed escludere il <td> settimana in quel caso
	{
		print '<td style="background-color:white;
		border:solid 3px black;">Set. '.$contatore_settimane.'</td>';
		$contatore_settimane++;		
	}								 // Queste due righe si occupano di creare dei <td> vuoti all'inizio della tabella
	for ($r = 1; $r < $s; $r++)		// in modo che i giorni numerici inizino nel corrispondente giorno della settimana.
		{print '<td style="background-color:white;border:0px;"></td>';}


	/* PROCEDURE PER TUTTO IL RESTO DEL CALENDARIO */

	$giorni_effettivi = 0; // contatore dei giorni effettivi
	
	$giorni_esclusi = explode("-",$dati_calendario[giorni_esclusi]);
	$numero_giorni_esclusi = count($giorni_esclusi);	

	$giorni_inclusi = explode("-",$dati_calendario[giorni_inclusi]);
	$numero_giorni_inclusi = count($giorni_inclusi);
	
	for ($i = $mktime_inizio; $i <= $mktime_fine; $i = $i + 24*3600) //ciclo che scrive i vari <td> con il numero del giorno.
	{// il ciclo sfrutta il mktime e aumenta di un giorno (24*/3600) ad ogni iterazione, fino al mktime dell'ultimo giorno.
		
		if (date("N", $i) == 1) //se il giorno è lunedì scrive il <td> della settimana
		{
			print '<td style="background-color:white;
		border:solid 3px black;">Set. '.$contatore_settimane.'</td>';
		$contatore_settimane++;
		}
		
		print '<td style="background-color:#B5BEC1;
		border:solid 3px black;';
	
		$giorni_effettivi++;
	
		if (date("z", $i) == date("z", time())) // queste due righe danno lo sfondo bianco alla casella del giorno corrnete.
			{print ' background-color:white; border-color:red;';}


		for ($a = 0; $a <= $numero_giorni_esclusi; $a++)
		{
			if ($i == $giorni_esclusi[$a])
			{
				print 'opacity:0.3;';
			}
		}
	
	/* if (date("N", $i) == 7)
	{print ' style="background-color:red;"';}
	
	if (date("N", $i) == 6)
	{print ' style="background-color:blue;"';} */
	
	/* le seguenti righe si occupano di verificare se il giorno della settimana che si stà considerando è un giorno di grest
	 * oppure no (confrontando con quelli indicati nel form). Se no i giorni vengono mostrati in trasparenza.
	*/

		$nomi_giorni = array ('ciao','lun', 'mar', 'mer', 'gio', 'ven', 'sab', 'dom'); //per avere i nomi in italiano dei giorni. 
		$num = date("N", $i);	// indica il numero del giorno della settimana (1=>lun...7=>dom).
		$z = $nomi_giorni[$num]; // $z è il nome del giorno della settimana.
		$c = 0;
		if ($dati_calendario[$z] == 0)	// $_POST[$z] è il nome del campo del form in cui è richiesto quel giorno della settmana.
							// se è 1 (true) il giorno fa parte del grest, altrimenti no.
		{

			for ($a = 0; $a <= $numero_giorni_inclusi; $a++)
			{
				if ($i == $giorni_inclusi[$a])
				{
				$c = 1;
				}
			}
		if ($c == 0)
			{
				print 'opacity:0.3;';				
			}
			
			$giorni_effettivi--; //se quel giorno non c'è il grest allora non viene contato tra i giorni effettivi.
		}	
		print '"';
		print '>';
	
		print date("j", $i); // scrive il giorno in numero
		print '<input type="checkbox" name="'.$giorni_gite.'" value="'.$i.'"/>';
		$giorni_gite++;
		print '</td>';
	
		if (date("N", $i) == 7) // se il giorno è una domenica chiude anche la riga e inizia quella dopo.
		{	
			print '</tr><tr>';
		}	
	}
	
	print '</tr>';
	print '</table>';
	/*FINE DELLA VISUALIZZAZIONE DEL CALENDARIO*/
	print '<input type="submit" value="inserisci">';
		}
	}
	if ($_POST[passaggi] == 'primosenzaperiodo') //secondo passaggio nel caso in cui non sia attivato il calendario
	{	
		print '<h3>Gestione Eventi</h3>';
		print '
		Inserisci le informazioni sugli\'eventi:
		Per ciascun evento dovrai indicare il nome, potrai inserive una breve descrizione e delle note.
		Avrai inoltre la possibilità abilitare o meno la gestione delle liste dell\'evento: se abiliti
		le liste di un evento potrai segnalare la partecipazione o meno ad esso di ciascun iscritto,
		animatore e collaboratore.<br/>
		<form action="gestione_gite.php" method="post">
		<input type="hidden" name="passaggi" value="secondo">';
		for ($a=1; $a<=$_POST[numero]; $a++)
		{
			print '<h2>Evento '.$a.'</h2>
			Data: <input type="text" name="giorno_'.$a.'" maxlength="2" size="2" placeholder="GG" required"> - 
				  <input type="text" name="mese_'.$a.'" maxlength="2"size="2" placeholder="MM" required"> - 
				  <input type="text" name="anno_'.$a.'" maxlength="4"size="2" placeholder="AAAA" required"><br/>
			Nome: <input type="text" name="nome_'.$a.'" required ><br>
			Tipologia: <select name="tipo_'.$a.'">
			<option value="">NON INDICATO</option>
			<option value="gita">GITA</option>
			<option value="serata">SERATA</option>
			<option value="torneo">TORNEO</option>
			<option value="cerimonia">CERIMONIA</option>
			</select>
			Liste: <input type="checkbox" value="1" name="liste_'.$a.'">
			<br/>
			Descrizione: <textarea name="descrizione_'.$a.'"></textarea><br/>
			Note: <input type="text" name="note_'.$a.'"><br/>';
		}
		print '<input type="hidden" name="numero" value="'.$_POST[numero].'">';
		print '<br/><br/><input type="submit" value="conferma">';
	}	
	
	if ($_POST[passaggi] == 'primo') //secondo passaggio
	{	
		print '<h3>Gestione Eventi</h3>';
		print '
		Inserisci le informazioni sugli\'eventi:
		Per ciascun evento dovrai indicare il nome, potrai inserive una breve descrizione e delle note.
		Avrai inoltre la possibilità abilitare o meno la gestione delle liste dell\'evento: se abiliti
		le liste di un evento potrai segnalare la partecipazione o meno ad esso di ciascun iscritto,
		animatore e collaboratore.<br/>
		<form action="gestione_gite.php" method="post">
		<input type="hidden" name="passaggi" value="secondo">';
		for ($giorni_gite = 0; $giorni_gite <= 365; $giorni_gite++)
		{
			if (isset($_POST[$giorni_gite]))
			{	
				$a++;
				print '<h2>Evento '.$a.'</h2>';
				print '<input type="hidden" name="giorno_'.$a.'" value="'.$_POST[$giorni_gite].'">';
				print 'Data: '.date("j-m-Y",$_POST[$giorni_gite]).'<br/>';
				print'Nome: <input type="text" name="nome_'.$a.'" required ><br>
				Tipologia: <select name="tipo_'.$a.'">
				<option value="">NON INDICATO</option>
				<option value="gita">GITA</option>
				<option value="serata">SERATA</option>
				<option value="torneo">TORNEO</option>
				<option value="cerimonia">CERIMONIA</option>
				</select>
				Liste: <input type="checkbox" value="1" name="liste_'.$a.'">
				<br/>
				Descrizione: <textarea name="descrizione_'.$a.'"></textarea><br/>
				Note: <input type="text" name="note_'.$a.'"><br/>';
				
			}
		}
		print '<input type="hidden" name="numero" value="'.$a.'">';
		print '<br/><br/><input type="submit" value="conferma">';
	}

	if ($_POST[passaggi] == 'secondo') //terzo passaggio: mostra i valori per la conferma
	{	
		print '<h2>Conferma dati inseriti</h2>';
		print '<form action="gestione_gite.php" method="post">';
		print'<input type="hidden" name="numero" value="'.$_POST[numero].'">';		
		print '<input type="hidden" name="passaggi"value="terzo">';
		
		if ($dati_grest[periodo] == 0) // controllo se è attivata la gesitone del periodo
		{
			for ($a=1; $a<=$_POST[numero]; $a++) //in tal caso sostituisco il valore $_POST[giorno] con il mktime della data
			{
				$giorno = 'giorno_'.$a;
				$mese = 'mese_'.$a;
				$anno = 'anno_'.$a;
				$_POST[$giorno] = mktime(0,0,0,$_POST[$mese],$_POST[$giorno],$_POST[$anno]);
			}
		}
	
		for ($a=1; $a<=$_POST[numero]; $a++)
		{
			$giorno = 'giorno_'.$a;
			$nome = 'nome_'.$a;
			$tipo = 'tipo_'.$a;
			$liste = 'liste_'.$a;
			$descrizione = 'descrizione_'.$a;
			$note = 'note_'.$a;
			print"<h2>$_POST[$nome]</h2>";
			print'Data: '.date("j-m-Y",$_POST[$giorno]).'<br/>';
			print"Tipo: $_POST[$tipo]<br/>";
			print"Liste:";
			if ($_POST[$liste] == 1)
			{print '<img src="immagini/ico_ok.png">';}
			if ($_POST[$liste] == 0)
			{print '<img src="immagini/ico_no.png">';}
			print '<br/>';			
			print"Descrizione: $_POST[$descrizione]<br/>";
			print"Note: $_POST[$note]<br/><br/>";
		}
		for ($a=1; $a<=$_POST[numero]; $a++)
		{
			$giorno = 'giorno_'.$a;
			$nome = 'nome_'.$a;
			$tipo = 'tipo_'.$a;
			$liste = 'liste_'.$a;
			$descrizione = 'descrizione_'.$a;
			$note = 'note_'.$a;
			print '<input type="hidden" name="'.$giorno.'" value="'.$_POST[$giorno].'">';
			print '<input type="hidden" name="'.$tipo.'" value="'.$_POST[$tipo].'">';
			print '<input type="hidden" name="'.$liste.'" value="'.$_POST[$liste].'">';
			print '<input type="hidden" name="'.$nome.'" value="'.$_POST[$nome].'">';
			print '<input type="hidden" name="'.$descrizione.'" value="'.$_POST[$descrizione].'">';
			print '<input type="hidden" name="'.$note.'" value="'.$_POST[$note].'">';
		}
	print '<br/><br/><input type="submit" value="Inserisci eventi">';
	print '</form>';
	}
	
	
	
	
		if ($_POST[passaggi] == 'terzo') //quarto passaggio: inserisce i valori nel database
	{
		connetti();
		mysql_query("UPDATE `grests` SET gite = '1' WHERE `id_grest` = '$_SESSION[id_grest]';");
		mysql_query("CREATE TABLE  `gite_$_SESSION[id_grest]` (`id_gita` INT( 3 ) NOT NULL AUTO_INCREMENT ,
					`giorno` INT(15) NOT NULL ,`nome` TEXT NOT NULL ,`tipo` TEXT NOT NULL ,
					 `liste` TINYINT(1) NOT NULL ,`descrizione` TEXT, `note` TEXT,
					PRIMARY KEY (  `id_gita` ))");		
		
		for ($a=1; $a<=$_POST[numero]; $a++)
		{
			$giorno = 'giorno_'.$a;
			$nome = 'nome_'.$a;
			$tipo = 'tipo_'.$a;
			$liste = 'liste_'.$a;
			$descrizione = 'descrizione_'.$a;
			$note = 'note_'.$a;
		
			mysql_query("INSERT INTO  `gite_$_SESSION[id_grest]` 
						(`id_gita` ,`giorno` ,`nome`,`tipo` ,`liste` ,`descrizione` ,`note`)
						VALUES (NULL ,  '$_POST[$giorno]', '$_POST[$nome]', '$_POST[$tipo]', 
						'$_POST[$liste]',  '$_POST[$descrizione]',  '$_POST[$note]');");
		}
		for ($a=1; $a<=$_POST[numero]; $a++)
		{	
			$liste = 'liste_'.$a;
			if ($_POST[$liste] == '1')
			{
			mysql_query("ALTER TABLE  `iscritti_$_SESSION[id_grest]` ADD  `presenza_evento_$a` TINYINT(1) NOT NULL ,
			ADD  `pagamento_evento_$a` TINYINT(1) NOT NULL ,
			ADD  `note_evento_$a` TEXT NOT NULL");	
			mysql_query("ALTER TABLE  `animatori_$_SESSION[id_grest]` ADD  `presenza_evento_$a` TINYINT(1) NOT NULL ,
			ADD  `pagamento_evento_$a` TINYINT(1) NOT NULL ,
			ADD  `note_evento_$a` TEXT NOT NULL");
			mysql_query("ALTER TABLE  `collaboratori_$_SESSION[id_grest]` ADD  `presenza_evento_$a` TINYINT(1) NOT NULL ,
			ADD  `pagamento_evento_$a` TINYINT(1) NOT NULL ,
			ADD  `note_evento_$a` TEXT NOT NULL");	
			}
		}
		print '<h2>Dati inseriti correttamente</h2><meta http-equiv="refresh" content="0;
			URL='.$_SERVER['HTTP_REFERER'].'">';
	}
}
else
{
	print'<h2>La gestione degli eventi è attiva</h2>
	<a href="#inserisci">Inserisci nuovo Evento</a><br/>
	<a href="disattiva.php?oggetto=eventi" onclick="return confermadisattiva ();">Disattiva definitivamente gestione Eventi</a><br/>
	<a href="sospendi.php?oggetto=eventi">Sospendi temporaneamente gestione Eventi</a>

	';
	connetti();
	$gite = mysql_query("SELECT * FROM  `gite_$_SESSION[id_grest]` ORDER BY giorno ASC");
	print '<table id="lista" align="center" width="100%"><thead>
	<tr>';
	print'<th scope="col">NOME</th>';
	print'<th scope="col">DATA</th>';
	print'<th scope="col">TIPO</th>';
	print'<th scope="col">LISTE</th>';
	print'<th scope="col">DESCRIZIONE</th>';
	print'<th scope="col">NOTE</th>';
	print'<th scope="col">MODIFICA</th>';
	print'<th scope="col">ELIMINA</th>';
	print'</thead></tr><tbody>';
	while ($dati_gite = mysql_fetch_array($gite, MYSQL_ASSOC))
	{	
		print '<tr>';
		print"<td>$dati_gite[nome]</td>";
		print '<td>'.date("d-m-Y",$dati_gite[giorno]).'</td>';
		print '<td>';
		if ($dati_gite[tipo] == 'gita') {print"<img src=\"immagini/gita.png\" />";}
		if ($dati_gite[tipo] == 'serata') {print"<img src=\"immagini/serata.png\" />";}
		if ($dati_gite[tipo] == 'torneo') {print"<img src=\"immagini/torneo.png\" />";}
		if ($dati_gite[tipo] == 'cerimonia') {print"<img src=\"immagini/cerimonia.png\" />";}
		print "<br/>$dati_gite[tipo]</td>";
		print '<td>';
		if ($dati_gite[liste] == 1)
		{print '<img src="immagini/ico_ok.png" alt="Si" border="0" title="Si"/>';}
		else
		{print '<img src="immagini/ico_no.png" alt="No" border="0" title="No"/>';}
		print '</td>';
		print"<td>$dati_gite[descrizione]</td>";
		print"<td>$dati_gite[note]</td>";
		print '<td><a onclick="modifica('.$dati_gite[id_gita].');"class="elimina"href="#"><img src="immagini/modifica.png" alt="modifica" border="0"  title="Modifica"/></a></td>';	
		print'<td><a href="elimina.php?oggetto=gita&id='.$dati_gite[id_gita].'"  onclick="return conferma ();" title="elimina"><img src="immagini/ico_no.png" alt="Elimina" border="0"/></a></td>';
		print'</tbody></tr>';


		print'<form action="modifica.php?oggetto=gita" method="post">
		<tr class="modifica" id="modifica'.$dati_gite[id_gita].'">';
		print'
		<td><input type="text" name="nome" size="7" value="'.$dati_gite[nome].'"></td>
		<td><input type="text" required name="giorno" pattern="[0-9]{2}"size="2" value="'.date("d",$dati_gite[giorno]).'" maxlength="2">-
	<input type="text" required name="mese" pattern="[0-9]{2}"size="2" value="'.date("m",$dati_gite[giorno]).'" maxlength="2">-
	<input type="text" required name="anno" pattern="[0-9]{4}"size="2" value="'.date("Y",$dati_gite[giorno]).'" maxlength="4"></td>
		<td><select name="tipo">
			<option value=""';
		if ($dati_gite[tipo] == '')
			{print 'selected';}			
		print '>NON INDICATO</option><option value="gita"';
		if ($dati_gite[tipo] == 'gita')
			{print 'selected';}
		print'>GITA</option><option value="serata"';
		if ($dati_gite[tipo] == 'serata')
			{print 'selected';}	
		print '>SERATA</option><option value="torneo"';
		if ($dati_gite[tipo] == 'torneo')
			{print 'selected';}
		print '>TORNEO</option><option value="cerimonia"';
		if ($dati_gite[tipo] == 'cerimonia')
			{print 'selected';}
		print '>CERIMONIA</option>
		</select>
		</td>
		<td>IMMODIFICABILE</td>
		<td><textarea name="descrizione" cols="8">'.$dati_gite[descrizione].'</textarea></td>
		<td><textarea name="note" cols="8">'.$dati_gite[note].'</textarea></td>
		<td><input type="submit" value="modifica">
		</td>
		<td><input type="hidden" name="id_gita" value="'.$dati_gite[id_gita].'"><a class="elimina" href="#" onclick="esci('.$dati_gite[id_gita].');">abbandona modifiche</a></td>
		</tr>
		</form>';
	}
	print '</table>';

	if (isset($_POST[passaggi]))
	{
		if ($_POST[passaggi] == 'modifica_primo')
		{
			$giorno = mktime(0, 0, 0, $_POST[mese], $_POST[giorno], $_POST[anno]);
			mysql_query("INSERT INTO `gite_$_SESSION[id_grest]` 
			(`id_gita`, `giorno`, `nome`, `tipo`, `liste`, `descrizione`, `note`)
			 VALUES (NULL,  '$giorno', '$_POST[nome]', '$_POST[tipo]', '$_POST[liste]', '$_POST[descrizione]', '$_POST[note]');");
			$ultimo_id = mysql_insert_id();
			mysql_query("ALTER TABLE  `iscritti_$_SESSION[id_grest]` ADD  `presenza_evento_$ultimo_id` TINYINT(1) NOT NULL ,
			ADD  `pagamento_evento_$ultimo_id` TINYINT(1) NOT NULL ,
			ADD  `note_evento_$ultimo_id` TEXT NOT NULL");	
			mysql_query("ALTER TABLE  `animatori_$_SESSION[id_grest]` ADD  `presenza_evento_$ultimo_id` TINYINT(1) NOT NULL ,
			ADD  `pagamento_evento_$ultimo_id` TINYINT(1) NOT NULL ,
			ADD  `note_evento_$ultimo_id` TEXT NOT NULL");
			mysql_query("ALTER TABLE  `collaboratori_$_SESSION[id_grest]` ADD  `presenza_evento_$ultimo_id` TINYINT(1) NOT NULL ,
			ADD  `pagamento_evento_$ultimo_id` TINYINT(1) NOT NULL ,
			ADD  `note_evento_$ultimo_id` TEXT NOT NULL");	
			print '<h2>Dati inseriti correttamente</h2>';
			print '<meta http-equiv="refresh" content="0;
			URL='.$_SERVER['HTTP_REFERER'].'">';
		}		
	}
	else
	{
		print '<a name="inserisci"></a>
		<br/><h2>Inserisci nuovo evento</h2>
		<form action="gestione_gite.php" method="post">
		Nome:<input type="text" name="nome"><br/>
		Giorno:	<input type="text" required name="giorno" pattern="[0-9]{2}"size="2" placeholder="GG" maxlength="2">-
		<input type="text" required name="mese" pattern="[0-9]{2}"size="2" placeholder="MM" maxlength="2">-
		<input type="text" required name="anno" pattern="[0-9]{4}"size="2" placeholder="AAAA" maxlength="4">
		Tipo:<select name="tipo">
		<option value="">NON INDICATO</option>
		<option value="gita">GITA</option>
		<option value="serata">SERATA</option>
		<option value="torneo">TORNEO</option>
		<option value="cerimonia">CERIMONIA</option>
		</select>
		Liste: <input type="checkbox" value="1" name="liste"><br/>
		Descrizione: <textarea name="descrizione"></textarea><br/>
		Note: <textarea name="note"></textarea><br/>
		<input type="hidden" name="passaggi" value="modifica_primo"><br/>
		<input type="submit" value="inserisci"><br/>
		</form>';
	}
	
}
}
?>
</div>
        <?php include ("pedice.php"); ?>
</div>
</body> 

</html>
