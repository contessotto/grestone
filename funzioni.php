<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<?php
include ("html_table.php");
// funzione per la connessione al server
function connetti () 
{
include ("connessione.php");
$connessione = mysql_connect($indirizzo,$nome_utente,$password);
$selezione = mysql_select_db($nome_database,$connessione);
}
function controllo_connessione () 
{
include ("connessione.php");
return $connessione = @mysql_connect($indirizzo,$nome_utente,$password);
mysql_close($connessione);
}
function controllo_database () 
{
include ("connessione.php");
$connessione = @mysql_connect($indirizzo,$nome_utente,$password);
return $selezione = @mysql_select_db($nome_database,$connessione);
mysql_close($connessione);
}

function mail_admin () 
{
include ("connessione.php");
return $mail_admin;
}
function disconnetti()
{

}

function verifica_utente()
{
	controllo_attivazione();
	// inizializzazione della sessione
	session_start();
	// controllo sul valore di sessione
	if (!isset($_SESSION['grestone']))
	{
		// reindirizzamento alla homepage in caso di login mancato
		header("Location: index.php");
		print 'Attenzione! è necessario effettuare il <a href="index.php">
		effettuare il login</a> per poter accedere a questa pagina.';
		exit;
	}
	else
	{//id_utente,nome_utente,id_parrocchia,id_grest,ruolo_utente
		connetti();
		$utente = mysql_query("SELECT * FROM utenti WHERE id_utente = '$_SESSION[grestone]'");
		$dati_utente = mysql_fetch_array($utente, MYSQL_ASSOC);
		return $dati_utente;
	}
}

function verifica_amministratore()
{
$dati_utente = verifica_utente();
if ($dati_utente[ruolo_utente] != 'amministratore' and $dati_utente[ruolo_utente] != 'normale')
	{print 'Attenzione! è necessario essere amministratori per poter accedere a questa pagina.';
	header("location: index.php");
	exit;
	}
}

function verifica_normale()
{
$dati_utente = verifica_utente();
if ($dati_utente[ruolo_utente] == 'osservatore')
	{print 'Attenzione! Non si dispone delle autorizzazioni per poter accedere a questa pagina.';
	header("location: index.php");
	exit;
	}
}

function verifica_admin()
{
	$dati_utente = verifica_utente();
	if ($dati_utente[id_utente] != '1')
	{
		print 'Attenzione! Zona riservata.';
		//header("location: index.php");
		exit;
	}
}
function verifica_grest()
{
	if (!isset($_SESSION[id_grest]))
	{
		print 'Attenzione! Bisogna effettuare il login con un grest per accedere a questa pagina.';
		header("location: index.php");
		exit;
	}
	else
	{
		connetti();
		$a = mysql_query("SELECT * FROM grests WHERE id_grest = '$_SESSION[id_grest]'");
		$dati_grest = mysql_fetch_array($a, MYSQL_ASSOC);
		disconnetti();
		return $dati_grest;
	}
}

function carica_impostazioni_parrocchia()
	{connetti();
	$b = mysql_query("SELECT * FROM parrocchie WHERE id_parrocchia = $_SESSION[id_parrocchia]");
	$impostazioni = mysql_fetch_array($b, MYSQL_ASSOC);
	return $impostazioni;
	disconnetti();
	}
	
function carica_impostazioni_grest()
	{connetti();
	$b = mysql_query("SELECT * FROM grests WHERE id_grest = $_SESSION[id_grest]");
	$impostazioni_grest = mysql_fetch_array($b, MYSQL_ASSOC);
	return $impostazioni_grest;
	disconnetti();
	}


function registro($nome_utente, $id_grest, $operazione)
	{
	$data = date("Y-m-d H:i:s");
	$registro = mysql_query(
	"INSERT INTO `registro` (`data` , `nome_utente` , `id_grest` , `operazione`)
	VALUES ('$data' , '$nome_utente' , '$id_grest' , '$operazione');");
	}


function elimina_grest($id)
	{	
		mysql_query("DELETE FROM `grests` WHERE `grests`.`id_grest` = $id");
        $utente = mysql_query("SELECT * FROM utenti");
		while ($impostazioni_utente = mysql_fetch_array($utente, MYSQL_ASSOC))
		{	$nuovo_id_grest = '';
			$id_grest_utente = explode("-",$impostazioni_utente[id_grest]);
			if (in_array($id, $id_grest_utente))
				{$chiave = array_search($id, $id_grest_utente);
				unset($id_grest_utente[$chiave]);
				$nuovo_array = array_merge($id_grest_utente);
				$nuovo_id_grest = implode("-",$nuovo_array);
			mysql_query("UPDATE  `utenti` SET  `id_grest` =  '$nuovo_id_grest'
			WHERE  `utenti`.`id_utente` =$impostazioni_utente[id_utente];");
				}
		}
        mysql_query("DELETE FROM `periodo` WHERE `periodo`.`id_grest` = $id");
        mysql_query("DROP TABLE squadre_$id");
        mysql_query("DROP TABLE eta_$id");
        mysql_query("DROP TABLE laboratori_$id");
        mysql_query("DROP TABLE iscritti_$id");
        mysql_query("DROP TABLE animatori_$id");
        mysql_query("DROP TABLE collaboratori_$id");
        mysql_query("DROP TABLE gite_$id");
        mysql_query("DROP TABLE gruppi_$id");
    }
	
function elimina_utente($id)
	{	
		mysql_query("DELETE FROM `utenti` WHERE `utenti`.`id_utente` = $id");
		registro("admin", "", "elimina utente $id");
	}
	
function elimina_parrocchia($id)
	{
        mysql_query("DELETE FROM `parrocchie` WHERE `parrocchie`.`id_parrocchia` = $id");//elimina parrocchia
        registro("admin", "", "elimina parrocchia $id");
        $grest = mysql_query("SELECT * FROM grests WHERE id_parrocchia = $id"); // individua grest
		while ($impostazioni_grest = mysql_fetch_array($grest, MYSQL_ASSOC))
		{
			elimina_grest($impostazioni_grest[id_grest]); // elimina grest
		}
		
        $utente = mysql_query("SELECT * FROM utenti WHERE id_parrocchia = $id"); // individua utenti
		while ($impostazioni_utente = mysql_fetch_array($utente, MYSQL_ASSOC))
		{
			elimina_utente($impostazioni_utente[id_utente]); // elimina utenti
		}
	}
	
function controllo_attivazione()
	{
		if (@fopen('creazione.php', 'r') != 0)
		{
			print '<h3>Grestone non è ancora stasto configurato correttmente.</h3>
			<a href="creazione.php"> Clicca qui </a>per completare la procedura.<br/>
			Al termine della procedura cancella o rinomina il file creazione.php.';
			exit;
		}
	}
function classe($codice)
	{
		$classi = array('','asilo','1 elementare', '2 elementare', ' 3 elementare', '4 elementare', '5 elementare',
		'1 media', '2 media', '3 media', '1 superiore', '2 superiore', '3 superiore', '4 superiore', '4 superiore', 'altro');
		$classe = $classi[$codice];
		return $classe;
	}
function presenze($settimana)
	{
		$dati_grest = verifica_grest();
		$periodo = mysql_query("SELECT * FROM periodo WHERE id_grest = $_SESSION[id_grest]");
		$dati_periodo = mysql_fetch_array($periodo, MYSQL_ASSOC);
		
		$mktime_inizio = $dati_periodo[mktime_inizio];
		$mktime_fine = $dati_periodo[mktime_fine];
		$giorni_esclusi = explode("-",$dati_periodo[giorni_esclusi]);
		$numero_giorni_esclusi = count($giorni_esclusi);
		$giorni_inclusi = explode("-",$dati_periodo[giorni_inclusi]);
		$numero_giorni_inclusi = count($giorni_inclusi);
		$inizi_settimane = explode('-',$dati_periodo[inizi_settimane]);
		$fini_settimane = explode('-',$dati_periodo[fini_settimane]);
		$inizi_settimane[1]=$mktime_inizio;
		$fini_settimane[$dati_periodo[numero_settimane]]=$mktime_fine;
		for ($i = $inizi_settimane[$settimana]; $i <= $fini_settimane[$settimana]; $i = $i + 24*3600) //ciclo che scrive i vari <td> con il numero del giorno.
		{
			// il ciclo sfrutta il mktime e aumenta di un giorno (24*/3600) ad ogni iterazione, fino al mktime dell'ultimo giorno.
			
			print '<span style="background-color:#B5BEC1;
			border:solid 1px #3366FF;padding:3px;';
			if (date("z", $i) == date("z", time())) // queste due righe danno lo sfondo bianco alla casella del giorno corrnete.
				{print ' background-color:white; border-color:red;';}
			
			for ($d = 0; $d <= $numero_giorni_esclusi; $d++)
			{
				if ($i == $giorni_esclusi[$d])
				{
					print 'opacity:0.3;';
				}
			}
			if ($dati_grest[gite] == 1)
			{		
				$gite = mysql_query("SELECT * FROM gite_$_SESSION[id_grest]  WHERE tipo = 'gita'");//visualizza le gite
				while ($dati_gite = mysql_fetch_array($gite, MYSQL_ASSOC))
				{
					if ($i == $dati_gite[giorno])
						{print 'background-color:#FF8D6F;';}
				}
			}
			/* le seguenti righe si occupano di verificare se il giorno della settimana che si stà considerando è un giorno di grest
			* oppure no (confrontando con quelli indicati nel form). Se no i giorni vengono mostrati in trasparenza.
			*/
			$nomi_giorni = array ('ciao','lun', 'mar', 'mer', 'gio', 'ven', 'sab', 'dom'); //per avere i nomi in italiano dei giorni. 
			$num = date("N", $i);	// indica il numero del giorno della settimana (1=>lun...7=>dom).
			$z = $nomi_giorni[$num]; // $z è il nome del giorno della settimana.
			$e = 0;
			if ($dati_periodo[$z] == 0)	// $_POST[$z] è il nome del campo del form in cui è richiesto quel giorno della settmana.
			// se è 1 (true) il giorno fa parte del grest, altrimenti no.
			{
				for ($d = 0; $d <= $numero_giorni_inclusi; $d++)
				{
					if ($i == $giorni_inclusi[$d])
					{
						$e = 1;
					}
				}
				if ($e == 0)
				{
					print 'opacity:0;';				
				}
			}	
			print '"';
			print '>';
			$lettere_giorni = array ('','Lu', 'Ma', 'Me', 'Gi', 'Ve', 'Sa', 'Do'); //per avere i nomi in italiano dei giorni. 
			$num = date("N", $i);	// indica il numero del giorno della settimana (1=>lun...7=>dom).
			print "<strong>$lettere_giorni[$num]</strong>";
			print '</span>';
		}
	}
	
	
function traduci_colori($colore_squadra)
{
	switch ($colore_squadra) {
    case "Blue":
        $colore_squadra = " - Blu";
		return $colore_squadra;
        break;
    case "Cyan":
        $colore_squadra = " - Azzurri";
		return $colore_squadra;
        break;
    case "Gray":
        $colore_squadra = " - Grigi";
		return $colore_squadra;
        break;
    case "Green":
        $colore_squadra = " - Verdi";
		return $colore_squadra;
        break;
    case "Maroon":
        $colore_squadra = " - Marroni";
		return $colore_squadra;
        break;
    case "Red":
        $colore_squadra = " - Rossi";
		return $colore_squadra;
        break;
    case "White":
        $colore_squadra = " - Bianchi";
		return $colore_squadra;
        break;
    case "Yellow":
        $colore_squadra = " - Gialli";
		return $colore_squadra;
        break;
    case "Orange":
        $colore_squadra = " - Arancioni";
		return $colore_squadra;
        break;
    case "Black":
        $colore_squadra = " - Neri";
		return $colore_squadra;
        break;
    case "Magenta":
        $colore_squadra = " - Magenta";
		return $colore_squadra;
        break;
    }
}
function eta($classe)
{
	$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest]");
	while ($dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC))
	{
		if ($classe >= $dati_eta[min] AND $classe <= $dati_eta[max])
		{
			return $dati_eta[id_eta];
			break;
		}
	}
}
	
	
	
class PDF extends PDF_HTML_Table
{
function Footer()
{

    $dati_grest = verifica_grest();
    $dati_utente = verifica_utente();
	$dati_parrocchia = carica_impostazioni_parrocchia();
	$data = date("d/m/Y H:i:s");
    // Va a 1.5 cm dal fondo della pagina
    $this->SetY(-20);
	$this->Line(5, 285, 200, 285);
    // Seleziona Arial corsivo 8
    $this->SetFont('Arial','I',8);
	$this->Cell(0,10, "Stampa generata da: $dati_utente[nome_utente] - aggiornata al $data");
	// Interruzione di linea
    $this->Ln(6);
    $this->Cell(0,10,'GrestOne - Software Gestione Grest - grestone.it',0,0,'C');
    // Stampa il numero di pagina centrato
    $this->Cell(0,10,'Pagina '.$this->PageNo(),0,0,'R');
}
function Header()
{
    $dati_grest = verifica_grest();
    $dati_utente = verifica_utente();
	$dati_parrocchia = carica_impostazioni_parrocchia();
	// immagine logo parrocchia
	//se l'immagine è la predefinita (in png), uso quella in B/N JPG
	if ($dati_parrocchia[logo_parrocchia] == "immagini/loghi_parrocchie/g_grestone.png")
	{ $dati_parrocchia[logo_parrocchia] = "immagini/loghi_parrocchie/g_grestone.jpg"; }
	$this->Image($dati_parrocchia[logo_parrocchia],7,5,15);
    // Seleziona Arial grassetto 15
    $this->SetFont('Times','I',13);
    // Muove verso destra
    $this->Cell(15);
    // Titolo in riquadro
    $this->Cell(30,10,"$dati_parrocchia[nome_parrocchia]",0,0,'L');
    // Interruzione di linea
    $this->Ln(6);
    // Seleziona Arial grassetto 15
    $this->SetFont('Arial','B',12);
    // Muove verso destra
    $this->Cell(15);
    // Titolo in riquadro
    $this->Cell(30,10,"Grest $dati_grest[anno_grest]  - $dati_grest[titolo_grest] - $dati_grest[sottotitolo_grest]",0,0,'L');
    // Interruzione di linea
    $this->Ln(20);
	$this->Line(5, 25, 200, 25);
}

function presenze_stampa($settimana)
	{
		$dati_grest = verifica_grest();
		$periodo = mysql_query("SELECT * FROM periodo WHERE id_grest = $_SESSION[id_grest]");
		$dati_periodo = mysql_fetch_array($periodo, MYSQL_ASSOC);
		
		$mktime_inizio = $dati_periodo[mktime_inizio];
		$mktime_fine = $dati_periodo[mktime_fine];
		$giorni_esclusi = explode("-",$dati_periodo[giorni_esclusi]);
		$numero_giorni_esclusi = count($giorni_esclusi);
		$giorni_inclusi = explode("-",$dati_periodo[giorni_inclusi]);
		$numero_giorni_inclusi = count($giorni_inclusi);
		$inizi_settimane = explode('-',$dati_periodo[inizi_settimane]);
		$fini_settimane = explode('-',$dati_periodo[fini_settimane]);
		$inizi_settimane[1]=$mktime_inizio;
		$fini_settimane[$dati_periodo[numero_settimane]]=$mktime_fine;
		for ($i = $inizi_settimane[$settimana]; $i <= $fini_settimane[$settimana]; $i = $i + 24*3600)
		{
			$siono = 'si';
			//il ciclo sfrutta il mktime e aumenta di un giorno (24*/3600) ad ogni iterazione, fino al mktime dell'ultimo giorno.
			
			for ($d = 0; $d <= $numero_giorni_esclusi; $d++)
			{
				if ($i == $giorni_esclusi[$d])
				{
					$siono = 'no';
				}
			}
		//	if ($dati_grest[gite] == 1)
		//	{		
		//		$gite = mysql_query("SELECT * FROM gite_$_SESSION[id_grest]  WHERE tipo = 'gita'");//visualizza le gite
		//		while ($dati_gite = mysql_fetch_array($gite, MYSQL_ASSOC))
		//		{
		//			if ($i == $dati_gite[giorno])
		//			{
		//			}
		//		}
		//	}
			
			/* le seguenti righe si occupano di verificare se il giorno della settimana che si stà considerando è un giorno di grest
			* oppure no (confrontando con quelli indicati nel form). Se no i giorni vengono mostrati in trasparenza.
			*/
			$nomi_giorni = array ('ciao','lun', 'mar', 'mer', 'gio', 'ven', 'sab', 'dom'); //per avere i nomi in italiano dei giorni. 
			$num = date("N", $i);	// indica il numero del giorno della settimana (1=>lun...7=>dom).
			$z = $nomi_giorni[$num]; // $z è il nome del giorno della settimana.
			$e = 0;
			
			if ($dati_periodo[$z] == 0)	// $_POST[$z] è il nome del campo del form in cui è richiesto quel giorno della settmana.
			// se è 1 (true) il giorno fa parte del grest, altrimenti no.
			{
				for ($d = 0; $d <= $numero_giorni_inclusi; $d++)
				{
					if ($i == $giorni_inclusi[$d])
					{
						$e = 1;
					}
				}
				if ($e == 0)
				{
					$siono = 'no';			
				}
			}
			$lettere_giorni = array ('','Lu', 'Ma', 'Me', 'Gi', 'Ve', 'Sa', 'Do'); //per avere i nomi in italiano dei giorni. 
			$num = date("N", $i);	// indica il numero del giorno della settimana (1=>lun...7=>dom).
			if ($siono  == 'si')
			{
				$stringa .= ' |'.$lettere_giorni[$num].'| ';
			}
		}
	//print $stringa;
	return $stringa;
	}
}
?>
