<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
verifica_admin();
?>
	
    <div id="principale">

        <?php $impostazioni = carica_impostazioni_parrocchia();?>

        <div id="intestazione">
		<br/><h2>Pagina di Configurazione Generale di GrestOne</h2>
		</div>

        <?php include ("menu_configurazione.php"); ?>

        <div id="contenuto">
			
<?php 
connetti();
if ($_GET[utente] == 1)
{
	print '<br/><br/><br/><br/><br/><br/><br/><h2>ATTENZIONE!<br/> Impossibile modificare l\'utente amministratore in questa sezione</h2>
	<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	<br/><br/><br/><br/><br/><br/><br/><br/><br/>';
	exit;
}
		print'<h2 name="utenti">Modifica Dati Personali Utente</h2>';
		$utente = mysql_query("SELECT * FROM utenti WHERE id_utente = $_GET[utente]");
		$dati_utente = mysql_fetch_array($utente, MYSQL_ASSOC);
		print '<form action="modifica.php?oggetto=utente&deviazione=admin" method="post">
		<input type="hidden" name="id_utente" value="'.$_GET[utente].'">
		<table id="lista" align="center">
		<tr><td>Nome</td><td><input type="text" name="nome_utente" value="'.$dati_utente[nome_utente].'"></td></tr>
		<tr><td>Nuova Password</td><td><input type="password" name="password" autocomplete="off"></td></tr>
		<tr><td>Conferma Password</td><td><input type="password" name="c_password" autocomplete="off"></td></tr>
		<tr><td>Ruolo Utente</td><td><select name="ruolo_utente" >
				<option value="normale"';
				if ($dati_utente[ruolo_utente] == 'normale')
					{print'selected="selected"';}
				print '>Normale</option>	<option value="amministratore"';
				if ($dati_utente[ruolo_utente] == 'amministratore')
					{print'selected="selected"';}
				print '>Amministratore</option>	<option value="osservetore"';
				if ($dati_utente[ruolo_utente] == 'osservatore')
					{print'selected="selected"';}
				print '>Osservatore</option>
			</select>
		</td></tr><tr><td>Grest</td><td>';
		$id_grest_utente = explode("-",$dati_utente[id_grest]);
		$numero_grest_utente = count($id_grest_utente);
		$grest = mysql_query("SELECT * FROM  grests WHERE id_parrocchia = $dati_utente[id_parrocchia]");
		while ($dati_grest = mysql_fetch_array($grest, MYSQL_ASSOC))
		{
			//mostra il nome del grest e il pulsante
			print $dati_grest[titolo_grest];
			print '<input type="checkbox" ';	
			for ($asdf = 0; $asdf <= $numero_grest_utente; $asdf++)
			{
				if ($id_grest_utente[$asdf] == $dati_grest[id_grest])
				{
					print 'checked ';
				}
			}
			print 'name="';
			print "id_grest_$dati_grest[id_grest]"; //il nome di ciascuna checkbox è id_grest_ e l'id del grest
			print '" value="1"><br/>'; //il valore è 1 (true)
		}
		
		print '</td></tr></table><input type="hidden" name="deviazione" value="admin">
		<input type="submit" value="modifica">
		</form>';
?>

        </div>
        
        <?php include ("pedice.php"); ?>
        
    </div>
</body> 

</html>
