<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<?php
print '
<div id="pedice">
  <table width="100%" border="0" class="pedice">
    <tr>
       <td width="10%"><a href="assistenza.php" target="_blank"><img src="immagini/assistenza.png" alt="Assistenza" name="assistenza" border="0" id="assistenza" title="Richiedi Assistenza"/></a></td>
	      <td width="80%"><br/>
            GrestOne - Software di Gestione per Gr.Est - versione 12.08 FireLand BETA<br/>
            Copyright &copy; 2012 - 2013 Contessotto Marco - Ziero Samuele<br/>
            Software libero distribuito su licenza GPL</td>
       <td width="10%"><a href="http://www.grestone.it" target="_blank"><img src="immagini/loghi_parrocchie/g_grestone.png" height="40" border="0" title="GrestOne.it"/></a></td>
    </tr>
  </table>
</div>
';
?>
