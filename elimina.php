<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>Conferma Eliminazione Campo</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
<body>
<div id="eliminato"><br/><br/><strong>
<?php
	include ("funzioni.php"); 
	verifica_amministratore($_SESSION[Grestone]);

	
	switch ($_GET[oggetto]) {
    case 'utente':
        elimina_utente($_GET[id]);
		print 'eliminato';
		break;
        
    case 'grest':
        elimina_grest($_GET[id]);
		print 'eliminato';
		break;
        
        
    case 'parrocchia':
        elimina_parrocchia($_GET[id]);
		print 'eliminato';
        break;
    
    case 'iscritto':
		mysql_query("DELETE FROM `iscritti_$_SESSION[id_grest]` WHERE `id` = $_GET[id]");
		print 'eliminato';
		break;
		
    case 'animatore':
		mysql_query("DELETE FROM `animatori_$_SESSION[id_grest]` WHERE `id` = $_GET[id]");
		print 'eliminato';
		break;

    case 'collaboratore':
		mysql_query("DELETE FROM `collaboratori_$_SESSION[id_grest]` WHERE `id` = $_GET[id]");
		print 'eliminato';
		break;
		
	
	case 'gruppo':
		$dati_grest = verifica_grest();
		mysql_query("DELETE FROM `gruppi_$_SESSION[id_grest]` WHERE `id_gruppo` = $_GET[id]");
		print 'eliminato';
		$animatori = mysql_query("SELECT * FROM animatori_$_SESSION[id_grest]");
		while($dati_animatori = mysql_fetch_array($animatori))
		{
			if ($dati_animatori[gruppo] == $_GET[id])
			{
				mysql_query("UPDATE `animatori_$_SESSION[id_grest]` SET  `gruppo` =  '0' WHERE  `animatori_$_SESSION[id_grest]`.`id` = $dati_animatori[id];");
			}
		}
		$collaboratori = mysql_query("SELECT * FROM collaboratori_$_SESSION[id_grest]");
		while($dati_collaboratori = mysql_fetch_array($collaboratori))
		{
			if ($dati_collaboratori[gruppo] == $_GET[id])
			{
				mysql_query("UPDATE `collaboratori_$_SESSION[id_grest]` SET  `gruppo` =  '0' WHERE  `collaboratori_$_SESSION[id_grest]`.`id` = $dati_collaboratori[id];");
			}
		}
		break;
		
	case 'eta':
		$dati_grest = verifica_grest();
		mysql_query("DELETE FROM `eta_$_SESSION[id_grest]` WHERE `id_eta` = $_GET[id]");
		print 'eliminato';
		$iscritti = mysql_query("SELECT * FROM iscritti_$_SESSION[id_grest]");
		while($dati_iscritti = mysql_fetch_array($iscritti))
		{
			if ($dati_iscritti[eta] == $_GET[id])
			{
				mysql_query("UPDATE `iscritti_$_SESSION[id_grest]` SET  `eta` =  '0' WHERE  `iscritti_$_SESSION[id_grest]`.`id` = $dati_iscritti[id];");
			}
		}

		$animatori = mysql_query("SELECT * FROM animatori_$_SESSION[id_grest]");
		while($dati_animatori = mysql_fetch_array($animatori))
		{
			if ($dati_animatori[eta] == $_GET[id])
			{
				mysql_query("UPDATE `animatori_$_SESSION[id_grest]` SET  `eta` =  '0' WHERE  `animatori_$_SESSION[id_grest]`.`id` = $dati_animatori[id];");
			}
		}
		
		$collaboratori = mysql_query("SELECT * FROM collaboratori_$_SESSION[id_grest]");
		while($dati_collaboratori = mysql_fetch_array($collaboratori))
		{
			if ($dati_collaboratori[eta] == $_GET[id])
			{
				mysql_query("UPDATE `collaboratori_$_SESSION[id_grest]` SET  `eta` =  '0' WHERE  `collaboratori_$_SESSION[id_grest]`.`id` = $dati_collaboratori[id];");
			}
		}
	break;
		
	case 'gita':
		$dati_grest = verifica_grest();
		$gita = mysql_query("SELECT * FROM gite_$_SESSION[id_grest] WHERE `id_gita` = $_GET[id]");
		$dati_gita = mysql_fetch_array($gita, MYSQL_ASSOC);
		if ($dati_gita[liste] == 1)
		{
			mysql_query("ALTER TABLE `animatori_$_SESSION[id_grest]`
			DROP `presenza_evento_$_GET[id]`,
			DROP `pagamento_evento_$_GET[id]`,
			DROP `note_evento_$_GET[id]`;");
			
			mysql_query("ALTER TABLE `collaboratori_$_SESSION[id_grest]`
			DROP `presenza_evento_$_GET[id]`,
			DROP `pagamento_evento_$_GET[id]`,
			DROP `note_evento_$_GET[id]`;");
			
			mysql_query("ALTER TABLE `iscritti_$_SESSION[id_grest]`
			DROP `presenza_evento_$_GET[id]`,
			DROP `pagamento_evento_$_GET[id]`,
			DROP `note_evento_$_GET[id]`;");
		}
		mysql_query("DELETE FROM `gite_$_SESSION[id_grest]` WHERE `id_gita` = $_GET[id]");
		print 'eliminato';
		break;

	case 'laboratorio':
	$dati_grest = verifica_grest();
	mysql_query("DELETE FROM `laboratori_$_SESSION[id_grest]` WHERE `id_laboratorio` = $_GET[id]");
	print 'eliminato';
	for ($a = 1; $a <= $dati_grest[laboratori_periodo]; $a++)
	{
		$laboratorio = 'laboratorio_'.$a;
		$iscritti = mysql_query("SELECT * FROM iscritti_$_SESSION[id_grest]");
		while($dati_iscritti = mysql_fetch_array($iscritti))
		{
			if ($dati_iscritti[$laboratorio] == $_GET[id])
			{
				mysql_query("UPDATE `iscritti_$_SESSION[id_grest]` SET  `$laboratorio` =  '0' WHERE  `id` = $dati_iscritti[id];");
			}
		}
		
		$animatori = mysql_query("SELECT * FROM animatori_$_SESSION[id_grest]");
		while($dati_animatori = mysql_fetch_array($animatori))
		{
			if ($dati_animatori[$laboratorio] == $_GET[id])
			{
				mysql_query("UPDATE `animatori_$_SESSION[id_grest]` SET  `$laboratorio` =  '0' WHERE  `id` = $dati_animatori[id];");
			}
		}
		$collaboratori = mysql_query("SELECT * FROM collaboratori_$_SESSION[id_grest]");
		while($dati_collaboratori = mysql_fetch_array($collaboratori))
		{
			if ($dati_collaboratori[$laboratorio] == $_GET[id])
			{
				mysql_query("UPDATE `collaboratori_$_SESSION[id_grest]` SET  `$laboratorio` =  '0' WHERE  `id` = $dati_collaboratori[id];");
			}
		}
	}
		break;

	case 'squadra':
		$dati_grest = verifica_grest();
		mysql_query("DELETE FROM `squadre_$_SESSION[id_grest]` WHERE `id_squadra` = $_GET[id]");
		print 'eliminato';
		$iscritti = mysql_query("SELECT * FROM iscritti_$_SESSION[id_grest]");
		while($dati_iscritti = mysql_fetch_array($iscritti))
		{
			if ($dati_iscritti[squadra] == $_GET[id])
			{
				mysql_query("UPDATE `iscritti_$_SESSION[id_grest]` SET  `squadra` =  '0' WHERE  `iscritti_$_SESSION[id_grest]`.`id` = $dati_iscritti[id];");
			}
		}
		$animatori = mysql_query("SELECT * FROM animatori_$_SESSION[id_grest]");
		while($dati_animatori = mysql_fetch_array($animatori))
		{
			if ($dati_animatori[squadra] == $_GET[id])
			{
				mysql_query("UPDATE `animatori_$_SESSION[id_grest]` SET  `squadra` =  '0' WHERE  `animatori_$_SESSION[id_grest]`.`id` = $dati_animatori[id];");
			}
		}
		$collaboratori = mysql_query("SELECT * FROM collaboratori_$_SESSION[id_grest]");
		while($dati_collaboratori = mysql_fetch_array($collaboratori))
		{
			if ($dati_collaboratori[squadra] == $_GET[id])
			{
				mysql_query("UPDATE `collaboratori_$_SESSION[id_grest]` SET  `squadra` =  '0' WHERE  `collaboratori_$_SESSION[id_grest]`.`id` = $dati_collaboratori[id];");
			}
		}
		break;
}
	
	
?>
		<meta http-equiv="refresh" content="3;
		URL=<?=$_SERVER['HTTP_REFERER']?>">
</strong><br/></div>
</body> 

</html>
