<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>
	<script src="script.js" type="text/javascript"></script>
</head>
<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_report.php"); ?>

<div id="contenuto">
<?php
connetti();

/*SEZIONE PRIMA: SI OCCUPA DI LEGGERE IL DB E CARICARE SU ARRAY TUTTI I DATI NECESSARI ALLA VISUALIZZAZIONE DELLA PAGINA*/
if ($dati_grest[periodo] == 1)
{
	$periodo = mysql_query("SELECT * FROM periodo WHERE id_grest=$_SESSION[id_grest]");
	$dati_periodo = mysql_fetch_array($periodo, MYSQL_ASSOC);
}
if ($dati_grest[eta] == 1)
{
	$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest]");
	while ($dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC))
	{

		$eta_selezionate[id][] = $dati_eta[id_eta];
		$eta_selezionate[nome][] = $dati_eta[nome];
		$numero_eta_selezionate++;
	}
}
if ($dati_grest[squadre] == 1)
{
	$squadre = mysql_query("SELECT * FROM squadre_$_SESSION[id_grest]");
	$impostazioni_squadre = mysql_fetch_array($squadre, MYSQL_ASSOC);
	while ($dati_squadre = mysql_fetch_array($squadre, MYSQL_ASSOC))
	{
		$squadre_selezionate[id_squadra][] = $dati_squadre[id_squadra];
		if ($impostazioni_squadre[nome])
		{$squadre_selezionate[nome][] = $dati_squadre[nome];}
		if ($impostazioni_squadre[colore])
		{$squadre_selezionate[colore][] = $dati_squadre[colore];}
		$numero_squadre_selezionate++;
	}
}
if ($dati_grest[laboratori] == 1)
{
	$laboratori = mysql_query("SELECT * FROM laboratori_$_SESSION[id_grest]");
	while ($dati_laboratori = mysql_fetch_array($laboratori, MYSQL_ASSOC))
	{
		$laboratori_selezionati[id_laboratorio][] = $dati_laboratori[id_laboratorio];
		$laboratori_selezionati[nome][] = $dati_laboratori[nome];
		$numero_laboratori_selezionati++;
	}
}
	$periodo_lab = mysql_query("SELECT laboratori_periodo FROM grests WHERE id_grest=$_SESSION[id_grest]");
	$dati_periodo_lab = mysql_fetch_array($periodo_lab, MYSQL_ASSOC);
$eventi = mysql_query("SELECT * FROM gite_$_SESSION[id_grest] WHERE liste = 1");
while ($dati_eventi = mysql_fetch_array($eventi, MYSQL_ASSOC))
{
	$eventi_selezionati[id][] = $dati_eventi[id_gita];
	$eventi_selezionati[nome][] = $dati_eventi[nome];
	$numero_eventi_selezionati++;	
}
$gruppi = mysql_query("SELECT * FROM gruppi_$_SESSION[id_grest]");
while ($dati_gruppi = mysql_fetch_array($gruppi, MYSQL_ASSOC))
{
	$gruppi_selezionati[id][] = $dati_gruppi[id_gruppo];
	$gruppi_selezionati[nome][] = $dati_gruppi[nome];
	$numero_gruppi_selezionati++;
}


/*SEZIONE SECONDA: SI OCCUPA DELLA VISUALIZZAZIONE*/

$iscanicol = array('iscritti','animatori','collaboratori');
$iscanicol[bello] = array('Animati','Animatori','Collaboratori');

print '<h3><a id="img_ElGenIs" href="javascript:mostra(\'mostra_ElGenIs\',\'img_ElGenIs\');">
<img src="immagini/piu.png" title="Espandi" border="0" /></a> Elenchi Generali Iscritti</h3>';
print '<div id="mostra_ElGenIs" style="display: none;">';
if (!isset($dati_periodo[numero_settimane]))
{$dati_periodo[numero_settimane] = 1;}
for ($a=1; $a<=$dati_periodo[numero_settimane]; $a++)
{
	if ($dati_grest[periodo] == 1)
	{
	print '<br/><b>';
	print 'Settimana '.$a;
	print '</b><br/>';}
	
	
	print '<table id="lista" align="center">';
	
	print '<tr>';
	print '<td></td>';
	$c_iscanicol=0;
	while ($c_iscanicol <3)
	{
		print '<td>'.$iscanicol[bello][$c_iscanicol].'</td>';
		$c_iscanicol++;
	}
	print '</tr>';
	$b = 0;
	do
	{
		print '<tr>';
		if ($numero_eta_selezionate <> 0)
			{print '<td>'.$eta_selezionate[nome][$b].'</td>';}
		$c_iscanicol =0;
		while ($c_iscanicol <3)
		{
			$query = "SELECT * FROM $iscanicol[$c_iscanicol]_$_SESSION[id_grest] WHERE 1";
			//print '<td><a href="elenchi_generale.php?';
			if ($dati_grest[periodo] == 1)
				{	//print '
					$query .= ' AND settimana_'.$a.'= 1 ';
				}
			if ($dati_grest[eta] == 1)
				{$query .= ' AND eta = '.$eta_selezionate[id][$b];}
			//print 'iscritti=1&animatori=1';
			//print'"></a>';
			//print '</td>';
			//print $query;
			$numero = mysql_query("$query");
			$numero_iscritti = mysql_num_rows($numero);
			print '<td><h2>'.$numero_iscritti.'</h2></td>';
			
			$somme[$c_iscanicol] = $somme[$c_iscanicol]+$numero_iscritti;
			$c_iscanicol++;
		}
		print '</tr>';
		$b++;
	}
	while ($b<$numero_eta_selezionate);
	print '<tr>';
	print '<td><h3>Totale</h3></td>';
	$c_iscanicol=0;
	while ($c_iscanicol <3)
	{
		print '<td><h2>'.$somme[$c_iscanicol].'</h2></td>';
		$c_iscanicol++;
	}
	print '</tr>';
	print '</table>';
	unset ($somme);
}
print '</div>';

if ($dati_grest[squadre] == 1)
{
print '<br/>';
print '<h3><a id="img_ElSq" href="javascript:mostra(\'mostra_ElSq\',\'img_ElSq\');">
<img src="immagini/piu.png" title="Espandi" border="0" /></a> Elenchi per Squadre</h3>';
print '<div id="mostra_ElSq" style="display: none;">';
if (!isset($dati_periodo[numero_settimane]))
{$dati_periodo[numero_settimane] = 1;}
for ($a=1; $a<=$dati_periodo[numero_settimane]; $a++)
{
	if ($dati_grest[periodo] == 1)
	{
	print '<br/><b>';
	print 'Settimana '.$a;
	print '</b><br/>';}
	print '<table id="lista" width="100%">';
	$b = 0;
	do
	{
		print '<tr>';
		if ($numero_eta_selezionate <> 0)
			{print '<td><b>'.$eta_selezionate[nome][$b].'</b></td>';}
		$c = 0;
		do
		{
			print '<td><a href="elenchi_squadre.php?';
			if ($dati_grest[periodo] == 1)
				{print 'settimana_'.$a.'=1&';}
			if ($dati_grest[eta] == 1)
				{print 'eta_'.$eta_selezionate[id][$b].'=1&';}
			print 'squadra_'.$squadre_selezionate[id_squadra][$c].'=1&';
			print 'iscritti=1&animatori=1';
			print'">'.$squadre_selezionate[nome][$c].'<br/>';
			if ($impostazioni_squadre[colore]==1)
			{print '<img src="immagini/squadre/'.$squadre_selezionate[colore][$c].'.png" alt="'.$squadre_selezionate[colore][$c].'" border="0" title="'.$squadre_selezionate[colore][$c].'"/></a>';}
			print '</td>';
			$c++;
		}
		while ($c < $numero_squadre_selezionate);
		print '</tr>';
		$b++;
	}
	while ($b<$numero_eta_selezionate);
	
	print '</table>';
}
}
print '</div>';


if ($dati_grest[laboratori] == 1)
{
print '<br/>';
print '<h3><a id="img_ElLab" href="javascript:mostra(\'mostra_ElLab\',\'img_ElLab\');">
<img src="immagini/piu.png" title="Espandi" border="0" /></a> Elenchi per Laboratori</h3>';
print '<div id="mostra_ElLab" style="display: none;">';
for ($a=1; $a<=$dati_periodo_lab[laboratori_periodo]; $a++)
{
	print '<br/><b>';
	print 'Laboratorio '.$a;
	print '</b><br/>';
	print '<table id="lista" width="100%">';
	$b = 0;
	do
	{
		print '<tr>';
		if ($numero_eta_selezionate <> 0)
			{print '<td><b>'.$eta_selezionate[nome][$b].'</b></td>';}
		$c = 0;
		do
		{
			print '<td><a href="elenchi_laboratori.php?';
			print 'periodo_'.$a.'=1&';
			if ($dati_grest[eta] == 1)
				{print 'eta_'.$eta_selezionate[id][$b].'=1&';}
			print 'laboratorio_'.$laboratori_selezionati[id_laboratorio][$c].'=1&';
			print 'iscritti=1&animatori=1';
			print'">'.$laboratori_selezionati[nome][$c].'</a>';
			print '</td>';
			$c++;
		}
		while ($c < $numero_laboratori_selezionati);
		print '</tr>';
		$b++;
	}
	while ($b<$numero_eta_selezionate);
	
	print '</table>';
}
}
print '</div>';

if ($dati_grest[gite] == 1)
{
print '<br/>';
print '<h3><a id="img_ElEv" href="javascript:mostra(\'mostra_ElEv\',\'img_ElEv\');">
<img src="immagini/piu.png" title="Espandi" border="0" /></a> Elenchi per Eventi</h3>';
print '<div id="mostra_ElEv" style="display: none;">';
$a = 0;
do
{	
	print '<br/><b>';
	print $eventi_selezionati[nome][$a];
	print '</b><br/>';
	print '<table id="lista" width="100%">';
	$b = 0;
	do
	{
		print '<tr>';
		if ($numero_eta_selezionate <> 0)
			{print '<td><b>'.$eta_selezionate[nome][$b].'</b></td>';}
		$c = 0;
		do
		{
			print '<td><a href="elenchi_gite.php?';
			print 'evento_'.$eventi_selezionati[id][$a].'=1&';
			if ($dati_grest[eta] == 1)
				{print 'eta_'.$eta_selezionate[id][$b].'=1&';}
			print 'squadra_'.$squadre_selezionate[id_squadra][$c].'=1&';
			print 'iscritti=1&animatori=1';
			print'">'.$squadre_selezionate[nome][$c].'<br/>';
			if ($impostazioni_squadre[colore]==1)
			{print '<img src="immagini/squadre/'.$squadre_selezionate[colore][$c].'.png" alt="'.$squadre_selezionate[colore][$c].'" border="0" title="'.$squadre_selezionate[colore][$c].'"/></a>';}
			print '</td>';
			$c++;
		}
		while ($c < $numero_squadre_selezionate);
		print '</tr>';
		$b++;
	}
	while ($b<$numero_eta_selezionate);
	print '</table>';
$a++;
} while($a<$numero_eventi_selezionati);
}
print '</div>';


if ($dati_grest[gruppi] == 1)
{
print '<br/>';
print '<h3><a id="img_ElGrp" href="javascript:mostra(\'mostra_ElGrp\',\'img_ElGrp\');">
<img src="immagini/piu.png" title="Espandi" border="0" /></a> Elenchi per Gruppi</h3>';
print '<div id="mostra_ElGrp" style="display: none;">';
$a = 0;
do
{	
	print '<br/><b>';
	print $gruppi_selezionati[nome][$a];
	print '</b><br/>';
	print '<table id="lista" align="center">';
	$b = 0;
	do
	{
		print '<tr>';
		if ($numero_eta_selezionate <> 0)
			{print '<td>'.$eta_selezionate[nome][$b].'</td>';}
		print '<td><a href="elenchi_gruppi.php?';
		print 'gruppo_'.$gruppi_selezionati[id][$a].'=1&';
		if ($dati_grest[eta] == 1)
			{print 'eta_'.$eta_selezionate[id][$b].'=1&';}
		print 'animatori=1&collaboratori=1';
		print'">Lista</a>';
		//print'">'.Lista.'</a>';
		print '</td>';
		print '</tr>';
		$b++;
	}
	while ($b<$numero_eta_selezionate);
$a++;
	print '</table>';
} while($a<$numero_gruppi_selezionati);
}
print '</div>';

?>
</div>
        <?php include ("pedice.php"); ?>
</div>
</body> 

</html>
