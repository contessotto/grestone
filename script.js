/*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
	
// JavaScript per GrestOne

/* Funzione cambiamento immagine */
function cambia(ImageName,ImageFile){
    ImageName.src = ImageFile;}
    

	
/* Funzione preload immagini */
// Array
var imgs = new Array(3); 
    imgs[0] = "immagini/menu/inserisci_animatore_on.png"; 
    imgs[1] = "immagini/menu/inserisci_collaboratore_on.png"; 
    imgs[2] = "immagini/menu/inserisci_iscritto_on.png"; 
    imgs[3] = "immagini/menu/liste_animati_on.png"; 
    imgs[4] = "immagini/menu/liste_animatori_on.png"; 
    imgs[5] = "immagini/menu/liste_collaboratori_on.png"; 
    imgs[6] = "immagini/menu/gestione_parrocchia_on.png"; 
    imgs[7] = "immagini/menu/gestione_gite_on.png"; 
    imgs[8] = "immagini/menu/gestione_laboratori_on.png"; 
    imgs[9] = "immagini/menu/gestione_periodo_on.png"; 
    imgs[10] = "immagini/menu/gestione_squadre_on.png";
    imgs[11] = "immagini/menu/gestione_gruppi_on.png"; 
	imgs[12] = "immagini/menu/gestione_gruppi_on.png"; 
    imgs[13] = "immagini/menu/menu_principale_on.png"; 
	imgs[14] = "immagini/home/inserisci_on.png";
    imgs[15] = "immagini/home/vedi_on.png";
    imgs[16] = "immagini/home/stampa_on.png";
    imgs[17] = "immagini/home/statistiche_on.png";
    imgs[18] = "immagini/home/impostazioni_on.png";
	imgs[19] = "immagini/menu/elenchi_predisposti_on.png";
	imgs[20] = "immagini/menu/elenchi_generali_on.png";
	imgs[21] = "immagini/menu/elenchi_laboratori_on.png";
	imgs[22] = "immagini/menu/elenchi_squadre_on.png";
	imgs[23] = "immagini/menu/elenchi_laboratori_on.png";
	imgs[24] = "immagini/menu/elenchi_eventi_on.png";
	imgs[25] = "immagini/menu/elenchi_gruppi_on.png";
	imgs[26] = "immagini/menu/stampe_predisposte_on.png";
	imgs[27] = "immagini/menu/stampe_generali_on.png";
	imgs[28] = "immagini/menu/stampe_laboratori_on.png";
	imgs[29] = "immagini/menu/stampe_squadre_on.png";
	imgs[30] = "immagini/menu/stampe_laboratori_on.png";
	imgs[31] = "immagini/menu/stampe_eventi_on.png";
	imgs[32] = "immagini/menu/stampe_gruppi_on.png";
	imgs[33] = "immagini/home/modifiche_rapide_on.png";
	imgs[34] = "immagini/menu/modifica_animati_on.png";
	imgs[35] = "immagini/menu/modifica_animatori_on.png";
	imgs[36] = "immagini/menu/modifica_collaboratori_on.png";
	imgs[37] = "immagini/meno.png";
	imgs[38] = "immagini/menu/statistiche_generali_on.png";
	imgs[39] = "immagini/menu/statistiche_squadre_on.png";
	imgs[40] = "immagini/menu/statistiche_laboratori_on.png";
	imgs[41] = "immagini/menu/statistiche_eventi_on.png";
	imgs[42] = "immagini/menu/statistiche_gruppi_on.png";
	imgs[43] = "immagini/menu/divisione_fasce_animati_on.png";
	imgs[44] = "immagini/menu/divisione_squadre_animati_on.png";
	imgs[45] = "immagini/menu/divisione_squadre_animatori_on.png";
	
	
// preload
function preload() { 
   var tmp = null; 
   for (var j = 0; j < imgs.length; j++) { 
     tmp = imgs[j]; 
     imgs[j] = new Image(); 
     imgs[j].src = tmp; 
   } 
  } 
  void(preload());
  
 /* controllo eliminazione campi accidentale */
function conferma () {
  if (!window.confirm ("ATTENZIONE: La cancellazione è un processo irreversibile!\n\nSei sicuro/a di voler cancellare?")) return false;
  return window.confirm ("Attenzione, te lo richiedo:\n\nSei DAVVERO DAVVERO sicuro/a di voler cancellare questo campo?");
}
function confermadisattiva () {
  if (!window.confirm ("ATTENZIONE: La disattivazione è un processo irreversibile!\n\nSei sicuro/a di voler disattivare e cancellare?")) return false;
  return window.confirm ("Attenzione, te lo richiedo:\n\nSei DAVVERO DAVVERO sicuro/a di voler disattivare questa funzione di GrestOne?");
}
 /* espandi e riduci div con immagine per il controllo */
//<a id="nome_link" href="javascript:mostra('nome_id','nome_link');"><img src="immagini/piu.png" title="Espandi" border="0" /></a>
//<div id="nome_id" style="display: none;">

function mostra(MostraDivNascosto, CambiaIcona) {
        var ele = document.getElementById(MostraDivNascosto);
        var imageEle = document.getElementById(CambiaIcona);
        if(ele.style.display == "block") {
                 ele.style.display = "none";
		         imageEle.innerHTML = '<img src="immagini/piu.png" title="Espandi" border="0" />';
        }
        else {
                ele.style.display = "block";
                imageEle.innerHTML = '<img src="immagini/meno.png" title="Riduci" border="0" />';
             }
}

function mostraricerca(MostraDivNascosto, CambiaIcona) {
        var ele = document.getElementById(MostraDivNascosto);
        var imageEle = document.getElementById(CambiaIcona);
        if(ele.style.display == "block") {
                 ele.style.display = "none";
		         imageEle.innerHTML = '<img src="immagini/ricerca.png" title="Espandi" border="0" /> Ricerca Rapida Iscritti';
        }
        else {
                ele.style.display = "block";
                imageEle.innerHTML = '<img src="immagini/meno.png" title="Riduci" border="0" /> Ricerca Rapida Iscritti';
             }
}
