<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
verifica_admin();
?>
	
    <div id="principale">

        <?php $impostazioni = carica_impostazioni_parrocchia();?>

        <div id="intestazione">
		<br/><h2>Pagina di Configurazione Generale di GrestOne</h2>
		</div>

        <?php include ("menu_configurazione.php"); ?>

        <div id="contenuto">
			
<?php 
if ($_GET[parrocchia]==null)
{print"<br/><br/><br/><br/><br/><br/><h2>ATTNEZIONE! Selezionare una parrocchia!!!</h2><br/><br/><br/><br/><br/><br/><br/><br/>";}
else
{
		connetti();
		print'<h2 name="utenti">Modifica Impostazioni Parrocchia</h2>';
		$parrocchia = mysql_query("SELECT * FROM parrocchie WHERE id_parrocchia = $_GET[parrocchia]");
		$dati_parrocchia = mysql_fetch_array($parrocchia, MYSQL_ASSOC);
		print '<form action="modifica.php?oggetto=parrocchia&deviazione=configurazione_parrocchie" method="post">
		<input type="hidden" name="id_parrocchia" value="'.$_GET[parrocchia].'">
		<table id="lista" align="center">
		<tr><td>Nome</td><td><input type="text" size="50" required maxlength="50" name="nome_parrocchia" value="'.$dati_parrocchia[nome_parrocchia].'"></td></tr>
		<tr><td>Mail</td><td><input type="text" size="50" required maxlength="50" name="mail_parrocchia" value="'.$dati_parrocchia[mail_parrocchia].'"></td></tr>
		</table>
		<input type="submit" value="modifica">
		</form>';	
}
?>

        </div>
        
        <?php include ("pedice.php"); ?>
        
    </div>
</body> 

</html>
