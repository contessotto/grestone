<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>Installazione di GrestOne</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
<body>
<?php
/* ATTENZIONE! QUESTO FILE RACCHIUDE LE QUERY PER LA CREAZIONE DI TUTTE LE TABELLE NECESSARIE A GRESTONE */
include("funzioni.php");
include("connessione.php");
if ($_POST[verifica] == null)
{
	print '<br/><br/><br/><br/>
	<h2>Benvenuto nella pagina di installazione di GrestOne</h2>';
	print 'Questa pagina si occupa di preparare le tabelle del database per il programma.<br/>
	Per prima cosa verifica di aver settato i parametri della connessione in modo corretto:<br/><br/>
	<span style="color: red;"><strong>
	';
	print "Indirizzo: $indirizzo<br/>";
	print "Nome database: $nome_database<br/>";
	print "Nome Utente: $nome_utente<br/>";
	print "Password: $password<br/><br/>";
	//print "Mail Contatto Tecnico Amministratore: $mail_admin</strong></span><br/><br/>";
	print 'se le informazioni <span style="color: red;">non</span> 
	sono corrette devi modificarle aprendo con un editor di testo il file
	<span style="color: red;">connessione.php</span><br/><br/>
	<br/>	';
	$connessione = @controllo_connessione();
	$selezione = @controllo_database();
	if ($connessione == false)
	{
		print '<h2>La connessione a mysql non è riuscita. Verifica i dati inseriti</h2>';
		exit;
	}
	else
	{
		if ($selezione == false)
		{
			print '<h2>La connessione con il database '.$nome_database.' non è riuscita. Verifica i dati inseriti</h2>';
			exit;
		}
	}
	print '<h2>Le informazioni relative alla connessione sono corrette.<br/> Si può procedere all\'installazione del
			grestone</h2><br/>';

	print '<form action="creazione.php" method="post">
	<input type="hidden" name="verifica" value="primo">
	<input type="submit" value="Installa Grestone">
	</form>';
}
	
	if ($_POST[verifica] == 'primo')
	{
	// creazione tabella GRESTS
connetti();
mysql_query("
CREATE TABLE IF NOT EXISTS `grests` (
  `id_grest` int(11) NOT NULL auto_increment,
  `id_parrocchia` int(5) NOT NULL,
  `titolo_grest` varchar(50) NOT NULL,
  `sottotitolo_grest` varchar(50) NOT NULL,
  `anno_grest` varchar(15) NOT NULL,
  `squadre` tinyint(1) NOT NULL,
  `periodo` tinyint(1) NOT NULL,
  `laboratori` tinyint(1) NOT NULL,
  `gite` tinyint(1) NOT NULL,
  `gruppi` tinyint(1) NOT NULL,
  `eta` tinyint(1) NOT NULL,
  `laboratori_periodo` int(11) NOT NULL,
  `laboratori_durata` int(11) NOT NULL,
  `s_squadre` tinyint(1) NOT NULL,
  `s_periodo` tinyint(1) NOT NULL,
  `s_laboratori` tinyint(1) NOT NULL,
  `s_gruppi` tinyint(1) NOT NULL,
  `s_gite` tinyint(1) NOT NULL,
  `s_eta` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id_grest`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;
");
print 'Tabella grests creata<br/>';
mysql_query("
CREATE TABLE IF NOT EXISTS `parrocchie` (
  `id_parrocchia` int(11) NOT NULL auto_increment,
  `nome_parrocchia` varchar(50) NOT NULL,
  `mail_parrocchia` varchar(50) NOT NULL,
  `logo_parrocchia` text NOT NULL,
  PRIMARY KEY  (`id_parrocchia`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;
");
print 'Tabella parrocchie creata<br/>';
mysql_query("
CREATE TABLE IF NOT EXISTS `periodo` (
  `id_grest` int(5) NOT NULL auto_increment,
  `mktime_inizio` int(11) NOT NULL,
  `mktime_fine` int(11) NOT NULL,
  `giorni_esclusi` text NOT NULL,
  `giorni_inclusi` text NOT NULL,
  `numero_settimane` int(11) NOT NULL,
  `lun` tinyint(1) NOT NULL,
  `mar` tinyint(1) NOT NULL,
  `mer` tinyint(1) NOT NULL,
  `gio` tinyint(1) NOT NULL,
  `ven` tinyint(1) NOT NULL,
  `sab` tinyint(1) NOT NULL,
  `dom` tinyint(1) NOT NULL,
  `inizi_settimane` text NOT NULL,
  `fini_settimane` text NOT NULL,
  PRIMARY KEY  (`id_grest`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;
");
print 'Tabella periodo creata<br/>';
mysql_query("
CREATE TABLE IF NOT EXISTS `registro` (
  `id_registro` int(11) NOT NULL auto_increment,
  `data` datetime NOT NULL,
  `nome_utente` varchar(25) NOT NULL,
  `id_grest` varchar(50) NOT NULL,
  `operazione` text NOT NULL,
  PRIMARY KEY  (`id_registro`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;
");
print 'Tabella registro creata<br/>';
mysql_query("
CREATE TABLE IF NOT EXISTS `utenti` (
  `id_utente` int(11) NOT NULL auto_increment,
  `nome_utente` varchar(25) NOT NULL,
  `password` varchar(50) NOT NULL,
  `id_parrocchia` int(5) NOT NULL,
  `id_grest` varchar(50) NOT NULL,
  `ruolo_utente` varchar(20) NOT NULL,
  PRIMARY KEY  (`id_utente`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;
");
print 'Tabella utenti creata<br/>';
// creazione utente amministratore
	mysql_query("
	INSERT INTO `utenti` 
	(`id_utente`, `nome_utente`, `password`, `id_parrocchia`, `id_grest`, `ruolo_utente`)
	VALUES (NULL, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', '', '', 'amministratore');
	");
print 'Utente admin creato<br/>';
	print '<h2>Tutte le tabelle sono state create</h2>
	Per poter attivare il Grestone non ti rimane altro che eliminare il file creazione.php!
	Una volta fatto <a href="index.php" >effettua il login </a>con l\'utente admin';
	
}
?>
</body> 

</html>
