<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<?php
    $calendario = mysql_query("SELECT * FROM periodo WHERE id_grest = $_SESSION[id_grest]");
    $dati_calendario = mysql_fetch_array($calendario, MYSQL_ASSOC);
    $mktime_inizio = $dati_calendario[mktime_inizio];
    $mktime_fine = $dati_calendario[mktime_fine];
    print '
	<table align="center" id="calendario">
	<tr>
	<td colspan="8">Calendario Grest</td>

	</tr>
	<tr>
		<td style="background-color:#A4A2A7;border:solid 2px #3366FF;">Settimana</td>
		<td style="background-color:#A4A2A7;border:solid 2px #3366FF;">Lun</td>
		<td style="background-color:#A4A2A7;border:solid 2px #3366FF;">Mar</td>
		<td style="background-color:#A4A2A7;border:solid 2px #3366FF;">Mer</td>
		<td style="background-color:#A4A2A7;border:solid 2px #3366FF;">Gio</td>
		<td style="background-color:#A4A2A7;border:solid 2px #3366FF;">Ven</td>
		<td style="background-color:#A4A2A7;border:solid 2px #3366FF;">Sab</td>
		<td style="background-color:#A4A2A7;border:solid 2px #3366FF;">Dom</td>
	</tr>
	<tr>';
	/* PROCEDURE PER LA PRIMA SETTIMANA */
	$contatore_settimane = 1;
	$s = date("N", $mktime_inizio);
	if ($s <> 1) //controllo per verificare se la settimana comincia di lunedì ed escludere il <td> settimana in quel caso
	{
		print '<td style="background-color:white;
		border:solid 2px #3366FF;">Set. '.$contatore_settimane.'</td>';
		$contatore_settimane++;		
	}								 // Queste due righe si occupano di creare dei <td> vuoti all'inizio della tabella
	for ($r = 1; $r < $s; $r++)		// in modo che i giorni numerici inizino nel corrispondente giorno della settimana.
		{print '<td style="background-color:white;border:0px;"></td>';}


	/* PROCEDURE PER TUTTO IL RESTO DEL CALENDARIO */

	$giorni_effettivi = 0; // contatore dei giorni effettivi
	
	$giorni_esclusi = explode("-",$dati_calendario[giorni_esclusi]);
	$numero_giorni_esclusi = count($giorni_esclusi);	

	$giorni_inclusi = explode("-",$dati_calendario[giorni_inclusi]);
	$numero_giorni_inclusi = count($giorni_inclusi);
	

	for ($i = $mktime_inizio; $i <= $mktime_fine; $i = $i + 24*3600) //ciclo che scrive i vari <td> con il numero del giorno.
	{// il ciclo sfrutta il mktime e aumenta di un giorno (24*/3600) ad ogni iterazione, fino al mktime dell'ultimo giorno.
		
		if (date("N", $i) == 1) //se il giorno è lunedì scrive il <td> della settimana
		{
			print '<td style="background-color:white;
		border:solid 2px #3366FF;">Set. '.$contatore_settimane.'</td>';
		$contatore_settimane++;
		}
		
		print '<td style="background-color:#B5BEC1;
		border:solid 2px #3366FF;';
	
		$giorni_effettivi++;
	
		if (date("z", $i) == date("z", time())) // queste due righe danno lo sfondo bianco alla casella del giorno corrnete.
			{print ' background-color:white; border-color:red;';}


		for ($a = 0; $a <= $numero_giorni_esclusi; $a++)
		{
			if ($i == $giorni_esclusi[$a])
			{
				print 'opacity:0.3;';
			}
		}
		if ($dati_grest[gite] == 1)
		{		
			$gite = mysql_query("SELECT * FROM  `gite_$_SESSION[id_grest]`");//visualizza le gite
			while ($dati_gite = mysql_fetch_array($gite, MYSQL_ASSOC))
			{
				if ($i == $dati_gite[giorno])
				{print 'background-color:#ff4444;';}
			}
		}
	/* if (date("N", $i) == 7)
	{print ' style="background-color:red;"';}
	
	if (date("N", $i) == 6)
	{print ' style="background-color:blue;"';} */
	
	/* le seguenti righe si occupano di verificare se il giorno della settimana che si stà considerando è un giorno di grest
	 * oppure no (confrontando con quelli indicati nel form). Se no i giorni vengono mostrati in trasparenza.
	*/

		$nomi_giorni = array ('ciao','lun', 'mar', 'mer', 'gio', 'ven', 'sab', 'dom'); //per avere i nomi in italiano dei giorni. 
		$num = date("N", $i);	// indica il numero del giorno della settimana (1=>lun...7=>dom).
		$z = $nomi_giorni[$num]; // $z è il nome del giorno della settimana.
		$c = 0;
		if ($dati_calendario[$z] == 0)	// $_POST[$z] è il nome del campo del form in cui è richiesto quel giorno della settmana.
							// se è 1 (true) il giorno fa parte del grest, altrimenti no.
		{

			for ($a = 0; $a <= $numero_giorni_inclusi; $a++)
			{
				if ($i == $giorni_inclusi[$a])
				{
				$c = 1;
				}
			}
		if ($c == 0)
			{
				print 'opacity:0.3;';				
			}
			
			$giorni_effettivi--; //se quel giorno non c'è il grest allora non viene contato tra i giorni effettivi.
		}	
		print '"';
		print '>';
		if ($dati_grest[gite] == 1)
		{
			$gite = mysql_query("SELECT * FROM  `gite_$_SESSION[id_grest]`");//visualizza le gite
			while ($dati_gite = mysql_fetch_array($gite, MYSQL_ASSOC))
			{
				if ($i == $dati_gite[giorno])
				{
					print '<a href="gestione_gite.php" title="'.$dati_gite[nome].'">';
					$d=1;
				}
			}
		}
		print date("j", $i); // scrive il giorno in numero
		if ($d == 1)
		{
			print '</a>';
			$d=0;
		}
		print '</td>';
	
		if (date("N", $i) == 7) // se il giorno è una domenica chiude anche la riga e inizia quella dopo.
		{	
			print '</tr><tr>';
		}	
	}
	
	print '</tr>';
	print '</table>';
?>
