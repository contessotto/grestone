<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<?php
session_start();
unset ($_SESSION[id_grest]);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
	<style>
	tr.modifica{display:none;}
	</style>
<script src="script.js" type="text/javascript"></script>
	        <script>
        	function modifica(id,oggetto)
        	{document.getElementById('modifica_'+oggetto+'_'+id).style.display = 'table-row';}
        	function esci(id,oggetto)
        	{document.getElementById('modifica_'+oggetto+'_'+id).style.display = 'none';}        
        </script>
<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
?>

<?php
verifica_amministratore();
?>
	
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

        <?php $impostazioni = carica_impostazioni_parrocchia();?>

        <div id="intestazione">
		<img id="logo" src='<?php print "$impostazioni[logo_parrocchia]"?>' alt="logo parrocchia" style="border: 3px solid #E1EAF0;"/>
		<h1><?php print "$impostazioni[nome_parrocchia]"?></h1>
		</div>

	<div id="menu"><?php include ("dataora.php"); ?><a href="home.php"><img src="immagini/menu/menu_principale.png" alt="MENU PRINCIPALE" name="menu" border="0" onmouseover="cambia(menu,'immagini/menu/menu_principale_on.png')" onmouseout="cambia(menu,'immagini/menu/menu_principale.png')"/></a></div>

        <div id="contenuto">
			
<?php 
connetti();

//PARTE 1: modifica del logo (contiene sia il form che lo svolgimento dell'operazione)

	$doc=$_FILES[logo_parrocchia];
	print '<h2>Personalizzazione Parrocchia</h2>
		<form enctype="multipart/form-data" action="personalizzazione_parrocchia.php" method="post">';
	
	if ($doc[error]==4 or $doc==null)
	{
		print'
		<input type="hidden" name="MAX_FILE_SIZE" value="100000000"><br/>
		Seleziona un logo della parrocchia:<br/>
		<input name="logo_parrocchia" type="file"> <br/><br/>';
	}
		else
	{
		$estensione = explode(".",$doc[name]);
		if ($estensione[1] == 'php' or $estensione[1] == 'html' or $estensione[1] == 'htm' or $estensione[1] == 'js' or $estensione[1] == 'conf' or
		$estensione[1] == 'PHP' or $estensione[1] == 'HTML' or $estensione[1] == 'HTM' or $estensione[1] == 'JS' or $estensione[1] == 'CONF')
		{
			print '<err><strong>ATTENZIONE! Si sta cercando di caricare un file potenzialmente pericoloso per il sistema.<br/> Il caricamento
			di questi files non è consentito.</strong></err></div>';
			include ("pedice.php");
			include("connessione.php");
			$messaggio =  "ATTENZIONE: TENTATIVO DI CARICAMENTO FILE PERICOLOSO ($doc[name]) DA PARTE DELL'UTENTE:
	$dati_utente[id_utente] - $dati_utente[nome_utente] - DELLA PARROCCHIA CON ID: $dati_utente[id_parrocchia], DENOMINATA: $impostazioni[nome_parrocchia]";
			mail($mail_admin , 'GRESTONE', $messaggio, 'from: noreply@grestone.it');
			exit;

		}
		if ($estensione[1] != 'jpg' and $estensione[1] != 'png' and $estensione[1] != 'gif'
		  and $estensione[1] != 'JPG' and $estensione[1] != 'PNG' and $estensione[1] != 'GIF')
		{
			print '<err><strong>Attenzione: Possono essere caricati solo file con estensione jpg, gif o png. <br/><a href="personalizzazione_parrocchia.php">Riprova e cambia file</a></strong></err></div>';
			include ("pedice.php");
			exit;
		}
		
		else
		{
			
			if ($doc[error]==1 or $doc[error]==2) 
				{print'<err><strong>Ops!</strong></err><br/><h3>Ci dispiace ma la dimensione massima consentita per il logo è di 100 mb</h3>';}
			if ($doc[error]==3)
				{print'<err><strong>Ops!</strong></err><br/><h3>C\'è stato un errore nel caricameto del logo. <a href="#">Riprova!</a></h3>';}
			if ($doc[error]==0)
			{
			
				move_uploaded_file("$doc[tmp_name]","immagini/loghi_parrocchie/parrocchia_$dati_utente[id_parrocchia].$estensione[1]");
			//AGGIUNGERE LIBRERIA TRATTAMENTO IMMAGINI
			include "SmartImage.class.php";
			
			//se è jpeg, controllo bitrate
			if ($estensione[1] == 'jpg' or $estensione[1] == 'JPG')
			{
			   $img = new SmartImage("immagini/loghi_parrocchie/parrocchia_$dati_utente[id_parrocchia].$estensione[1]");
			   $img->saveImage("immagini/loghi_parrocchie/parrocchia_$dati_utente[id_parrocchia].jpg");
			   $directoryimmagine = "immagini/loghi_parrocchie/parrocchia_$dati_utente[id_parrocchia].$estensione[1]";
			   unlink($directoryimmagine);
			}
			//se è png, converto in jpg
			if ($estensione[1] == 'png' or $estensione[1] == 'PNG')
			{
			   $img = new SmartImage("immagini/loghi_parrocchie/parrocchia_$dati_utente[id_parrocchia].$estensione[1]");
			   $img->saveImage("immagini/loghi_parrocchie/parrocchia_$dati_utente[id_parrocchia].jpg");
			   $directoryimmagine = "immagini/loghi_parrocchie/parrocchia_$dati_utente[id_parrocchia].$estensione[1]";
			   unlink($directoryimmagine);
			}
			//se è gif, converto in jpg
			if ($estensione[1] == 'gif' or $estensione[1] == 'GIF')
			{
			   $img = new SmartImage("immagini/loghi_parrocchie/parrocchia_$dati_utente[id_parrocchia].$estensione[1]");
			   $img->saveImage("immagini/loghi_parrocchie/parrocchia_$dati_utente[id_parrocchia].jpg");
			   $directoryimmagine = "immagini/loghi_parrocchie/parrocchia_$dati_utente[id_parrocchia].$estensione[1]";
			   unlink($directoryimmagine);
			}
				
		print'<h2><img src="immagini/ico_ok.png"/> LOGO CARICATO CORRETTAMENTE <img src="immagini/ico_ok.png"/></h2>
		<meta http-equiv="refresh" content="1;
		URL=personalizzazione_parrocchia.php">';
				mysql_query ("UPDATE  `parrocchie` SET  `logo_parrocchia` =  'immagini/loghi_parrocchie/parrocchia_$dati_utente[id_parrocchia].jpg' WHERE  `parrocchie`.`id_parrocchia` = $dati_utente[id_parrocchia]; ");
				registro($dati_utente[nome_utente],$_SESSION[id_grest],"Caricamento logo $doc[name]");
			}
		}
	}
	
//PARTE 2: modifica del nome (contiene sia il form che lo svolgimento dell'operazione)
	
	if($_POST[nome_parrocchia] == null)
	{
		print'Nome della parrocchia:<br/>
		<input name="nome_parrocchia" type="text"> <br/><br>';
	}
	else
	{
		mysql_query ("UPDATE  `parrocchie` SET  `nome_parrocchia` =  '$_POST[nome_parrocchia]'
		 WHERE  `parrocchie`.`id_parrocchia` = $dati_utente[id_parrocchia]; ");
		mysql_query("INSERT INTO `registro` (`data` ,`nome_utente` ,`id_parrocchia`,`operazione`)
		VALUES ('$data', '$dati_utente[nome_utente]','$dati_utente[id_parrocchia]', 
		'Modifica nome parrocchia in $_POST[nome_parrocchia]');");
		print'<h2><img src="immagini/ico_ok.png"/> NOME MODIFICATO CORRETTAMENTE <img src="immagini/ico_ok.png"/></h2>
		<meta http-equiv="refresh" content="1;
		URL=personalizzazione_parrocchia.php">';
	}		
	
	print '
	<input type="submit" value="Applica modifiche"><br/>
	</form>
	<br/><br/><br/><br/>';
		
//PARTE 3: gestione dei Grest
		
	print'<h2 name="grest">Gestione Grest Parrocchia</h2>';
	$grest = mysql_query("SELECT * FROM grests WHERE  `grests`.`id_parrocchia` =$dati_utente[id_parrocchia]");
	$righe_grest = mysql_num_rows($grest);
	if ($righe_grest == 0) //se non ci sono grest nella tabella
	{
		print'<h4>Nessun Grest inserito</h4>';
	}
	else // se invece ce no sono
	{//scrive l'intestazione della tabella
		print'
		<table id="lista" width="100%"><thead>
		<tr>
		<th scope="col">ID</th>
		<th scope="col">TITOLO</th>
		<th scope="col">SOTTOTITOLO</th>
		<th scope="col">ANNO</th>
		<th scope="col">MODIFICA</th>
		<th scope="col">ELIMINA</th>
		</tr></thead><tbody>';
		//ciclo per mostrare tutti i grest
		while ($impostazioni_grest = mysql_fetch_array($grest, MYSQL_ASSOC))
		{	// per recuperare i nomi delle parrocchie
			$parrocchia = mysql_query("SELECT * FROM parrocchie 
			WHERE id_parrocchia = $impostazioni_grest[id_parrocchia]");
			$impostazioni_parrocchia = mysql_fetch_array($parrocchia, MYSQL_ASSOC);
			print"
			<tr>
			<td>$impostazioni_grest[id_grest]</td>
			<td>$impostazioni_grest[titolo_grest]</td>
			<td>$impostazioni_grest[sottotitolo_grest]</td>
			<td>$impostazioni_grest[anno_grest]</td>";
			print '<td><a onclick="modifica('.$impostazioni_grest[id_grest].',\'grest\');" class="elimina" href="#grest"><img src="immagini/modifica.png" alt="modifica" border="0" title="Modifica"/></a></td>';
			print '<td><a class="elimina"href="elimina.php?'.
			"oggetto=grest&id=$impostazioni_grest[id_grest]".
			'" onclick="return conferma ();"><img src="immagini/ico_no.png" alt="elimina" border="0" title="Elimina"/></a></td></tr>';
			
			
		// riga nascosta della tabella che si occupa delle modifiche
			print'<form action="modifica.php?oggetto=grest&deviazione=parrocchia" method="post">	
			<tr class="modifica" id="modifica_grest_'.$impostazioni_grest[id_grest].'">
			<td>MODIFICA</td>
			<td><input type="text" size="5"name="titolo_grest" value="'.$impostazioni_grest[titolo_grest].'"></td>
			<td><input type="text" size="5"name="sottotitolo_grest"  value="'.$impostazioni_grest[sottotitolo_grest].'"></td>
			<td><input type="text" size="5"name="anno_grest"  value="'.$impostazioni_grest[anno_grest].'"></td>
			<td><input type="hidden" name="id_grest" value="'.$impostazioni_grest[id_grest].'">
			<a class="elimina"href="#grest" onclick="esci('.$impostazioni_grest[id_grest].',\'grest\');">abbandona modifiche</a>
			</td>				
			<td><input type="submit" value="modifica"></td>
			</tr>	
			</form>';
		}
		print'<tbody></table><br/>';
	}
	
//PARTE 4: inserimento del grest (contiene sia il form che lo svolgimento dell'operazione)

	// se tutti i campi sono compilati li inserisce nel database
	if ($_POST[titolo_grest]<> null AND $_POST[sottotitolo_grest] <> null AND $_POST[anno_grest] <> null)
	{
		registro("$_dati_utente[nome_utente]", "","inserisce il grest $_POST[titolo_grest]");
		
		mysql_query("
		INSERT INTO  `grests` 
		(`id_grest` ,`id_parrocchia` ,`titolo_grest`,`sottotitolo_grest` ,`anno_grest`)
		VALUES (NULL ,'$dati_utente[id_parrocchia]', '$_POST[titolo_grest]',  '$_POST[sottotitolo_grest]','$_POST[anno_grest]');
		");
		$ultimo_id = mysql_insert_id();
		
		//riga che si occupa di inserire il grest tra quelli controllati dall'utente.
		if ($dati_utente[id_grest] == null)
		{$nuovo_id_grest = $ultimo_id;}
		else
		{$nuovo_id_grest = $dati_utente[id_grest].'-'.$ultimo_id;}
		mysql_query("UPDATE  `utenti` SET  `id_grest` =  '$nuovo_id_grest' WHERE  `id_utente` = $_SESSION[Grestone];");
		
		$a = 'iscritti_'.$ultimo_id;
		mysql_query("
		CREATE TABLE  `$a` (`id` INT( 11 ) NOT NULL AUTO_INCREMENT, `nome` VARCHAR( 20 ) NOT NULL ,
		`cognome` VARCHAR( 20 ) NOT NULL , `sesso` CHAR( 1 ) NOT NULL , `nascita` INT( 20 ) NOT NULL ,
		`classe` VARCHAR( 10 ) NOT NULL , `indirizzo` VARCHAR( 30 ) NOT NULL , `paese` VARCHAR( 30 ) NOT NULL ,
		`telefono` VARCHAR( 20 ) NOT NULL , `cellulare` VARCHAR( 20 ) NOT NULL, `note` TEXT NOT NULL,
		FULLTEXT (nome, cognome) ,
		PRIMARY KEY (  `id` )) 
		");
		$b = 'animatori_'.$ultimo_id;
		mysql_query("
		CREATE TABLE  `$b` (`id` INT( 11 ) NOT NULL AUTO_INCREMENT, `nome` VARCHAR( 20 ) NOT NULL ,
		`cognome` VARCHAR( 20 ) NOT NULL , `sesso` CHAR( 1 ) NOT NULL , `nascita` INT( 20 ) NOT NULL ,
		`classe` VARCHAR( 10 ) NOT NULL , `indirizzo` VARCHAR( 30 ) NOT NULL , `paese` VARCHAR( 30 ) NOT NULL ,
		`telefono` VARCHAR( 20 ) NOT NULL , `cellulare` VARCHAR( 20 ) NOT NULL, `note` TEXT NOT NULL,
		FULLTEXT (nome, cognome) ,
		PRIMARY KEY (  `id` )) 
		");
		$c = 'collaboratori_'.$ultimo_id;
		mysql_query("
		CREATE TABLE  `$c` (`id` INT( 11 ) NOT NULL AUTO_INCREMENT, `nome` VARCHAR( 20 ) NOT NULL ,
		`cognome` VARCHAR( 20 ) NOT NULL ,`indirizzo` VARCHAR( 30 ) NOT NULL , `paese` VARCHAR( 30 ) NOT NULL ,
		`telefono` VARCHAR( 20 ) NOT NULL , `cellulare` VARCHAR( 20 ) NOT NULL, `note` TEXT NOT NULL,
		FULLTEXT (nome, cognome) ,
		PRIMARY KEY (  `id` )) 
		");		
		mysql_query("
		INSERT INTO `periodo` 
		(`id_grest`, `mktime_inizio`, `mktime_fine`, `lun`, `mar`, `mer`, `gio`, `ven`, `sab`, `dom`)
		 VALUES (NULL, '0', '0', '0', '0', '0', '0', '0', '0', '0');
		 ");
		 print 'GREST INSERITO CORRETTAMENTE';
		 print '<meta http-equiv="refresh" content="0;URL='.$_SERVER['HTTP_REFERER'].'">';
	}
	else // se non tutti i campi del form sono compilati:
	{		
		// se solo qualcuno è compilato:
		if ($_POST[titolo_grest] <> null OR $_POST[sottotitolo_grest] <> null OR $_POST[anno_grest] <> null) 
		
		{
			print 'NON TUTTI I CAMPI SONO COMPILATI';
		}
		else // se nessun campo è compilato:
		{ 
			print'
			<h3>Aggiungi Grest</h3>
			<form action="personalizzazione_parrocchia.php" method="post">
			<br/>
			Titolo Grest: <input type="text" name="titolo_grest"><br/><br/>
			Sottotitolo Grest: <input type="text" name="sottotitolo_grest"><br/><br/>
			Anno Grest: <input type="text" name="anno_grest"><br/><br/>
			<input type="submit" value="crea">
			</form>
			<br/><br/><br/><br/>';
		}
	}
	
//PARTE 5: gestione degli utenti (contiene sia il form che lo svolgimento dell'operazione)

		print'<h2 name="utenti">Gestione Utenti Parrocchia</h2>';
		$conto = mysql_query("SELECT * FROM utenti WHERE id_parrocchia = $dati_utente[id_parrocchia]");//conto il numero di utenti
		$righe = mysql_num_rows($conto);
		print'
		<table id="lista" width="100%"><thead>
		<tr>
		<th scope="col">ID</th>
		<th scope="col">NOME</th>
		<th scope="col">GREST</th>
		<th scope="col">RUOLO</th>
		<th scope="col">MODIFICA</th>
		<th scope="col">ELIMINA</th>
		</tr></thead><tbody>';
		//ciclo per  estrarre e mostrare gli utenti
		while ($impostazioni_utente = mysql_fetch_array($conto, MYSQL_ASSOC))
		{	
			$parrocchia = mysql_query("SELECT * FROM parrocchie 
			WHERE id_parrocchia = $impostazioni_utente[id_parrocchia]");
			$impostazioni_parrocchia = mysql_fetch_array($parrocchia, MYSQL_ASSOC);
						
			
			$id_grest_utente = explode("-",$impostazioni_utente[id_grest]);
			$numero_grest_utente = count($id_grest_utente);
			
			
			$i  = 0;
			
			print"
			<tr>
			<td>$impostazioni_utente[id_utente]</td>
			<td>$impostazioni_utente[nome_utente]</td>
			<td>";
			if ( $impostazioni_utente[id_grest] != null)
			{
				while ($i < $numero_grest_utente)
				{
					$grest = mysql_query("SELECT * FROM grests 
					WHERE id_grest = $id_grest_utente[$i]");
					$impostazioni_grest = mysql_fetch_array($grest, MYSQL_ASSOC);
					print "$impostazioni_grest[titolo_grest]<br/>";
					$i++;
				}
			}
			else
			{
				print 'NESSUN GREST';
				}

			print"
			</td>
			<td>$impostazioni_utente[ruolo_utente]</td>";
			print '<td><a href="modifica_utente.php?utente='.$impostazioni_utente[id_utente].'"><img src="immagini/password.png" alt="Modifica Nome Utente e Password" title="Modifica Nome Utente e Password" border="0"/></a>';
			print '<a onclick="modifica('.$impostazioni_utente[id_utente].',\'utente\');"class="elimina"href="#utenti"><img src="immagini/modifica.png" alt="modifica" title="Modifica" border="0"/></a></td>';
			print '<td><a class="elimina"href="elimina.php?'.
			"oggetto=utente&id=$impostazioni_utente[id_utente]".
			'" onclick="return conferma ();"><img src="immagini/ico_no.png" alt="elimina" title="Elimina" border="0"/></a></td></tr>';
			
			
			// riga nascosta della tabella che si occupa delle modifiche
			print'<form action="modifica.php?oggetto=utente&deviazione=parrocchia" method="post">	
			<tr class="modifica" id="modifica_utente_'.$impostazioni_utente[id_utente].'">
			<td>MODIFICA</td>
			<td><input type="text" size="5" name="nome_utente" value="'.$impostazioni_utente[nome_utente].'"></td>
			<td nowrap>';
			$grest = mysql_query("SELECT * FROM  grests WHERE id_parrocchia = $impostazioni_utente[id_parrocchia]");
			while ($impostazioni_grest = mysql_fetch_array($grest, MYSQL_ASSOC))
			{	
				//mostra il nome del grest e il pulsante
				print $impostazioni_grest[titolo_grest];
				print '<input type="checkbox" ';	
				for ($asdf = 0; $asdf <= $numero_grest_utente; $asdf++)
				{
					if ($id_grest_utente[$asdf] == $impostazioni_grest[id_grest])
					{
						print 'checked ';
					}
				}
				print 'name="';
				print "id_grest_$impostazioni_grest[id_grest]"; //il nome di ciascuna checkbox è id_grest_ e l'id del grest
				print '"value="1"><br/>'; //il valore è 1 (true)
			}
			print'
			</td>
			<td><select name="ruolo_utente" >
				<option value="normale"';
				if ($impostazioni_utente[ruolo_utente] == 'normale')
					{print'selected="selected"';}
				print '>Normale</option>	<option value="amministratore"';
				if ($impostazioni_utente[ruolo_utente] == 'amministratore')
					{print'selected="selected"';}
				print '>Amministratore</option>	<option value="osservetore"';
				if ($impostazioni_utente[ruolo_utente] == 'osservatore')
					{print'selected="selected"';}
				print '>Osservatore</option>
			</select>
			</td>
			<td><input type="hidden" name="id_utente" value="'.$impostazioni_utente[id_utente].'">
			<a class="elimina"href="#utenti" onclick="esci('.$impostazioni_utente[id_utente].',\'utente\');">abbandona modifiche</a>
			</td>
			<td><input type="submit" value="modifica"></td>
			</tr>	
			</form>';
	
		}
		print'</tbody></table>
		<br/><br/><br/><br/>';
		
		//form per l'aggiuta di uno o più utenti
		print'
		<h2>Aggiungi Utente</h2>
		<br/><br/>
		<form action="crea_utente.php" method="post">
		<h3>Nome Utente:</h3> <input type="text" name="nome_utente"><br/><br/>
		<h3>Password:</h3> <input type="password" name="password"><br/><br/>
		<h3>Conferma password:</h3> <input type="password" name="cpassword"><br/><br/>
			
		<h3>Grest/s:</h3>';
				
			//ciclo per mostrare i vari grest della parrocchia
			$grest = mysql_query("SELECT * FROM grests WHERE  `grests`.`id_parrocchia` =$dati_utente[id_parrocchia]");
			while ($impostazioni_grest = mysql_fetch_array($grest, MYSQL_ASSOC))
			{	
				//mostra il nome del grest e il pulsante
				print ' --> '.$impostazioni_grest[titolo_grest];
				print '<input type="checkbox" name="';
				print "id_grest_$impostazioni_grest[id_grest]"; //il nome di ciascuna checkbox è id_grest_ e l'id del grest
				print '"value="1"><br/>'; //il valore è 1 (true)
			}
			print'<br/>';
		
		
		print'
		<h3>Ruolo:</h3>
		<select name="ruolo_utente" >
		<option value="normale" selected="selected">Normale</option>
		<option value="amministratore">Amministratore</option>
		<option value="osservetore">Osservatore</option>
		</select>
			
		<br/><br/><br/>
						
		<input type="submit" value="Inserisci Utente">
		</form>';

?>

        </div>
        
        <?php include ("pedice.php"); ?>
        
    </div>
</body> 

</html>
