--------GrestOne Sofrware Gestione Grest-------------

Se state installando la versione server di GrestOne potete trovare le indicazioni per l'installazione nel file "Installazione.txt".
Per quanto riguarda la versione portable non è necessaria alcuna installazione e il programma è già preimpostato.

Per eventuali problemi, osservezioni, consigli o quant'altro potete contattare gli sviluppatori Marco e Samuele tramite gli indirizzi email:
marco.contessotto@gmail.com e ziosam.web@gmail.com

GrestOne è un software libero, rilasciato con licenza GPL (General Public License).
Ciò vuol dire che POTETE UTILIZZARLO per qualsiasi scopo, DISTRIBUIRLO a chiunque, MODIFICARLO secondo le vostre esigenze e PUBBLICARE le vostre modifiche ad esso senza bisogno di alcuna autorizzazione aggiuntiva rispetto alla GPL. Trovate il testo ufficiale della licenza nel file COPYING.txt.

Chiunque desiderasse contibuire al progetto è bene accolto! Se volete sviluppare una vostra modifica al GrestOne siamo disponibili per qualsiasi indicazione sul codice originale e sul funzionamento del programma. Saremo molto felici di inserire nella prossima versione del GrestOne tutte le modifiche, le correzioni, le migliorie che avrete fatto. Contattateci tramite le nostre email o nel forum del sito www.grestone.it
