<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>
	<script src="script.js" type="text/javascript"></script>
</head>
<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_modifiche.php"); ?>

<div id="contenuto">
<?php
print '<br/><h2>Modifiche Rapide</h2>';
	
if ($_POST[passaggi] == '')
{
	print '<form action="modifiche_rapide.php" method="post" name="modifiche_rapide">';
	
	print '<br/>Seleziona i parametri per effettuare le modifiche:</br><table id="lista" align="center"><tr>';
	if ($dati_grest[squadre]==1)
	{print '<tr><td><input type="checkbox" name="squadre" value="1"></td><td><span onClick="document.modifiche_rapide.squadre.checked=(! document.modifiche_rapide.squadre.checked);">Squadre</span></td></tr>';}
	if ($dati_grest[laboratori]==1)
	{print '<tr><td><input type="checkbox" name="laboratori" value="1"></td><td><span onClick="document.modifiche_rapide.laboratori.checked=(! document.modifiche_rapide.laboratori.checked);">Laboratori</span></td></tr>';}
	//if ($dati_grest[gruppi]==1)
	//{print 'Gruppi: <input type="checkbox" name="gruppi" value="1"></br>';}
	if ($dati_grest[eta]==1)
	{print '<tr><td><input type="checkbox" name="eta" value="1"></td><td><span onClick="document.modifiche_rapide.eta.checked=(! document.modifiche_rapide.eta.checked);">Età</span></td></tr>';}
	print '</table>';
	print '<input type="hidden" name="passaggi" value="primo">';
	print '<br/><br/>Seleziona la categoria su cui effettuare le modifiche:<br/>';
    print '<table id="lista" align="center">';
	print '<tr><td><input type="radio" name="categoria" value="iscritti" required></td><td>Iscritti</td></tr>
		   <tr><td><input type="radio" name="categoria" value="animatori" required></td><td>Animatori</td></tr>
		   <tr><td><input type="radio" name="categoria" value="collaboratori" required></td><td>Collaboratori</td></tr>';
    print '</table>';
	print '</br></br><input type="submit" value="passa alle modifiche">';
	print '</form>';
}



do 
{
if ($_POST[passaggi] == 'primo')
{	
	$numero_per_pagina = 3;
	
	$id_iniziale = $_POST[id_iniziale];
	if ($_POST[id_iniziale]==0)
	{$id_iniziale = 0;}
	$conto_iscritti = mysql_query("SELECT * FROM iscritti_$_SESSION[id_grest]");
	$iscritti_totali = mysql_num_rows($conto_iscritti);
	if ($id_iniziale >= $iscritti_totali)
	{
		print '<h2>FINE DELL\'ELENCO</h2>';
		break;
	}
	if ($_POST[id_iniziale] != null)
	{
		//INSERIMENTO NEL DB DEI DATI	
		for ($b = 1; $b <=$numero_per_pagina; $b++)
		{
			$eta = $_POST["$b-eta"];
			$squadra = $_POST["$b-squadra"];
		
			$query_inserimento = "UPDATE  iscritti_$_SESSION[id_grest] SET ";
			if ($_POST[eta] == 1)
			{
				$query_inserimento .= "eta =  $eta ";
				if ($_POST[squadre] == 1 OR $_POST[laboratori] == 1) $query_inserimento .= ',';
			}
			if ($_POST[squadre] == 1)
			{
				$query_inserimento .= "squadra =  $squadra ";
				if ($_POST[laboratori] == 1) $query_inserimento .= ',';
			}	
			if ($_POST[laboratori] == 1)			
			{
				for ($c =1;$c<=$dati_grest[laboratori_periodo];$c++)
				{
					$laboratorio = $_POST["$b-laboratorio_$c"];
					$query_inserimento .= "laboratorio_$c =  $laboratorio ";
					if ($c <> $dati_grest[laboratori_periodo]) $query_inserimento .=',';
				}
			}
			
			$query_inserimento .= ' WHERE id = '.$_POST[$b];
			print $query_inserimento;
			print '<br>';
			mysql_query("$query_inserimento");
			unset($query_inserimento);
			unset($array_query_inserimento);
		}
	}
	print '
	<form action="modifiche_rapide.php" method="post">
	<table id="lista"><thead>
	<tr>
	<th scope="col">NOME</th>
	<th scope="col">COGNOME</th>
	<th scope="col">CLASSE</th>';
	if ($_POST[squadre]==1)
	{
		print '<th scope="col">SQUADRA</th>';		
	}
	if ($_POST[laboratori]==1)
	{
		print '<th scope="col">LABs</th>';		
	}
	//if ($_POST[gruppi]==1)
	//{
	//	print '<th scope="col">GRUPPI</th>';		
	//}
	if ($_POST[eta]==1)
	{
		print '<th scope="col">ETA</th></tr></thead><tbody>';		
	}
	$query = 'SELECT * FROM iscritti_'.$_SESSION[id_grest].' LIMIT '.$id_iniziale.','.$numero_per_pagina;
	print $query;
	$iscritti = mysql_query("$query");
	
	$numero_query==0;
	//INIZIO DEL CICLO DI VISUALIZZAZIONE
	while ($dati_iscritti = mysql_fetch_array($iscritti, MYSQL_ASSOC))
	{
		$numero_query++;
		print '<tr>
		<td>'.$dati_iscritti[nome].'</td>
		<td>'.$dati_iscritti[cognome].'</td>
		<td>'.classe($dati_iscritti[classe]).
		'<input type="hidden" name="'.$numero_query.'" value="'.$dati_iscritti[id].'"></td>';
	
	
		if ($_POST[squadre]==1)
		{
			print '<td>';
			$squadra = mysql_query("SELECT * FROM squadre_$_SESSION[id_grest]");
			$impostazioni_squadre = mysql_fetch_array($squadra, MYSQL_ASSOC);
			print '<select name="'.$numero_query.'-squadra">
			<option value="">NESSUNA SQUADRA  <img src="immagini/ico_no.png"/></option>';
			while ($dati_squadra = mysql_fetch_array($squadra, MYSQL_ASSOC))
			{
				print '<option value="'.$dati_squadra[id_squadra].'"';
				if ($dati_iscritti[squadra] == $dati_squadra[id_squadra])
					{print 'selected ';}
				if ($impostazioni_squadre[colore]==1)
					{print 'style="background:'.$dati_squadra[colore].'"';}	
				print'>';
				if ($impostazioni_squadre[nome]==1)
					{print "$dati_squadra[nome] "; }
				else
					{print "$dati_squadra[colore]";}					
				print '</option>';	
			}
			print'</select></td>';		
		}
	
		if ($_POST[laboratori]==1)
		{
			print '<td>';
			for ($a=1; $a<=$dati_grest[laboratori_periodo]; $a++)
			{
				print 'Lab. '.$a.':<select name="'.$numero_query.'-laboratorio_'.$a.'">';
				$laboratori = mysql_query("SELECT * FROM laboratori_$_SESSION[id_grest]");
				print '<option value="">NESSUN LABORATORIO <img src="immagini/ico_no.png"/></option>';
				while ($dati_laboratori = mysql_fetch_array($laboratori, MYSQL_ASSOC))
				{
					print '<option value="'.$dati_laboratori[id_laboratorio].'"';
					$laboratorio = 'laboratorio_'.$a;
					if ($dati_iscritti[$laboratorio] == $dati_laboratori[id_laboratorio])
						{print 'selected';}
					print '>'.
					$dati_laboratori[nome]
					.'</option>';
				}
				print'</select></br>';
			}	
			print '</td>';
		}
		//if ($_POST[gruppi]==1)
		//{
		//	print '<td>'.$dati_iscritti[gruppo].'</td>';
		//}
		if ($_POST[eta]==1)
		{
			print '<td>';
			print 'Età: <select name="'.$numero_query.'-eta">';
			$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest]");
			print '<option value="">NESSUNA FASCIA D\'ETA <img src="immagini/ico_no.png"/></option>';
			while ($dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC))
			{
				print '<option value="'.$dati_eta[id_eta].'"';
				if ($dati_iscritti[eta] == $dati_eta[id_eta])
					{print 'selected';}
				print '>'.
				$dati_eta[nome]
				.'</option>';
			}
			print'</select><br/>';
			print '</td>';
		}
	}
	print '
	<input type="hidden" name="squadre" value="'.$_POST[squadre].'">
	<input type="hidden" name="eta" value="'.$_POST[eta].'">
	<input type="hidden" name="laboratori" value="'.$_POST[laboratori].'">';
	//print '<input type="hidden" name="gruppi" value="'.$_POST[gruppi].'">';
	print '<input type="hidden" name="categoria" value="'.$_POST[categoria].'">
	<input type="hidden" name="id_iniziale" value="'.($id_iniziale+$numero_per_pagina).'">
	</tr>
	</tbody></table>
	<input type="hidden" name="passaggi" value="primo">
	<input type="submit" value="inserisci e passa ai successivi">
	</form>';
}
} while (false)


?>
        </div>
        
        <?php include ("pedice.php"); ?>     
        
    </div>
</body> 

</html>
