<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<?php
include ("funzioni.php"); 

connetti();
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
$dati_parrocchia = carica_impostazioni_parrocchia();

define(FPDF_FONTPATH,"./font/"); //percorso della cartella font
$pdf=new PDF('P','mm',A4);
$pdf->Settitle("$dati_grest[titolo_grest] - $dati_grest[sottotitolo_grest] - $dati_grest[anno_grest] - Elenchi Generali");
$pdf->SetSubject('Report stampabile dei dati del grest');
$pdf->SetKeywords('grest elenchi liste grestone generali');
$pdf->SetCreator('GrestOne - Software di gestione GrEst tramite FPDF 1.7');
$pdf->SetAuthor("$dati_parrocchia[nome_parrocchia]");
$pdf->SetFont('Arial','',10);


$iscanicol = array('iscritti','animatori','collaboratori');
$iscanicol[bello] = array('Animati','Animatori','Collaboratori');
if ($dati_grest[periodo] == 1)
{
	$periodo = mysql_query("SELECT * FROM periodo WHERE id_grest = $_SESSION[id_grest]");
	$dati_periodo = mysql_fetch_array($periodo, MYSQL_ASSOC);
	$a = 1;
	while ($a <= $dati_periodo[numero_settimane])
	{
		if ($_GET['settimana_'.$a])
			{
				$settimane_selezionate[] = 'settimana_'.$a;
				$settimane_selezionate[id][] = $a;
				$numero_settimane_selezionate++;
			}
		$a++;
	}
}
if ($dati_grest[eta] == 1)
{
	$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest]");
	while ($dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC))
	{
		if ($_GET['eta_'.$dati_eta[id_eta]]== 1)
			{
				$eta_selezionate[id][] = $dati_eta[id_eta];
				$eta_selezionate[nome][] = $dati_eta[nome];
				$numero_eta_selezionate++;
				
			}
	}
}



$a = -1;
do
{
$a++;

$b = -1;
do
{
$b++;
$pdf->AddPage();
if ($numero_settimane_selezionate != 0)
{
$pdf->SetFont('Arial','B',15);
$settimane_per_stampa = $settimane_selezionate[id][$a];
$pdf->Cell(20,10,"Settimana $settimane_per_stampa",0,0,'C'); //Scritta valore settimana
}

$pdf->SetFont('Arial','',10);
if ($numero_eta_selezionate != 0)
{
	$pdf->SetX(-30);
	$pdf->SetFont('Arial','B',13);
	$eta = $eta_selezionate[nome][$b];
	$pdf->Cell(20,-10,"$eta",0,0,'R'); //Scritta in alto a dx Grandi o Piccoli
	$pdf->SetFont('Arial','',10);
	$pdf->Ln(10);
}
$i = 0;
while ($i < 3)
{
if ($_GET[$iscanicol[$i]])
{
	
	$query = "SELECT * FROM	$iscanicol[$i]_$_SESSION[id_grest] WHERE 1 ";
	if ($dati_grest[periodo] == 1)
		{
			if ($numero_settimane_selezionate != 0)
				{$query .= " AND $settimane_selezionate[$a] = 1";}
		}
	if ($dati_grest[eta] == 1)
		{
			if ($numero_eta_selezionate != 0)
				{
					$eta_per_query = $eta_selezionate[id][$b];
					$query .= " AND eta = '$eta_per_query'";
				}
		}
	if ($dati_grest[squadre] == 1)
		{
			if ($numero_squadre_selezionate != 0)
				{
					$squadra_per_query = $squadre_selezionate[id_squadra][$c];
					$query .= " AND squadra = '$squadra_per_query'";
				}
		}		
	$query .= " ORDER BY  `cognome`,`nome` ASC ";

	$iscritti = mysql_query("$query");
	$pdf->SetFont('Arial','B',15);
	$iscanicol_per_stampa = $iscanicol[bello][$i];
	$pdf->Cell(100,10,"Elenco $iscanicol_per_stampa",0,1,'C');
	$pdf->SetFont('Arial','',10);
	if (mysql_num_rows($iscritti) == null)
	{
		$pdf->Cell(35);
		$pdf->Cell(100,10,"NESSUN ISCRITTO PER QUESTA CATEGORIA",1,1,'C');
		$pdf->Ln(30);
	}
	else
	{
		$tabella = '
			<table id="lista" border="1" width="500">
			<tr>
			<td width="110">NOME</td>
			<td width="110">COGNOME</td>
			<td width="130">TELEFONO</td>
			<td width="130">CELLULARE</td>';
		if ($dati_grest[periodo] == 1)
			{
				if ($numero_settimane_selezionate != 0)
					{$tabella .= '<td width="220">APPELLO PRESENZE</td>';}
				else
				{
					for ($set=1;$set<=$dati_periodo[numero_settimane];$set++)
					{
						$tabella .= '<td width="60">SET. '.$set.'</td>';
					}
				}
			}
			
			$tabella .= '</tr>';
		while ($dati_iscritti =  mysql_fetch_array($iscritti, MYSQL_ASSOC))
		{//$dati_iscritti[cellulare].
			$tabella .='
			<tr>
			<td width="110">'.$dati_iscritti[nome].'</td>
			<td width="110">'.$dati_iscritti[cognome].'</td>';
			// INIZIO Blocco Telefoni con controllo
			$tabella .='<td width="130">';
			if (empty($dati_iscritti[telefono]) == '0')
				{$tabella .=''.$dati_iscritti[telefono].'';}
			else
			    {$tabella .='&nbsp;';}
			$tabella .='</td>';
			
			$tabella .='<td width="130">';
			if (empty($dati_iscritti[cellulare]) == '0')
				{$tabella .=''.$dati_iscritti[cellulare].'';}
			else
			    {$tabella .='&nbsp;';}
			$tabella .='</td>';
			// FINE Blocco Telefoni con controllo
			if ($dati_grest[periodo] == 1)
			{
				if ($numero_settimane_selezionate != 0)
				{
					$tabella .='
					<td width="220">'.$pdf->presenze_stampa($a+1).'</td>';
				}
				else
				{
					for ($set=1;$set<=$dati_periodo[numero_settimane];$set++)
					{
						$tabella .= '<td width="60">';
						if ($dati_iscritti["settimana_$set"])
						{$tabella .= 'Si';}
						else
						{$tabella .= 'No';}
						$tabella .= '</td>';
					}
				}
			}
			$tabella .= '</tr>';
			
		}
		$tabella .='</table>';
		$pdf->WriteHTML("$tabella");
	//$pdf->Cell(100,10,$pdf->presenze_stampa($a+1),0,1,'C');

		$tabella = '';
	}
}
$i++;
}

}while ($b < $numero_eta_selezionate-1);

}while ($a < ($numero_settimane_selezionate-1));
?>
<?php



$pdf->Output("$dati_grest[titolo_grest]_Elenchi_Generali.pdf",D);
?>

