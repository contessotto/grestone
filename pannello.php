<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
	<div id='pannello'>
	<br/>
	Accesso eseguito come:<br/><user>
	<?php  
	print $dati_utente[nome_utente].' <br/></user>
	<a id="esci" href="logout.php?nome='.$dati_utente[nome_utente].'"><img src="immagini/ico_logout.png" border=0 /> Effettua il LogOut</a>';
	?>
	</div>
