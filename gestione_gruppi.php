<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>
	<script src="script.js" type="text/javascript"></script>
</head>
	<style>
	tr.modifica{display:none;}
	</style>
    <script>
       	function modifica(id)
       	{document.getElementById('modifica'+id).style.display = 'table-row';}
       	function esci(id)
       	{document.getElementById('modifica'+id).style.display = 'none';}        
    </script>
<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
<?php
verifica_amministratore($_SESSION[Grestone]);
?>
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_personalizzazione.php"); ?>

        <div id="contenuto">

<?php
if ($dati_grest[s_gruppi] == 1)
{
	print '<h2>Gestione Gruppi</h2>';
	print 'La gestione dei Gruppi è attualmente sospesa.<br/>
	<a href="riprendi.php?oggetto=gruppi">Riprendi</a>';
}
else
{
if ($dati_grest[gruppi] == 0)
{
	if (!isset($_POST[passaggi])) //primo passaggio:indica il numero di laboratori
	{
	print '<h2>Gestione Gruppi</h2>';
	print '
	<form action="gestione_gruppi.php" method="post">
	<input type="hidden" name="passaggi" value="primo">
	Numero gruppi:
	<select name="numero">';
	for ($a=1;$a<=20;$a++)
		{print '<option value="'.$a.'">'.$a.'</option>';}
	print '</select><br/>
	<input type="submit" value="inserisci">';
	}
	
	
	if ($_POST[passaggi] == 'primo') //secondo passaggio: indica i valori per ogni squadra
	{	
	print '<h2>Gestione Gruppi</h2>';
	print '
	Inserisci i gruppi: per ciascuna devi indicare obbligatoriamente il NOME, mentre a tua scelta
	puoi decidere di inserire una descrizione e delle note.<br/>
	<form action="gestione_gruppi.php" method="post">
	<input type="hidden" name="passaggi" value="secondo">
	<input type="hidden" name="numero" value="'.$_POST[numero].'">';	
	for ($a=1; $a<=$_POST[numero]; $a++)
	{
		print '
		<h2>Gruppo '.$a.'</h2>';
		print'Nome: <input type="text" required name="nome_'.$a.'"><br/>';
		print'Descrizione:<textarea name="descrizione_'.$a.'" rows="5" cols="40"></textarea><br/>';
		print'Note: <textarea name="note_'.$a.'" rows="3" cols="40"></textarea><br/>';
	}	
	print '<br/><br/>
	<input type="submit" value="inserisci">
	</form>';
	}
	
	if ($_POST[passaggi] == 'secondo') //terzo passaggio: mostra i valori per la conferma
	{	
		print '<h2>Conferma dati inseriti</h2>';
		print '<form action="gestione_gruppi.php" method="post">';
		print'<input type="hidden" name="numero" value="'.$_POST[numero].'">';		
		print '<input type="hidden" name="passaggi"value="terzo">';
		for ($a=1; $a<=$_POST[numero]; $a++)
		{
			$nome = 'nome_'.$a;
			$descrizione = 'descrizione_'.$a;
			$note = 'note_'.$a;
			print"<h2>$_POST[$nome]</h2>";
			print"Descrizione: $_POST[$descrizione]<br/>";
			print"Note: $_POST[$note]<br/><br/>";
		}
		for ($a=1; $a<=$_POST[numero]; $a++)
		{
			$nome = 'nome_'.$a;
			$descrizione = 'descrizione_'.$a;
			$note = 'note_'.$a;
			print '<input type="hidden" name="'.$nome.'" value="'.$_POST[$nome].'">';
			print '<input type="hidden" name="'.$descrizione.'" value="'.$_POST[$descrizione].'">';
			print '<input type="hidden" name="'.$note.'" value="'.$_POST[$note].'">';
		}
		
	print '<br/><input type="submit" value="Inserisci Gruppi">';
	print '</form>';
	}
	
	
	
	
		if ($_POST[passaggi] == 'terzo') //quarto passaggio: inserisce i valori nel database
	{
		connetti();
		mysql_query("UPDATE `grests` SET gruppi = '1' WHERE `id_grest` = '$_SESSION[id_grest]';");
		mysql_query("CREATE TABLE  `gruppi_$_SESSION[id_grest]` (`id_gruppo` INT( 3 ) NOT NULL AUTO_INCREMENT ,
					`nome` TEXT NOT NULL , `descrizione` TEXT NOT NULL , `note` TEXT NOT NULL ,
					PRIMARY KEY (  `id_gruppo` ))");
		mysql_query("ALTER TABLE  `animatori_$_SESSION[id_grest]` ADD  `gruppo` INT NOT NULL AFTER  `note`");
		mysql_query("ALTER TABLE  `collaboratori_$_SESSION[id_grest]` ADD  `gruppo` INT NOT NULL AFTER  `note`");
	
		for ($a=1; $a<=$_POST[numero]; $a++)
		{
			$nome = 'nome_'.$a;
			$descrizione = 'descrizione_'.$a;
			$note = 'note_'.$a;
		
			mysql_query("INSERT INTO  `gruppi_$_SESSION[id_grest]` (`id_gruppo` ,`nome` ,`descrizione` ,`note`)
						VALUES (NULL ,  '$_POST[$nome]',  '$_POST[$descrizione]',  '$_POST[$note]');");
		}
		registro("$dati_utente[nome_utente]" , "$_SESSION[id_grest]" , 
		"Inserisce gruppi grest $dati_grest[titolo_grest]");					
		print '<h2>Dati inseriti correttamente</h2><meta http-equiv="refresh" content="0;
			URL='.$_SERVER['HTTP_REFERER'].'">';
	}
}
else
{
	print'<h2>La gestione dei gruppi è attiva</h2>
	<a href="#inserisci">Inserisci nuovo Gruppo</a><br/>
	<a href="disattiva.php?oggetto=gruppi" onclick="return confermadisattiva ();">Disattiva definitivamente gestione Gruppi</a><br/>
	<a href="sospendi.php?oggetto=gruppi">Sospendi temporaneamente gestione Gruppi</a>

	';
	connetti();
	$gruppi = mysql_query("SELECT * FROM  `gruppi_$_SESSION[id_grest]`");
	print '<table id="lista" align="center" width="100%"><thead>
	<tr>';
	print'<th scope="col"></th>';
	print'<th scope="col">NOME</th>';
	print'<th scope="col">DESCRIZIONE</th>';
	print'<th scope="col">NOTE</th>';
	print'<th scope="col">MODIFICA</th>';
	print'<th scope="col">ELIMINA</th>';
	print'</thead></tr><tbody>';
	while ($dati_gruppi = mysql_fetch_array($gruppi, MYSQL_ASSOC))
	{	
		print '<tr>';
		print '<td></td>';
		print"<td>$dati_gruppi[nome]</td>";
		print"<td>$dati_gruppi[descrizione]</td>";
		print"<td>$dati_gruppi[note]</td>";
		print '<td><a onclick="modifica('.$dati_gruppi[id_gruppo].');"class="elimina"href="#"><img src="immagini/modifica.png" alt="modifica" border="0" title="Modifica"/></a></td>';	
		print'<td><a href="elimina.php?oggetto=gruppo&id='.$dati_gruppi[id_gruppo].'" onclick="return conferma ();"><img src="immagini/ico_no.png" alt="elimina" border="0"  title="Elimina"/></a></td>';

		print'</tr>';
	
		print'<form action="modifica.php?oggetto=gruppo" method="post">
					
		<tr class="modifica" id="modifica'.$dati_gruppi[id_gruppo].'">
		<td><input type="hidden" name="id_gruppo" value="'.$dati_gruppi[id_gruppo].'">
		<a class="elimina" href="#" onclick="esci('.$dati_gruppi[id_gruppo].');">abbandona modifiche</a>
		</td>
		<td><input type="text" name="nome" size="3" value="'.$dati_gruppi[nome].'"></td>
		<td><textarea name="descrizione" cols="15">'.$dati_gruppi[descrizione].'</textarea></td>
		<td><textarea name="note" cols="15">'.$dati_gruppi[note].'</textarea></td>
		<td><input type="submit" value="modifica"></td>
		</tr>
		</form>';
	
	
	}
	print '</tbody></table>';
	
	if (isset($_POST[passaggi]))
	{
		if ($_POST[passaggi] == 'modifica_primo')
		{
			mysql_query("INSERT INTO `gruppi_$_SESSION[id_grest]` 
			(`id_gruppo`, `nome`, `descrizione`, `note`)
			 VALUES (NULL, '$_POST[nome]', '$_POST[descrizione]', '$_POST[note]');");
			print '<h2>Dati inseriti correttamente</h2>';
			print '<meta http-equiv="refresh" content="0;
			URL='.$_SERVER['HTTP_REFERER'].'">';
		}		
	}
	else
	{
		print '<a name="inserisci"></a>
		<br/><h2>Inserisci nuovo gruppo</h2>
		<form action="gestione_gruppi.php" method="post">
		Nome:<input type="text" name="nome"><br/>
		Descrizione: <textarea name="descrizione"></textarea><br/>
		Note: <textarea name="note"></textarea><br/>
		<input type="hidden" name="passaggi" value="modifica_primo"><br/>
		<input type="submit" value="inserisci"><br/>
		</form>';
	}
}
}
?>	
        </div>
        
        <?php include ("pedice.php"); ?>
        
    </div>
</body> 

</html>
