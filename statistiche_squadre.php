<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>
	<script src="script.js" type="text/javascript"></script>
</head>
<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_statistiche.php"); ?>

<div id="contenuto">
<?php
connetti();

/*SEZIONE PRIMA: SI OCCUPA DI LEGGERE IL DB E CARICARE SU ARRAY TUTTI I DATI NECESSARI ALLA VISUALIZZAZIONE DELLA PAGINA*/
if ($dati_grest[periodo] == 1)
{
	$periodo = mysql_query("SELECT * FROM periodo WHERE id_grest=$_SESSION[id_grest]");
	$dati_periodo = mysql_fetch_array($periodo, MYSQL_ASSOC);
}
if ($dati_grest[eta] == 1)
{
	$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest]");
	while ($dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC))
	{

		$eta_selezionate[id][] = $dati_eta[id_eta];
		$eta_selezionate[nome][] = $dati_eta[nome];
		$numero_eta_selezionate++;
	}
}
if ($dati_grest[squadre] == 1)
{
	$squadre = mysql_query("SELECT * FROM squadre_$_SESSION[id_grest]");
	$impostazioni_squadre = mysql_fetch_array($squadre, MYSQL_ASSOC);
	while ($dati_squadre = mysql_fetch_array($squadre, MYSQL_ASSOC))
	{
		$squadre_selezionate[id_squadra][] = $dati_squadre[id_squadra];
		if ($impostazioni_squadre[nome])
		{$squadre_selezionate[nome][] = $dati_squadre[nome];}
		if ($impostazioni_squadre[colore])
		{$squadre_selezionate[colore][] = $dati_squadre[colore];}
		$numero_squadre_selezionate++;
	}
}

$iscanicol = array('iscritti','animatori','collaboratori');
$iscanicol[bello] = array('Animati','Animatori','Collaboratori');

print '<h2>Statistiche Squadre</h2><br/>';
	print '<table id="lista" width="100%">';
	print '</tr>';
	print '<th></th>';
	$c = 0;
	do
	{
		print '<th>';
		print $squadre_selezionate[nome][$c].'<br/>';
		if ($impostazioni_squadre[colore]==1)
			{print '<img src="immagini/squadre/'.$squadre_selezionate[colore][$c].'.png" alt="'.$squadre_selezionate[colore][$c].'" border="0" title="'.$squadre_selezionate[colore][$c].'"/></a>';}
		print '</th>';
		$c++;
	}
	while ($c < $numero_squadre_selezionate);
	print '</tr>';
$c_iscanicol=0;
while ($c_iscanicol <3)
{
print '<tr><td><strong>'.$iscanicol[bello][$c_iscanicol].'</strong></td>';
	$c = 0;
	do
	{	
		print '<td><h2>';
		$query = "SELECT * FROM $iscanicol[$c_iscanicol]_$_SESSION[id_grest] WHERE 1 ";
		$query .= ' AND squadra = '.$squadre_selezionate[id_squadra][$c];
		$numero = mysql_query("$query");
		print $numero_iscritti = mysql_num_rows($numero);	
		$numero_totale_iscritti[$c] += $numero_iscritti;			
		print '</h2></td>';
		$c++;
	}
	while ($c < $numero_squadre_selezionate);
	print '</tr>';
	$c_iscanicol++;
}

print '<tr><td><strong><span style="color: red;">TOTALE</span></strong></td>';
	$c = 0;
	do
	{
		print '<td><h2><span style="color: red;">'.$numero_totale_iscritti[$c].'</h2></td>';
		$c++;
	} while($c<$numero_squadre_selezionate);

print '</tr></table>';

for ($c_iscanicol=0;$c_iscanicol<3;$c_iscanicol++)
{
print '<br/><h3><a id="img_'.$iscanicol[bello][$c_iscanicol].'" href="javascript:mostra(\'mostra_'.$iscanicol[bello][$c_iscanicol].'\', \'img_'.$iscanicol[bello][$c_iscanicol].'\');">
<img src="immagini/piu.png" title="Espandi" border="0" /></a> Visualizza il dettaglio '.$iscanicol[bello][$c_iscanicol].'</h3>';
print '<div id="mostra_'.$iscanicol[bello][$c_iscanicol].'" style="display: none;">';
if (!isset($dati_periodo[numero_settimane]))
{$dati_periodo[numero_settimane] = 1;}
for ($a=1; $a<=$dati_periodo[numero_settimane]; $a++)
{
	if ($dati_grest[periodo] == 1)
	{
	print '<br/><b>';
	print 'Settimana '.$a;
	print '</b><br/>';}
	print '<table id="lista" width="100%">';
	print '<tr>';
	if ($numero_eta_selezionate <> 0)
		{print '<th></th>';}
	$c = 0;
	do
	{
		print '<th>';
		print $squadre_selezionate[nome][$c].'<br/>';
		if ($impostazioni_squadre[colore]==1)
			{print '<img src="immagini/squadre/'.$squadre_selezionate[colore][$c].'.png" alt="'.$squadre_selezionate[colore][$c].'" border="0" title="'.$squadre_selezionate[colore][$c].'"/></a>';}
		print '</th>';
		$c++;
	}
	while ($c < $numero_squadre_selezionate);
	print '</tr>';
	$b = 0;
	do
	{
		print '<tr>';
		if ($numero_eta_selezionate <> 0)
			{print '<td><b>'.$eta_selezionate[nome][$b].'</b></td>';} 
		$c = 0;
		do
		{
			$query = "SELECT * FROM $iscanicol[$c_iscanicol]_$_SESSION[id_grest] WHERE 1 ";
			
			if ($dati_grest[periodo] == 1)
			{
				$query .= ' AND settimana_'.$a.'=1 ';
			}
			if ($dati_grest[eta] == 1)
				{$query .= ' AND eta = '.$eta_selezionate[id][$b];}
			$query .= ' AND squadra = '.$squadre_selezionate[id_squadra][$c];
			$numero = mysql_query("$query");
			$numero_iscritti = mysql_num_rows($numero);
			print '<td>';
			print '<h2>'.$numero_iscritti.'</h2>';
			print '</td>';
			$c++;
		}
		while ($c < $numero_squadre_selezionate);
		print '</tr>';
		$b++;
	}
	while ($b<$numero_eta_selezionate);
	if ($numero_eta_selezionate <> 0)
	{
		print '<tr><td><strong><span style="color: red;">TOTALE</span></strong></td>';
		$c = 0;
		do
		{
			print '<td><h2><span style="color: red;">';
			$query = "SELECT * FROM $iscanicol[$c_iscanicol]_$_SESSION[id_grest] WHERE 1 ";
			if ($dati_grest[periodo] == 1)
			{
				$query .= 'AND settimana_'.$a.'= 1';
			}
			$query .= ' AND squadra = '.$squadre_selezionate[id_squadra][$c];
			$numero = mysql_query("$query");
			print $numero_iscritti = mysql_num_rows($numero);		
			print '</span></h2></td>';
			$c++;
		}
		while ($c < $numero_squadre_selezionate);
		print '</tr>';
	}
	print '</table>';
}
print '</div>';
}

?>
</div>
        <?php include ("pedice.php"); ?>
</div>
</body> 

</html>
