<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
<script src="script.js" type="text/javascript"></script>
<body>
	
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
verifica_grest($_SESSION[id_grest]);
$dati_grest = verifica_grest();
?>

<?php
verifica_amministratore($_SESSION[Grestone]);

registro("$dati_utente[nome_utente]" , "$_SESSION[id_grest]" , 
"Accesso in amministrazione");

?>
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_personalizzazione.php"); ?>

        <div id="contenuto">
			
<br/><h2>Personalizzazione di Grestone</h2>
<br/><img src="immagini/personalizzazione_home.png" alt="Usa il menu a sinistra per modificare i dati del Grest"/><br/>
        <?php include ("pannello_amministrativo.php"); ?></div>
        
        <?php include ("pedice.php"); ?>
        
    </div>
</body> 

</html>
