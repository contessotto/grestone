<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
        <?php
        $impostazioni = carica_impostazioni_parrocchia();
        $impostazioni_grest = carica_impostazioni_grest();
        ?>
        <div id="intestazione">
		<a href="personalizzazione_parrocchia.php" target="blank"><img id="logo" src='<?php print "$impostazioni[logo_parrocchia]"?>' alt="logo parrocchia" style="border: 3px solid #E1EAF0;"/></a>
		<h1><?php print "$impostazioni[nome_parrocchia]"?></h1><br/>
		<spec_grest><?php print "$impostazioni_grest[anno_grest] - $impostazioni_grest[titolo_grest] - $impostazioni_grest[sottotitolo_grest]";?></spec_grest>
		</div>
