<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>Configurazione di GrestOne</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
	<style>
	tr.modifica{display:none;}
	</style>
	<script src="script.js" type="text/javascript"></script>
	        <script>
        	function modifica(id)
        	{document.getElementById('modifica'+id).style.display = 'table-row';}
        	function esci(id)
        	{document.getElementById('modifica'+id).style.display = 'none';}        
        </script>
<body>

		<?php include ("funzioni.php"); 
		verifica_admin();
		registro('admin', '', 'entra in configurazione grest');		
		?>
		
	<div id="principale">
		<div id="intestazione">
		<br/><h2>Pagina di Configurazione Generale di GrestOne</h2>
		</div>
        
        <?php include ("menu_configurazione.php"); ?>

        
        
        <div id="contenuto">
		<h3>Lista Grest Gestiti</h3>
		
		<?php // se tutti i campi sono compilati li inserisce nel database
			if ($_POST[id_parrocchia] <> null AND $_POST[titolo_grest]<> null)
			{
				registro("admin", "","inserisce il grest $_POST[titolo_grest]");
				mysql_query("
				INSERT INTO  `grests` 
				(`id_grest` ,`id_parrocchia` ,`titolo_grest`,`sottotitolo_grest` ,`anno_grest`)
				VALUES (NULL ,'$_POST[id_parrocchia]', '$_POST[titolo_grest]',  '$_POST[sottotitolo_grest]','$_POST[anno_grest]');
				");
				$ultimo_id = mysql_insert_id();
				$a = 'iscritti_'.$ultimo_id;
				mysql_query("
				CREATE TABLE  `$a` (`id` INT( 11 ) NOT NULL AUTO_INCREMENT, `nome` VARCHAR( 20 ) NOT NULL ,
				`cognome` VARCHAR( 20 ) NOT NULL , `sesso` CHAR( 1 ) NOT NULL , `nascita` INT( 20 ) NOT NULL ,
				`classe` VARCHAR( 10 ) NOT NULL , `indirizzo` VARCHAR( 30 ) NOT NULL , `paese` VARCHAR( 30 ) NOT NULL ,
				`telefono` VARCHAR( 20 ) NOT NULL , `cellulare` VARCHAR( 20 ) NOT NULL, `note` TEXT NOT NULL,
				FULLTEXT (nome, cognome) ,
				PRIMARY KEY (  `id` )) 
				 ");
				$b = 'animatori_'.$ultimo_id;
				mysql_query("
				CREATE TABLE  `$b` (`id` INT( 11 ) NOT NULL AUTO_INCREMENT, `nome` VARCHAR( 20 ) NOT NULL ,
				`cognome` VARCHAR( 20 ) NOT NULL , `sesso` CHAR( 1 ) NOT NULL , `nascita` INT( 20 ) NOT NULL ,
				`classe` VARCHAR( 10 ) NOT NULL , `indirizzo` VARCHAR( 30 ) NOT NULL , `paese` VARCHAR( 30 ) NOT NULL ,
				`telefono` VARCHAR( 20 ) NOT NULL , `cellulare` VARCHAR( 20 ) NOT NULL, `note` TEXT NOT NULL,
				FULLTEXT (nome, cognome) ,				
				PRIMARY KEY (  `id` )) 
				 ");
				$c = 'collaboratori_'.$ultimo_id;
				mysql_query("
				CREATE TABLE  `$c` (`id` INT( 11 ) NOT NULL AUTO_INCREMENT, `nome` VARCHAR( 20 ) NOT NULL ,
				`cognome` VARCHAR( 20 ) NOT NULL, `indirizzo` VARCHAR( 30 ) NOT NULL , `paese` VARCHAR( 30 ) NOT NULL ,
				`telefono` VARCHAR( 20 ) NOT NULL , `cellulare` VARCHAR( 20 ) NOT NULL, `note` TEXT NOT NULL,
				FULLTEXT (nome, cognome) ,				
				PRIMARY KEY (  `id` )) 
				 ");
				mysql_query("
				INSERT INTO `periodo` 
				(`id_grest`, `mktime_inizio`, `mktime_fine`, `lun`, `mar`, `mer`, `gio`, `ven`, `sab`, `dom`)
				 VALUES (NULL, '0', '0', '0', '0', '0', '0', '0', '0', '0');
				 ");
				print '<h2>Grest inserito correttamente</h2>';
				print'<meta http-equiv="refresh" content="0;
				URL='.$_SERVER['HTTP_REFERER'].'">';
			}
			else // se non tutti i campi del form sono compilati:
			{		
				// se solo qualcuno è compilato:
				if ($_POST[titolo_grest] <> null OR $_POST[sottotitolo_grest] <> null OR $_POST[anno_grest] <> null) 
				
				{
					print 'NON TUTTI I CAMPI SONO COMPILATI';
				}
				else // se nessun campo è compilato:
				{ 
					
					/* inizio visualizzazione pagina*/
					
					$grest = mysql_query("SELECT * FROM grests");
					$righe_grest = mysql_num_rows($grest);
					if ($righe_grest == 0) //se non ci sono grest nella tabella
					{
						print'<h4>Nessun Grest inserito</h4>';
					}
					else // se invece ce no sono
					{//scrive l'intestazione della tabella
						print'
						<table id="lista" width="100%"><thead>
						<tr>
						<th scope="col">ID</th>
						<th scope="col">TITOLO</th>
						<th scope="col">SOTTOTITOLO</th>
						<th scope="col">ANNO</th>
						<th scope="col">PARROCCHIA</th>
						<th scope="col">MODIFICA</th>
						<th scope="col">ELIMINA</th>
						</tr></thead><tbody>';
						//ciclo per mostrare tutti i grest
						while ($impostazioni_grest = mysql_fetch_array($grest, MYSQL_ASSOC))
						{	// per recuperare i nomi delle parrocchie
							$parrocchia = mysql_query("SELECT * FROM parrocchie 
							WHERE id_parrocchia = $impostazioni_grest[id_parrocchia]");
							$impostazioni_parrocchia = mysql_fetch_array($parrocchia, MYSQL_ASSOC);
											
							print"
							<tr>
							<td>$impostazioni_grest[id_grest]</td>
							<td>$impostazioni_grest[titolo_grest]</td>
							<td>$impostazioni_grest[sottotitolo_grest]</td>
							<td>$impostazioni_grest[anno_grest]</td>
							<td>$impostazioni_parrocchia[nome_parrocchia]</td>";
							
							print '<td><a class="elimina" href="modifica_grest_admin.php?grest='.$impostazioni_grest[id_grest].'"><img src="immagini/modifica.png" alt="modifica" border="0" title="Modifica"/></a></td>';
							print '<td><a class="elimina" href="elimina.php?'.
							"oggetto=grest&id=$impostazioni_grest[id_grest]".
							'" onclick="return conferma ();"><img src="immagini/ico_no.png" alt="elimina" border="0" title="Elimina"/></a></td>';
							print'</tr>';}
						print'</tbody></table>';
					}
				}
					print '<br/><br/><br/><br/>';

				print'
				<h3>Aggiungi Grest</h3>
				<table align="center" id="lista">
				<tr><td>
				<form action="configurazione_grest.php" method="post">
				Parrocchia</td><td>';
				$parrocchia = mysql_query("SELECT * FROM parrocchie");
				$righe_parrocchia = mysql_num_rows($parrocchia);
				if ($righe_parrocchia == null) //se non ci sono parrocchie inserite
				{
					print'NESSUNA PARROCCHIA INSERITA';
				}
				else //se ce ne sono
				{
					print'<select name="id_parrocchia">';
					//mostra il select con tutte le parrocchie
					while ($impostazioni_parrocchia = mysql_fetch_array($parrocchia, MYSQL_ASSOC))
					{	
						print"
						<option value=$impostazioni_parrocchia[id_parrocchia]>$impostazioni_parrocchia[id_parrocchia] -$impostazioni_parrocchia[nome_parrocchia]</option>
						";
					}
					print '</select>';
				}	
				print'</td></tr><td>
				Titolo Grest</td><td><input type="text" required maxlength="20" name="titolo_grest" autocomplete="off"></td></tr><td>
				Sottotitolo Grest</td><td><input type="text" maxlength="50" name="sottotitolo_grest" autocomplete="off"></td></tr><td>
				Anno Grest</td><td><input type="text" maxlength="15"name="anno_grest" autocomplete="off"></td></tr></table>
				<input type="submit" value="crea">
				</form>';
			}
		?>
        
        </div>
        
        <?php include ("pedice.php"); ?>     
        
    </div>
</body> 

</html>

