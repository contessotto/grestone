<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<div id="ricerca">
<a id="Ricerca" href="javascript:mostraricerca('mostraRicerca','Ricerca');">
<img src="immagini/ricerca.png" title="Espandi" border="0" /> Ricerca Rapida Iscritti</a>
<div id="mostraRicerca" style="display: none;">
<hr width="100%" size="1" color="#0033CC" />
<b>Animati</b><br/>
<form action="lista_iscritti.php" method="get"><input type="text" name="ricerca" size="5"><input type="submit" value="vai">
</form>
<b>Animatori</b><br/>
<form action="lista_animatori.php" method="get"><input type="text" name="ricerca"  size="5"><input type="submit" value="vai">
</form>
<b>Collaboratori</b><br/>
<form action="lista_collaboratori.php" method="get"><input type="text" name="ricerca"  size="5"><input type="submit" value="vai">
</form>
<hr width="100%" size="1" color="#0033CC" />

</div>
</div>
