<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<?php include ("funzioni.php"); 
verifica_admin();
registro('admin', '', 'entra in configurazione generale');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>Configurazione di GrestOne</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
<script src="script.js" type="text/javascript"></script>
<body>


		
	<div id="principale">
		<div id="intestazione">
		<br/><h2>Pagina di Configurazione Generale di GrestOne</h2>
		</div>
        
        <?php include ("menu_configurazione.php"); ?>

        
        
        <div id="contenuto">
		<h2>Pannello di Controllo Amministratore</h2>
        
        
        <?php include ("controllo_versione.php"); ?> 
        
        
        </div>
        
        <?php include ("pedice.php"); ?>     
        
    </div>
</body> 

</html>
