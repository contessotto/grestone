<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>Configurazione di GrestOne</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
	<style>
	tr.modifica{display:none;}
	</style>
	<script src="script.js" type="text/javascript"></script>
<body>

		<?php 
		include ("funzioni.php"); 
		verifica_amministratore();
		verifica_admin();
		registro('admin', '', 'entra in configurazione utenti');
		?>
		
	<div id="principale">
		<div id="intestazione">
		<br/><h2>Pagina di Configurazione Generale di GrestOne</h2>
		</div>
        
        <?php include ("menu_configurazione.php"); ?>

        
        <div id="contenuto">
		<?php
	$grest = mysql_query("SELECT * FROM  grests");
	$id_grest = '';
	while ($dati_grest = mysql_fetch_array($grest, MYSQL_ASSOC))
	{
		if ($_POST["id_grest_$dati_grest[id_grest]"] == '1') // verifica se esiste almeno un $_POST["id_grest_$i"]
		{	
			$verifica = 1; // se si setta  $verifica a 1
			// il seguente if inserisce in $id_grest il nuovo numero:
			if ($id_grest == '') //se è il primo valore ad essere inserito non mette -
				{$id_grest = $dati_grest[id_grest];} 
			else
				{$id_grest = $id_grest."-$dati_grest[id_grest]";} //altrimenti lo mette prima prima del nuovo numero
			//print $id_grest;
		}
	}
				// se ci sono tutti i campi del form...
			if (isset($_POST[nome_utente], $_POST[password], $_POST[cpassword], $_POST[id_parrocchia]))
			{	
				if ($_POST[password] <> $_POST[cpassword]) // se la conferma password non è corretta...
				{
					print 'Incongruenza nella password inserita';
					print '<meta http-equiv="refresh" content="1;
					URL='.$_SERVER['HTTP_REFERER'].'">';
				}
				else // se invece è corretta: inserimento del nuovo utente nel database...
				{
					$nomi = mysql_query("SELECT nome_utente FROM utenti");//conto il numero di utenti	
					while ($verifica_nomi = mysql_fetch_array($nomi, MYSQL_ASSOC))
					{
						if ($verifica_nomi[nome_utente] == $_POST[nome_utente])
						{
							$controllo_nome = 1;
							break;
						}
					}
					if ($controllo_nome != 1)
					{ 
						registro("admin", "","inserisce l'utente $_POST[nome_utente]");
						$password = sha1($_POST[password]);
						$a = mysql_query("
						INSERT INTO  `utenti` 
						(`id_utente` ,`nome_utente` ,`password` ,`id_parrocchia` ,`id_grest` ,`ruolo_utente`)
						VALUES (NULL ,  '$_POST[nome_utente]',  '$password',  '$_POST[id_parrocchia]', 
						'$id_grest',  '$_POST[ruolo_utente]')
						");
						print '<h2>Utente Inserito</h2>';
						print '<meta http-equiv="refresh" content="0;
						URL='.$_SERVER['HTTP_REFERER'].'">';
					}
					else
					{
						print 'NOME UTENTE NON DISPONIBILE';
						print '<meta http-equiv="refresh" content="2;
						URL='.$_SERVER['HTTP_REFERER'].'">';
					}
				}
			}
			else // se non tutti i campi del form sono compilati:
			{
				if ($_POST[nome_utente]<>'' or $_POST[password]<>'' or $_POST[cpassword]<>''
				or $_POST[id_parrocchia]<>'') // se solo qualcuno è compilato
				{
					print 'NON TUTTI I CAMPI SONO COMPILATI';
					print '<meta http-equiv="refresh" content="2;
					URL='.$_SERVER['HTTP_REFERER'].'">';
				}
				else // se nessun campo è compilato:
				{ 
				/* inizio visualizzazione pagina  */
				
					//  visualizzazione della tabella utenti
					
					
					$conto = mysql_query("SELECT * FROM utenti");//conto il numero di utenti
					$righe = mysql_num_rows($conto);
					
					//se non ci sono utenti non mostro la tabella
					if ($righe <= 1) // utilizzo  <=1 perchè così non viene visualizzato l'utente ADMIN (che ha id 1)
					{
						print'<h4>Nessun Utente Iscritto</h4>';
					}
					else //se invece c'è almeno un utente
					{
						print'<h3>Lista Utenti Iscritti</h3>
						<table id="lista" width="100%"><thead>
						<tr>
						<th scope="col">ID</th>
						<th scope="col">NOME</th>
						<th scope="col">PARROCCHIA</th>
						<th scope="col">GREST</th>
						<th scope="col">RUOLO</th>
						<th scope="col">MODIFICA</th>
						<th scope="col">ELIMINA</th>
						</tr></thead><tbody>';
						//ciclo per  estrarre e mostrare gli utenti
						$impostazioni_utente = mysql_fetch_array($conto, MYSQL_ASSOC); // per escludere ADMIN
						while ($impostazioni_utente = mysql_fetch_array($conto, MYSQL_ASSOC))
						{	
							$parrocchia = mysql_query("SELECT * FROM parrocchie 
							WHERE id_parrocchia = $impostazioni_utente[id_parrocchia]");
							$impostazioni_parrocchia = mysql_fetch_array($parrocchia, MYSQL_ASSOC);
							
							$id_grest_utente = explode("-",$impostazioni_utente[id_grest]);
							$numero_grest_utente = count($id_grest_utente);
							
							$i  = 0;

							
							print"
							<tr>
							<td>$impostazioni_utente[id_utente]</td>
							<td>$impostazioni_utente[nome_utente]</td>
							<td>$impostazioni_parrocchia[nome_parrocchia]</td>
							<td>";
							if ( $impostazioni_utente[id_grest] != null)
							{
								while ($i < $numero_grest_utente)
								{
									$grest = mysql_query("SELECT * FROM grests 
								WHERE id_grest = $id_grest_utente[$i]");
								$impostazioni_grest = mysql_fetch_array($grest, MYSQL_ASSOC);
								print "$impostazioni_grest[titolo_grest]<br/>";
								$i++;
								}
							}
							else
							{
								print 'NESSUN GREST';
							}
							print"
							</td>
							<td>$impostazioni_utente[ruolo_utente]</td>";
							print '<td>';
							print '<a href="modifica_utente_admin.php?utente='.$impostazioni_utente[id_utente].'"><img src="immagini/password.png" alt="Modifica Nome Utente e Password" title="Modifica Nome Utente e Password" border="0"/></a>';
							//print '<a onclick="modifica('.$impostazioni_utente[id_utente].');" class="elimina" href="#"><img src="immagini/modifica.png" alt="modifica" border="0" title="Modifica"/></a>';	
							print '</td>';
							print '<td><a class="elimina" href="elimina.php?'.
							"oggetto=utente&id=$impostazioni_utente[id_utente]".
							'" onclick="return conferma ();"><img src="immagini/ico_no.png" alt="elimina" border="0" title="Elimina"/></a></td></tr>';
						
							/*print'<form action="modifica.php?oggetto=utente" method="post">
					
							<tr class="modifica" id="modifica'.$impostazioni_utente[id_utente].'">
							<td>MODIFICA</td>
							<td><input type="text" size="5" name="nome_utente"></td>
							<td>';
							print '<select name="id_parrocchia" >';
							$parrocchie = mysql_query("SELECT * FROM parrocchie");
							while ($impostazioni_parrocchie = mysql_fetch_array($parrocchie, MYSQL_ASSOC))
							{print '
							<option value="'.$impostazioni_parrocchie[id_parrocchia].'">
								'.$impostazioni_parrocchie[nome_parrocchia].'
							</option>
							';}
							print '</select>';
							print'</td>
							<td>';
							$grest = mysql_query("SELECT * FROM  grests");
							while ($impostazioni_grest = mysql_fetch_array($grest, MYSQL_ASSOC))
							{	
								//mostra il nome del grest e il pulsante
								print $impostazioni_grest[titolo_grest];
								print '<input type="checkbox" name="';
								print "id_grest_$impostazioni_grest[id_grest]"; //il nome di ciascuna checkbox è id_grest_ e l'id del grest
								print '"value="1"><br/>'; //il valore è 1 (true)
							}
							print'
							</td>
							<td><select name="ruolo_utente" >
								<option value="normale" selected="selected">Normale</option>
								<option value="amministratore">Amministratore</option>
								<option value="osservetore">Osservatore</option>
								</select>
							</td>
							<td><input type="hidden" name="id_utente" value="'.$impostazioni_utente[id_utente].'">
							<a class="elimina" href="#" onclick="esci('.$impostazioni_utente[id_utente].');">abbandona modifiche</a>
							</td>
							<td><input type="submit" value="modifica"></td>
							</tr>
						
							</form>';*/
						}
						print'</tbody></table>';
					}
				
					print '<br/><br/><br/><br/>';
					//form per l'aggiuta di uno o più utenti
					print'
					<h2>Aggiungi Utente</h2>
					<form action="configurazione_utenti.php" method="post">
					<h3>Nome Utente:</h3> <input type="text" name="nome_utente" required><br/><br/>
					<h3>Password:</h3> <input type="password" name="password" required><br/><br/>
					<h3>Conferma password:</h3> <input type="password" name="cpassword" required><br/><br/>
					
					<h3>Parrocchia e Grest/s:</h3>';
					// inizio procedura per estrarre i nomi delle parrocchie e i rispettivi grest e mostrarli
					print'<table align="center" id="lista">';
					
					
					$parrocchia = mysql_query("SELECT * FROM parrocchie"); //conta il numero di parrocchie
					$righe_parrocchia = mysql_num_rows($parrocchia);
					if ($righe_parrocchia == null) //se non ci sono parrocchie nel database
					{
						print'<tr><td><h4>Nessuna Parrocchia Iscritta</h4></td></tr></table>';
					}
					else
					{	
						while ($impostazioni_parrocchia = mysql_fetch_array($parrocchia, MYSQL_ASSOC)) //ciclo per scorrere tutte le parrocchie
						{
							print'<tr><td><strong>';
							$z = 0;
							$grest = mysql_query("SELECT * FROM grests WHERE id_parrocchia = $impostazioni_parrocchia[id_parrocchia]"); //conta tutti i grest
							$righe_grest = mysql_num_rows($grest);
							if ($righe_grest <> 0) //se la parrocchia ha almeno un grest
							{	
								//scrive il nome e il pulsante di scelta
								print $impostazioni_parrocchia[nome_parrocchia].' 
								<input type="radio" required name="id_parrocchia" value="'.$impostazioni_parrocchia[id_parrocchia].'"></strong><br/>';
								
								//ciclo per mostrare i vari grest della parrocchia
								while ($impostazioni_grest = mysql_fetch_array($grest, MYSQL_ASSOC))
								{	
										//mostra il nome del grest e il pulsante
										print ' --> '.$impostazioni_grest[titolo_grest];
										print '<input type="checkbox" name="';
										print "id_grest_$impostazioni_grest[id_grest]"; //il nome di ciascuna checkbox è id_grest_ e l'id del grest
										print '"value="1"><br/>'; //il valore è 1 (true)
								}
								print'</td></tr>';
							}
							else
							{
							print $impostazioni_parrocchia[nome_parrocchia].' 
								<input type="radio" name="id_parrocchia" value="'.$impostazioni_parrocchia[id_parrocchia].'"><br/>';
							print 'Nessun grest inserito';
							}
							print'</td></tr>';
						}
						print'</table>';
					}	
					print'
					<h3>Ruolo:</h3>
					<select name="ruolo_utente" >
					<option value="normale" selected="selected">Normale</option>
					<option value="amministratore">Amministratore</option>
					<option value="osservetore">Osservatore</option>
					</select>
					
					<br/><br/><br/>
					
					<input type="submit" value="Inserisci Utente">
					</form>';
				}
			}
			?>
        </div>
        
        <?php include ("pedice.php"); ?>     
        
    </div>
</body> 

</html>
