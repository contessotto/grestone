<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
	<style>
	#elenco{border:0px solid;}
	td{border:1px solid;}
	.elimina{color:black;}
	tr.modifica{display:none;
	background-color:grey;}
	</style>
<script src="script.js" type="text/javascript"></script>
	        <script>
        	function modifica(id)
        	{document.getElementById('modifica'+id).style.display = 'table-row';}
        	function esci(id)
        	{document.getElementById('modifica'+id).style.display = 'none';}        
        </script>
<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
verifica_grest($_SESSION[id_grest]);
?>

<?php
verifica_amministratore($_SESSION[Grestone]);
?>
	
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_personalizzazione.php"); ?>

        <div id="contenuto">

<?php
connetti();
if ($_POST[titolo_grest] == null and $_POST[sottotitolo_grest] == null and $_POST[anno_grest] == null)
	{
	if (isset($_GET[modificato]))
		{print'Modifiche effettuate con successo';
		}
	else
		{print'
		<h3>Personalizzazione Grest in GrestOne</h3>
		<form action="personalizzazione_grest.php" method="post">
		Titolo del Grest:<br/>
		<input name="titolo_grest"type="text"><br/>
		Sottotitolo:<br/>
		<input name="sottotitolo_grest"type="text"><br/>
		Anno del grest:<br/>
		<input name="anno_grest"type="text"><br/><br/>
		<input type="submit" value="invia">
		</form>';
		}
	}
else
	{
	if ($_POST[titolo_grest] <> null)
		{print "Titolo grest modificato in: $_POST[titolo_grest]";
		mysql_query ("UPDATE  `grests` SET  `titolo_grest` =  '$_POST[titolo_grest]' WHERE  `grests`.`id_grest` = $_SESSION[id_grest]; ");
		registro("$dati_utente[nome_utente]" , "$_SESSION[id_grest]" , "Modifica titolo grest in $_POST[titolo_grest]");
		header("location: personalizzazione_grest.php?modificato=si");
		}
	if ($_POST[sottotitolo_grest] <> null)
		{print "Sottotitolo grest modificato in: $_POST[sottotitolo_grest]";
		mysql_query ("UPDATE  `grests` SET  `sottotitolo_grest` =  '$_POST[sottotitolo_grest]' WHERE  `grests`.`id_grest` = $_SESSION[id_grest]; ");
		registro("$dati_utente[nome_utente]" , "$_SESSION[id_grest]" , "Modifica sottotitolo grest in $_POST[sottotitolo_grest]");
		header("location: personalizzazione_grest.php?modificato=si");
		}	
	if ($_POST[anno_grest] <> null)
		{print "Anno grest modificato in: $_POST[anno_grest]";
		mysql_query ("UPDATE  `grests` SET  `anno_grest` =  '$_POST[anno_grest]' WHERE  `grests`.`id_grest` = $_SESSION[id_grest]; ");
		registro("$dati_utente[nome_utente]" , "$_SESSION[id_grest]" , "Modifica anno grest in $_POST[anno_grest]");
		header("location: personalizzazione_grest.php?modificato=si");
		}		
	}
disconnetti();
?>			
        </div>
        
        <?php include ("pedice.php"); ?>
        
    </div>
</body> 

</html>
