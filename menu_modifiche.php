<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?> 
 <div id="menu">
 
<?php include ("dataora.php"); ?>
<?php include ("ricerca_rapida.php"); ?>
 
 
        <br/>
        <a href="home.php"><img src="immagini/menu/menu_principale.png" alt="MENU PRINCIPALE" name="menu" border="0" onmouseover="cambia(menu,'immagini/menu/menu_principale_on.png')" onmouseout="cambia(menu,'immagini/menu/menu_principale.png')"/></a><br/>
	<br/>
<h3>MODIFICHE RAPIDE</h3>
        <a href="modifiche_rapide_iscritti.php"><img src="immagini/menu/modifica_animati.png" alt="Modifiche Iscritti" name="modanimati" border="0" onmouseover="cambia(modanimati,'immagini/menu/modifica_animati_on.png')" onmouseout="cambia(modanimati,'immagini/menu/modifica_animati.png')"/></a><br/>
        <a href="modifiche_rapide_animatori.php"><img src="immagini/menu/modifica_animatori.png" alt="Modifiche Animatori" name="modanimatori" border="0" onmouseover="cambia(modanimatori,'immagini/menu/modifica_animatori_on.png')" onmouseout="cambia(modanimatori,'immagini/menu/modifica_animatori.png')"/></a><br/>
        <a href="modifiche_rapide_collaboratori.php"><img src="immagini/menu/modifica_collaboratori.png" alt=" Modifiche Collaboratori" name="modcollaboratori" border="0" onmouseover="cambia(modcollaboratori,'immagini/menu/modifica_collaboratori_on.png')" onmouseout="cambia(modcollaboratori,'immagini/menu/modifica_collaboratori.png')"/></a><br/>
      <br/>  
<?php
if ($dati_grest[squadre]==1 OR $dati_grest[eta]==1)
{print '<h3>DIVISIONI AUTOMATICHE</h3> ';}  

if ($dati_grest[squadre]==1)
	{print '<a href="divisione_squadre_animati.php">
	<img src="immagini/menu/divisione_squadre_animati.png" alt="Divisione Squadre Animati" 
	name="divsqanimati" border="0" onmouseover="
	cambia(divsqanimati,\'immagini/menu/divisione_squadre_animati_on.png\')"
	 onmouseout="cambia(divsqanimati,\'immagini/menu/divisione_squadre_animati.png\')"/></a><br/> 
        
	<a href="divisione_squadre_animatori.php">
	<img src="immagini/menu/divisione_squadre_animatori.png" alt="Divisione Squadre Animatori" 
	name="divsqanimatori" border="0" onmouseover="cambia(divsqanimatori,\'immagini/menu/divisione_squadre_animatori_on.png\')"
	onmouseout="cambia(divsqanimatori,\'immagini/menu/divisione_squadre_animatori.png\')"/></a><br/> ';
    }
    
if ($dati_grest[eta]==1)
	{print '	
	<a href="divisione_eta_animati.php">
	<img src="immagini/menu/divisione_fasce_animati.png" alt="Divisione Fasce Età Animati" 
	name="divetaanimati" border="0" onmouseover="cambia(divetaanimati,\'immagini/menu/divisione_fasce_animati_on.png\')"
	 onmouseout="cambia(divetaanimati,\'immagini/menu/divisione_fasce_animati.png\')"/></a><br/><br/>';}
?>
    </div>
