<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<?php
include ("funzioni.php"); 

connetti();
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
$dati_parrocchia = carica_impostazioni_parrocchia();

define(FPDF_FONTPATH,"./font/"); //percorso della cartella font
$pdf=new PDF('P','mm',A4);
$pdf->Settitle("$dati_grest[titolo_grest] - $dati_grest[sottotitolo_grest] - $dati_grest[anno_grest] - Elenchi Generali");
$pdf->SetSubject('Report stampabile dei dati del grest');
$pdf->SetKeywords('grest elenchi liste grestone generali');
$pdf->SetCreator('GrestOne - Software di gestione GrEst tramite FPDF 1.7');
$pdf->SetAuthor("$dati_parrocchia[nome_parrocchia]");
$pdf->SetFont('Arial','',10);


$iscanicol = array('iscritti','animatori','collaboratori');
$iscanicol[bello] = array('Animati','Animatori','Collaboratori');


	$gruppi = mysql_query("SELECT * FROM gruppi_$_SESSION[id_grest]");
	while ($dati_gruppi = mysql_fetch_array($gruppi, MYSQL_ASSOC))
	{
		if ($_GET['gruppo_'.$dati_gruppi[id_gruppo]] == 1)
			{
				$gruppi_selezionati[id][] = $dati_gruppi[id_gruppo];
				$gruppi_selezionati[nome][] = $dati_gruppi[nome];
				$numero_gruppi_selezionati++;
				
			}
	}

if ($dati_grest[eta] == 1)
{
	$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest]");
	while ($dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC))
	{
		if ($_GET['eta_'.$dati_eta[id_eta]]== 1)
			{
				$eta_selezionate[id][] = $dati_eta[id_eta];
				$eta_selezionate[nome][] = $dati_eta[nome];
				$numero_eta_selezionate++;
				
			}
	}
}
if ($dati_grest[squadre] == 1)
{
	$squadre = mysql_query("SELECT * FROM squadre_$_SESSION[id_grest]");
	$impostazioni_squadre = mysql_fetch_array($squadre, MYSQL_ASSOC);
	while ($dati_squadre = mysql_fetch_array($squadre, MYSQL_ASSOC))
	{
		if ($_GET['squadra_'.$dati_squadre[id_squadra]]== 1)
			{
				$squadre_selezionate[id_squadra][] = $dati_squadre[id_squadra];
				if ($impostazioni_squadre[nome])
				{$squadre_selezionate[nome][] = $dati_squadre[nome];}
				if ($impostazioni_squadre[colore])
				{$squadre_selezionate[colore][] = $dati_squadre[colore];}
				$numero_squadre_selezionate++;
			}
	}
}


$a = -1;
do
{
$a++;
if ($numero_gruppi_selezionati == 0)
{
	$pdf->AddPage();
	$pdf->SetFont('Arial','B',20);
	$pdf->Cell(5);
	$pdf->Cell(180,10,"NESSUN GRUPPO SELEZIONATO",1,1,'C');
	break;
}
$gruppi_per_stampa = $gruppi_selezionati[nome][$a];
$c = -1;
do
{
$c++;
if ($numero_squadre_selezionate != 0)
{
	$squadre_per_stampa = $squadre_selezionate[nome][$c].$squadre_selezionate[colore][$c];
}
$b = -1;
do
{
$b++;
if ($numero_eta_selezionate != 0)
{
	$eta_per_stampa = $eta_selezionate[nome][$b];
}
$pdf->AddPage();

$pdf->SetFont('Arial','B',15);
$pdf->Cell(20,10,"$gruppi_per_stampa",0,0,'L'); //Scritta valore settimana
$pdf->SetFont('Arial','',10);
//$eventi_per_stampa = '';

$pdf->SetX(10);
$pdf->Ln(10);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(20,10,"$squadre_per_stampa",0,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->Ln(10);
//$squadre_per_stampa = '';

$pdf->SetX(-30);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(20,-10,"$eta_per_stampa",0,0,'R'); //Scritta in alto a dx Grandi o Piccoli
$pdf->SetFont('Arial','',10);
$pdf->Ln(10);
//$eta_per_stampa = '';

$i = 0;
while ($i < 3)
{
if ($_GET[$iscanicol[$i]])
{
	$query = "SELECT * FROM	$iscanicol[$i]_$_SESSION[id_grest] WHERE 1 ";
	if ($dati_grest[gruppi] == 1)
		{
			if ($numero_gruppi_selezionati != 0)
			{
				$gruppi_per_query = $gruppi_selezionati[id][$a];
				$query .= " AND gruppo = '$gruppi_per_query'";
			}			
		}
	if ($dati_grest[eta] == 1)
		{
			if ($numero_eta_selezionate != 0)
				{
					$eta_per_query = $eta_selezionate[id][$b];
					$query .= " AND eta = '$eta_per_query'";
				}
		}
	if ($dati_grest[squadre] == 1)
		{
			if ($numero_squadre_selezionate != 0)
				{
					$squadra_per_query = $squadre_selezionate[id_squadra][$c];
					$query .= " AND squadra = '$squadra_per_query'";
				}
		}
	$query .= " ORDER BY  `cognome`,`nome` ASC ";
	// print 'query:'.$query;
	$iscritti = mysql_query("$query");

	$pdf->SetFont('Arial','B',15);
	$iscanicol_per_stampa = $iscanicol[bello][$i];
	$pdf->Cell(100,10,"Elenco $iscanicol_per_stampa",0,1,'C');
	$pdf->SetFont('Arial','',10);
	
	if (mysql_num_rows($iscritti) == null)
	{
		$pdf->Cell(35);
		$pdf->Cell(100,10,"NESSUN ISCRITTO PER QUESTA CATEGORIA",1,1,'C');
		$pdf->Ln(30);
	}
	else
	{
		$tabella = '
		<table id="lista" border="1" width="500">
		<tr>
		<td width="110">NOME</td>
		<td width="110">COGNOME</td>
		<td width="130">TELEFONO</td>
		<td width="130">CELLULARE</td>
		</tr>';
		while ($dati_iscritti =  mysql_fetch_array($iscritti, MYSQL_ASSOC))
		{
		$tabella .='
		<tr>
		<td width="110">'.$dati_iscritti[nome].'</td>
		<td width="110">'.$dati_iscritti[cognome].'</td>';
			// INIZIO Blocco Telefoni con controllo
			$tabella .='<td width="130">';
			if (empty($dati_iscritti[telefono]) == '0')
				{$tabella .=''.$dati_iscritti[telefono].'';}
			else
			    {$tabella .='&nbsp;';}
			$tabella .='</td>';
			
			$tabella .='<td width="130">';
			if (empty($dati_iscritti[cellulare]) == '0')
				{$tabella .=''.$dati_iscritti[cellulare].'';}
			else
			    {$tabella .='&nbsp;';}
			$tabella .='</td>';
			// FINE Blocco Telefoni con controllo
		$tabella .='</tr>';
		}
	}
	$tabella .='</table>';
	$pdf->WriteHTML("$tabella");
	$tabella = '';
}
$i++;
}

}while ($b < $numero_eta_selezionate-1);

}while ($c < $numero_squadre_selezionate-1);

}while ($a < ($numero_gruppi_selezionati-1));
$pdf->Output("$dati_grest[titolo_grest]_Elenchi_Gruppi.pdf",D);

	?>
