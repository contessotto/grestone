<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>Conferma Eliminazione Campo</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
<body>
<div id="eliminato"><br/><br/><strong>
<?php
	include ("funzioni.php"); 
	verifica_amministratore($_SESSION[Grestone]);
	$dati_grest = verifica_grest();

	$squadre = mysql_query("SELECT * FROM  `squadre_$_SESSION[id_grest]`");
	$dati_squadre = mysql_fetch_array($squadre, MYSQL_ASSOC);
	switch ($_GET[oggetto]) {
	case 'colori':
		if ($_GET[funzione] == 'abilita')
		{
			mysql_query("UPDATE  `squadre_$_SESSION[id_grest]` SET  `colore` =  '1' WHERE  `id_squadra` = 1;");
			print 'colori abilitati';
		}
		if ($_GET[funzione] == 'disabilita')
		{
			mysql_query("UPDATE  `squadre_$_SESSION[id_grest]` SET  `colore` =  '0' WHERE  `id_squadra` = 1;");
			print 'colori disabilitati';
		}
	break;
	
	case 'nomi':
		if ($_GET[funzione] == 'abilita')
		{
			mysql_query("UPDATE  `squadre_$_SESSION[id_grest]` SET  `nome` =  '1' WHERE  `id_squadra` = 1;");
			print 'nomi abilitati';
		}
		if ($_GET[funzione] == 'disabilita')
		{
			mysql_query("UPDATE  `squadre_$_SESSION[id_grest]` SET  `nome` =  '0' WHERE  `id_squadra` = 1;");
			print 'nomi disabilitati';
		}
	break;
}
	
	
?>
		<meta http-equiv="refresh" content="2;
		URL=<?=$_SERVER['HTTP_REFERER']?>">
</strong><br/></div>
</body> 

</html>
