<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>Conferma Eliminazione Campo</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
<body>
<div id="eliminato"><br/><br/><strong>
<?php
	include ("funzioni.php"); 
	verifica_amministratore();
	$dati_grest = verifica_grest();
	
	switch ($_GET[oggetto]) {
	case 'gruppi':
		mysql_query("UPDATE  `grests` SET  `gruppi` =  '0' WHERE  `id_grest` = $_SESSION[id_grest];");
		mysql_query("ALTER TABLE `animatori_$_SESSION[id_grest]` DROP `gruppo`");
		mysql_query("ALTER TABLE `collaboratori_$_SESSION[id_grest]` DROP `gruppo`");
		mysql_query("DROP TABLE gruppi_$_SESSION[id_grest]");
		print 'Gestione gruppi disattivata';
	break;
	case 'eta':
		mysql_query("UPDATE  `grests` SET  `eta` =  '0' WHERE  `id_grest` = $_SESSION[id_grest];");
		mysql_query("ALTER TABLE `iscritti_$_SESSION[id_grest]` DROP `eta`");
		mysql_query("ALTER TABLE `animatori_$_SESSION[id_grest]` DROP `eta`");
		mysql_query("ALTER TABLE `collaboratori_$_SESSION[id_grest]` DROP `eta`");
		mysql_query("DROP TABLE eta_$_SESSION[id_grest]");
		print 'Gestione fasce d\'età disattivata';
	break;
	case 'laboratori':
		mysql_query("UPDATE  `grests` SET  `laboratori` =  '0' WHERE  `id_grest` = $_SESSION[id_grest];");
		for ($a = 1; $a <= $dati_grest[laboratori_periodo]; $a++)
		{
			mysql_query("ALTER TABLE `iscritti_$_SESSION[id_grest]` DROP `laboratorio_$a`");
			mysql_query("ALTER TABLE `animatori_$_SESSION[id_grest]` DROP `laboratorio_$a`");
			mysql_query("ALTER TABLE `collaboratori_$_SESSION[id_grest]` DROP `laboratorio_$a`");
		}
		mysql_query("UPDATE  `grests` SET  `laboratori_periodo` =  '0' WHERE  `id_grest` = $_SESSION[id_grest];");
		mysql_query("DROP TABLE laboratori_$_SESSION[id_grest]");
		print 'Gestione laboratori disattivata';
	break;
	
	case 'squadre':
		mysql_query("UPDATE  `grests` SET  `squadre` =  '0' WHERE  `id_grest` = $_SESSION[id_grest];");
		mysql_query("ALTER TABLE `iscritti_$_SESSION[id_grest]` DROP `squadra`");
		mysql_query("ALTER TABLE `animatori_$_SESSION[id_grest]` DROP `squadra`");
		mysql_query("ALTER TABLE `collaboratori_$_SESSION[id_grest]` DROP `squadra`");
		mysql_query("DROP TABLE squadre_$_SESSION[id_grest]");
		print 'Gestione squadre disattivata';
	break;
	
	case 'periodo':
		mysql_query("UPDATE  `grests` SET  `periodo` =  '0' WHERE  `id_grest` = $_SESSION[id_grest];");
		$periodo = mysql_query("SELECT * FROM periodo WHERE id_grest = $_SESSION[id_grest]");
		$dati_periodo = mysql_fetch_array ($periodo, MYSQL_ASSOC);
		for ($a = 1; $a <= $dati_periodo[numero_settimane]; $a++)
		{
			mysql_query("ALTER TABLE `iscritti_$_SESSION[id_grest]` DROP `settimana_$a`");
			mysql_query("ALTER TABLE `animatori_$_SESSION[id_grest]` DROP `settimana_$a`");
			mysql_query("ALTER TABLE `collaboratori_$_SESSION[id_grest]` DROP `settimana_$a`");
		}
		mysql_query("
		UPDATE  `periodo` SET  `mktime_inizio` =  '0',
		`mktime_fine` =  '0',
		`giorni_esclusi` = '',
		`giorni_inclusi` = '',
		`inizi_settimane` = '',
		`fini_settimane` = '',
		`numero_settimane` =  '0',
		`lun` =  '0',
		`mar` =  '0',
		`mer` =  '0',
		`gio` =  '0',
		`ven` =  '0' WHERE  `id_grest` = $_SESSION[id_grest];
		");
		print 'Gestione periodo disattivata';
	break;
	
	case 'eventi':
		mysql_query("UPDATE  `grests` SET  `gite` =  '0' WHERE  `id_grest` = $_SESSION[id_grest];");
		$eventi = mysql_query("SELECT * FROM gite_$_SESSION[id_grest] WHERE liste = 1");
		while ($dati_eventi = mysql_fetch_array($eventi, MYSQL_ASSOC))
		{			
			mysql_query("ALTER TABLE `iscritti_$_SESSION[id_grest]` DROP `presenza_evento_$dati_eventi[id_gita]`");
			mysql_query("ALTER TABLE `iscritti_$_SESSION[id_grest]` DROP `pagamento_evento_$dati_eventi[id_gita]`");
			mysql_query("ALTER TABLE `iscritti_$_SESSION[id_grest]` DROP `note_evento_$dati_eventi[id_gita]`");
			
			mysql_query("ALTER TABLE `animatori_$_SESSION[id_grest]` DROP `presenza_evento_$dati_eventi[id_gita]`");
			mysql_query("ALTER TABLE `animatori_$_SESSION[id_grest]` DROP `pagamento_evento_$dati_eventi[id_gita]`");
			mysql_query("ALTER TABLE `animatori_$_SESSION[id_grest]` DROP `note_evento_$dati_eventi[id_gita]`");

			mysql_query("ALTER TABLE `collaboratori_$_SESSION[id_grest]` DROP `presenza_evento_$dati_eventi[id_gita]`");
			mysql_query("ALTER TABLE `collaboratori_$_SESSION[id_grest]` DROP `pagamento_evento_$dati_eventi[id_gita]`");
			mysql_query("ALTER TABLE `collaboratori_$_SESSION[id_grest]` DROP `note_evento_$dati_eventi[id_gita]`");
		}
		
		mysql_query("DROP TABLE gite_$_SESSION[id_grest]");
		print 'Gestione eventi disattivata';
	break;
}
	
	
?>
		<meta http-equiv="refresh" content="2;
		URL=<?=$_SERVER['HTTP_REFERER']?>">
</strong><br/></div>
</body> 

</html>
