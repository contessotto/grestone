<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>
	<script src="script.js" type="text/javascript"></script>
</head>
<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_statistiche.php"); ?>

<div id="contenuto">
<?php
connetti();

/*SEZIONE PRIMA: SI OCCUPA DI LEGGERE IL DB E CARICARE SU ARRAY TUTTI I DATI NECESSARI ALLA VISUALIZZAZIONE DELLA PAGINA*/

if ($dati_grest[eta] == 1)
{
	$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest]");
	while ($dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC))
	{

		$eta_selezionate[id][] = $dati_eta[id_eta];
		$eta_selezionate[nome][] = $dati_eta[nome];
		$numero_eta_selezionate++;
	}
}

if ($dati_grest[laboratori] == 1)
{
	$laboratori = mysql_query("SELECT * FROM laboratori_$_SESSION[id_grest]");
	while ($dati_laboratori = mysql_fetch_array($laboratori, MYSQL_ASSOC))
	{
		$laboratori_selezionati[id_laboratorio][] = $dati_laboratori[id_laboratorio];
		$laboratori_selezionati[nome][] = $dati_laboratori[nome];
		$numero_laboratori_selezionati++;
	}
}
	$periodo_lab = mysql_query("SELECT laboratori_periodo FROM grests WHERE id_grest=$_SESSION[id_grest]");
	$dati_periodo_lab = mysql_fetch_array($periodo_lab, MYSQL_ASSOC);


/*SEZIONE SECONDA: SI OCCUPA DELLA VISUALIZZAZIONE*/
$iscanicol = array('iscritti','animatori','collaboratori');
$iscanicol[bello] = array('Animati','Animatori','Collaboratori');

	print '<h2>Statistiche Laboratori</h2><br/>
	<table id="lista" width="100%">
	</tr><th></th>';
	$c = 0;
	do
	{
		print '<th>';
		print $laboratori_selezionati[nome][$c];
		print '</th>';
		$c++;
	}
	while ($c < $numero_laboratori_selezionati);
	
	$c_iscanicol=0;
	while ($c_iscanicol <3)
	{	
		print '</tr>';
		print '<tr><td><strong>'.$iscanicol[bello][$c_iscanicol].'</strong></td>';

		$c = 0;
		do
		{
			print '<td><h2>';
			$query = "SELECT * FROM $iscanicol[$c_iscanicol]_$_SESSION[id_grest] WHERE 1 AND (";
			for ($a=1; $a<=$dati_periodo_lab[laboratori_periodo]; $a++)
			{
				$query .= 'laboratorio_'.$a.'= '.$laboratori_selezionati[id_laboratorio][$c];
				if ($a != $dati_periodo_lab[laboratori_periodo])
				{$query .= ' OR ';}
			}
			$query .= ')';
			//print $query;
			$numero = mysql_query("$query");
			print $numero_iscritti = mysql_num_rows($numero);	
			$numero_totale_iscritti[$c] += $numero_iscritti;		
			print '</h2></td>';
			$c++;
		}
		while ($c < $numero_laboratori_selezionati);
		print '</tr>';
		$c_iscanicol++;
	}


print '<tr><td><strong><span style="color: red;">TOTALE</span></strong></td>';
	$c = 0;
	do
	{
		print '<td><h2><span style="color: red;">'.$numero_totale_iscritti[$c].'</h2></td>';
		$c++;
	} while($c<$numero_laboratori_selezionati);

print '</tr></table>';



print '<br><br>';
$c = 0;
do
{
	print '<br/><h3><a id="img_'.$laboratori_selezionati[nome][$c].'" href="javascript:mostra(\'mostra_'.$laboratori_selezionati[nome][$c].'\',\'img_'.$laboratori_selezionati[nome][$c].'\');">
	<img src="immagini/piu.png" title="Espandi" border="0" /></a> Visualizza i dettagli per '.$laboratori_selezionati[nome][$c].'</h3>';
	print '<div id="mostra_'.$laboratori_selezionati[nome][$c].'" style="display: none;">';
	print '<br/><h2>'.$laboratori_selezionati[nome][$c].'</h2>';
	$c_iscanicol=0;
	while ($c_iscanicol <3)
	{
		print '<br/><h3>'.$iscanicol[bello][$c_iscanicol].'</h3>';
		print '<table id="lista" width="100%">';
		print '<tr>';
		print '<th></th>';
		for ($a=1; $a<=$dati_periodo_lab[laboratori_periodo]; $a++)
		{
			print '<th>Periodo '.$a.'</th>';
		}
		print '</tr>';

		$b = 0;
		do
		{
			print '<tr><td><br/><strong>'.$eta_selezionate[nome][$b].'</strong></td>';
	
			for ($a=1; $a<=$dati_periodo_lab[laboratori_periodo]; $a++)
			{
				$query = 'SELECT * FROM '.$iscanicol[$c_iscanicol].'_'.$_SESSION[id_grest].' WHERE 1 AND ';
				$query .= 'laboratorio_'.$a.' = '.$laboratori_selezionati[id_laboratorio][$c];
				if ($numero_eta_selezionate <> 0)
					{$query .= ' AND eta = '.$eta_selezionate[id][$b];}
				$numero = mysql_query("$query");
				$numero_iscritti = mysql_num_rows($numero);
				print '<td><h2>'.$numero_iscritti.'</h2></td>';	
			}
			print '</tr>';
		$b++;
		}
		while ($b<$numero_eta_selezionate);
		if ($numero_eta_selezionate <> 0)
		{
			print '<tr><td><strong><span style="color: red;">TOTALE</span></strong></td>';
			for ($a=1; $a<=$dati_periodo_lab[laboratori_periodo]; $a++)
			{
				$query = 'SELECT * FROM '.$iscanicol[$c_iscanicol].'_'.$_SESSION[id_grest].' WHERE 1 AND ';
				$query .= 'laboratorio_'.$a.' = '.$laboratori_selezionati[id_laboratorio][$c];
				$numero = mysql_query("$query");
				$numero_iscritti = mysql_num_rows($numero);		
				print '<td><h2><span style="color: red;">';
				print $numero_iscritti;
				print '</span></h2></td>';
			}
			print '</tr>';
		}
		$c_iscanicol++;
		print '</table>';
	}
	print '</div>';
	$c++;
}
while ($c < $numero_laboratori_selezionati);

?>
</div>
        <?php include ("pedice.php"); ?>
</div>
</body> 

</html>
