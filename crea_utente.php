<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>Conferma Eliminazione Campo</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
<body>
<div id="eliminato"><br/><br/>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();


//questo for serve per verificare che ci sia almeno un grest selezionato
for ($i=1; $i <= 100; $i++) 
{
	if ($_POST["id_grest_$i"] == '1') // verifica se esiste almeno un $_POST["id_grest_$i"]
	{	
		$verifica = 1; // se si setta  $verifica a 1
		// il seguente if inserisce in $id_grest il nuovo numero:
		if ($id_grest == '') //se è il primo valore ad essere inserito non mette -
			{$id_grest = $i;} 
		else
			{$id_grest = $id_grest."-$i";} //altrimenti lo mette prima prima del nuovo numero
	}
}
// se ci sono tutti i campi del form...		$conto = mysql_query("SELECT * FROM utenti WHERE id_parrocchia = $dati_utente[id_parrocchia]");//conto il numero di utenti

if (isset($_POST[nome_utente], $_POST[password], $_POST[cpassword], $verifica))
{	
	if ($_POST[password] <> $_POST[cpassword]) // se la conferma password non è corretta...
	{
		print 'Incongruenza nella password inserita';
	}
	else // se invece è corretta: inserimento del nuovo utente nel database...
	{
		$nomi = mysql_query("SELECT nome_utente FROM utenti");//conto il numero di utenti	
		while ($verifica_nomi = mysql_fetch_array($nomi, MYSQL_ASSOC))
		{
			if ($verifica_nomi[nome_utente] == $_POST[nome_utente])
			{
				$controllo_nome = 1;
				break;
			}
		}
		if ($controllo_nome != 1)
		{
			registro("admin", "","inserisce l'utente $_POST[nome_utente]");
			$password = sha1($_POST[password]);
			$a = mysql_query("
			INSERT INTO  `utenti` 
			(`id_utente` ,`nome_utente` ,`password` ,`id_parrocchia` ,`id_grest` ,`ruolo_utente`)
			VALUES (NULL ,  '$_POST[nome_utente]',  '$password',  '$dati_utente[id_parrocchia]', 
			'$id_grest',  '$_POST[ruolo_utente]')
			");
		}
		else
		{
			print 'NOME UTENTE NON DISPONIBILE';
		}
	}
}
else // se non tutti i campi del form sono compilati:
{
	print 'NON TUTTI I CAMPI SONO COMPILATI';
}
?>
		<meta http-equiv="refresh" content="2;
		URL=<?=$_SERVER['HTTP_REFERER']?>">
</strong><br/></div>
</body> 

</html>
