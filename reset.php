<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>Configurazione di GrestOne</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
<script src="script.js" type="text/javascript"></script>
<body>

		<?php include ("funzioni.php"); 
		verifica_admin($_SESSION[Grestone]);
		registro('admin', '', 'entra in reset');		
		?>
		
	<div id="principale">
		<div id="intestazione">
		<br/><h2>Reset di GrestOne</h2>
		</div>
        

        <?php include ("menu_configurazione.php"); ?>
        
        <div id="contenuto">
		<?php
			if($_POST[reset] == 1)
			{
				$parrocchie = mysql_query("SELECT * FROM parrocchie");
				while ($dati_parrocchie = mysql_fetch_array($parrocchie, MYSQL_ASSOC))
					{elimina_parrocchia($dati_parrocchie[id_parrocchia]);}
				mysql_query ("TRUNCATE `grests`;");
				mysql_query ("TRUNCATE `parrocchie`;");
				mysql_query ("TRUNCATE `periodo`;");
				mysql_query ("TRUNCATE `registro`;");
				mysql_query ("TRUNCATE `utenti`;");
				mysql_query ("INSERT INTO `utenti` 
				(`id_utente`, `nome_utente`, `password`, `id_parrocchia`, `id_grest`, `ruolo_utente`) 
				VALUES (NULL, 'admin', 'admin', '0', '0', 'amministratore');
				");
				print '<h2>Reset effettuato correttamente</h2>
				<h3>Accedi con admin per impostare il programma</h3>';
			}
			else
			{
				print '
		<h3>Reset di Grestone</h3>
		<err><strong>ATTENZIONE!</strong><br/>Il seguente bottone resetta in modo definitivo tutti i dati del database.</br>Clicca se ne sei veramente consapevole.</err>
		<form action="reset.php"method="post">
		<input type="hidden" name="reset" value="1">
		<err>--></err><input type="submit"value="resetta "><err><--</err>
		</form>
				';
			}
		
		if ($_POST[password] <> null or $_POST[cpassword] <> null)
		{
			if ($_POST[password] == $_POST[cpassword])
			{
				$password = sha1($_POST[password]);
				mysql_query ("UPDATE  `utenti` SET  `password` =  '$password' WHERE  `utenti`.`id_utente` = 1; ");
				registro("$dati_utente[nome_utente]" , "" , "Modifica password utente admin");
				print '<h2>Modifica effettuata con successo</h2>';
			}
			else
			{
				print '<h3>errore nella conferma password</h3>';
			}
		}
		else
		{
		print '</br></br><h3>Modifica Password Utente Admin</h3>
		<form action="reset.php" method="post">
		<table id="lista" align="center">
		<tr><td>Password</td><td><input type="password" name="password"></td></tr>
		<tr><td>Conferma Password</td><td><input type="password" name="cpassword"></td></tr></table>
		<input type="submit" value="modifica">
		</form>';
		}

		if ($_POST[nome_utente])
		{
				mysql_query ("UPDATE  `utenti` SET  `nome_utente` =  '$_POST[nome_utente]' WHERE  `utenti`.`id_utente` = 1; ");
				registro("$dati_utente[nome_utente]" , "" , "Modifica nome utente admin");
				print '<h2>Modifica effettuata con successo</h2>';
		}
		else
		{
		print '</br></br><h3>Modifica Nome Utente Admin</h3>
		<form action="reset.php" method="post">
		<table id="lista" align="center">
		<tr><td>Nuovo nome utente: <input type="text" name="nome_utente"></td></tr></table>
		<input type="submit" value="modifica">
		</form>';
		}
		?>
        </div>
        
        <?php include ("pedice.php"); ?>     
        
    </div>
</body> 

</html>

