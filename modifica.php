<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>Conferma Modifica Campo</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>

</head>
<body>
<div id="modificato"><br/><br/><strong>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
verifica_amministratore();
?>

<?php
connetti();

switch ($_GET[oggetto]) {
case 'utente':
	if ($dati_utente[id_utente] == '1')
	{
		$grest = mysql_query("SELECT * FROM  grests");
	}
	else
	{
	$grest = mysql_query("SELECT * FROM  grests WHERE id_parrocchia = $dati_utente[id_parrocchia]");
	}
	$id_grest = '';
	while ($dati_grest = mysql_fetch_array($grest, MYSQL_ASSOC))
	{
		if ($_POST["id_grest_$dati_grest[id_grest]"] == '1') // verifica se esiste almeno un $_POST["id_grest_$i"]
		{	
			$verifica = 1; // se si setta  $verifica a 1
			// il seguente if inserisce in $id_grest il nuovo numero:
			if ($id_grest == '') //se è il primo valore ad essere inserito non mette -
				{$id_grest = $dati_grest[id_grest];} 
			else
				{$id_grest = $id_grest."-$dati_grest[id_grest]";} //altrimenti lo mette prima prima del nuovo numero
			//print $id_grest;
		}
	}
	$controllo_nome = 0;
	if ($_POST[nome_utente] != null)
	{	
		$nomi = mysql_query("SELECT nome_utente FROM utenti");//conto il numero di utenti	
		while ($verifica_nomi = mysql_fetch_array($nomi, MYSQL_ASSOC))
		{
			if ($verifica_nomi[nome_utente] == $_POST[nome_utente])
			{
				$controllo_nome = 1;
				break;
			}
		}
	}
	
	if ($dati_utente[id_utente] == '1')
	{
		$utente_modificato = mysql_query ("SELECT * FROM utenti WHERE id_utente = $_POST[id_utente]");
		$dati_utente_modificato = mysql_fetch_array($utente_modificato, MYSQL_ASSOC);
		if ($dati_utente_modificato[nome_utente] == $_POST[nome_utente])
		{
			$controllo_nome = 0;
		}
	}
	else
	{
		if ($dati_utente[nome_utente] == $_POST[nome_utente])
		{
			$controllo_nome = 0;
		}	
	}

	if ($controllo_nome != 1)
	{
		//print "Nome utente modificato in: $_POST[nome_utente]<br/>";
		mysql_query ("UPDATE  `utenti` SET  `nome_utente` =  '$_POST[nome_utente]' WHERE  `utenti`.`id_utente` = $_POST[id_utente]; ");
		//header("location: configurazione_utenti.php");
	}
	else
	{
		$errore = 2;
	}
	
	if ($_POST[password] <> null OR $_POST[c_password] <> null)
	{	
		if ($_POST[password] == $_POST[c_password])
		{
			$password = sha1($_POST[password]);
			//print "Password modificata<br/>";
			mysql_query ("UPDATE  `utenti` SET  `password` =  '$password' WHERE  `utenti`.`id_utente` = $_POST[id_utente]; ");
			//header("location: configurazione_utenti.php");
		}
		else
		{
		//	print'ERRORE NELLA PASSWORD';
			$errore = 1;
		}
	}
	if ($_POST[id_parrocchia] <> null)
	{	
		print "Id parrocchia modificato in: $_POST[id_parrocchia]<br/>";
		mysql_query ("UPDATE  `utenti` SET  `id_parrocchia` =  '$_POST[id_parrocchia]' WHERE  `utenti`.`id_utente` = $_POST[id_utente]; ");
		//header("location: configurazione_utenti.php");
	}	
	if ($verifica <> null)
	{
		//print "Id grest modificato in: $id_grest<br/>";
		mysql_query ("UPDATE  `utenti` SET  `id_grest` =  '$id_grest' WHERE  `utenti`.`id_utente` = $_POST[id_utente]; ");
		//header("location: configurazione_utenti.php");
	}
	else
	{
		print "NESSUN GREST SELEZIONATO<br/>";
		mysql_query ("UPDATE  `utenti` SET  `id_grest` =  '' WHERE  `utenti`.`id_utente` = $_POST[id_utente]; ");
	}
	if ($_POST[ruolo_utente] <> null)
	{
		//print "Ruolo utente modificato in: $_POST[ruolo_utente]<br/>";
		mysql_query ("UPDATE  `utenti` SET  `ruolo_utente` =  '$_POST[ruolo_utente]' WHERE  `utenti`.`id_utente` = $_POST[id_utente]; ");
		//header("location: configurazione_utenti.php");
	}
	print '<h2>';
	if ($errore == null)
		{print 'MODIFICHE EFFETTUATE CORRETTAMENTE!<br/>';}
	if ($errore == 1)
		{print 'Incongruenza nella password';}
	if ($errore == 2)
		{print 'Nome Utente già in uso';}
	if ($verifica == null)
		{print 'ATTENZIONE: Nessun Grest selezionato';}
	print '</h2>';
	break;

case 'grest':
	if ($_POST[titolo_grest] == null and $_POST[sottotitolo_grest] == null and $_POST[anno_grest] == null)
	{
	print 'NESSUNA MODIFICA SELEZIONATA';
	}
	else
	{
		if ($_POST[titolo_grest] <> null)
		{	
			print "Titolo grest modificato in: $_POST[titolo_grest]<br/>";
			mysql_query ("UPDATE  `grests` SET  `titolo_grest` =  '$_POST[titolo_grest]' WHERE  `grests`.`id_grest` = $_POST[id_grest]; ");
			registro("$dati_utente[nome_utente]" , "" , "Modifica titolo grest $_POST[id_grest] in $_POST[titolo_grest]");
			//header("location: configurazione_grest.php");
		}
		if ($_POST[sottotitolo_grest] <> null)
		{	
			print "Sottotitolo grest modificato in: $_POST[sottotitolo_grest]<br/>";
			mysql_query ("UPDATE  `grests` SET  `sottotitolo_grest` =  '$_POST[sottotitolo_grest]' WHERE  `grests`.`id_grest` = $_POST[id_grest]; ");
			registro("$dati_utente[nome_utente]" , "" , "Modifica sottotitolo grest $_POST[id_grest] in $_POST[sottotitolo_grest]");
			//	header("location: configurazione_grest.php");

		}	 
		if ($_POST[anno_grest] <> null)
		{
			print "Anno grest modificato in: $_POST[anno_grest]<br/>";
			mysql_query ("UPDATE  `grests` SET  `anno_grest` =  '$_POST[anno_grest]' WHERE  `grests`.`id_grest` = $_POST[id_grest]; ");
			registro("$dati_utente[nome_utente]" , "" , "Modifica anno grest $_POST[id_grest] in $_POST[anno_grest]");
			//header("location: configurazione_grest.php");
		}		
	}
	break;
case 'parrocchia':
	if($_POST[nome_parrocchia] != null)
	{
		mysql_query ("UPDATE  `parrocchie` SET  `nome_parrocchia` =  '$_POST[nome_parrocchia]' WHERE  `parrocchie`.`id_parrocchia` = $_POST[id_parrocchia]; ");
		mysql_query("INSERT INTO `registro` (`data` ,`nome_utente` ,`id_grest`,`operazione`)VALUES ('$data', '','', 'Modifica nome parrocchia in $_POST[nome_parrocchia]');");
		//header("location: configurazione_parrocchie.php");
	}
	if ($_POST[mail_parrocchia] != null)
	{	mysql_query ("UPDATE  `parrocchie` SET  `mail_parrocchia` =  '$_POST[mail_parrocchia]' WHERE  `parrocchie`.`id_parrocchia` = $_POST[id_parrocchia]; ");
		mysql_query("INSERT INTO `registro` (`data` ,`nome_utente` ,`id_grest`,`operazione`)VALUES ('$data', '','', 'Modifica mail parrocchia in $_POST[mail_parrocchia]');");
	}
	print 'MODIFICHE EFFETTUATE CORRETTAMENTE';
	break;
	
case 'gruppo':
	{
		mysql_query ("UPDATE  `gruppi_$_SESSION[id_grest]` SET 
		`nome` =  '$_POST[nome]',
		`descrizione` =  '$_POST[descrizione]',
		`note` =  '$_POST[note]'
		WHERE  `id_gruppo` = $_POST[id_gruppo]; ");
		print '<h2>Modificato</h2>';
	}	
	break;

case 'eta':
	{
		mysql_query ("UPDATE  `eta_$_SESSION[id_grest]` SET 
		`nome` =  '$_POST[nome]',
		`min` =  '$_POST[min]',
		`max` =  '$_POST[max]',
		`descrizione` =  '$_POST[descrizione]',
		`note` =  '$_POST[note]'
		WHERE  `id_eta` = $_POST[id_eta]; ");
		print '<h2>Modificato</h2>';
	}	
	break;

case 'gita':
	{
		$giorno = mktime(0, 0, 0, $_POST[mese], $_POST[giorno], $_POST[anno]);
		
		mysql_query ("UPDATE  `gite_$_SESSION[id_grest]` SET 
		`nome` =  '$_POST[nome]',
		`giorno` =  '$giorno',
		`tipo` =  '$_POST[tipo]',
		`descrizione` =  '$_POST[descrizione]',
		`note` =  '$_POST[note]'
		WHERE  `id_gita` = $_POST[id_gita]; ");
		print '<h2>Modificato</h2>';
	}	
	break;
case 'laboratorio':
	{
		mysql_query ("UPDATE  `laboratori_$_SESSION[id_grest]` SET 
		`nome` =  '$_POST[nome]',
		`descrizione` =  '$_POST[descrizione]',
		`note` =  '$_POST[note]'
		WHERE  `id_laboratorio` = $_POST[id_laboratorio]; ");
		print '<h2>Modificato</h2>';
	}	
	break;
case 'squadra':
	{
		mysql_query ("UPDATE  `squadre_$_SESSION[id_grest]` SET 
		`colore` =  '$_POST[colore]',
		`nome` =  '$_POST[nome]'
		WHERE  `id_squadra` = $_POST[id_squadra]; ");
		print '<h2>Modificato</h2>';
	}	
	break;
}
if ($_GET[deviazione] == null)
{
	print'<meta http-equiv="refresh" content="3;
	URL='.$_SERVER['HTTP_REFERER'].'">';
}
else
{
	if ($_GET[deviazione]=='parrocchia')
	{
		print'<meta http-equiv="refresh" content="3;
		URL=personalizzazione_parrocchia.php">';
	}
	if ($_GET[deviazione]=='configurazione_parrocchie')
	{
		print'<meta http-equiv="refresh" content="3;
		URL=configurazione_parrocchie.php">';
	}
	if ($_GET[deviazione]=='configurazione_grest')
	{
		print'<meta http-equiv="refresh" content="3;
		URL=configurazione_grest.php">';
	}
	if ($_GET[deviazione]=='admin')
	{
		print'<meta http-equiv="refresh" content="3;
		URL=configurazione_utenti.php">';
	}	
}
?>		
</strong><br/></div>
</body> 

</html>
