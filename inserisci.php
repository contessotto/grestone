<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet" media="screen"/>
	<link type="text/css" href="stili/stampa.css" rel="stylesheet" media="print"/>

</head>
<script src="script.js" type="text/javascript"></script>
<body>

		<?php include ("funzioni.php"); 
		$dati_utente = verifica_utente();
		$dati_grest = verifica_grest();
		?>
		
		<div id="principale">
		<?php include ("pannello.php");
		include ("intestazione.php");
		include ("menu_inserisci.php"); ?>
  
        
        <div id="contenuto">
        <br/><h2>Inserimento Iscritti al Gr.Est.</h2>
		<br/><img src="immagini/inserisci_home.png" alt="Usa il menu a sinistra per inserire gli iscritti"/><br/>
        <a href="inserisci_iscritto.php"><img src="immagini/menu/inserisci_iscritto.png" alt="Inserisci Iscritto" name="InserisciIscritto2" border="0" onmouseover="cambia(InserisciIscritto,'immagini/menu/inserisci_iscritto_on.png')" onmouseout="cambia(InserisciIscritto,'immagini/menu/inserisci_iscritto.png')"/></a>		
		<a href="inserisci_animatore.php"><img src="immagini/menu/inserisci_animatore.png" alt="Inserisci Animatore" name="InserisciAnimatore2" border="0" onmouseover="cambia(InserisciAnimatore,'immagini/menu/inserisci_animatore_on.png')" onmouseout="cambia(InserisciAnimatore,'immagini/menu/inserisci_animatore.png')"/></a>
		<a href="inserisci_collaboratore.php"><img src="immagini/menu/inserisci_collaboratore.png" alt="Inserisci Collaboratore" name="InserisciCollaboratore2" border="0" onmouseover="cambia(InserisciCollaboratore,'immagini/menu/inserisci_collaboratore_on.png')" onmouseout="cambia(InserisciCollaboratore,'immagini/menu/inserisci_collaboratore.png')"/></a><br/>     
</div>
        
        <?php include ("pedice.php"); ?>     
        
    </div>
</body> 

</html>
