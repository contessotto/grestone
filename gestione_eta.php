<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>
	<script src="script.js" type="text/javascript"></script>
</head>
	<style>
	tr.modifica{display:none;}
	</style>
    <script>
       	function modifica(id)
       	{document.getElementById('modifica'+id).style.display = 'table-row';}
       	function esci(id)
       	{document.getElementById('modifica'+id).style.display = 'none';}        
    </script>
<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
<?php
verifica_amministratore($_SESSION[Grestone]);
?>
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_personalizzazione.php"); ?>

        <div id="contenuto">

<?php
if ($dati_grest[s_eta] == 1)
{
	print '<h2>Gestione Fasce d\'Età</h2>';
	print 'La gestione per fasce d\'età è attualmente sospesa.<br/>
	<a href="riprendi.php?oggetto=eta">Riprendi</a>';
}
else
{
if ($dati_grest[eta] == 0)
{
	if (!isset($_POST[passaggi])) //primo passaggio:indica il numero di laboratori
	{
	print '<h2>Gestione Fasce d\'Età</h2>';
	print '
	<form action="gestione_eta.php" method="post">
	<input type="hidden" name="passaggi" value="primo">
	Numero fasce d\'età:
	<select name="numero">';
	for ($a=1;$a<=20;$a++)
		{print '<option value="'.$a.'">'.$a.'</option>';}
	print '</select><br/>
	<input type="submit" value="inserisci">';
	}
	
	
	if ($_POST[passaggi] == 'primo') //secondo passaggio: indica i valori per ogni squadra
	{	
	print '<h2>Gestione Fasce d\'Età</h2>';
	print '
	Inserisci le fasce d\'età in cui saranno suddivisi i bambini:
	per ciascuna devi indicare obbligatoriamente il NOME, 
	e i limiti d\'età mentre a tua scelta puoi decidere di inserire una descrizione e delle note.<br/>
	<form action="gestione_eta.php" method="post">
	<input type="hidden" name="passaggi" value="secondo">
	<input type="hidden" name="numero" value="'.$_POST[numero].'">';	
	for ($a=1; $a<=$_POST[numero]; $a++)
	{
		print '
		<h2>Fascia '.$a.'</h2>';
		print'da:
		<select name="min_'.$a.'">
			<option value="1">asilo</option>
			<option value="2">1 elementare</option>
			<option value="3">2 elementare</option>
			<option value="4">3 elementare</option>
			<option value="5">4 elementare</option>
			<option value="6">5 elementare</option>
			<option value="7">1 media</option>
			<option value="8">2 media</option>
			<option value="9">3 media</option>
			<option value="10">superiori</option>
		</select>';
		print'a:
		<select name="max_'.$a.'">
			<option value="1">asilo</option>
			<option value="2">1 elementare</option>
			<option value="3">2 elementare</option>
			<option value="4">3 elementare</option>
			<option value="5">4 elementare</option>
			<option value="6">5 elementare</option>
			<option value="7">1 media</option>
			<option value="8">2 media</option>
			<option value="9">3 media</option>
			<option value="10">superiori</option>
		</select>';
		print'Nome: <input type="text" required name="nome_'.$a.'"><br/>';
		print'Descrizione:<textarea name="descrizione_'.$a.'" rows="5" cols="40"></textarea><br/>';
		print'Note: <textarea name="note_'.$a.'" rows="3" cols="40"></textarea><br/>';
	}	
	print '<br/><br/>
	<input type="submit" value="inserisci">
	</form>';
	}
	
	if ($_POST[passaggi] == 'secondo') //terzo passaggio: mostra i valori per la conferma
	{	
		print '<h2>Conferma dati inseriti</h2>';
		print '<form action="gestione_eta.php" method="post">';
		print'<input type="hidden" name="numero" value="'.$_POST[numero].'">';		
		print '<input type="hidden" name="passaggi"value="terzo">';
		for ($a=1; $a<=$_POST[numero]; $a++)
		{
			print'
			<h2>Fascia '.$a.'</h2>';
			$nome = 'nome_'.$a;
			$min = 'min_'.$a;
			$max = 'max_'.$a;
			$descrizione = 'descrizione_'.$a;
			$note = 'note_'.$a;
			print"Nome: $_POST[$nome]<br/>";
			print"Dalla ";
			print classe($_POST[$min]);
			print" - Alla ";
			print classe($_POST[$max]);
			print "<br/>";
			print"Descrizione: $_POST[$descrizione]<br/>";
			print"Note: $_POST[$note]";
		}
		for ($a=1; $a<=$_POST[numero]; $a++)
		{
			$nome = 'nome_'.$a;
			$min = 'min_'.$a;
			$max = 'max_'.$a;
			$descrizione = 'descrizione_'.$a;
			$note = 'note_'.$a;
			print '<input type="hidden" name="'.$nome.'" value="'.$_POST[$nome].'">';
			print '<input type="hidden" name="'.$min.'" value="'.$_POST[$min].'">';
			print '<input type="hidden" name="'.$max.'" value="'.$_POST[$max].'">';
			print '<input type="hidden" name="'.$descrizione.'" value="'.$_POST[$descrizione].'">';
			print '<input type="hidden" name="'.$note.'" value="'.$_POST[$note].'">';
		}
		
	print '<br/><input type="submit" value="Inserisci fasce">';
	print '</form>';
	}
	
	
	
	
		if ($_POST[passaggi] == 'terzo') //quarto passaggio: inserisce i valori nel database
	{
		connetti();
		mysql_query("UPDATE `grests` SET eta = '1' WHERE `id_grest` = '$_SESSION[id_grest]';");
		mysql_query("CREATE TABLE  `eta_$_SESSION[id_grest]` (`id_eta` INT( 3 ) NOT NULL AUTO_INCREMENT ,
					`nome` TEXT NOT NULL , `min` INT( 3 ) NOT NULL , `max` INT( 3 ) NOT NULL ,
					`descrizione` TEXT NOT NULL , `note` TEXT NOT NULL ,
					PRIMARY KEY (  `id_eta` ))");
		mysql_query("ALTER TABLE  `iscritti_$_SESSION[id_grest]` ADD  `eta` INT( 3 ) NOT NULL AFTER  `note`");	
		mysql_query("ALTER TABLE  `animatori_$_SESSION[id_grest]` ADD  `eta` INT( 3 ) NOT NULL AFTER  `note`");	
		mysql_query("ALTER TABLE  `collaboratori_$_SESSION[id_grest]` ADD  `eta` INT( 3 ) NOT NULL AFTER  `note`");	
		for ($a=1; $a<=$_POST[numero]; $a++)
		{
			$nome = 'nome_'.$a;
			$min = 'min_'.$a;
			$max = 'max_'.$a;
			$descrizione = 'descrizione_'.$a;
			$note = 'note_'.$a;
		
			mysql_query("INSERT INTO  `eta_$_SESSION[id_grest]` (`id_eta` ,`nome` ,`min` ,`max` ,`descrizione` ,`note`)
						VALUES (NULL ,  '$_POST[$nome]', '$_POST[$min]', '$_POST[$max]',
						  '$_POST[$descrizione]',  '$_POST[$note]');");
		}				
		print '<h2>Dati inseriti correttamente</h2><meta http-equiv="refresh" content="0;
			URL='.$_SERVER['HTTP_REFERER'].'">';
	}
}
else
{
	print'<h2>La gestione delle fasce d\'età è attiva</h2>
	<a href="#inserisci">Inserisci nuova fascia</a><br/>
	<a href="disattiva.php?oggetto=eta" onclick="return confermadisattiva ();">Disattiva definitivamente la gestione Fasce</a><br/>
	<a href="sospendi.php?oggetto=eta">Sospendi temporaneamente la gestione Fasce</a>
	';
	connetti();
	$eta = mysql_query("SELECT * FROM  `eta_$_SESSION[id_grest]`");
	print '<table id="lista" align="center" width="100%"><thead>
	<tr>';
	print'<th scope="col"></th>';
	print'<th scope="col">NOME</th>';
	print'<th scope="col">DA</th>';
	print'<th scope="col">A</th>';
	print'<th scope="col">DESCRIZIONE</th>';
	print'<th scope="col">NOTE</th>';
	print'<th scope="col">MODIFICA</th>';
	print'<th scope="col">ELIMINA</th>';
	print'</thead></tr><tbody>';
	while ($dati_eta = mysql_fetch_array($eta, MYSQL_ASSOC))
	{	
		print '<tr>';
		print '<td></td>';
		print"<td>$dati_eta[nome]</td>";
		print'<td>';
		print classe($dati_eta[min]);
		print'</td>';
		print'<td>';
		print classe($dati_eta[max]);
		print'</td>';
		print"<td>$dati_eta[descrizione]</td>";
		print"<td>$dati_eta[note]</td>";
		print '<td><a onclick="modifica('.$dati_eta[id_eta].');"class="elimina"href="#"><img src="immagini/modifica.png" alt="modifica" border="0" title="Modifica"/></a></td>';	
		print'<td><a href="elimina.php?oggetto=eta&id='.$dati_eta[id_eta].'" onclick="return conferma ();"><img src="immagini/ico_no.png" alt="elimina" border="0" title="Elimina"/></a></td>';

		print'</tr>';
	
		print'<form action="modifica.php?oggetto=eta" method="post">
					
		<tr class="modifica" id="modifica'.$dati_eta[id_eta].'">
		<td><input type="hidden" name="id_eta" value="'.$dati_eta[id_eta].'">
		<a class="elimina" href="#" onclick="esci('.$dati_eta[id_eta].');">abbandona modifiche</a>
		</td>
		<td><input type="text" name="nome" size="3" value="'.$dati_eta[nome].'"></td>
		<td>
		
			<select name="min">';
	
	print'<option value="1"';
	if ($dati_eta[min] == '1')
	{print'selected';}
	print'
	>Asilo</option>	<option value="2"';
	if ($dati_eta[min] == '2')
	{print'selected';}
	print'
	>1 elementare</option>	<option value="3"
	';
	if ($dati_eta[min] == '3')
	{print'selected';}
	print'	
	>2 elementare</option>	<option value="4"
	';
	if ($dati_eta[min] == '4')
	{print'selected';}
	print'
	>3 elementare</option>	<option value="5"
	';
	if ($dati_eta[min] == '5')
	{print'selected';}
	print'
	>4 elementare</option>	<option value="6"
	';
	if ($dati_eta[min] == '6')
	{print'selected';}
	print'
	>5 elementare</option>	<option value="7"
	';
		if ($dati_eta[min] == '7')
	{print'selected';}
	print'
	>1 media</option>	<option value="8"
	';
	if ($dati_eta[min] == '8')
	{print'selected';}
	print'
	>2 media</option>	<option value="9"
	';
	if ($dati_eta[min] == '9')
	{print'selected';}
	print'
	>3 media</option>	<option value="10"
	';
	if ($dati_eta[min] == '10')
	{print'selected';}
	print'
	>Superiori</option>
	</select>
		</td>
		<td>
			<select name="max">';
	
	print'<option value="1"';
	if ($dati_eta[max] == '1')
	{print'selected';}
	print'
	>Asilo</option>	<option value="2"';
	if ($dati_eta[max] == '2')
	{print'selected';}
	print'
	>1 elementare</option>	<option value="3"
	';
	if ($dati_eta[max] == '3')
	{print'selected';}
	print'	
	>2 elementare</option>	<option value="4"
	';
	if ($dati_eta[max] == '4')
	{print'selected';}
	print'
	>3 elementare</option>	<option value="5"
	';
	if ($dati_eta[max] == '5')
	{print'selected';}
	print'
	>4 elementare</option>	<option value="6"
	';
	if ($dati_eta[max] == '6')
	{print'selected';}
	print'
	>5 elementare</option>	<option value="7"
	';
		if ($dati_eta[max] == '7')
	{print'selected';}
	print'
	>1 media</option>	<option value="8"
	';
	if ($dati_eta[max] == '8')
	{print'selected';}
	print'
	>2 media</option>	<option value="9"
	';
	if ($dati_eta[max] == '9')
	{print'selected';}
	print'
	>3 media</option>	<option value="10"
	';
	if ($dati_eta[max] == '10')
	{print'selected';}
	print'
	>Superiori</option>
	</select>
		
		</td>
		<td><textarea name="descrizione" cols="15">'.$dati_eta[descrizione].'</textarea></td>
		<td><textarea name="note" cols="15">'.$dati_eta[note].'</textarea></td>
		<td><input type="submit" value="modifica"></td>
		</tr>
		</form>';
	
	
	}
	print '</tbody></table>';
	
	if (isset($_POST[passaggi]))
	{
		if ($_POST[passaggi] == 'modifica_primo')
		{
			mysql_query("INSERT INTO `eta_$_SESSION[id_grest]` 
			(`id_eta`, `nome`, `min`,  `max`,`descrizione`, `note`)
			 VALUES (NULL, '$_POST[nome]', '$_POST[min]', '$_POST[max]', '$_POST[descrizione]', '$_POST[note]');");
			print '<h2>Dati inseriti correttamente</h2>';
			print '<meta http-equiv="refresh" content="0;
			URL='.$_SERVER['HTTP_REFERER'].'">';
		}		
	}
	else
	{
		print '<a name="inserisci"></a>
		<br/><h2>Inserisci nuova fascia d\'età</h2>
		<form action="gestione_eta.php" method="post">
		Nome:<input type="text" name="nome"><br/>
		Da:<select name="min">
			<option value="1">asilo</option>
			<option value="2">1 elementare</option>
			<option value="3">2 elementare</option>
			<option value="4">3 elementare</option>
			<option value="5">4 elementare</option>
			<option value="6">5 elementare</option>
			<option value="7">1 media</option>
			<option value="8">2 media</option>
			<option value="9">3 media</option>
			<option value="10">superiori</option>
		</select>
		A:<select name="max">
			<option value="1">asilo</option>
			<option value="2">1 elementare</option>
			<option value="3">2 elementare</option>
			<option value="4">3 elementare</option>
			<option value="5">4 elementare</option>
			<option value="6">5 elementare</option>
			<option value="7">1 media</option>
			<option value="8">2 media</option>
			<option value="9">3 media</option>
			<option value="10">superiori</option>
		</select><br/>
		Descrizione: <textarea name="descrizione"></textarea><br/>
		Note: <textarea name="note"></textarea><br/>
		<input type="hidden" name="passaggi" value="modifica_primo"><br/>
		<input type="submit" value="inserisci"><br/>
		</form>';
	}
}
}
?>			
        </div>
        
        <?php include ("pedice.php"); ?>
        
    </div>
</body> 

</html>
