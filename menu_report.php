<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?> 
 <div id="menu">
 
<?php include ("dataora.php"); ?>
<?php include ("ricerca_rapida.php"); ?>
<?php
//$dati_utente = verifica_utente;
//$dati_grest = verifica_grest();
?>

        <br/>
        <a href="home.php"><img src="immagini/menu/menu_principale.png" alt="MENU PRINCIPALE" name="menu" border="0" onmouseover="cambia(menu,'immagini/menu/menu_principale_on.png')" onmouseout="cambia(menu,'immagini/menu/menu_principale.png')"/></a><br/>
	<br/>	 
<h3>LISTE COMPLETE</h3>
        <a href="lista_iscritti.php"><img src="immagini/menu/liste_animati.png" alt="Liste Animati" name="ListeAnimati" border="0" id="ListeAnimati" onmouseover="cambia(ListeAnimati,'immagini/menu/liste_animati_on.png')" onmouseout="cambia(ListeAnimati,'immagini/menu/liste_animati.png')"/></a>
		 
		<a href="lista_animatori.php"><img src="immagini/menu/liste_animatori.png" alt="Liste Animatori" name="ListeAnimatori" border="0" onmouseover="cambia(ListeAnimatori,'immagini/menu/liste_animatori_on.png')" onmouseout="cambia(ListeAnimatori,'immagini/menu/liste_animatori.png')"/></a>
		 
		<a href="lista_collaboratori.php"><img src="immagini/menu/liste_collaboratori.png" alt="Liste Collaboratori" name="ListeCollaboratori" border="0" onmouseover="cambia(ListeCollaboratori,'immagini/menu/liste_collaboratori_on.png')" onmouseout="cambia(ListeCollaboratori,'immagini/menu/liste_collaboratori.png')"/></a>
		 
        <br/><br/>
<h3>ELENCHI</h3>
		<a href="elenchi_predisposti.php"><img src="immagini/menu/elenchi_predisposti.png" alt="Elenchi Predisposti" name="ElenchiPredisposti" border="0" onmouseover="cambia(ElenchiPredisposti,'immagini/menu/elenchi_predisposti_on.png')" onmouseout="cambia(ElenchiPredisposti,'immagini/menu/elenchi_predisposti.png')"/></a>
        
		<a href="elenchi.php?oggetto="><img src="immagini/menu/elenchi_generali.png" alt="Elenchi Generali" name="ElenchiGenerali" border="0" onmouseover="cambia(ElenchiGenerali,'immagini/menu/elenchi_generali_on.png')" onmouseout="cambia(ElenchiGenerali,'immagini/menu/elenchi_generali.png')"/></a>
        
<?php        
     if ($dati_grest[squadre]==1)
	{
        print '<a href="elenchi.php?oggetto=squadre"><img src="immagini/menu/elenchi_squadre.png" alt="Elenchi Squadre" name="ElenchiSquadre" border="0" onmouseover="cambia(ElenchiSquadre,\'immagini/menu/elenchi_squadre_on.png\')" onmouseout="cambia(ElenchiSquadre,\'immagini/menu/elenchi_squadre.png\')"/></a>';		
	}      
     if ($dati_grest[laboratori]==1)
	{
        print '<a href="elenchi.php?oggetto=laboratori"><img src="immagini/menu/elenchi_laboratori.png" alt="Elenchi Laboratori" name="ElenchiLaboratori" border="0" onmouseover="cambia(ElenchiLaboratori,\'immagini/menu/elenchi_laboratori_on.png\')" onmouseout="cambia(ElenchiLaboratori,\'immagini/menu/elenchi_laboratori.png\')"/></a>';		
	}      
     if ($dati_grest[gite]==1)
	{
        print '<a href="elenchi.php?oggetto=eventi"><img src="immagini/menu/elenchi_eventi.png" alt="Elenchi Eventu" name="ElenchiEventi" border="0" onmouseover="cambia(ElenchiEventi,\'immagini/menu/elenchi_eventi_on.png\')" onmouseout="cambia(ElenchiEventi,\'immagini/menu/elenchi_eventi.png\')"/></a>';		
	}      
     if ($dati_grest[gruppi]==1)
	{
        print '<a href="elenchi.php?oggetto=gruppi"><img src="immagini/menu/elenchi_gruppi.png" alt="Elenchi Gruppi" name="ElenchiGruppi" border="0" onmouseover="cambia(ElenchiGruppi,\'immagini/menu/elenchi_gruppi_on.png\')" onmouseout="cambia(ElenchiGruppi,\'immagini/menu/elenchi_gruppi.png\')"/></a>';		
	}      
	
?>
    </div>
