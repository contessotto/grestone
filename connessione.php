<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<?php
/* PARAMETRI PER LA CONNESSIONE AL SERVER*/
$indirizzo = '127.0.0.1';		// indirizzo internet del sito
$nome_database = 'nome_database';	// nome del database
$nome_utente = 'nome_utente';			// nome utente del database
$password = 'password_database';			// password utente del database

/* ALTRI PARAMETRI FONDAMENTALI */
//mail del gestore o dei gestori del grestone
//se sono più di una bisogna indicarle separate da virgola
//prima@esempio.it,seconda@esempio.it
$mail_admin = 'info@grestone.it'; 

?>