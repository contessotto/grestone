<?php /*
	GrestOne Software di Gestione per Gr.Est.
	Copyright (C) 2012 Contessotto Marco & Ziero Samuele

    This file is part of GrestOne.
    GrestOne is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GrestOne is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Grestone.  If not, see <http://www.gnu.org/licenses/>.
	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">

<head>
	<title>GrestOne - Gestione Grest</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link type="text/css" href="stili/stilehome.css" rel="stylesheet"/>
	<script src="script.js" type="text/javascript"></script>
</head>

<body>
<?php
include ("funzioni.php"); 
$dati_utente = verifica_utente();
$dati_grest = verifica_grest();
?>
    <div id="principale">
		
		<?php include ("pannello.php"); ?>

		<?php include ("intestazione.php"); ?>

		<?php include ("menu_report.php"); ?>

<div id="contenutoaltezzafissa">
	<?php
	connetti();
	print '<h2>ELENCO COMPLETO DEGLI ANIMATORI DEL GREST</h2><br/>';
	$a = 'animatori_'.$_SESSION[id_grest];
	
		$ordine = $_GET[ordine];
		$direzione = $_GET[direzione];
		if ($direzione == null)
			{$direzione = 'ASC';}	
		if ($ordine == null)
			{$ordine = 'cognome';}
		$ord ="ORDER BY  $ordine $direzione";
		
	if ($_GET[ricerca] != null)
	{
		if ( ! get_magic_quotes_gpc() )
		{$_GET[ricerca] = mysql_escape_string ($_GET[ricerca]);}
		$ric =	"SELECT *, MATCH(nome, cognome) 
		AGAINST('$_GET[ricerca]') AS attinenza FROM 
		$a WHERE MATCH(nome, cognome) AGAINST('$_GET[ricerca]') 
		ORDER BY attinenza DESC ";
		print "<br><h2>Ricerca di <strong><span style=\"color: red;\">$_GET[ricerca]</span></strong></h2>";
		print '<a href="lista_animatori.php">Torna alla lista completa</a>';
	}
	else
	{
		$ric = "SELECT * FROM $a $ord";
	}
	
	$iscritti = mysql_query("$ric");



if (mysql_num_rows($iscritti) == null)
{
	print '<h3>NESSUN ANIMATORE ISCRITTO AL GREST</h3>';
}
else
{
/*
	print '
	<form action="lista_animatori.php" method="get">
	ordina per :
	<select name="ordine">
	<option value="nome">nome</option>
	<option value="cognome">cognome</option>
	<option value="sesso">sesso</option>
	<option value="nascita">data di nascita</option>';
	if ($dati_grest[squadre])
	{print '<option value="squadra">squadra</option>';}
	if ($dati_grest[eta])
	{print '<option value="eta">fascia d\' età</option>';}
	if ($dati_grest[gruppi])
	{print '<option value="gruppo">gruppo</option>';}
	if ($dati_grest[laboratori]==1)
	{
		for ($a=1; $a<=$dati_grest[laboratori_periodo]; $a++)
		{
			print '<option value="laboratorio_'.$a.'">Laboratorio '.$a.'</option>';
		}
	}
	if ($dati_grest[periodo]==1)
	{
		$periodo = mysql_query("SELECT * FROM periodo WHERE id_grest = $_SESSION[id_grest]");
		$dati_periodo = mysql_fetch_array($periodo, MYSQL_ASSOC);
		for ($a=1; $a<=$dati_periodo[numero_settimane]; $a++)
		{
			$settimana = 'settimana_'.$a;
			print '<option value="'.$settimana.'">Settimana '.$a.'</option>';
		}
	}
	if ($dati_grest[gite]==1)
	{
		$gite = mysql_query("SELECT * FROM gite_$_SESSION[id_grest] WHERE liste = 1 ");
			while($dati_gite = mysql_fetch_array($gite, MYSQL_ASSOC)) //utilizzo il while solamente per contare le gite
		//in questo modo non ci sono neanche problemi in caso di eliminazione di una gita 
			{
				print '<option value="presenza_evento_'.$dati_gite[id_gita].'">'.$dati_gite[nome].'</option>';
			}
	}
	print '
	<option value="id">inserimento</option>
	</select>
	crescente<input type="radio" name="direzione" checked="checked" value="ASC">
	decrescente <input type="radio" name="direzione" value="DESC">
	<br/><input type="text" name="ricerca">(ricerca per nome o cognome)
	<br/><input type="submit" value="esegui">
	</form>
	';
*/
	if ($_GET[ordine] == null)
	{$_GET[ordine]='cognome';}	
	if ($_GET[direzione] == null)
	{$_GET[direzione]='ASC';}
	
	print '
		<table id="lista"><thead>
		<tr>
		<th scope="col"> </th>
		<th scope="col"> </th>
		<!--<th scope="col">ID</th>-->
		<th scope="col"><a href="lista_animatori.php?ordine=nome&direzione=';
if ($_GET[ordine]=='nome')
		{
			if($_GET[direzione]=='ASC')
			{print'DESC';}else{print'ASC';}
		}
	print '">NOME';
		if ($_GET[ordine]=='nome')
		{
			if($_GET[direzione]=='ASC')
				{print'<img width="25px"src="immagini/ord_su.png">';}
			else
				{print'<img width="25px"src="immagini/ord_giu.png">';}
		}
	print '</a></th>
		<th scope="col"><a href="lista_animatori.php?ordine=cognome&direzione=';
		if ($_GET[ordine]=='cognome')
		{
			if($_GET[direzione]=='ASC')
			{print'DESC';}else{print'ASC';}
		}
	print '">COGNOME';
		if ($_GET[ordine]=='cognome')
		{
			if($_GET[direzione]=='ASC')
				{print'<img width="25px"src="immagini/ord_su.png">';}
			else
				{print'<img width="25px"src="immagini/ord_giu.png">';}
		}
	print '</th>
		<th scope="col"><a href="lista_animatori.php?ordine=sesso&direzione=';
		if ($_GET[ordine]=='sesso')
		{
			if($_GET[direzione]=='ASC')
			{print'DESC';}else{print'ASC';}
		}
	print '">SESSO';
		if ($_GET[ordine]=='sesso')
		{
			if($_GET[direzione]=='ASC')
				{print'<img width="25px"src="immagini/ord_su.png">';}
			else
				{print'<img width="25px"src="immagini/ord_giu.png">';}
		}
	print '</th>
		<th scope="col"><a href="lista_animatori.php?ordine=nascita&direzione=';
		if ($_GET[ordine]=='nascita')
		{
			if($_GET[direzione]=='ASC')
			{print'DESC';}else{print'ASC';}
		}
	print '">NASCITA';
		if ($_GET[ordine]=='nascita')
		{
			if($_GET[direzione]=='ASC')
				{print'<img width="25px"src="immagini/ord_su.png">';}
			else
				{print'<img width="25px"src="immagini/ord_giu.png">';}
		}
	print '</th>
		<th scope="col"><a href="lista_animatori.php?ordine=classe&direzione=';
		if ($_GET[ordine]=='classe')
		{
			if($_GET[direzione]=='ASC')
			{print'DESC';}else{print'ASC';}
		}
	print '">CLASSE';
		if ($_GET[ordine]=='classe')
		{
			if($_GET[direzione]=='ASC')
				{print'<img width="25px"src="immagini/ord_su.png">';}
			else
				{print'<img width="25px"src="immagini/ord_giu.png">';}
		}
/*	print '</th>
		<th scope="col">INDIRIZZO</th>
		<th scope="col">PAESE</th>
		<th scope="col">TELEFONO</th>
		<th scope="col">CELLULARE</th>
		<th scope="col">NOTE</th>';
*/
	if ($dati_grest[gruppi]==1)
	{
		print '<th scope="col">GRUPPO</th>';		
	}
	if ($dati_grest[squadre]==1)
	{
		print '<th scope="col">SQUADRA</th>';		
	}
	if ($dati_grest[eta]==1)
	{
		print '<th>ETA</th>';		
	}

/*  if ($dati_grest[laboratori]==1)
	{	
		for ($a=1; $a<=$dati_grest[laboratori_periodo]; $a++)
		{	
		print '<th scope="col">LAB. '.$a.'</th>';
		}
	}
	if ($dati_grest[periodo]==1)
	{
		$periodo = mysql_query("SELECT * FROM periodo WHERE id_grest = $_SESSION[id_grest]");
		$dati_periodo = mysql_fetch_array($periodo, MYSQL_ASSOC);
		for ($a=1; $a<=$dati_periodo[numero_settimane]; $a++)
		{	
			$settimana = 'settimana_'.$a;
			print '<th scope="col">SETT. '.$a.'</th>';
		}
	} 
	if ($dati_grest[gite]==1)
	{
		$gite = mysql_query("SELECT * FROM gite_$_SESSION[id_grest] WHERE liste = 1 ");
			while($dati_gite = mysql_fetch_array($gite, MYSQL_ASSOC)) //utilizzo il while solamente per contare le gite
		//in questo modo non ci sono neanche problemi in caso di eliminazione di una gita 
			{
				print '<th scope="col">'.$dati_gite[nome].'</th>';
			}
	}
*/
	print '</tr></thead><tbody>';
	while ($dati_iscritti =  mysql_fetch_array($iscritti, MYSQL_ASSOC))
	{
		print'
		<tr>';
		if ($dati_utente[ruolo_utente] == 'normale' or $dati_utente[ruolo_utente] == 'amministratore')
		{
		print '<td><a class="elimina" href="modifica_animatore.php?oggetto=animatore&id='.$dati_iscritti[id].'"><img src="immagini/modifica.png" alt="modifica" border="0" title="Modifica"/></a></a></td>';	
	print '<td><a class="elimina" href="elimina.php?'.
	"oggetto=animatore&id=$dati_iscritti[id]".
	'"  onclick="return conferma ();"><img src="immagini/ico_no.png" alt="elimina" border="0" title="Elimina"/></a></td>';
		}
		else
		{
		print '<td></td><td></td>';
		}
		print'<!--<td>'.$dati_iscritti[id].'</td>-->
		<td><a href="visualizza_animatore.php?id='.$dati_iscritti[id].'">'.$dati_iscritti[nome].'</a></td>
		<td><a href="visualizza_animatore.php?id='.$dati_iscritti[id].'">'.$dati_iscritti[cognome].'</a></td>
		<td>';
		if ($dati_iscritti[sesso] == 'm')
		{print '<img src="immagini/m.png" title="Maschio"/>';}
		if ($dati_iscritti[sesso] == 'f')
		{print '<img src="immagini/f.png" title="Femmina"/>';}
		print '</td>
		<td>';
		if ($dati_iscritti[nascita] != null)
			{print $nascita = date("d-m-Y",$dati_iscritti[nascita]);}
		else
			{print 'DATA NON INSERITA';}
		print '</td>
		<td>'.classe($dati_iscritti[classe]).'</td>';

/*		print '<td>'.$dati_iscritti[indirizzo].'</td>
		<td>'.$dati_iscritti[paese].'</td>
		<td>'.$dati_iscritti[telefono].'</td>
		<td>'.$dati_iscritti[cellulare].'</td>
		<td>'.$dati_iscritti[note].'</td>';
*/		
		if ($dati_grest[gruppi]==1)
		{
			$gruppo = mysql_query("SELECT * FROM gruppi_$_SESSION[id_grest] WHERE id_gruppo = $dati_iscritti[gruppo]");	
			$dati_gruppo = mysql_fetch_array ($gruppo, MYSQL_ASSOC);
			print '<td>'.$dati_gruppo[nome].'</td>';
		}

		if ($dati_grest[squadre]==1)
		{
			$squadra = mysql_query("SELECT * FROM squadre_$_SESSION[id_grest]");
			$impostazioni_squadre = mysql_fetch_array ($squadra, MYSQL_ASSOC);
			$squadra = mysql_query("SELECT * FROM squadre_$_SESSION[id_grest] WHERE id_squadra = $dati_iscritti[squadra]");
			$dati_squadra = mysql_fetch_array ($squadra, MYSQL_ASSOC);
			print '<td>';
			if ($impostazioni_squadre[nome] == 1)
				{print "$dati_squadra[nome] ";}
			if ($impostazioni_squadre[colore] == 1)
			{
				if ($dati_squadra[colore])
					{print '<img src="immagini/squadre/'.$dati_squadra[colore].'.png" alt="'.$dati_squadra[colore].'" border="0" title="'.$dati_squadra[colore].'"/>';}
			}
			print'</td>';		
		}
		if ($dati_grest[eta]==1)
		{
			$eta = mysql_query("SELECT * FROM eta_$_SESSION[id_grest] WHERE id_eta = $dati_iscritti[eta]");	
			$dati_eta = mysql_fetch_array ($eta, MYSQL_ASSOC);
			print '<td>'.$dati_eta[nome].'</td>';
		}		
/*	if ($dati_grest[laboratori]==1)
	{
		for ($a=1; $a<=$dati_grest[laboratori_periodo]; $a++)
		{	
			$laboratorio = 'laboratorio_'.$a;
		
		$laboratorio = mysql_query("SELECT * FROM laboratori_$_SESSION[id_grest] WHERE id_laboratorio = $dati_iscritti[$laboratorio]");
		$dati_laboratorio = mysql_fetch_array ($laboratorio, MYSQL_ASSOC);
		print '<td>';
		print "$dati_laboratorio[nome] ";
		print'</td>';
		}		
	}
	if ($dati_grest[periodo]==1)
	{
		$periodo = mysql_query("SELECT * FROM periodo WHERE id_grest = $_SESSION[id_grest]");
		$dati_periodo = mysql_fetch_array($periodo, MYSQL_ASSOC);
		for ($a=1; $a<=$dati_periodo[numero_settimane]; $a++)
		{	
			$settimana = 'settimana_'.$a;
			print '<td>';
			if ($dati_iscritti[$settimana] == 1)
			{
				print '<img src="immagini/ico_ok.png" alt="Presente" title="Presente"/>';
			}
			else
			{
				print '<img src="immagini/ico_no.png" alt="Assente" title="Assente"/>';
			}
			print '</td>';
		}	
	}
	
	if ($dati_grest[gite]==1)
	{
		$gite = mysql_query("SELECT * FROM gite_$_SESSION[id_grest] WHERE liste = 1 ");
			while($dati_gite = mysql_fetch_array($gite, MYSQL_ASSOC)) //utilizzo il while solamente per contare le gite
		//in questo modo non ci sono neanche problemi in caso di eliminazione di una gita 
		{
			$presenza_gita = 'presenza_evento_'.$dati_gite[id_gita];
			$pagamento_gita = 'pagamento_evento_'.$dati_gite[id_gita];
			$note_gita = 'note_evento_'.$dati_gite[id_gita];
			print '<td>';
			if ($dati_iscritti[$presenza_gita] == 1)
			{
				print '<img src="immagini/ico_ok.png" alt="Iscritto" title="Iscritto"/><br/>';
				if ($dati_iscritti[$pagamento_gita] == 1)
					{print '<img src="immagini/pagato.png" alt="Pagato" title="Pagato"/><br/>';}
				print $dati_iscritti[$note_gita];
			}
			else
			{
				print '<img src="immagini/ico_no.png" alt="Assente" title="Assente"/>';
			}
			print '</td>';
		}
	}
*/
	print '</tr>';
	}
	print'	</tbody></table>';
}
	?>
</div>
        <?php include ("pedice.php"); ?>
</div>
</body> 

</html>
